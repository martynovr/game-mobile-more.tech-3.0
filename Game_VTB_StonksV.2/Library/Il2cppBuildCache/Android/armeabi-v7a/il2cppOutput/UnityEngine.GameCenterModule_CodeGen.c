﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.Social::get_Active()
extern void Social_get_Active_m74A08BB58636C0AC5934CAB46856765A271F842D (void);
// 0x00000002 UnityEngine.SocialPlatforms.ILocalUser UnityEngine.Social::get_localUser()
extern void Social_get_localUser_m3C570106A10EE66C0172C291AE2084829B64C468 (void);
// 0x00000003 System.Void UnityEngine.Social::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern void Social_ReportScore_m9D47693DE21BDF21D8933E2B039A96D7E579EE13 (void);
// 0x00000004 System.Void UnityEngine.Social::ShowAchievementsUI()
extern void Social_ShowAchievementsUI_m0A816B3C7E5AC8A093F0579442D00BD6AA6CE9D2 (void);
// 0x00000005 System.Void UnityEngine.Social::ShowLeaderboardUI()
extern void Social_ShowLeaderboardUI_mDD136D499783509D4BB9D9DE756FC68295160EAA (void);
// 0x00000006 UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.Local::get_localUser()
extern void Local_get_localUser_m28A33DACA9EA63D30308B52B6F48D68D27484FF5 (void);
// 0x00000007 System.Void UnityEngine.SocialPlatforms.Local::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern void Local_ReportScore_m90CAB1AF5068A4E89BB48F3B8188C3F40C4E40AF (void);
// 0x00000008 System.Void UnityEngine.SocialPlatforms.Local::ShowAchievementsUI()
extern void Local_ShowAchievementsUI_mA9430D238E97F9A50CC7477A3F60BA9895D7A5BD (void);
// 0x00000009 System.Void UnityEngine.SocialPlatforms.Local::ShowLeaderboardUI()
extern void Local_ShowLeaderboardUI_m0AD9E483514F7525F60CC444AA76FF75311FBC10 (void);
// 0x0000000A System.Boolean UnityEngine.SocialPlatforms.Local::VerifyUser()
extern void Local_VerifyUser_mCDE924381D08F6A3CF15EE8A5EB2EEAED43159D1 (void);
// 0x0000000B System.Void UnityEngine.SocialPlatforms.Local::.ctor()
extern void Local__ctor_mFD403BE76E647B8E7261B5C3479F5E7673CB9F05 (void);
// 0x0000000C System.Void UnityEngine.SocialPlatforms.Local::.cctor()
extern void Local__cctor_m196557F27F297DECA3AA9A99822342CB2B13AB86 (void);
// 0x0000000D UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::get_Instance()
extern void ActivePlatform_get_Instance_m5FB1343B475512D548FE04A7C2BC3DCCAF694AA9 (void);
// 0x0000000E UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::SelectSocialPlatform()
extern void ActivePlatform_SelectSocialPlatform_mA92EB9D7D4AD4D2009750EDA6DEA17C304D4480E (void);
// 0x0000000F UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.ISocialPlatform::get_localUser()
// 0x00000010 System.Void UnityEngine.SocialPlatforms.ISocialPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
// 0x00000011 System.Void UnityEngine.SocialPlatforms.ISocialPlatform::ShowAchievementsUI()
// 0x00000012 System.Void UnityEngine.SocialPlatforms.ISocialPlatform::ShowLeaderboardUI()
// 0x00000013 System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
// 0x00000014 System.String UnityEngine.SocialPlatforms.IUserProfile::get_id()
// 0x00000015 System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern void LocalUser__ctor_mE1D89C27BD5A6DAE982610A87A472E736644605A (void);
// 0x00000016 System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern void LocalUser_get_authenticated_m7D219E0639A779F4527590E43E66F31C2A482BD1 (void);
// 0x00000017 System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern void UserProfile__ctor_m65DBFCC8D74A39E029B7EEBF5B24A8790C3668AA (void);
// 0x00000018 System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern void UserProfile_ToString_mE86E8A99DF0EE73C93A55624E890806213567924 (void);
// 0x00000019 System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern void UserProfile_get_userName_mB127337E5455C94299C39FB66C2965FF3421142E (void);
// 0x0000001A System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern void UserProfile_get_id_m2C9612A3B81A7F76F86C4FE87AD4D3FADEF3E385 (void);
// 0x0000001B System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern void UserProfile_get_isFriend_m7ACC8963AD01FB384D215AF8C215DD93CB5322EA (void);
// 0x0000001C UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern void UserProfile_get_state_mF93F969F5923A0A3528923E5A452138D00D4E248 (void);
// 0x0000001D System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern void Score__ctor_m94026C33B1E9EE0F31614511DB8F512E8A7A4FE3 (void);
// 0x0000001E System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern void Score_ToString_mBE1B8E8846C633AAA58A0680C888F21915454FC4 (void);
// 0x0000001F System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern void Score_get_leaderboardID_m079BDC75AED9D8C06DC36F78AC233171F023FB4F (void);
// 0x00000020 System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern void Score_set_leaderboardID_m05255F12ADDC2FC6D7E9646CBAE68D2924E66F65 (void);
// 0x00000021 System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern void Score_get_value_m750646EDC0619A11179177F597C21374F13FCB9E (void);
// 0x00000022 System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern void Score_set_value_mFECF5E9D1928EEAE7E46F497CDA159549EEB74C4 (void);
// 0x00000023 System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern void Leaderboard_SetScores_m2DB8C9FB943AD849B23FCEB56D76FAE4E11C2084 (void);
// 0x00000024 System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern void Leaderboard_get_id_m4A262BB19BCACE6C9B19874F5D68C777846C6CD6 (void);
// 0x00000025 UnityEngine.SocialPlatforms.IScore[] UnityEngine.SocialPlatforms.Impl.Leaderboard::get_scores()
extern void Leaderboard_get_scores_m11FC708301EB87BDCF7ADC6EB0CBE0187499DD2D (void);
static Il2CppMethodPointer s_methodPointers[37] = 
{
	Social_get_Active_m74A08BB58636C0AC5934CAB46856765A271F842D,
	Social_get_localUser_m3C570106A10EE66C0172C291AE2084829B64C468,
	Social_ReportScore_m9D47693DE21BDF21D8933E2B039A96D7E579EE13,
	Social_ShowAchievementsUI_m0A816B3C7E5AC8A093F0579442D00BD6AA6CE9D2,
	Social_ShowLeaderboardUI_mDD136D499783509D4BB9D9DE756FC68295160EAA,
	Local_get_localUser_m28A33DACA9EA63D30308B52B6F48D68D27484FF5,
	Local_ReportScore_m90CAB1AF5068A4E89BB48F3B8188C3F40C4E40AF,
	Local_ShowAchievementsUI_mA9430D238E97F9A50CC7477A3F60BA9895D7A5BD,
	Local_ShowLeaderboardUI_m0AD9E483514F7525F60CC444AA76FF75311FBC10,
	Local_VerifyUser_mCDE924381D08F6A3CF15EE8A5EB2EEAED43159D1,
	Local__ctor_mFD403BE76E647B8E7261B5C3479F5E7673CB9F05,
	Local__cctor_m196557F27F297DECA3AA9A99822342CB2B13AB86,
	ActivePlatform_get_Instance_m5FB1343B475512D548FE04A7C2BC3DCCAF694AA9,
	ActivePlatform_SelectSocialPlatform_mA92EB9D7D4AD4D2009750EDA6DEA17C304D4480E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalUser__ctor_mE1D89C27BD5A6DAE982610A87A472E736644605A,
	LocalUser_get_authenticated_m7D219E0639A779F4527590E43E66F31C2A482BD1,
	UserProfile__ctor_m65DBFCC8D74A39E029B7EEBF5B24A8790C3668AA,
	UserProfile_ToString_mE86E8A99DF0EE73C93A55624E890806213567924,
	UserProfile_get_userName_mB127337E5455C94299C39FB66C2965FF3421142E,
	UserProfile_get_id_m2C9612A3B81A7F76F86C4FE87AD4D3FADEF3E385,
	UserProfile_get_isFriend_m7ACC8963AD01FB384D215AF8C215DD93CB5322EA,
	UserProfile_get_state_mF93F969F5923A0A3528923E5A452138D00D4E248,
	Score__ctor_m94026C33B1E9EE0F31614511DB8F512E8A7A4FE3,
	Score_ToString_mBE1B8E8846C633AAA58A0680C888F21915454FC4,
	Score_get_leaderboardID_m079BDC75AED9D8C06DC36F78AC233171F023FB4F,
	Score_set_leaderboardID_m05255F12ADDC2FC6D7E9646CBAE68D2924E66F65,
	Score_get_value_m750646EDC0619A11179177F597C21374F13FCB9E,
	Score_set_value_mFECF5E9D1928EEAE7E46F497CDA159549EEB74C4,
	Leaderboard_SetScores_m2DB8C9FB943AD849B23FCEB56D76FAE4E11C2084,
	Leaderboard_get_id_m4A262BB19BCACE6C9B19874F5D68C777846C6CD6,
	Leaderboard_get_scores_m11FC708301EB87BDCF7ADC6EB0CBE0187499DD2D,
};
static const int32_t s_InvokerIndices[37] = 
{
	2024,
	2024,
	1618,
	2038,
	2038,
	1162,
	415,
	1193,
	1193,
	1181,
	1193,
	2038,
	2024,
	2024,
	1162,
	415,
	1193,
	1193,
	1181,
	1162,
	1193,
	1181,
	1193,
	1162,
	1162,
	1162,
	1181,
	1151,
	90,
	1162,
	1162,
	1023,
	1152,
	1015,
	1023,
	1162,
	1162,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_GameCenterModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_GameCenterModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_GameCenterModule_CodeGenModule = 
{
	"UnityEngine.GameCenterModule.dll",
	37,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_GameCenterModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
