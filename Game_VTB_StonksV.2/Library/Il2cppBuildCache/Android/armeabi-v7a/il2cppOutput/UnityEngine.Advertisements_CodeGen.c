﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Advertisements.IPlatform UnityEngine.Advertisements.Advertisement::get_platform()
extern void Advertisement_get_platform_m7C6FB11D5F216ED89592DD14EAE230FF77DD51D1 (void);
// 0x00000002 System.Void UnityEngine.Advertisements.Advertisement::set_platform(UnityEngine.Advertisements.IPlatform)
extern void Advertisement_set_platform_m9FA2A6A70A41EA51FE0BA96BD0C8BCB00E95F2EA (void);
// 0x00000003 UnityEngine.Advertisements.IUnityEngineApplication UnityEngine.Advertisements.Advertisement::get_application()
extern void Advertisement_get_application_m455009E0124B252B57E8073AD8C3CF3D291118B4 (void);
// 0x00000004 System.Void UnityEngine.Advertisements.Advertisement::set_application(UnityEngine.Advertisements.IUnityEngineApplication)
extern void Advertisement_set_application_m310EAB6008BD57C02D8982C79919E0B0B255C3EA (void);
// 0x00000005 System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern void Advertisement_get_isInitialized_m1B20FCB52BF511CA59C6677CFC5DE25657DAE291 (void);
// 0x00000006 System.Void UnityEngine.Advertisements.Advertisement::set_isInitialized(System.Boolean)
extern void Advertisement_set_isInitialized_mA3053403CB0BB1A645BB8D518F5C80183C37B968 (void);
// 0x00000007 System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern void Advertisement_get_isSupported_mB2844EB06ED9B956210C4D19D1CD84A91346F436 (void);
// 0x00000008 System.Boolean UnityEngine.Advertisements.Advertisement::get_debugMode()
extern void Advertisement_get_debugMode_m1F03EB76185EA03A4B26C6F010E092CCEA8F5718 (void);
// 0x00000009 System.Void UnityEngine.Advertisements.Advertisement::set_debugMode(System.Boolean)
extern void Advertisement_set_debugMode_m07DAE0ADC40E9C405FC1C203E34C7F76DCD47229 (void);
// 0x0000000A System.String UnityEngine.Advertisements.Advertisement::get_version()
extern void Advertisement_get_version_m2BAA6073750D584979C5BFA3AE3C0AA00D0EF580 (void);
// 0x0000000B System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern void Advertisement_get_isShowing_m26D3967DF88555434C75721E15B05A72F048C90F (void);
// 0x0000000C System.Void UnityEngine.Advertisements.Advertisement::set_isShowing(System.Boolean)
extern void Advertisement_set_isShowing_m318336D64BA4CF33A2747F3161FA3A11677E2624 (void);
// 0x0000000D System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern void Advertisement__cctor_m57B7C8775C79CDF0DA65FD0C0D55ADF0D1EB5219 (void);
// 0x0000000E System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String)
extern void Advertisement_Initialize_m0F11D7CB204F288550555A1D353D92E11F117EB1 (void);
// 0x0000000F System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern void Advertisement_Initialize_m320CA0C1DF8C130E99B011335A2E30167A8614D6 (void);
// 0x00000010 System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern void Advertisement_IsReady_m19441D73BCDBC5BEA4E71D315E47F85EFCA08B5E (void);
// 0x00000011 System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern void Advertisement_IsReady_m8210628F9797095011B967F5F300DAD519B3E469 (void);
// 0x00000012 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState()
extern void Advertisement_GetPlacementState_mFCA0A6D5221C19DD8C92453075C9B98C0FAE2548 (void);
// 0x00000013 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState(System.String)
extern void Advertisement_GetPlacementState_mB1550CF4D6D3BF6B7A18ECBEEF6450F27E67351D (void);
// 0x00000014 System.Void UnityEngine.Advertisements.Advertisement::Show()
extern void Advertisement_Show_mF3736780D5BD0C95CD22EC36AD4901928BD97AF0 (void);
// 0x00000015 System.Void UnityEngine.Advertisements.Advertisement::Show(UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_mBE84E0FABAFFA252880CE1BC1FB480578B6F2970 (void);
// 0x00000016 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern void Advertisement_Show_mFE7E1B0A97096DE79AF12F1590AF9B3D4ADC77F4 (void);
// 0x00000017 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_mA94968EC7B2AE585301451F56848B1A462A0F1CA (void);
// 0x00000018 System.Void UnityEngine.Advertisements.Advertisement::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Advertisement_SetMetaData_mFC38B21497FFA48D760188542BCAC2DA10545F12 (void);
// 0x00000019 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load()
extern void Banner_Load_m81A605BBF41D1EE961124FC253D1E47033DC5AD8 (void);
// 0x0000001A System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m95621B0C4DF1FC839E95DED4844EAF6EF099BE30 (void);
// 0x0000001B System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String)
extern void Banner_Load_m1773A0585078B63734B207F27B52B2F89BEBEFBC (void);
// 0x0000001C System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_mB4EDD60310846E5EF2BB6BA858BE3E454550EB49 (void);
// 0x0000001D System.Void UnityEngine.Advertisements.Advertisement/Banner::Show()
extern void Banner_Show_mF4010062868A18AECEF92B9EA358FB65A47575D2 (void);
// 0x0000001E System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_mEB8D6E9B8118C05D90C23FE30CCFD54A128830E5 (void);
// 0x0000001F System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String)
extern void Banner_Show_m11FEE442448755EBF95951E6D400ED69D9DCCF82 (void);
// 0x00000020 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_mD087E74D10F6A713B342213511301620DE027981 (void);
// 0x00000021 System.Void UnityEngine.Advertisements.Advertisement/Banner::Hide(System.Boolean)
extern void Banner_Hide_m4437A96FF313B515C956060ABBE4A0239D40CD54 (void);
// 0x00000022 System.Void UnityEngine.Advertisements.Advertisement/Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_mDD7A6E426DB2A05E7BBDD16463B640B606273881 (void);
// 0x00000023 System.Boolean UnityEngine.Advertisements.Advertisement/Banner::get_isLoaded()
extern void Banner_get_isLoaded_m14179EF87AF4CDDE880843A5B76A4492ADF627DB (void);
// 0x00000024 System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m2A34D989F2BFC0E9E02C8CEF6E72C05A8B231C34 (void);
// 0x00000025 System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass6_1::.ctor()
extern void U3CU3Ec__DisplayClass6_1__ctor_m42EA990509B8C51AF2A335C4239AF77071828AEE (void);
// 0x00000026 System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass6_1::<Load>b__0(System.Object,System.EventArgs)
extern void U3CU3Ec__DisplayClass6_1_U3CLoadU3Eb__0_m4870B5D9CF739D795D41312B37D17E502E02AD0F (void);
// 0x00000027 System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass6_2::.ctor()
extern void U3CU3Ec__DisplayClass6_2__ctor_mC39B30DF48C657CC7C602A4A7FD40BE3FE7FB131 (void);
// 0x00000028 System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass6_2::<Load>b__1(System.Object,UnityEngine.Advertisements.ErrorEventArgs)
extern void U3CU3Ec__DisplayClass6_2_U3CLoadU3Eb__1_m855F1E0061C9D6978470E932EFB5FA4DAAD0E23F (void);
// 0x00000029 System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mFA5ED412A0D00A87A03431F5DFB73EF98D73F284 (void);
// 0x0000002A System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass10_0::<Show>b__1(System.Object,UnityEngine.Advertisements.StartEventArgs)
extern void U3CU3Ec__DisplayClass10_0_U3CShowU3Eb__1_m96304B2CC91259285A3F42C1B6403C908B8D7A9D (void);
// 0x0000002B System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c__DisplayClass10_0::<Show>b__2(System.Object,UnityEngine.Advertisements.HideEventArgs)
extern void U3CU3Ec__DisplayClass10_0_U3CShowU3Eb__2_m1F1A61BA6C4B90DBD5CA5C21580C7176E52D639A (void);
// 0x0000002C System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c::.cctor()
extern void U3CU3Ec__cctor_mF570E9B7619012EDE3AC698FB14E772A42FB6878 (void);
// 0x0000002D System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c::.ctor()
extern void U3CU3Ec__ctor_mEDE3C937EF27C1E6D6D490991846461080D61BA5 (void);
// 0x0000002E System.Void UnityEngine.Advertisements.Advertisement/Banner/<>c::<Show>b__10_0(System.Object,System.EventArgs)
extern void U3CU3Ec_U3CShowU3Eb__10_0_m947CF17067EB096AF64591D8CF1FBF0286CDCB5D (void);
// 0x0000002F System.Void UnityEngine.Advertisements.Advertisement/<>c::.cctor()
extern void U3CU3Ec__cctor_m2BB9A1C7D8BE2C8BB12A0C9EC5B43514EFEA290F (void);
// 0x00000030 System.Void UnityEngine.Advertisements.Advertisement/<>c::.ctor()
extern void U3CU3Ec__ctor_m5FD11ACF209392359E3162BA64832E91F25ED11B (void);
// 0x00000031 System.Void UnityEngine.Advertisements.Advertisement/<>c::<Initialize>b__26_0(System.Object,UnityEngine.Advertisements.StartEventArgs)
extern void U3CU3Ec_U3CInitializeU3Eb__26_0_m2D645661DFFC69933E4F408C156E273F930C99DF (void);
// 0x00000032 System.Void UnityEngine.Advertisements.Advertisement/<>c::<Initialize>b__26_1(System.Object,UnityEngine.Advertisements.FinishEventArgs)
extern void U3CU3Ec_U3CInitializeU3Eb__26_1_m1FCF5AEE4B33C620C0D4852FE6D3E29DB695B7EA (void);
// 0x00000033 System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mC6E7F87EE379750F3E6232BC867582FF58C46A2B (void);
// 0x00000034 System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass34_0::<Show>b__0(System.Object,UnityEngine.Advertisements.FinishEventArgs)
extern void U3CU3Ec__DisplayClass34_0_U3CShowU3Eb__0_m4A4E89874BE5A64FAC64E2523D605FDB29C40361 (void);
// 0x00000035 System.Void UnityEngine.Advertisements.Banner::add_OnShow(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void Banner_add_OnShow_mB458F372D3DD207CA96F82388E162918002CCFE7 (void);
// 0x00000036 System.Void UnityEngine.Advertisements.Banner::remove_OnShow(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void Banner_remove_OnShow_m45410E5D53456E1D26E2739D9D96B8AE7B250079 (void);
// 0x00000037 System.Void UnityEngine.Advertisements.Banner::add_OnHide(System.EventHandler`1<UnityEngine.Advertisements.HideEventArgs>)
extern void Banner_add_OnHide_m730316CF87D563577C8E8575B7932FE7B1840A22 (void);
// 0x00000038 System.Void UnityEngine.Advertisements.Banner::remove_OnHide(System.EventHandler`1<UnityEngine.Advertisements.HideEventArgs>)
extern void Banner_remove_OnHide_mBF26DD5714334DA7EAE18F9E0763012CCF6F4920 (void);
// 0x00000039 System.Void UnityEngine.Advertisements.Banner::add_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void Banner_add_OnError_mD89F56EB324871C96484456D707F6B92F24B7E9B (void);
// 0x0000003A System.Void UnityEngine.Advertisements.Banner::remove_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void Banner_remove_OnError_mCEECA5E24574CAF952B46ACBF59A09D900C3466C (void);
// 0x0000003B System.Void UnityEngine.Advertisements.Banner::add_OnUnload(System.EventHandler`1<System.EventArgs>)
extern void Banner_add_OnUnload_m1BF26B67825DA1F46ED4A1AA74B2E250EEDE6209 (void);
// 0x0000003C System.Void UnityEngine.Advertisements.Banner::remove_OnUnload(System.EventHandler`1<System.EventArgs>)
extern void Banner_remove_OnUnload_mF9B5937FD6742DACD006BEFEDDDC08A5314DCCB3 (void);
// 0x0000003D System.Void UnityEngine.Advertisements.Banner::add_OnLoad(System.EventHandler`1<System.EventArgs>)
extern void Banner_add_OnLoad_mA6BD3F3063092371D316D8E498F775687ED784FB (void);
// 0x0000003E System.Void UnityEngine.Advertisements.Banner::remove_OnLoad(System.EventHandler`1<System.EventArgs>)
extern void Banner_remove_OnLoad_m1C5738D2036E92F62503550ECABE253C4F7E9ADE (void);
// 0x0000003F System.Void UnityEngine.Advertisements.Banner::.ctor(UnityEngine.AndroidJavaObject,UnityEngine.Advertisements.CallbackExecutor)
extern void Banner__ctor_m7C991A0CBB714BBCA4C6F0F43653D9BE06C83731 (void);
// 0x00000040 System.Boolean UnityEngine.Advertisements.Banner::get_isLoaded()
extern void Banner_get_isLoaded_mA8CDDFBEAE826ED0876FC0CF05BF004D2ED8DF80 (void);
// 0x00000041 System.Void UnityEngine.Advertisements.Banner::Load(System.String)
extern void Banner_Load_mE813F0B190305FC6561FF799530FB88A620EF988 (void);
// 0x00000042 System.Void UnityEngine.Advertisements.Banner::Hide(System.Boolean)
extern void Banner_Hide_mF69AC4D8C23E479D74B805576AAF181AF85CB8BF (void);
// 0x00000043 System.Void UnityEngine.Advertisements.Banner::Show(System.String)
extern void Banner_Show_m82A5BC3E81548301C773E2532CD8AF0AED7C1862 (void);
// 0x00000044 System.Void UnityEngine.Advertisements.Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_mC42ED72EDE1FB067C06C050AFF9002475C4ED0FF (void);
// 0x00000045 System.Void UnityEngine.Advertisements.Banner::onUnityBannerShow(System.String)
extern void Banner_onUnityBannerShow_mFBDF608B6B3F186D09756F26259D7D2149566C85 (void);
// 0x00000046 System.Void UnityEngine.Advertisements.Banner::onUnityBannerHide(System.String)
extern void Banner_onUnityBannerHide_m7DBD6C052007093E32FBCDEE17216EC3575336ED (void);
// 0x00000047 System.Void UnityEngine.Advertisements.Banner::onUnityBannerLoaded(System.String,UnityEngine.AndroidJavaObject)
extern void Banner_onUnityBannerLoaded_mB0A3EF6A1B1A3AFA5F3D2C7D81AD998BB9D2EEE2 (void);
// 0x00000048 System.Void UnityEngine.Advertisements.Banner::onUnityBannerUnloaded(System.String)
extern void Banner_onUnityBannerUnloaded_mFAA320F4FA82EAFE492BF849B6D67DE47B36778C (void);
// 0x00000049 System.Void UnityEngine.Advertisements.Banner::onUnityBannerClick(System.String)
extern void Banner_onUnityBannerClick_m413F97E4703BFF1640CE0A6C3BDC9B8294A63940 (void);
// 0x0000004A System.Void UnityEngine.Advertisements.Banner::onUnityBannerError(System.String)
extern void Banner_onUnityBannerError_m7E31CE9F0B8D887248AA3F22949E6064C8017A2B (void);
// 0x0000004B System.Void UnityEngine.Advertisements.Banner::<Hide>b__25_0()
extern void Banner_U3CHideU3Eb__25_0_m4288A525B768F059418C0DA0D2BA2AEA36DBCE86 (void);
// 0x0000004C System.Void UnityEngine.Advertisements.Banner::<Show>b__26_0()
extern void Banner_U3CShowU3Eb__26_0_m4B04202038DEB894954DAD7BE90E863763FB6D87 (void);
// 0x0000004D System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m3116ED1F831369AE0F2E4823E2B30DD4CE65F75C (void);
// 0x0000004E System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass24_0::<Load>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass24_0_U3CLoadU3Eb__0_m6A7E013A12A69898D4D385CFBFB6F7237CDEAC51 (void);
// 0x0000004F System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m691FBB351BFB5AA6067858036AE8AFAC67D4A84B (void);
// 0x00000050 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass28_0::<onUnityBannerShow>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass28_0_U3ConUnityBannerShowU3Eb__0_mBFF54362E224248BE9960FC31D7D4A7BE118AFD1 (void);
// 0x00000051 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m202E5EFBD066D9CF5F403094DBAB912C565190C0 (void);
// 0x00000052 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass29_0::<onUnityBannerHide>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass29_0_U3ConUnityBannerHideU3Eb__0_m625E20737E0A112AFF5CC926C0B4A6C21A1D11A7 (void);
// 0x00000053 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m7A8652DBCA702AB712F61737C71CEF8DE647EF05 (void);
// 0x00000054 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass30_0::<onUnityBannerLoaded>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass30_0_U3ConUnityBannerLoadedU3Eb__0_m8C46EAFF90F0C6E725B9AC9706BA6FFEF799B6EA (void);
// 0x00000055 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m2C68289E266CFF94D0F0A505F5529A4968F8A9F6 (void);
// 0x00000056 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass31_0::<onUnityBannerUnloaded>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass31_0_U3ConUnityBannerUnloadedU3Eb__0_mE4F622FFA859D678C2375B57526D57B1B0E6DD27 (void);
// 0x00000057 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m54F21C10B2B1E11021BD01138469C9A845BDD3C2 (void);
// 0x00000058 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass33_0::<onUnityBannerError>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass33_0_U3ConUnityBannerErrorU3Eb__0_mEBB79626E4F0AF503A0830B4151FFACC503578B6 (void);
// 0x00000059 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_showCallback()
extern void BannerOptions_get_showCallback_m793328B516C2F58F409DDDFF299D9861A176DE58 (void);
// 0x0000005A System.Void UnityEngine.Advertisements.BannerOptions::set_showCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_showCallback_mB0F6E62E0D920E48A8F23D97B970D08110EC3B08 (void);
// 0x0000005B UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_hideCallback()
extern void BannerOptions_get_hideCallback_mCEA5BD2AC305AD7363B73563683E2CD5B58BE1C2 (void);
// 0x0000005C System.Void UnityEngine.Advertisements.BannerOptions::set_hideCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_hideCallback_m3B0CE3795CA06887F96D10B95E7A247E0CC0B23A (void);
// 0x0000005D System.Void UnityEngine.Advertisements.BannerOptions::.ctor()
extern void BannerOptions__ctor_mB19612A3908B4F10F056A1E8E18EAC702386678B (void);
// 0x0000005E System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::.ctor(System.Object,System.IntPtr)
extern void BannerCallback__ctor_m31692084D0ADC57B0491F9B18DC3B6C235AB61B8 (void);
// 0x0000005F System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::Invoke()
extern void BannerCallback_Invoke_m7867A02B6C4B19FCFF874503030E691C1ECA49AA (void);
// 0x00000060 System.IAsyncResult UnityEngine.Advertisements.BannerOptions/BannerCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void BannerCallback_BeginInvoke_m929FCD6E6F1FE9075FFAA9A50FB462B033A9A935 (void);
// 0x00000061 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::EndInvoke(System.IAsyncResult)
extern void BannerCallback_EndInvoke_mE0B2609EC56B3B4DFD4BE8E352A4D0400C81023E (void);
// 0x00000062 UnityEngine.Advertisements.BannerLoadOptions/LoadCallback UnityEngine.Advertisements.BannerLoadOptions::get_loadCallback()
extern void BannerLoadOptions_get_loadCallback_m301BC53E2AD64C6A00325C4CE455AE0DBF4D3961 (void);
// 0x00000063 System.Void UnityEngine.Advertisements.BannerLoadOptions::set_loadCallback(UnityEngine.Advertisements.BannerLoadOptions/LoadCallback)
extern void BannerLoadOptions_set_loadCallback_m4329F2F398B593D91C8A2BD968B381CCB8F5EB60 (void);
// 0x00000064 UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback UnityEngine.Advertisements.BannerLoadOptions::get_errorCallback()
extern void BannerLoadOptions_get_errorCallback_m8CC522E0F86CC2236C352E7A1D1FC391C909ABC4 (void);
// 0x00000065 System.Void UnityEngine.Advertisements.BannerLoadOptions::set_errorCallback(UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback)
extern void BannerLoadOptions_set_errorCallback_m390340D855BAF0725C9D01F57D369E0EA4B9F8B7 (void);
// 0x00000066 System.Void UnityEngine.Advertisements.BannerLoadOptions::.ctor()
extern void BannerLoadOptions__ctor_m071F23738321390737048156D7DE6F3F585BCA36 (void);
// 0x00000067 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::.ctor(System.Object,System.IntPtr)
extern void LoadCallback__ctor_m2677EBE7D7CFF74CAD64C3614155E9B5DD8B8307 (void);
// 0x00000068 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::Invoke()
extern void LoadCallback_Invoke_m3C9354A8EF01FDA616B4D4CCF1DDC0AF652828EC (void);
// 0x00000069 System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LoadCallback_BeginInvoke_m90898C6D561A8657C84D6600272DA0B622D43F17 (void);
// 0x0000006A System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::EndInvoke(System.IAsyncResult)
extern void LoadCallback_EndInvoke_mF4FFF55D1B0CAC1CF4F4120A8972A362555EFBB1 (void);
// 0x0000006B System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::.ctor(System.Object,System.IntPtr)
extern void ErrorCallback__ctor_mAFF05E90F0C43B41779C4470DF3A20B3D22517BF (void);
// 0x0000006C System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::Invoke(System.String)
extern void ErrorCallback_Invoke_mA52A39A42A4C56388A7991B8189C101F2C7A69D1 (void);
// 0x0000006D System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ErrorCallback_BeginInvoke_m7E1FA38B85C9E9D7283FA7B6244E470EE30BA826 (void);
// 0x0000006E System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::EndInvoke(System.IAsyncResult)
extern void ErrorCallback_EndInvoke_m0271EA9DFD3724777452300F18B85769178247F6 (void);
// 0x0000006F System.Void UnityEngine.Advertisements.CallbackExecutor::Post(System.Action`1<UnityEngine.Advertisements.CallbackExecutor>)
extern void CallbackExecutor_Post_m7FD0DC6C8EF656F3668277DBA7BC9B9275583B5E (void);
// 0x00000070 System.Void UnityEngine.Advertisements.CallbackExecutor::Update()
extern void CallbackExecutor_Update_m34D0244B598BDC75116DA40F4F8D8850C6D50A06 (void);
// 0x00000071 System.Void UnityEngine.Advertisements.CallbackExecutor::.ctor()
extern void CallbackExecutor__ctor_m8EFA81DC12CBA78724868A77977814AFB05F1DD9 (void);
// 0x00000072 System.Boolean UnityEngine.Advertisements.Configuration::get_enabled()
extern void Configuration_get_enabled_mADAEF056D5A0E01FA69D62FE1B82F612E7A0F091 (void);
// 0x00000073 System.Void UnityEngine.Advertisements.Configuration::set_enabled(System.Boolean)
extern void Configuration_set_enabled_m2CA8460B705180466AC6010D39ABEF8C8E3776E2 (void);
// 0x00000074 System.String UnityEngine.Advertisements.Configuration::get_defaultPlacement()
extern void Configuration_get_defaultPlacement_mF8DE16AF8DB46335BD5B7E7F72018C5F3149C306 (void);
// 0x00000075 System.Void UnityEngine.Advertisements.Configuration::set_defaultPlacement(System.String)
extern void Configuration_set_defaultPlacement_mF5E167689F8D411F9E90E6ABF6ABDA8E8FDB0DE5 (void);
// 0x00000076 System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngine.Advertisements.Configuration::get_placements()
extern void Configuration_get_placements_mE3540699DF4055931FCD807291B8C3A4E1E5AF60 (void);
// 0x00000077 System.Void UnityEngine.Advertisements.Configuration::set_placements(System.Collections.Generic.Dictionary`2<System.String,System.Boolean>)
extern void Configuration_set_placements_m12D2057B3503DE4FE1C68ECF38A1FDBEDCDB2992 (void);
// 0x00000078 System.Void UnityEngine.Advertisements.Configuration::.ctor(System.String)
extern void Configuration__ctor_m9FE624424FC986D42DF5F91C2DB9ADBDBD6F67D2 (void);
// 0x00000079 System.Boolean UnityEngine.Advertisements.Creator::get_initializeOnStartup()
extern void Creator_get_initializeOnStartup_m0AE8A62B6E4BFF8267C1AD71D621291B5D0963DD (void);
// 0x0000007A System.Boolean UnityEngine.Advertisements.Creator::get_enabled()
extern void Creator_get_enabled_m5A2D8485AF393ED2FBFCF3ED382B6B51CC22288C (void);
// 0x0000007B System.Boolean UnityEngine.Advertisements.Creator::get_testMode()
extern void Creator_get_testMode_m132AB482D8CC7ADB4F224188E91CCFE72179298E (void);
// 0x0000007C System.String UnityEngine.Advertisements.Creator::get_gameId()
extern void Creator_get_gameId_mF25F9CD49A1CE71197ADA50BBA5D28649694F608 (void);
// 0x0000007D UnityEngine.Advertisements.IPlatform UnityEngine.Advertisements.Creator::CreatePlatform()
extern void Creator_CreatePlatform_mE971A325794AD2E912613C0EAF0A2A2AD787D4D2 (void);
// 0x0000007E System.Void UnityEngine.Advertisements.Creator::LoadRuntime()
extern void Creator_LoadRuntime_mB7761D71A241BBAB13D943F62B2EB85427D3FC2F (void);
// 0x0000007F System.Int64 UnityEngine.Advertisements.ErrorEventArgs::get_error()
extern void ErrorEventArgs_get_error_m1E30CAFD188430D4587CB19F0806111FBEF1876A (void);
// 0x00000080 System.Void UnityEngine.Advertisements.ErrorEventArgs::set_error(System.Int64)
extern void ErrorEventArgs_set_error_m506F8363A4E8CD857210D80CC85C889C5A472726 (void);
// 0x00000081 System.String UnityEngine.Advertisements.ErrorEventArgs::get_message()
extern void ErrorEventArgs_get_message_m365EB32B5200AAB8E7B02AC66F4A40A26826903A (void);
// 0x00000082 System.Void UnityEngine.Advertisements.ErrorEventArgs::set_message(System.String)
extern void ErrorEventArgs_set_message_m1DF258C1D22BEC328A18DDA537BECF4A27DE91B0 (void);
// 0x00000083 System.Void UnityEngine.Advertisements.ErrorEventArgs::.ctor(System.String)
extern void ErrorEventArgs__ctor_mB28D910A0C0E5B11377C0A66C614DA51F671D5A9 (void);
// 0x00000084 System.Void UnityEngine.Advertisements.ErrorEventArgs::.ctor(System.Int64,System.String)
extern void ErrorEventArgs__ctor_mB6707214B2E73FB4EE40DF5BDC417C68512C8ABA (void);
// 0x00000085 System.String UnityEngine.Advertisements.FinishEventArgs::get_placementId()
extern void FinishEventArgs_get_placementId_mD7AA8A53A304FF18B720BBC68D2C16CC041E82C1 (void);
// 0x00000086 System.Void UnityEngine.Advertisements.FinishEventArgs::set_placementId(System.String)
extern void FinishEventArgs_set_placementId_m9DA2870864B91EF0359648D81353309809D92E8B (void);
// 0x00000087 UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.FinishEventArgs::get_showResult()
extern void FinishEventArgs_get_showResult_m1F131AC5478F70C0E9327A3815A4AD385207CC93 (void);
// 0x00000088 System.Void UnityEngine.Advertisements.FinishEventArgs::set_showResult(UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs_set_showResult_m18E14A55B95BA4D544F64A3D615F6079CE481966 (void);
// 0x00000089 System.Void UnityEngine.Advertisements.FinishEventArgs::.ctor(System.String,UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs__ctor_m33AA3917151F9156DF4CD42455521B9C1FFE38F8 (void);
// 0x0000008A System.String UnityEngine.Advertisements.HideEventArgs::get_placementId()
extern void HideEventArgs_get_placementId_m97CFB9AE5E30F2643462801AB415E44493FAFC81 (void);
// 0x0000008B System.Void UnityEngine.Advertisements.HideEventArgs::set_placementId(System.String)
extern void HideEventArgs_set_placementId_m41695EC676F0170C50C27028F925F0A9DCB44800 (void);
// 0x0000008C System.Void UnityEngine.Advertisements.HideEventArgs::.ctor(System.String)
extern void HideEventArgs__ctor_m38C8B7C976347F85B0B9164E09E93E8450B26CA6 (void);
// 0x0000008D System.Void UnityEngine.Advertisements.IBanner::add_OnShow(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
// 0x0000008E System.Void UnityEngine.Advertisements.IBanner::remove_OnShow(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
// 0x0000008F System.Void UnityEngine.Advertisements.IBanner::add_OnHide(System.EventHandler`1<UnityEngine.Advertisements.HideEventArgs>)
// 0x00000090 System.Void UnityEngine.Advertisements.IBanner::remove_OnHide(System.EventHandler`1<UnityEngine.Advertisements.HideEventArgs>)
// 0x00000091 System.Void UnityEngine.Advertisements.IBanner::add_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
// 0x00000092 System.Void UnityEngine.Advertisements.IBanner::remove_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
// 0x00000093 System.Void UnityEngine.Advertisements.IBanner::add_OnUnload(System.EventHandler`1<System.EventArgs>)
// 0x00000094 System.Void UnityEngine.Advertisements.IBanner::remove_OnUnload(System.EventHandler`1<System.EventArgs>)
// 0x00000095 System.Void UnityEngine.Advertisements.IBanner::add_OnLoad(System.EventHandler`1<System.EventArgs>)
// 0x00000096 System.Void UnityEngine.Advertisements.IBanner::remove_OnLoad(System.EventHandler`1<System.EventArgs>)
// 0x00000097 System.Boolean UnityEngine.Advertisements.IBanner::get_isLoaded()
// 0x00000098 System.Void UnityEngine.Advertisements.IBanner::Load(System.String)
// 0x00000099 System.Void UnityEngine.Advertisements.IBanner::Show(System.String)
// 0x0000009A System.Void UnityEngine.Advertisements.IBanner::Hide(System.Boolean)
// 0x0000009B System.Void UnityEngine.Advertisements.IBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x0000009C System.Void UnityEngine.Advertisements.IPlatform::add_OnReady(System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>)
// 0x0000009D System.Void UnityEngine.Advertisements.IPlatform::remove_OnReady(System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>)
// 0x0000009E System.Void UnityEngine.Advertisements.IPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
// 0x0000009F System.Void UnityEngine.Advertisements.IPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
// 0x000000A0 System.Void UnityEngine.Advertisements.IPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
// 0x000000A1 System.Void UnityEngine.Advertisements.IPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
// 0x000000A2 System.Void UnityEngine.Advertisements.IPlatform::add_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
// 0x000000A3 System.Void UnityEngine.Advertisements.IPlatform::remove_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
// 0x000000A4 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.IPlatform::get_Banner()
// 0x000000A5 System.Boolean UnityEngine.Advertisements.IPlatform::get_isInitialized()
// 0x000000A6 System.Boolean UnityEngine.Advertisements.IPlatform::get_isSupported()
// 0x000000A7 System.String UnityEngine.Advertisements.IPlatform::get_version()
// 0x000000A8 System.Boolean UnityEngine.Advertisements.IPlatform::get_debugMode()
// 0x000000A9 System.Void UnityEngine.Advertisements.IPlatform::set_debugMode(System.Boolean)
// 0x000000AA System.Void UnityEngine.Advertisements.IPlatform::Initialize(System.String,System.Boolean)
// 0x000000AB System.Boolean UnityEngine.Advertisements.IPlatform::IsReady(System.String)
// 0x000000AC UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.IPlatform::GetPlacementState(System.String)
// 0x000000AD System.Void UnityEngine.Advertisements.IPlatform::Show(System.String)
// 0x000000AE System.Void UnityEngine.Advertisements.IPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x000000AF System.Void UnityEngine.Advertisements.IPurchasingEventSender::SendPurchasingEvent(System.String)
// 0x000000B0 System.Boolean UnityEngine.Advertisements.IUnityEngineApplication::get_isEditor()
// 0x000000B1 UnityEngine.RuntimePlatform UnityEngine.Advertisements.IUnityEngineApplication::get_platform()
// 0x000000B2 System.String UnityEngine.Advertisements.IUnityEngineApplication::get_unityVersion()
// 0x000000B3 System.String UnityEngine.Advertisements.MetaData::get_category()
extern void MetaData_get_category_mABA6F5C05F3B9045D2C8796F3D466905092C2C07 (void);
// 0x000000B4 System.Void UnityEngine.Advertisements.MetaData::set_category(System.String)
extern void MetaData_set_category_m6A186106F731708B3BCE62D245A8CE3DF20E5FC5 (void);
// 0x000000B5 System.Void UnityEngine.Advertisements.MetaData::.ctor(System.String)
extern void MetaData__ctor_m574B4A87132789B95EEFF22187C0004117015168 (void);
// 0x000000B6 System.Void UnityEngine.Advertisements.MetaData::Set(System.String,System.Object)
extern void MetaData_Set_mA4C4BFF6CA7EC678FA74DD766D35561B5667714B (void);
// 0x000000B7 System.Object UnityEngine.Advertisements.MetaData::Get(System.String)
extern void MetaData_Get_m4A4E2BA6B066D8A443EE78158C10A1C4CB40C688 (void);
// 0x000000B8 System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::Values()
extern void MetaData_Values_m7BC5E8B4541043824B25A53BAD4E796964C9181B (void);
// 0x000000B9 System.String UnityEngine.Advertisements.MetaData::ToJSON()
extern void MetaData_ToJSON_m613DEE9282FCEA51A93A04621F4E6BD46CA8CE00 (void);
// 0x000000BA System.Void UnityEngine.Advertisements.Placeholder::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Placeholder_add_OnFinish_m20EF50FB8F5F846EA19B6478268C373E6A65E030 (void);
// 0x000000BB System.Void UnityEngine.Advertisements.Placeholder::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Placeholder_remove_OnFinish_mBEACC10936874A7C89423DEF5768EBE74489B3EE (void);
// 0x000000BC System.Void UnityEngine.Advertisements.Placeholder::Awake()
extern void Placeholder_Awake_m02E6F946B0F5D383D43569BAB641BB7A0363393E (void);
// 0x000000BD System.Void UnityEngine.Advertisements.Placeholder::Show(System.String,System.Boolean)
extern void Placeholder_Show_m14E05651FBFF590BBF3FD5D0893F14CDDA0EB0D1 (void);
// 0x000000BE System.Void UnityEngine.Advertisements.Placeholder::OnGUI()
extern void Placeholder_OnGUI_m91C9A3429A81B9CBC44BEA5293DB649D486AAE15 (void);
// 0x000000BF System.Void UnityEngine.Advertisements.Placeholder::OnApplicationQuit()
extern void Placeholder_OnApplicationQuit_m61A2462B718D0D6C3B23B6222594E512B0B72476 (void);
// 0x000000C0 System.Void UnityEngine.Advertisements.Placeholder::ModalWindowFunction(System.Int32)
extern void Placeholder_ModalWindowFunction_mCBE6EA3A812C61FD8F7A85F645DECE5CBB3748DE (void);
// 0x000000C1 System.Void UnityEngine.Advertisements.Placeholder::.ctor()
extern void Placeholder__ctor_mDE91870F62BD48520C84B873B93A7A28702F8A80 (void);
// 0x000000C2 System.Void UnityEngine.Advertisements.Platform::onUnityAdsReady(System.String)
extern void Platform_onUnityAdsReady_m8BFC3FBD2AFF5507E87DC2EAA656F3160DA3C694 (void);
// 0x000000C3 System.Void UnityEngine.Advertisements.Platform::onUnityAdsStart(System.String)
extern void Platform_onUnityAdsStart_m36699A88059E36E76CF9E44B69A335ED5FF4BC2F (void);
// 0x000000C4 System.Void UnityEngine.Advertisements.Platform::onUnityAdsFinish(System.String,UnityEngine.AndroidJavaObject)
extern void Platform_onUnityAdsFinish_m76C43E2B575934A7B4DFED47967DBEAFA3A5A0A9 (void);
// 0x000000C5 System.Void UnityEngine.Advertisements.Platform::onUnityAdsError(UnityEngine.AndroidJavaObject,System.String)
extern void Platform_onUnityAdsError_mCDFF91DDC962F2B1D10A806D10DA1B41121AAD4A (void);
// 0x000000C6 System.Void UnityEngine.Advertisements.Platform::add_OnReady(System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>)
extern void Platform_add_OnReady_m3BD5AD3B947EECAC0FE362324F4E7834FA6A5DD3 (void);
// 0x000000C7 System.Void UnityEngine.Advertisements.Platform::remove_OnReady(System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>)
extern void Platform_remove_OnReady_mDA55BE1CF7B270E883EC0D4187D10A351E1219D1 (void);
// 0x000000C8 System.Void UnityEngine.Advertisements.Platform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void Platform_add_OnStart_m4D22DCE9B3752BCAAC63E6C778A75182F0D7C92C (void);
// 0x000000C9 System.Void UnityEngine.Advertisements.Platform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void Platform_remove_OnStart_mEF678584617D744F0C70A83077E3B2E342829DCF (void);
// 0x000000CA System.Void UnityEngine.Advertisements.Platform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Platform_add_OnFinish_mC6C769216CACBFA5615A736CF355F2265EA2A4CA (void);
// 0x000000CB System.Void UnityEngine.Advertisements.Platform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Platform_remove_OnFinish_m8A08CA7735D4D1ADB9FD9AC6626BF8CF2119CE45 (void);
// 0x000000CC System.Void UnityEngine.Advertisements.Platform::add_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void Platform_add_OnError_m9846EC63A177D70427E3D5F26AE42AE385A8FAD4 (void);
// 0x000000CD System.Void UnityEngine.Advertisements.Platform::remove_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void Platform_remove_OnError_mDE61C7A4BEE423C8D31B1C16CF7B962D2AFCC016 (void);
// 0x000000CE System.Void UnityEngine.Advertisements.Platform::.ctor()
extern void Platform__ctor_m5E67B85EE8A30FBA4CEDB38EB2A78F64B8A1913B (void);
// 0x000000CF System.Boolean UnityEngine.Advertisements.Platform::get_isInitialized()
extern void Platform_get_isInitialized_mD47BAD4392A94606D60BDD194CABD6FE1173B379 (void);
// 0x000000D0 System.Boolean UnityEngine.Advertisements.Platform::get_isSupported()
extern void Platform_get_isSupported_m6D874A8F247DF873514890EEA49764DFA11274A3 (void);
// 0x000000D1 System.String UnityEngine.Advertisements.Platform::get_version()
extern void Platform_get_version_m30B33CD24F5155CE95CF62C40B8D34F79C9F3387 (void);
// 0x000000D2 System.Boolean UnityEngine.Advertisements.Platform::get_debugMode()
extern void Platform_get_debugMode_mEB6F0019A80300F6D3BB18F759745BE30468E4A1 (void);
// 0x000000D3 System.Void UnityEngine.Advertisements.Platform::set_debugMode(System.Boolean)
extern void Platform_set_debugMode_mCA26D4279AAE9C5079A766A2F73488CD2DC78203 (void);
// 0x000000D4 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform::get_Banner()
extern void Platform_get_Banner_m4154E43252C76570A36D4C2D84827D2FB1496D7D (void);
// 0x000000D5 System.Void UnityEngine.Advertisements.Platform::Initialize(System.String,System.Boolean)
extern void Platform_Initialize_mA2DA7336B79E27A30C719222441C3921B5D4C6AD (void);
// 0x000000D6 System.Boolean UnityEngine.Advertisements.Platform::IsReady(System.String)
extern void Platform_IsReady_m1D81E9D84D4A642077803E8C4B9933499BBCD0FC (void);
// 0x000000D7 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform::GetPlacementState(System.String)
extern void Platform_GetPlacementState_m740DAAAD911233B7D3ABF4EBF31EC9BBE1EF34FF (void);
// 0x000000D8 System.Void UnityEngine.Advertisements.Platform::Show(System.String)
extern void Platform_Show_m6DE255783949D58C05694DA402B51100B95ECEAD (void);
// 0x000000D9 System.Void UnityEngine.Advertisements.Platform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Platform_SetMetaData_mB02DD5C746740F5A2B76D9782EA1242FC791F9E6 (void);
// 0x000000DA System.Void UnityEngine.Advertisements.Platform::UnityEngine.Advertisements.IPurchasingEventSender.SendPurchasingEvent(System.String)
extern void Platform_UnityEngine_Advertisements_IPurchasingEventSender_SendPurchasingEvent_m016172B52E8A042A4BA885A4306AABFC6B826465 (void);
// 0x000000DB System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m1613A5BF47CADB000B074F35C81C793475AB8FF1 (void);
// 0x000000DC System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass5_0::<onUnityAdsReady>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass5_0_U3ConUnityAdsReadyU3Eb__0_m3EF71DDA92F901246C1BB812D953F381DE4300C1 (void);
// 0x000000DD System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mED5B90013967767AE23DEE64F6E67B71B979423A (void);
// 0x000000DE System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass6_0::<onUnityAdsStart>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass6_0_U3ConUnityAdsStartU3Eb__0_m2112657D82DC7494D517D60D42ED74137B8F43D2 (void);
// 0x000000DF System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m4A927BAF77D4A01C339B0EB389AE27C1F4D0E082 (void);
// 0x000000E0 System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass7_0::<onUnityAdsFinish>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass7_0_U3ConUnityAdsFinishU3Eb__0_mB1B08B85506C917C743E7F41583AE100F7F5598F (void);
// 0x000000E1 System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mAC87B1ACD55F4B92A1C1B622744557EBD5CFBF62 (void);
// 0x000000E2 System.Void UnityEngine.Advertisements.Platform/<>c__DisplayClass8_0::<onUnityAdsError>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass8_0_U3ConUnityAdsErrorU3Eb__0_m8F172FB84DA9A1382E56F7A4AB3A0FBA7F29B906 (void);
// 0x000000E3 System.Void UnityEngine.Advertisements.Purchase::onPurchasingCommand(System.String)
extern void Purchase_onPurchasingCommand_mF52D1CA4995884EFACC39EFF17313F4F4627B4E7 (void);
// 0x000000E4 System.Void UnityEngine.Advertisements.Purchase::onGetPurchasingVersion()
extern void Purchase_onGetPurchasingVersion_m6E439EF6A69EDADE6B495641901AB56D93626160 (void);
// 0x000000E5 System.Void UnityEngine.Advertisements.Purchase::onGetProductCatalog()
extern void Purchase_onGetProductCatalog_m96AD6393EBFB3C2EE8B548F5CDCC8CD485546253 (void);
// 0x000000E6 System.Void UnityEngine.Advertisements.Purchase::onInitializePurchasing()
extern void Purchase_onInitializePurchasing_m19ABB20AD4799BC90D161614A6D2BF5ED92E1463 (void);
// 0x000000E7 System.Void UnityEngine.Advertisements.Purchase::SendEvent(System.String)
extern void Purchase_SendEvent_m6C10F30002325C3C24788949869A643172D3F2AF (void);
// 0x000000E8 System.Void UnityEngine.Advertisements.Purchase::Initialize(UnityEngine.Advertisements.IPurchasingEventSender)
extern void Purchase_Initialize_mB44553FC8EB31BBDA7C2A2D31EB0F2BFDFE561E2 (void);
// 0x000000E9 System.Void UnityEngine.Advertisements.Purchase::.ctor()
extern void Purchase__ctor_m34D951230B978B4C10FDB0C2C18651025E490174 (void);
// 0x000000EA System.Boolean UnityEngine.Advertisements.Purchasing::Initialize(UnityEngine.Advertisements.IPurchasingEventSender)
extern void Purchasing_Initialize_mF31BF8A655AD42BCBC980084C34AE65AD5820B8E (void);
// 0x000000EB System.Boolean UnityEngine.Advertisements.Purchasing::InitiatePurchasingCommand(System.String)
extern void Purchasing_InitiatePurchasingCommand_m5EE95E70493846EDEFBFFB7AA98054EEB289556E (void);
// 0x000000EC System.String UnityEngine.Advertisements.Purchasing::GetPurchasingCatalog()
extern void Purchasing_GetPurchasingCatalog_m3A898FF6D24C8266C555248A52167AB555465261 (void);
// 0x000000ED System.String UnityEngine.Advertisements.Purchasing::GetPromoVersion()
extern void Purchasing_GetPromoVersion_mCA88F7D25859BA3EA47A84DA0F9EC88124F5BF2E (void);
// 0x000000EE System.Boolean UnityEngine.Advertisements.Purchasing::SendEvent(System.String)
extern void Purchasing_SendEvent_m77680DD6BEA1CC4ED40B395D405AF5ADEE7F1621 (void);
// 0x000000EF System.Void UnityEngine.Advertisements.Purchasing::.cctor()
extern void Purchasing__cctor_mC83E752B881477E6EBCB91DEDF00FDED5FC1DED0 (void);
// 0x000000F0 System.String UnityEngine.Advertisements.ReadyEventArgs::get_placementId()
extern void ReadyEventArgs_get_placementId_m9B1A1462F7348F396D3E5F3AE4F2266B510ABA20 (void);
// 0x000000F1 System.Void UnityEngine.Advertisements.ReadyEventArgs::set_placementId(System.String)
extern void ReadyEventArgs_set_placementId_mF13E66593646DDEC39EA34E95436A614832B71BD (void);
// 0x000000F2 System.Void UnityEngine.Advertisements.ReadyEventArgs::.ctor(System.String)
extern void ReadyEventArgs__ctor_mDA2E033EAFD4BD334418D1E351478F9513909809 (void);
// 0x000000F3 System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern void ShowOptions_get_resultCallback_m884750654C53E4096E4F6EB00941DBA62282629E (void);
// 0x000000F4 System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern void ShowOptions_set_resultCallback_mCDCA3E88983BFC496AC7F999EAAA497A01A9CF38 (void);
// 0x000000F5 System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern void ShowOptions_get_gamerSid_m6D82587F5284F893760EA140F52B0C540A35C2B7 (void);
// 0x000000F6 System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern void ShowOptions_set_gamerSid_m281C98C7E79C7F11CA5C613F5D5DE8F35AB9B031 (void);
// 0x000000F7 System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern void ShowOptions__ctor_mEF76BE2E330805AAD1E9928EE8EAB7DF0A6D5DD8 (void);
// 0x000000F8 System.String UnityEngine.Advertisements.StartEventArgs::get_placementId()
extern void StartEventArgs_get_placementId_mC0CADA1095DAE9D9C54CDDB7AA056DA68F2BE5D9 (void);
// 0x000000F9 System.Void UnityEngine.Advertisements.StartEventArgs::set_placementId(System.String)
extern void StartEventArgs_set_placementId_m8AA1E6CADC23BB6EBC352803830BAAF00FF4C534 (void);
// 0x000000FA System.Void UnityEngine.Advertisements.StartEventArgs::.ctor(System.String)
extern void StartEventArgs__ctor_m9AD9C76375F5F73DC578B616FD65C2EC6A75E828 (void);
// 0x000000FB System.Boolean UnityEngine.Advertisements.UnityEngineApplication::get_isEditor()
extern void UnityEngineApplication_get_isEditor_m77B0FC2315FCBF3A4B246431CC16431926559F3D (void);
// 0x000000FC UnityEngine.RuntimePlatform UnityEngine.Advertisements.UnityEngineApplication::get_platform()
extern void UnityEngineApplication_get_platform_m3CE239D29E7C11040032758CDEF516E5A75A457F (void);
// 0x000000FD System.String UnityEngine.Advertisements.UnityEngineApplication::get_unityVersion()
extern void UnityEngineApplication_get_unityVersion_m7CD2101869F6EFE909C625C0D4E61C508238E314 (void);
// 0x000000FE System.Void UnityEngine.Advertisements.UnityEngineApplication::.ctor()
extern void UnityEngineApplication__ctor_mAA67CE53FEFB214FBAA525A23DABBF63DD64341E (void);
// 0x000000FF System.Void UnityEngine.Advertisements.UnsupportedPlatform::add_OnReady(System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>)
extern void UnsupportedPlatform_add_OnReady_m77C119B20E788C743A004A3A7543AA1D8AF5C41C (void);
// 0x00000100 System.Void UnityEngine.Advertisements.UnsupportedPlatform::remove_OnReady(System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>)
extern void UnsupportedPlatform_remove_OnReady_m71C7C9A6338F85D26C8E3E1DA0F3C843A2BA8CA9 (void);
// 0x00000101 System.Void UnityEngine.Advertisements.UnsupportedPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void UnsupportedPlatform_add_OnStart_m93A77F8C815BC41849585FA381880BD444E08766 (void);
// 0x00000102 System.Void UnityEngine.Advertisements.UnsupportedPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void UnsupportedPlatform_remove_OnStart_m878E6EBA1D4C4F76559FCF0495FE77A312C23023 (void);
// 0x00000103 System.Void UnityEngine.Advertisements.UnsupportedPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void UnsupportedPlatform_add_OnFinish_m5AAFA121BBF745D8AE848A225D0D6BF9ABA3B2BD (void);
// 0x00000104 System.Void UnityEngine.Advertisements.UnsupportedPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void UnsupportedPlatform_remove_OnFinish_m610E0A8FF608B4AB6F0C0B8A4F511689B92A4611 (void);
// 0x00000105 System.Void UnityEngine.Advertisements.UnsupportedPlatform::add_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void UnsupportedPlatform_add_OnError_mCCE203879136D93C190696160E5981481B0CF55F (void);
// 0x00000106 System.Void UnityEngine.Advertisements.UnsupportedPlatform::remove_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void UnsupportedPlatform_remove_OnError_mA125760D97199C912369E0A0976155531A43E0CF (void);
// 0x00000107 System.Boolean UnityEngine.Advertisements.UnsupportedPlatform::get_isInitialized()
extern void UnsupportedPlatform_get_isInitialized_m2290344D02FF5DA592CF103238B74D08EDA3F4D7 (void);
// 0x00000108 System.Boolean UnityEngine.Advertisements.UnsupportedPlatform::get_isSupported()
extern void UnsupportedPlatform_get_isSupported_mF0887192C8D7021576615EB37096E8F927C19E33 (void);
// 0x00000109 System.String UnityEngine.Advertisements.UnsupportedPlatform::get_version()
extern void UnsupportedPlatform_get_version_m2DD7B6B6ED7ECEE92CE28186F6E4CF066DFB493B (void);
// 0x0000010A System.Boolean UnityEngine.Advertisements.UnsupportedPlatform::get_debugMode()
extern void UnsupportedPlatform_get_debugMode_mFEEAF82E05A16A7C3DF6885421CF25C42695DF1C (void);
// 0x0000010B System.Void UnityEngine.Advertisements.UnsupportedPlatform::set_debugMode(System.Boolean)
extern void UnsupportedPlatform_set_debugMode_m0C77C0A09EA19CB802A302BF3EB885EBCBFCF45D (void);
// 0x0000010C UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.UnsupportedPlatform::get_Banner()
extern void UnsupportedPlatform_get_Banner_mBEA2BCB308CDF45A25641FCCF451CB3FA4D3F0EA (void);
// 0x0000010D System.Void UnityEngine.Advertisements.UnsupportedPlatform::Initialize(System.String,System.Boolean)
extern void UnsupportedPlatform_Initialize_m0FA08937CBBACFED60962EBD1C921160FE27D326 (void);
// 0x0000010E System.Boolean UnityEngine.Advertisements.UnsupportedPlatform::IsReady(System.String)
extern void UnsupportedPlatform_IsReady_mE18D2F51D03EDF5C7C6BCC34D2D845BF9FCD3745 (void);
// 0x0000010F UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.UnsupportedPlatform::GetPlacementState(System.String)
extern void UnsupportedPlatform_GetPlacementState_mBDF5C05354D5263F4430C2DE2309D806577C7C26 (void);
// 0x00000110 System.Void UnityEngine.Advertisements.UnsupportedPlatform::Show(System.String)
extern void UnsupportedPlatform_Show_m51A5F6E4F5831CB127C168CB351C4A09D3C2AC8E (void);
// 0x00000111 System.Void UnityEngine.Advertisements.UnsupportedPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void UnsupportedPlatform_SetMetaData_mBDF510F62784167760D14D58B106734F7A880927 (void);
// 0x00000112 System.Void UnityEngine.Advertisements.UnsupportedPlatform::.ctor()
extern void UnsupportedPlatform__ctor_m1E88979183745EC1566C699759768D0D13B4F97B (void);
// 0x00000113 System.Void UnityEngine.Advertisements.UnsupportedPlatform::.cctor()
extern void UnsupportedPlatform__cctor_m2D281BFEA03BCB2E8DDE3B6F129B814C01C05A6F (void);
// 0x00000114 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::add_OnShow(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void NullBanner_add_OnShow_mF8E1D6092CD1F0E98F8A401B4D75925008A1AA03 (void);
// 0x00000115 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::remove_OnShow(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void NullBanner_remove_OnShow_m54020C9F4FC73465598470B756A2C925B63AAF4E (void);
// 0x00000116 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::add_OnHide(System.EventHandler`1<UnityEngine.Advertisements.HideEventArgs>)
extern void NullBanner_add_OnHide_m05C3974EFA5C23EFB046F2DBAB3D95487F77CEE3 (void);
// 0x00000117 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::remove_OnHide(System.EventHandler`1<UnityEngine.Advertisements.HideEventArgs>)
extern void NullBanner_remove_OnHide_mB24F0CEF8D554891D3E4315C240DD9F476EEAEC8 (void);
// 0x00000118 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::add_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void NullBanner_add_OnError_mCAE7031EE1E2C462B7578F2C5D083A76F39C5A32 (void);
// 0x00000119 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::remove_OnError(System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>)
extern void NullBanner_remove_OnError_m190A1605D10F63EAFB96300E53805F81DB034941 (void);
// 0x0000011A System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::add_OnUnload(System.EventHandler`1<System.EventArgs>)
extern void NullBanner_add_OnUnload_m6A90F0D9F1E35CD9EF2E47D0BE48BCE5387104E0 (void);
// 0x0000011B System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::remove_OnUnload(System.EventHandler`1<System.EventArgs>)
extern void NullBanner_remove_OnUnload_m4C997D76A65F34379E5A417F932DD7447B59B4BB (void);
// 0x0000011C System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::add_OnLoad(System.EventHandler`1<System.EventArgs>)
extern void NullBanner_add_OnLoad_mA46D40D9AB564234E09A0F42EAE0EDFBA43E24D3 (void);
// 0x0000011D System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::remove_OnLoad(System.EventHandler`1<System.EventArgs>)
extern void NullBanner_remove_OnLoad_mC4272FA0B08ABCABF0A24AE7E7C2A5B66A2BC0B3 (void);
// 0x0000011E System.Boolean UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::get_isLoaded()
extern void NullBanner_get_isLoaded_mE93DC3BD1B4F591188B0B402A7A4DDF7A96E8FC1 (void);
// 0x0000011F System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::Load(System.String)
extern void NullBanner_Load_m7FAD762EF450871C167F81DE79C7CDDFC7D4D2B2 (void);
// 0x00000120 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::Hide(System.Boolean)
extern void NullBanner_Hide_m397D91F7B806AD2E0434DEED14BF33306F6BC807 (void);
// 0x00000121 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::Show(System.String)
extern void NullBanner_Show_m343E800435D5FEE266F3F1A1A304539F0C5B9882 (void);
// 0x00000122 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void NullBanner_SetPosition_m79FA0E49CA3B7E5F00975D9FF640D60D2C9B38B9 (void);
// 0x00000123 System.Void UnityEngine.Advertisements.UnsupportedPlatform/NullBanner::.ctor()
extern void NullBanner__ctor_m0FA42D9F270CA7800AE3370D74D510A4B2E1BECE (void);
// 0x00000124 System.Object UnityEngine.Advertisements.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m4A530DF137AD6A50B1E7B9E21F5278AD7E12A56D (void);
// 0x00000125 System.String UnityEngine.Advertisements.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_m86A12F4404AD4A11BDE9A1FA98ED21C3FDC06851 (void);
// 0x00000126 System.Boolean UnityEngine.Advertisements.MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m1817F82992F8304E4FBCB72B51EC3BE9A64D794F (void);
// 0x00000127 System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::.ctor(System.String)
extern void Parser__ctor_mDBBFF1D039E039AB00193D9B155255B06C020869 (void);
// 0x00000128 System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::Parse(System.String)
extern void Parser_Parse_m89E9A5582D688091E09764848A861E8694516D40 (void);
// 0x00000129 System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::Dispose()
extern void Parser_Dispose_mD785A7EDE0F720417092F602677F0FCBC40CDE7F (void);
// 0x0000012A System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseObject()
extern void Parser_ParseObject_m737536AE424E9B309DC2B062077AF2904DE52D0D (void);
// 0x0000012B System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseArray()
extern void Parser_ParseArray_mCE36C2B63262664B0F9C0838D01C271B1A425899 (void);
// 0x0000012C System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseValue()
extern void Parser_ParseValue_m53686417FAA31D016D9FB178EDF56C0B50F740AC (void);
// 0x0000012D System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseByToken(UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN)
extern void Parser_ParseByToken_m529FD80061C6E1A3ED1470F6A3590A7167B28601 (void);
// 0x0000012E System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseString()
extern void Parser_ParseString_m741791EAF3B6CAF64010A16121B1DF7A8F948E36 (void);
// 0x0000012F System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_m50E089D05873A3FA4C5FB22B4CF2BB0763DD4758 (void);
// 0x00000130 System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_m694EFED3DF28C103A9EADEB24C460A670C51835A (void);
// 0x00000131 System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_m60107656CEDC411EF966B55F1BCDC0C827B07EE9 (void);
// 0x00000132 System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_mFE425E6767A33DDE3599510783066FB925597502 (void);
// 0x00000133 System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_mC383B30576FEC3E5A0A690983D8C4BEA6492914B (void);
// 0x00000134 UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m1420F3984B6C59DB6BA260D419BB53805C820B6A (void);
// 0x00000135 System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::.ctor()
extern void Serializer__ctor_mCF1A2F42C40273B37CBAD9E7A2AE250E9C297380 (void);
// 0x00000136 System.String UnityEngine.Advertisements.MiniJSON.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m75CF66178A0E0144742255D0089277E9C8AAFC74 (void);
// 0x00000137 System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m0994AAA71A5456A887085333CE1959731381233C (void);
// 0x00000138 System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_mBEF0C6D8C40A6D0F16D396E77AB0AA3A306657EB (void);
// 0x00000139 System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m2BC22F2D169FD1B44E02DBEEA137593ACEE64032 (void);
// 0x0000013A System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mC3EA03CC43A5C4D4EAD70F12B9CFA8589C5793A3 (void);
// 0x0000013B System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m3C3268660DDA90593E5689D647C5772D2257179E (void);
static Il2CppMethodPointer s_methodPointers[315] = 
{
	Advertisement_get_platform_m7C6FB11D5F216ED89592DD14EAE230FF77DD51D1,
	Advertisement_set_platform_m9FA2A6A70A41EA51FE0BA96BD0C8BCB00E95F2EA,
	Advertisement_get_application_m455009E0124B252B57E8073AD8C3CF3D291118B4,
	Advertisement_set_application_m310EAB6008BD57C02D8982C79919E0B0B255C3EA,
	Advertisement_get_isInitialized_m1B20FCB52BF511CA59C6677CFC5DE25657DAE291,
	Advertisement_set_isInitialized_mA3053403CB0BB1A645BB8D518F5C80183C37B968,
	Advertisement_get_isSupported_mB2844EB06ED9B956210C4D19D1CD84A91346F436,
	Advertisement_get_debugMode_m1F03EB76185EA03A4B26C6F010E092CCEA8F5718,
	Advertisement_set_debugMode_m07DAE0ADC40E9C405FC1C203E34C7F76DCD47229,
	Advertisement_get_version_m2BAA6073750D584979C5BFA3AE3C0AA00D0EF580,
	Advertisement_get_isShowing_m26D3967DF88555434C75721E15B05A72F048C90F,
	Advertisement_set_isShowing_m318336D64BA4CF33A2747F3161FA3A11677E2624,
	Advertisement__cctor_m57B7C8775C79CDF0DA65FD0C0D55ADF0D1EB5219,
	Advertisement_Initialize_m0F11D7CB204F288550555A1D353D92E11F117EB1,
	Advertisement_Initialize_m320CA0C1DF8C130E99B011335A2E30167A8614D6,
	Advertisement_IsReady_m19441D73BCDBC5BEA4E71D315E47F85EFCA08B5E,
	Advertisement_IsReady_m8210628F9797095011B967F5F300DAD519B3E469,
	Advertisement_GetPlacementState_mFCA0A6D5221C19DD8C92453075C9B98C0FAE2548,
	Advertisement_GetPlacementState_mB1550CF4D6D3BF6B7A18ECBEEF6450F27E67351D,
	Advertisement_Show_mF3736780D5BD0C95CD22EC36AD4901928BD97AF0,
	Advertisement_Show_mBE84E0FABAFFA252880CE1BC1FB480578B6F2970,
	Advertisement_Show_mFE7E1B0A97096DE79AF12F1590AF9B3D4ADC77F4,
	Advertisement_Show_mA94968EC7B2AE585301451F56848B1A462A0F1CA,
	Advertisement_SetMetaData_mFC38B21497FFA48D760188542BCAC2DA10545F12,
	Banner_Load_m81A605BBF41D1EE961124FC253D1E47033DC5AD8,
	Banner_Load_m95621B0C4DF1FC839E95DED4844EAF6EF099BE30,
	Banner_Load_m1773A0585078B63734B207F27B52B2F89BEBEFBC,
	Banner_Load_mB4EDD60310846E5EF2BB6BA858BE3E454550EB49,
	Banner_Show_mF4010062868A18AECEF92B9EA358FB65A47575D2,
	Banner_Show_mEB8D6E9B8118C05D90C23FE30CCFD54A128830E5,
	Banner_Show_m11FEE442448755EBF95951E6D400ED69D9DCCF82,
	Banner_Show_mD087E74D10F6A713B342213511301620DE027981,
	Banner_Hide_m4437A96FF313B515C956060ABBE4A0239D40CD54,
	Banner_SetPosition_mDD7A6E426DB2A05E7BBDD16463B640B606273881,
	Banner_get_isLoaded_m14179EF87AF4CDDE880843A5B76A4492ADF627DB,
	U3CU3Ec__DisplayClass6_0__ctor_m2A34D989F2BFC0E9E02C8CEF6E72C05A8B231C34,
	U3CU3Ec__DisplayClass6_1__ctor_m42EA990509B8C51AF2A335C4239AF77071828AEE,
	U3CU3Ec__DisplayClass6_1_U3CLoadU3Eb__0_m4870B5D9CF739D795D41312B37D17E502E02AD0F,
	U3CU3Ec__DisplayClass6_2__ctor_mC39B30DF48C657CC7C602A4A7FD40BE3FE7FB131,
	U3CU3Ec__DisplayClass6_2_U3CLoadU3Eb__1_m855F1E0061C9D6978470E932EFB5FA4DAAD0E23F,
	U3CU3Ec__DisplayClass10_0__ctor_mFA5ED412A0D00A87A03431F5DFB73EF98D73F284,
	U3CU3Ec__DisplayClass10_0_U3CShowU3Eb__1_m96304B2CC91259285A3F42C1B6403C908B8D7A9D,
	U3CU3Ec__DisplayClass10_0_U3CShowU3Eb__2_m1F1A61BA6C4B90DBD5CA5C21580C7176E52D639A,
	U3CU3Ec__cctor_mF570E9B7619012EDE3AC698FB14E772A42FB6878,
	U3CU3Ec__ctor_mEDE3C937EF27C1E6D6D490991846461080D61BA5,
	U3CU3Ec_U3CShowU3Eb__10_0_m947CF17067EB096AF64591D8CF1FBF0286CDCB5D,
	U3CU3Ec__cctor_m2BB9A1C7D8BE2C8BB12A0C9EC5B43514EFEA290F,
	U3CU3Ec__ctor_m5FD11ACF209392359E3162BA64832E91F25ED11B,
	U3CU3Ec_U3CInitializeU3Eb__26_0_m2D645661DFFC69933E4F408C156E273F930C99DF,
	U3CU3Ec_U3CInitializeU3Eb__26_1_m1FCF5AEE4B33C620C0D4852FE6D3E29DB695B7EA,
	U3CU3Ec__DisplayClass34_0__ctor_mC6E7F87EE379750F3E6232BC867582FF58C46A2B,
	U3CU3Ec__DisplayClass34_0_U3CShowU3Eb__0_m4A4E89874BE5A64FAC64E2523D605FDB29C40361,
	Banner_add_OnShow_mB458F372D3DD207CA96F82388E162918002CCFE7,
	Banner_remove_OnShow_m45410E5D53456E1D26E2739D9D96B8AE7B250079,
	Banner_add_OnHide_m730316CF87D563577C8E8575B7932FE7B1840A22,
	Banner_remove_OnHide_mBF26DD5714334DA7EAE18F9E0763012CCF6F4920,
	Banner_add_OnError_mD89F56EB324871C96484456D707F6B92F24B7E9B,
	Banner_remove_OnError_mCEECA5E24574CAF952B46ACBF59A09D900C3466C,
	Banner_add_OnUnload_m1BF26B67825DA1F46ED4A1AA74B2E250EEDE6209,
	Banner_remove_OnUnload_mF9B5937FD6742DACD006BEFEDDDC08A5314DCCB3,
	Banner_add_OnLoad_mA6BD3F3063092371D316D8E498F775687ED784FB,
	Banner_remove_OnLoad_m1C5738D2036E92F62503550ECABE253C4F7E9ADE,
	Banner__ctor_m7C991A0CBB714BBCA4C6F0F43653D9BE06C83731,
	Banner_get_isLoaded_mA8CDDFBEAE826ED0876FC0CF05BF004D2ED8DF80,
	Banner_Load_mE813F0B190305FC6561FF799530FB88A620EF988,
	Banner_Hide_mF69AC4D8C23E479D74B805576AAF181AF85CB8BF,
	Banner_Show_m82A5BC3E81548301C773E2532CD8AF0AED7C1862,
	Banner_SetPosition_mC42ED72EDE1FB067C06C050AFF9002475C4ED0FF,
	Banner_onUnityBannerShow_mFBDF608B6B3F186D09756F26259D7D2149566C85,
	Banner_onUnityBannerHide_m7DBD6C052007093E32FBCDEE17216EC3575336ED,
	Banner_onUnityBannerLoaded_mB0A3EF6A1B1A3AFA5F3D2C7D81AD998BB9D2EEE2,
	Banner_onUnityBannerUnloaded_mFAA320F4FA82EAFE492BF849B6D67DE47B36778C,
	Banner_onUnityBannerClick_m413F97E4703BFF1640CE0A6C3BDC9B8294A63940,
	Banner_onUnityBannerError_m7E31CE9F0B8D887248AA3F22949E6064C8017A2B,
	Banner_U3CHideU3Eb__25_0_m4288A525B768F059418C0DA0D2BA2AEA36DBCE86,
	Banner_U3CShowU3Eb__26_0_m4B04202038DEB894954DAD7BE90E863763FB6D87,
	U3CU3Ec__DisplayClass24_0__ctor_m3116ED1F831369AE0F2E4823E2B30DD4CE65F75C,
	U3CU3Ec__DisplayClass24_0_U3CLoadU3Eb__0_m6A7E013A12A69898D4D385CFBFB6F7237CDEAC51,
	U3CU3Ec__DisplayClass28_0__ctor_m691FBB351BFB5AA6067858036AE8AFAC67D4A84B,
	U3CU3Ec__DisplayClass28_0_U3ConUnityBannerShowU3Eb__0_mBFF54362E224248BE9960FC31D7D4A7BE118AFD1,
	U3CU3Ec__DisplayClass29_0__ctor_m202E5EFBD066D9CF5F403094DBAB912C565190C0,
	U3CU3Ec__DisplayClass29_0_U3ConUnityBannerHideU3Eb__0_m625E20737E0A112AFF5CC926C0B4A6C21A1D11A7,
	U3CU3Ec__DisplayClass30_0__ctor_m7A8652DBCA702AB712F61737C71CEF8DE647EF05,
	U3CU3Ec__DisplayClass30_0_U3ConUnityBannerLoadedU3Eb__0_m8C46EAFF90F0C6E725B9AC9706BA6FFEF799B6EA,
	U3CU3Ec__DisplayClass31_0__ctor_m2C68289E266CFF94D0F0A505F5529A4968F8A9F6,
	U3CU3Ec__DisplayClass31_0_U3ConUnityBannerUnloadedU3Eb__0_mE4F622FFA859D678C2375B57526D57B1B0E6DD27,
	U3CU3Ec__DisplayClass33_0__ctor_m54F21C10B2B1E11021BD01138469C9A845BDD3C2,
	U3CU3Ec__DisplayClass33_0_U3ConUnityBannerErrorU3Eb__0_mEBB79626E4F0AF503A0830B4151FFACC503578B6,
	BannerOptions_get_showCallback_m793328B516C2F58F409DDDFF299D9861A176DE58,
	BannerOptions_set_showCallback_mB0F6E62E0D920E48A8F23D97B970D08110EC3B08,
	BannerOptions_get_hideCallback_mCEA5BD2AC305AD7363B73563683E2CD5B58BE1C2,
	BannerOptions_set_hideCallback_m3B0CE3795CA06887F96D10B95E7A247E0CC0B23A,
	BannerOptions__ctor_mB19612A3908B4F10F056A1E8E18EAC702386678B,
	BannerCallback__ctor_m31692084D0ADC57B0491F9B18DC3B6C235AB61B8,
	BannerCallback_Invoke_m7867A02B6C4B19FCFF874503030E691C1ECA49AA,
	BannerCallback_BeginInvoke_m929FCD6E6F1FE9075FFAA9A50FB462B033A9A935,
	BannerCallback_EndInvoke_mE0B2609EC56B3B4DFD4BE8E352A4D0400C81023E,
	BannerLoadOptions_get_loadCallback_m301BC53E2AD64C6A00325C4CE455AE0DBF4D3961,
	BannerLoadOptions_set_loadCallback_m4329F2F398B593D91C8A2BD968B381CCB8F5EB60,
	BannerLoadOptions_get_errorCallback_m8CC522E0F86CC2236C352E7A1D1FC391C909ABC4,
	BannerLoadOptions_set_errorCallback_m390340D855BAF0725C9D01F57D369E0EA4B9F8B7,
	BannerLoadOptions__ctor_m071F23738321390737048156D7DE6F3F585BCA36,
	LoadCallback__ctor_m2677EBE7D7CFF74CAD64C3614155E9B5DD8B8307,
	LoadCallback_Invoke_m3C9354A8EF01FDA616B4D4CCF1DDC0AF652828EC,
	LoadCallback_BeginInvoke_m90898C6D561A8657C84D6600272DA0B622D43F17,
	LoadCallback_EndInvoke_mF4FFF55D1B0CAC1CF4F4120A8972A362555EFBB1,
	ErrorCallback__ctor_mAFF05E90F0C43B41779C4470DF3A20B3D22517BF,
	ErrorCallback_Invoke_mA52A39A42A4C56388A7991B8189C101F2C7A69D1,
	ErrorCallback_BeginInvoke_m7E1FA38B85C9E9D7283FA7B6244E470EE30BA826,
	ErrorCallback_EndInvoke_m0271EA9DFD3724777452300F18B85769178247F6,
	CallbackExecutor_Post_m7FD0DC6C8EF656F3668277DBA7BC9B9275583B5E,
	CallbackExecutor_Update_m34D0244B598BDC75116DA40F4F8D8850C6D50A06,
	CallbackExecutor__ctor_m8EFA81DC12CBA78724868A77977814AFB05F1DD9,
	Configuration_get_enabled_mADAEF056D5A0E01FA69D62FE1B82F612E7A0F091,
	Configuration_set_enabled_m2CA8460B705180466AC6010D39ABEF8C8E3776E2,
	Configuration_get_defaultPlacement_mF8DE16AF8DB46335BD5B7E7F72018C5F3149C306,
	Configuration_set_defaultPlacement_mF5E167689F8D411F9E90E6ABF6ABDA8E8FDB0DE5,
	Configuration_get_placements_mE3540699DF4055931FCD807291B8C3A4E1E5AF60,
	Configuration_set_placements_m12D2057B3503DE4FE1C68ECF38A1FDBEDCDB2992,
	Configuration__ctor_m9FE624424FC986D42DF5F91C2DB9ADBDBD6F67D2,
	Creator_get_initializeOnStartup_m0AE8A62B6E4BFF8267C1AD71D621291B5D0963DD,
	Creator_get_enabled_m5A2D8485AF393ED2FBFCF3ED382B6B51CC22288C,
	Creator_get_testMode_m132AB482D8CC7ADB4F224188E91CCFE72179298E,
	Creator_get_gameId_mF25F9CD49A1CE71197ADA50BBA5D28649694F608,
	Creator_CreatePlatform_mE971A325794AD2E912613C0EAF0A2A2AD787D4D2,
	Creator_LoadRuntime_mB7761D71A241BBAB13D943F62B2EB85427D3FC2F,
	ErrorEventArgs_get_error_m1E30CAFD188430D4587CB19F0806111FBEF1876A,
	ErrorEventArgs_set_error_m506F8363A4E8CD857210D80CC85C889C5A472726,
	ErrorEventArgs_get_message_m365EB32B5200AAB8E7B02AC66F4A40A26826903A,
	ErrorEventArgs_set_message_m1DF258C1D22BEC328A18DDA537BECF4A27DE91B0,
	ErrorEventArgs__ctor_mB28D910A0C0E5B11377C0A66C614DA51F671D5A9,
	ErrorEventArgs__ctor_mB6707214B2E73FB4EE40DF5BDC417C68512C8ABA,
	FinishEventArgs_get_placementId_mD7AA8A53A304FF18B720BBC68D2C16CC041E82C1,
	FinishEventArgs_set_placementId_m9DA2870864B91EF0359648D81353309809D92E8B,
	FinishEventArgs_get_showResult_m1F131AC5478F70C0E9327A3815A4AD385207CC93,
	FinishEventArgs_set_showResult_m18E14A55B95BA4D544F64A3D615F6079CE481966,
	FinishEventArgs__ctor_m33AA3917151F9156DF4CD42455521B9C1FFE38F8,
	HideEventArgs_get_placementId_m97CFB9AE5E30F2643462801AB415E44493FAFC81,
	HideEventArgs_set_placementId_m41695EC676F0170C50C27028F925F0A9DCB44800,
	HideEventArgs__ctor_m38C8B7C976347F85B0B9164E09E93E8450B26CA6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetaData_get_category_mABA6F5C05F3B9045D2C8796F3D466905092C2C07,
	MetaData_set_category_m6A186106F731708B3BCE62D245A8CE3DF20E5FC5,
	MetaData__ctor_m574B4A87132789B95EEFF22187C0004117015168,
	MetaData_Set_mA4C4BFF6CA7EC678FA74DD766D35561B5667714B,
	MetaData_Get_m4A4E2BA6B066D8A443EE78158C10A1C4CB40C688,
	MetaData_Values_m7BC5E8B4541043824B25A53BAD4E796964C9181B,
	MetaData_ToJSON_m613DEE9282FCEA51A93A04621F4E6BD46CA8CE00,
	Placeholder_add_OnFinish_m20EF50FB8F5F846EA19B6478268C373E6A65E030,
	Placeholder_remove_OnFinish_mBEACC10936874A7C89423DEF5768EBE74489B3EE,
	Placeholder_Awake_m02E6F946B0F5D383D43569BAB641BB7A0363393E,
	Placeholder_Show_m14E05651FBFF590BBF3FD5D0893F14CDDA0EB0D1,
	Placeholder_OnGUI_m91C9A3429A81B9CBC44BEA5293DB649D486AAE15,
	Placeholder_OnApplicationQuit_m61A2462B718D0D6C3B23B6222594E512B0B72476,
	Placeholder_ModalWindowFunction_mCBE6EA3A812C61FD8F7A85F645DECE5CBB3748DE,
	Placeholder__ctor_mDE91870F62BD48520C84B873B93A7A28702F8A80,
	Platform_onUnityAdsReady_m8BFC3FBD2AFF5507E87DC2EAA656F3160DA3C694,
	Platform_onUnityAdsStart_m36699A88059E36E76CF9E44B69A335ED5FF4BC2F,
	Platform_onUnityAdsFinish_m76C43E2B575934A7B4DFED47967DBEAFA3A5A0A9,
	Platform_onUnityAdsError_mCDFF91DDC962F2B1D10A806D10DA1B41121AAD4A,
	Platform_add_OnReady_m3BD5AD3B947EECAC0FE362324F4E7834FA6A5DD3,
	Platform_remove_OnReady_mDA55BE1CF7B270E883EC0D4187D10A351E1219D1,
	Platform_add_OnStart_m4D22DCE9B3752BCAAC63E6C778A75182F0D7C92C,
	Platform_remove_OnStart_mEF678584617D744F0C70A83077E3B2E342829DCF,
	Platform_add_OnFinish_mC6C769216CACBFA5615A736CF355F2265EA2A4CA,
	Platform_remove_OnFinish_m8A08CA7735D4D1ADB9FD9AC6626BF8CF2119CE45,
	Platform_add_OnError_m9846EC63A177D70427E3D5F26AE42AE385A8FAD4,
	Platform_remove_OnError_mDE61C7A4BEE423C8D31B1C16CF7B962D2AFCC016,
	Platform__ctor_m5E67B85EE8A30FBA4CEDB38EB2A78F64B8A1913B,
	Platform_get_isInitialized_mD47BAD4392A94606D60BDD194CABD6FE1173B379,
	Platform_get_isSupported_m6D874A8F247DF873514890EEA49764DFA11274A3,
	Platform_get_version_m30B33CD24F5155CE95CF62C40B8D34F79C9F3387,
	Platform_get_debugMode_mEB6F0019A80300F6D3BB18F759745BE30468E4A1,
	Platform_set_debugMode_mCA26D4279AAE9C5079A766A2F73488CD2DC78203,
	Platform_get_Banner_m4154E43252C76570A36D4C2D84827D2FB1496D7D,
	Platform_Initialize_mA2DA7336B79E27A30C719222441C3921B5D4C6AD,
	Platform_IsReady_m1D81E9D84D4A642077803E8C4B9933499BBCD0FC,
	Platform_GetPlacementState_m740DAAAD911233B7D3ABF4EBF31EC9BBE1EF34FF,
	Platform_Show_m6DE255783949D58C05694DA402B51100B95ECEAD,
	Platform_SetMetaData_mB02DD5C746740F5A2B76D9782EA1242FC791F9E6,
	Platform_UnityEngine_Advertisements_IPurchasingEventSender_SendPurchasingEvent_m016172B52E8A042A4BA885A4306AABFC6B826465,
	U3CU3Ec__DisplayClass5_0__ctor_m1613A5BF47CADB000B074F35C81C793475AB8FF1,
	U3CU3Ec__DisplayClass5_0_U3ConUnityAdsReadyU3Eb__0_m3EF71DDA92F901246C1BB812D953F381DE4300C1,
	U3CU3Ec__DisplayClass6_0__ctor_mED5B90013967767AE23DEE64F6E67B71B979423A,
	U3CU3Ec__DisplayClass6_0_U3ConUnityAdsStartU3Eb__0_m2112657D82DC7494D517D60D42ED74137B8F43D2,
	U3CU3Ec__DisplayClass7_0__ctor_m4A927BAF77D4A01C339B0EB389AE27C1F4D0E082,
	U3CU3Ec__DisplayClass7_0_U3ConUnityAdsFinishU3Eb__0_mB1B08B85506C917C743E7F41583AE100F7F5598F,
	U3CU3Ec__DisplayClass8_0__ctor_mAC87B1ACD55F4B92A1C1B622744557EBD5CFBF62,
	U3CU3Ec__DisplayClass8_0_U3ConUnityAdsErrorU3Eb__0_m8F172FB84DA9A1382E56F7A4AB3A0FBA7F29B906,
	Purchase_onPurchasingCommand_mF52D1CA4995884EFACC39EFF17313F4F4627B4E7,
	Purchase_onGetPurchasingVersion_m6E439EF6A69EDADE6B495641901AB56D93626160,
	Purchase_onGetProductCatalog_m96AD6393EBFB3C2EE8B548F5CDCC8CD485546253,
	Purchase_onInitializePurchasing_m19ABB20AD4799BC90D161614A6D2BF5ED92E1463,
	Purchase_SendEvent_m6C10F30002325C3C24788949869A643172D3F2AF,
	Purchase_Initialize_mB44553FC8EB31BBDA7C2A2D31EB0F2BFDFE561E2,
	Purchase__ctor_m34D951230B978B4C10FDB0C2C18651025E490174,
	Purchasing_Initialize_mF31BF8A655AD42BCBC980084C34AE65AD5820B8E,
	Purchasing_InitiatePurchasingCommand_m5EE95E70493846EDEFBFFB7AA98054EEB289556E,
	Purchasing_GetPurchasingCatalog_m3A898FF6D24C8266C555248A52167AB555465261,
	Purchasing_GetPromoVersion_mCA88F7D25859BA3EA47A84DA0F9EC88124F5BF2E,
	Purchasing_SendEvent_m77680DD6BEA1CC4ED40B395D405AF5ADEE7F1621,
	Purchasing__cctor_mC83E752B881477E6EBCB91DEDF00FDED5FC1DED0,
	ReadyEventArgs_get_placementId_m9B1A1462F7348F396D3E5F3AE4F2266B510ABA20,
	ReadyEventArgs_set_placementId_mF13E66593646DDEC39EA34E95436A614832B71BD,
	ReadyEventArgs__ctor_mDA2E033EAFD4BD334418D1E351478F9513909809,
	ShowOptions_get_resultCallback_m884750654C53E4096E4F6EB00941DBA62282629E,
	ShowOptions_set_resultCallback_mCDCA3E88983BFC496AC7F999EAAA497A01A9CF38,
	ShowOptions_get_gamerSid_m6D82587F5284F893760EA140F52B0C540A35C2B7,
	ShowOptions_set_gamerSid_m281C98C7E79C7F11CA5C613F5D5DE8F35AB9B031,
	ShowOptions__ctor_mEF76BE2E330805AAD1E9928EE8EAB7DF0A6D5DD8,
	StartEventArgs_get_placementId_mC0CADA1095DAE9D9C54CDDB7AA056DA68F2BE5D9,
	StartEventArgs_set_placementId_m8AA1E6CADC23BB6EBC352803830BAAF00FF4C534,
	StartEventArgs__ctor_m9AD9C76375F5F73DC578B616FD65C2EC6A75E828,
	UnityEngineApplication_get_isEditor_m77B0FC2315FCBF3A4B246431CC16431926559F3D,
	UnityEngineApplication_get_platform_m3CE239D29E7C11040032758CDEF516E5A75A457F,
	UnityEngineApplication_get_unityVersion_m7CD2101869F6EFE909C625C0D4E61C508238E314,
	UnityEngineApplication__ctor_mAA67CE53FEFB214FBAA525A23DABBF63DD64341E,
	UnsupportedPlatform_add_OnReady_m77C119B20E788C743A004A3A7543AA1D8AF5C41C,
	UnsupportedPlatform_remove_OnReady_m71C7C9A6338F85D26C8E3E1DA0F3C843A2BA8CA9,
	UnsupportedPlatform_add_OnStart_m93A77F8C815BC41849585FA381880BD444E08766,
	UnsupportedPlatform_remove_OnStart_m878E6EBA1D4C4F76559FCF0495FE77A312C23023,
	UnsupportedPlatform_add_OnFinish_m5AAFA121BBF745D8AE848A225D0D6BF9ABA3B2BD,
	UnsupportedPlatform_remove_OnFinish_m610E0A8FF608B4AB6F0C0B8A4F511689B92A4611,
	UnsupportedPlatform_add_OnError_mCCE203879136D93C190696160E5981481B0CF55F,
	UnsupportedPlatform_remove_OnError_mA125760D97199C912369E0A0976155531A43E0CF,
	UnsupportedPlatform_get_isInitialized_m2290344D02FF5DA592CF103238B74D08EDA3F4D7,
	UnsupportedPlatform_get_isSupported_mF0887192C8D7021576615EB37096E8F927C19E33,
	UnsupportedPlatform_get_version_m2DD7B6B6ED7ECEE92CE28186F6E4CF066DFB493B,
	UnsupportedPlatform_get_debugMode_mFEEAF82E05A16A7C3DF6885421CF25C42695DF1C,
	UnsupportedPlatform_set_debugMode_m0C77C0A09EA19CB802A302BF3EB885EBCBFCF45D,
	UnsupportedPlatform_get_Banner_mBEA2BCB308CDF45A25641FCCF451CB3FA4D3F0EA,
	UnsupportedPlatform_Initialize_m0FA08937CBBACFED60962EBD1C921160FE27D326,
	UnsupportedPlatform_IsReady_mE18D2F51D03EDF5C7C6BCC34D2D845BF9FCD3745,
	UnsupportedPlatform_GetPlacementState_mBDF5C05354D5263F4430C2DE2309D806577C7C26,
	UnsupportedPlatform_Show_m51A5F6E4F5831CB127C168CB351C4A09D3C2AC8E,
	UnsupportedPlatform_SetMetaData_mBDF510F62784167760D14D58B106734F7A880927,
	UnsupportedPlatform__ctor_m1E88979183745EC1566C699759768D0D13B4F97B,
	UnsupportedPlatform__cctor_m2D281BFEA03BCB2E8DDE3B6F129B814C01C05A6F,
	NullBanner_add_OnShow_mF8E1D6092CD1F0E98F8A401B4D75925008A1AA03,
	NullBanner_remove_OnShow_m54020C9F4FC73465598470B756A2C925B63AAF4E,
	NullBanner_add_OnHide_m05C3974EFA5C23EFB046F2DBAB3D95487F77CEE3,
	NullBanner_remove_OnHide_mB24F0CEF8D554891D3E4315C240DD9F476EEAEC8,
	NullBanner_add_OnError_mCAE7031EE1E2C462B7578F2C5D083A76F39C5A32,
	NullBanner_remove_OnError_m190A1605D10F63EAFB96300E53805F81DB034941,
	NullBanner_add_OnUnload_m6A90F0D9F1E35CD9EF2E47D0BE48BCE5387104E0,
	NullBanner_remove_OnUnload_m4C997D76A65F34379E5A417F932DD7447B59B4BB,
	NullBanner_add_OnLoad_mA46D40D9AB564234E09A0F42EAE0EDFBA43E24D3,
	NullBanner_remove_OnLoad_mC4272FA0B08ABCABF0A24AE7E7C2A5B66A2BC0B3,
	NullBanner_get_isLoaded_mE93DC3BD1B4F591188B0B402A7A4DDF7A96E8FC1,
	NullBanner_Load_m7FAD762EF450871C167F81DE79C7CDDFC7D4D2B2,
	NullBanner_Hide_m397D91F7B806AD2E0434DEED14BF33306F6BC807,
	NullBanner_Show_m343E800435D5FEE266F3F1A1A304539F0C5B9882,
	NullBanner_SetPosition_m79FA0E49CA3B7E5F00975D9FF640D60D2C9B38B9,
	NullBanner__ctor_m0FA42D9F270CA7800AE3370D74D510A4B2E1BECE,
	Json_Deserialize_m4A530DF137AD6A50B1E7B9E21F5278AD7E12A56D,
	Json_Serialize_m86A12F4404AD4A11BDE9A1FA98ED21C3FDC06851,
	Parser_IsWordBreak_m1817F82992F8304E4FBCB72B51EC3BE9A64D794F,
	Parser__ctor_mDBBFF1D039E039AB00193D9B155255B06C020869,
	Parser_Parse_m89E9A5582D688091E09764848A861E8694516D40,
	Parser_Dispose_mD785A7EDE0F720417092F602677F0FCBC40CDE7F,
	Parser_ParseObject_m737536AE424E9B309DC2B062077AF2904DE52D0D,
	Parser_ParseArray_mCE36C2B63262664B0F9C0838D01C271B1A425899,
	Parser_ParseValue_m53686417FAA31D016D9FB178EDF56C0B50F740AC,
	Parser_ParseByToken_m529FD80061C6E1A3ED1470F6A3590A7167B28601,
	Parser_ParseString_m741791EAF3B6CAF64010A16121B1DF7A8F948E36,
	Parser_ParseNumber_m50E089D05873A3FA4C5FB22B4CF2BB0763DD4758,
	Parser_EatWhitespace_m694EFED3DF28C103A9EADEB24C460A670C51835A,
	Parser_get_PeekChar_m60107656CEDC411EF966B55F1BCDC0C827B07EE9,
	Parser_get_NextChar_mFE425E6767A33DDE3599510783066FB925597502,
	Parser_get_NextWord_mC383B30576FEC3E5A0A690983D8C4BEA6492914B,
	Parser_get_NextToken_m1420F3984B6C59DB6BA260D419BB53805C820B6A,
	Serializer__ctor_mCF1A2F42C40273B37CBAD9E7A2AE250E9C297380,
	Serializer_Serialize_m75CF66178A0E0144742255D0089277E9C8AAFC74,
	Serializer_SerializeValue_m0994AAA71A5456A887085333CE1959731381233C,
	Serializer_SerializeObject_mBEF0C6D8C40A6D0F16D396E77AB0AA3A306657EB,
	Serializer_SerializeArray_m2BC22F2D169FD1B44E02DBEEA137593ACEE64032,
	Serializer_SerializeString_mC3EA03CC43A5C4D4EAD70F12B9CFA8589C5793A3,
	Serializer_SerializeOther_m3C3268660DDA90593E5689D647C5772D2257179E,
};
static const int32_t s_InvokerIndices[315] = 
{
	2024,
	2001,
	2024,
	2001,
	2030,
	2003,
	2030,
	2030,
	2003,
	2024,
	2030,
	2003,
	2038,
	2001,
	1854,
	2030,
	1966,
	2019,
	1910,
	2038,
	2001,
	2001,
	1851,
	2001,
	2038,
	2001,
	2001,
	1851,
	2038,
	2001,
	2001,
	1851,
	2003,
	1999,
	2030,
	1193,
	1193,
	669,
	1193,
	669,
	1193,
	669,
	669,
	2038,
	1193,
	669,
	2038,
	1193,
	669,
	669,
	1193,
	669,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	669,
	1181,
	1023,
	1039,
	1023,
	1014,
	1023,
	1023,
	669,
	1023,
	1023,
	1023,
	1193,
	1193,
	1193,
	1023,
	1193,
	1023,
	1193,
	1023,
	1193,
	1023,
	1193,
	1023,
	1193,
	1023,
	1162,
	1023,
	1162,
	1023,
	1193,
	668,
	1193,
	511,
	1023,
	1162,
	1023,
	1162,
	1023,
	1193,
	668,
	1193,
	511,
	1023,
	668,
	1023,
	341,
	1023,
	1023,
	1193,
	1193,
	1181,
	1039,
	1162,
	1023,
	1162,
	1023,
	1023,
	2030,
	2030,
	2030,
	2024,
	2024,
	2038,
	1152,
	1015,
	1162,
	1023,
	1023,
	656,
	1162,
	1023,
	1151,
	1014,
	666,
	1162,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1181,
	1023,
	1023,
	1039,
	1014,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1162,
	1181,
	1181,
	1162,
	1181,
	1039,
	671,
	906,
	775,
	1023,
	1023,
	1023,
	1181,
	1151,
	1162,
	1162,
	1023,
	1023,
	669,
	823,
	1162,
	1162,
	1023,
	1023,
	1193,
	671,
	1193,
	1193,
	1014,
	1193,
	1023,
	1023,
	669,
	669,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1193,
	1181,
	1181,
	1162,
	1181,
	1039,
	1162,
	671,
	906,
	775,
	1023,
	1023,
	1023,
	1193,
	1023,
	1193,
	1023,
	1193,
	1023,
	1193,
	1023,
	1023,
	1193,
	1193,
	1193,
	1023,
	1023,
	1193,
	1966,
	1966,
	2024,
	2024,
	1966,
	2038,
	1162,
	1023,
	1023,
	1162,
	1023,
	1162,
	1023,
	1193,
	1162,
	1023,
	1023,
	1181,
	1151,
	1162,
	1193,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1181,
	1181,
	1162,
	1181,
	1039,
	1162,
	671,
	906,
	775,
	1023,
	1023,
	1193,
	2038,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1023,
	1181,
	1023,
	1039,
	1023,
	1014,
	1193,
	1945,
	1945,
	1962,
	1023,
	1945,
	1193,
	1162,
	1162,
	1162,
	820,
	1162,
	1162,
	1193,
	1150,
	1150,
	1162,
	1151,
	1193,
	1945,
	1023,
	1023,
	1023,
	1023,
	1023,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_Advertisements_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule = 
{
	"UnityEngine.Advertisements.dll",
	315,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_Advertisements_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
