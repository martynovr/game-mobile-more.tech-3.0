﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Inventory::mDebug(System.String)
extern void Inventory_mDebug_mB459EE7ED35047B6EE7C811851AAF53AD118BD86 (void);
// 0x00000002 System.Void Inventory::SelectItem(Inventory/C_ItemAmount)
extern void Inventory_SelectItem_m7F857BF0258E93A959E557A3502CCF66E16E6ABB (void);
// 0x00000003 System.Void Inventory::TryToCloseItemDetailPanel(InventoryItem)
extern void Inventory_TryToCloseItemDetailPanel_m8BCBD521DF7890417A4E40B43A412E88F11F216B (void);
// 0x00000004 System.Void Inventory::CreateNewDetailViewPanel(Inventory/C_ItemAmount)
extern void Inventory_CreateNewDetailViewPanel_m1CFC060EE5E03C2FD8F25B4977AFBC8AAECC2695 (void);
// 0x00000005 System.Void Inventory::CreateNewUpdateViewPanel(Inventory/C_ItemAmount)
extern void Inventory_CreateNewUpdateViewPanel_mC77C9D7E3BCCE7960CCB00D3F5C464CE880A05F5 (void);
// 0x00000006 System.Collections.IEnumerator Inventory::ShowNewItems()
extern void Inventory_ShowNewItems_m730E58CD1F058A1050B9702ADAA94EBBEE955155 (void);
// 0x00000007 System.Void Inventory::Awake()
extern void Inventory_Awake_m2040A4C75E81B9580543F04EFA2364F3F1B9EEE0 (void);
// 0x00000008 System.Void Inventory::Start()
extern void Inventory_Start_mD4A36B4F39D7CDE6707542A799D6EF9D88756155 (void);
// 0x00000009 System.Collections.IEnumerator Inventory::delayedActualize()
extern void Inventory_delayedActualize_mD1E356C8C8C2EE201B75C5724CA238FDD552BD2D (void);
// 0x0000000A Inventory/C_ItemAmount Inventory::GetItemByKey(System.String)
extern void Inventory_GetItemByKey_m675DFD8C6621C9F28256FDFA7C87CBB6A8D7986B (void);
// 0x0000000B InventoryItem Inventory::GetItemFromCatalogByKey(System.String)
extern void Inventory_GetItemFromCatalogByKey_m8DC60D78B6FECE05CE3910AC8C2A488238DC6E47 (void);
// 0x0000000C System.Void Inventory::RemoveItem(System.String)
extern void Inventory_RemoveItem_mB3AF8F32E3759B3D5623849E6371493608A4808D (void);
// 0x0000000D System.Void Inventory::DeleteInventory()
extern void Inventory_DeleteInventory_mFBC0065645CC6768664B5E928E93FC7867C315DC (void);
// 0x0000000E System.Void Inventory::AddItemAmount(InventoryItem,System.Int32)
extern void Inventory_AddItemAmount_m86EDEFDFD3A1AA7CDC4C2A7A583C185CB1B3F53F (void);
// 0x0000000F System.Void Inventory::SetItemAmount(InventoryItem,System.Int32)
extern void Inventory_SetItemAmount_mF77009EF8BDA39B83AC1D8A58B3561967557B68B (void);
// 0x00000010 System.Void Inventory::GenerateItemEvent(InventoryItem,Inventory/E_ItemEventType)
extern void Inventory_GenerateItemEvent_mDB06AEB6B1F923E9A2E8BAF7CD0A5B68540DBBB9 (void);
// 0x00000011 System.Void Inventory::GenerateCategoryEvent(ItemDefinitions/E_ItemCategory,Inventory/E_ItemEventType)
extern void Inventory_GenerateCategoryEvent_m687B0C581B1E76E65B3EA77ED20020FFD48AD435 (void);
// 0x00000012 System.Void Inventory::ConsumeItem(InventoryItem)
extern void Inventory_ConsumeItem_mC2DEAAD748709E2EFC4888117EC8AA0F8709ECD0 (void);
// 0x00000013 System.Int32 Inventory::GetItemCount(InventoryItem)
extern void Inventory_GetItemCount_m30C9151653ED4990DD226955CAF69B3BEB525CD7 (void);
// 0x00000014 System.Int32 Inventory::GetItemCount(System.String)
extern void Inventory_GetItemCount_m9620EFA9D81413179C909868F13F6F63570A793C (void);
// 0x00000015 System.Void Inventory::inventoryChanged(System.Boolean)
extern void Inventory_inventoryChanged_mAE9B9C2A62DE22D806581AB21A7131F2E23367F5 (void);
// 0x00000016 System.Void Inventory::load()
extern void Inventory_load_m9DAB414CA54DA79E931AD4C34338B75351BBF43C (void);
// 0x00000017 System.Void Inventory::loadCatalog()
extern void Inventory_loadCatalog_m310FA7C9DBD1DB96AA9DF94A0C7DA69C61F239B3 (void);
// 0x00000018 System.Void Inventory::save()
extern void Inventory_save_m45FD5249D7EEA5B3D11276E812A4787C98B43463 (void);
// 0x00000019 System.Collections.Generic.List`1<System.String> Inventory::getTranslatableTerms()
extern void Inventory_getTranslatableTerms_mFF4B3CF2611E3FFF4A07C2905B37BC74B1305CF2 (void);
// 0x0000001A System.Void Inventory::.ctor()
extern void Inventory__ctor_mF2ACBF005FF40F23F68AE8E9E416A4870EC4B27C (void);
// 0x0000001B System.Void Inventory/mEvent::.ctor()
extern void mEvent__ctor_m96011D90D4F03173045F7BF890276D6A1F5EB216 (void);
// 0x0000001C System.Void Inventory/C_ItemDetail::.ctor()
extern void C_ItemDetail__ctor_m6C0817C48A16580E034F6D13B332D1588C39848E (void);
// 0x0000001D System.Void Inventory/C_ItemUpdateInformation::.ctor()
extern void C_ItemUpdateInformation__ctor_mC7F0FDBA5A9EBA8F3C3C063CCD16544A21DF8A0E (void);
// 0x0000001E System.Void Inventory/C_ItemEvent::.ctor()
extern void C_ItemEvent__ctor_mF06EBE26C12B781CF55D622B5089E6642624CEEF (void);
// 0x0000001F System.Void Inventory/C_CategoryEvent::.ctor()
extern void C_CategoryEvent__ctor_m67F9EB2C610B7D3E3635E2F91A408BDA40CB0E7A (void);
// 0x00000020 System.Void Inventory/C_ItemAmount::.ctor()
extern void C_ItemAmount__ctor_m333DA3DC423474D3202CA00283716AE24B535BA7 (void);
// 0x00000021 System.Void Inventory/C_Inventory::.ctor()
extern void C_Inventory__ctor_m70F386B20AAF70984FE851EA975B68D3B07497F4 (void);
// 0x00000022 System.Void Inventory/<ShowNewItems>d__11::.ctor(System.Int32)
extern void U3CShowNewItemsU3Ed__11__ctor_m4F93C52A740A6C441BE1B043BEDD162235DC2E30 (void);
// 0x00000023 System.Void Inventory/<ShowNewItems>d__11::System.IDisposable.Dispose()
extern void U3CShowNewItemsU3Ed__11_System_IDisposable_Dispose_m6A49C06D643DC646E3C55A44E5EE1AF4F627CB77 (void);
// 0x00000024 System.Boolean Inventory/<ShowNewItems>d__11::MoveNext()
extern void U3CShowNewItemsU3Ed__11_MoveNext_m8A7D2618753296EFBBB1B1266B958A29D1572F0A (void);
// 0x00000025 System.Void Inventory/<ShowNewItems>d__11::<>m__Finally1()
extern void U3CShowNewItemsU3Ed__11_U3CU3Em__Finally1_m32E5D89A0455F0FF9B70ED3FDBC826328A8AA501 (void);
// 0x00000026 System.Object Inventory/<ShowNewItems>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowNewItemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6967EF8B284149E3D86878CF77A0D80B53DD0B64 (void);
// 0x00000027 System.Void Inventory/<ShowNewItems>d__11::System.Collections.IEnumerator.Reset()
extern void U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_Reset_mFA50BB0229FBC728D2870928863FC9ABAC51774B (void);
// 0x00000028 System.Object Inventory/<ShowNewItems>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_get_Current_m76B1459E38683876F202AA8467A1C975E8068998 (void);
// 0x00000029 System.Void Inventory/<delayedActualize>d__18::.ctor(System.Int32)
extern void U3CdelayedActualizeU3Ed__18__ctor_mDF4DDB44936F798F1832FF0E64A6FA3A9A668481 (void);
// 0x0000002A System.Void Inventory/<delayedActualize>d__18::System.IDisposable.Dispose()
extern void U3CdelayedActualizeU3Ed__18_System_IDisposable_Dispose_mA51C00F967C3090D966C0D77B56F0B40453EE909 (void);
// 0x0000002B System.Boolean Inventory/<delayedActualize>d__18::MoveNext()
extern void U3CdelayedActualizeU3Ed__18_MoveNext_mA911D479758C0C43D8940E619521E6E136611347 (void);
// 0x0000002C System.Object Inventory/<delayedActualize>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayedActualizeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m271AEFDD2D8343B07E406091F9C94AFB84B0F4FC (void);
// 0x0000002D System.Void Inventory/<delayedActualize>d__18::System.Collections.IEnumerator.Reset()
extern void U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_Reset_mDEAD3BE3EAC90C8504A165A30341EC72AAF597BC (void);
// 0x0000002E System.Object Inventory/<delayedActualize>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_get_Current_m7B2A880A7C4382F85173E90E349433A1DBB661D1 (void);
// 0x0000002F System.Void InventoryItem::.ctor()
extern void InventoryItem__ctor_mDA2BEAE4ED6F565E6B7762D8A33C6D12BB7FD133 (void);
// 0x00000030 System.Void InventoryItem/C_ItemColors::.ctor()
extern void C_ItemColors__ctor_m644BBFCB015D4FB60529A98076371780A3741F91 (void);
// 0x00000031 System.Void Inventory_ChangeItem::executeItemChange(Inventory_ChangeItem/itemModifier)
extern void Inventory_ChangeItem_executeItemChange_m079B4C7C3D4F105A80552E9434950A2E210F3B5D (void);
// 0x00000032 System.Void Inventory_ChangeItem::AddItems()
extern void Inventory_ChangeItem_AddItems_m8FF60DA3F6FC5FEB46D74FEA4AB0EB50F63E04A0 (void);
// 0x00000033 System.Void Inventory_ChangeItem::.ctor()
extern void Inventory_ChangeItem__ctor_mA476F083EACA7B34E60D87B3200A519321CEE7D8 (void);
// 0x00000034 System.Void Inventory_ChangeItem/itemModifier::.ctor()
extern void itemModifier__ctor_mD83C5B04A7C979683D32D1715490D9F5DC334549 (void);
// 0x00000035 System.Void Inventory_UIItem::SetItem(Inventory/C_ItemAmount)
extern void Inventory_UIItem_SetItem_mC618F66DF12DEDA8C537BE014A2CAE49349E5B56 (void);
// 0x00000036 System.Void Inventory_UIItem::ActualizeAfterConsume()
extern void Inventory_UIItem_ActualizeAfterConsume_m5F32AC4C595891151C4559BEA2176E5CF1B1D554 (void);
// 0x00000037 System.Void Inventory_UIItem::ConsumeItem()
extern void Inventory_UIItem_ConsumeItem_m60C2AEC5BB9F9EEBC40C16018B5512285A6DCD51 (void);
// 0x00000038 System.Void Inventory_UIItem::SelectItem()
extern void Inventory_UIItem_SelectItem_mA2FCFC8E9AF9E5DE08E204D0759E0800918420C1 (void);
// 0x00000039 System.Void Inventory_UIItem::.ctor()
extern void Inventory_UIItem__ctor_mA4A0076AE2C2E526318EC428481163590B876431 (void);
// 0x0000003A System.Void Inventory_UIItemList::getRequiredInstances()
extern void Inventory_UIItemList_getRequiredInstances_m744AEE8F25AB3ACF59411CB6422B2B8311496EAA (void);
// 0x0000003B System.Void Inventory_UIItemList::Start()
extern void Inventory_UIItemList_Start_mCC4F36CEC4BC36B5EDF97E193774487957202A58 (void);
// 0x0000003C System.Void Inventory_UIItemList::OnInventoryChanged()
extern void Inventory_UIItemList_OnInventoryChanged_m67AC049CCD1881D1EAE9ECA0FDD9B7AB7A3ED59E (void);
// 0x0000003D System.Void Inventory_UIItemList::OnEnable()
extern void Inventory_UIItemList_OnEnable_m033FA8C04C0ABA2C0091B958707190945A5A1AB4 (void);
// 0x0000003E System.Collections.IEnumerator Inventory_UIItemList::delayedInit()
extern void Inventory_UIItemList_delayedInit_m3EAD041EF763A6EA538C188CA2DD32339660B976 (void);
// 0x0000003F System.Void Inventory_UIItemList::OnDestroy()
extern void Inventory_UIItemList_OnDestroy_mA5E1413BAE7DE8FD3668CCFA65D7BDFB9580B8A4 (void);
// 0x00000040 System.Boolean Inventory_UIItemList::GetShowItem(InventoryItem)
extern void Inventory_UIItemList_GetShowItem_m61FC1ACD1DFA11EC8CD20D19C65B1D2B49EE9FE2 (void);
// 0x00000041 System.Void Inventory_UIItemList::ActualizeUIList()
extern void Inventory_UIItemList_ActualizeUIList_m027F3C56B680D93A3D442C894878257F2B730991 (void);
// 0x00000042 System.Void Inventory_UIItemList::.ctor()
extern void Inventory_UIItemList__ctor_m5A596042B9924EA2C5E01F7F2E99C8A7903AFC9A (void);
// 0x00000043 System.Void Inventory_UIItemList/<delayedInit>d__10::.ctor(System.Int32)
extern void U3CdelayedInitU3Ed__10__ctor_m5157CE64BFCC04D43B1A35144363AF0F3182CC60 (void);
// 0x00000044 System.Void Inventory_UIItemList/<delayedInit>d__10::System.IDisposable.Dispose()
extern void U3CdelayedInitU3Ed__10_System_IDisposable_Dispose_mFA7F9DEEF57B724FD1133C7AA67D24BBE2D5B8D2 (void);
// 0x00000045 System.Boolean Inventory_UIItemList/<delayedInit>d__10::MoveNext()
extern void U3CdelayedInitU3Ed__10_MoveNext_mEA81EF9C62EF9C16B075D8759BDC785B338FDB94 (void);
// 0x00000046 System.Object Inventory_UIItemList/<delayedInit>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayedInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45AD32E3413F826B1340FE94180B993E9A7C9609 (void);
// 0x00000047 System.Void Inventory_UIItemList/<delayedInit>d__10::System.Collections.IEnumerator.Reset()
extern void U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_Reset_mF78B2D14D8B84D2B048230AB5BA8BC2256EAB1F6 (void);
// 0x00000048 System.Object Inventory_UIItemList/<delayedInit>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_get_Current_m723D3863BA16237C6A75150DA0E6000F8445C5C6 (void);
// 0x00000049 System.Void ItemDefinitions::.ctor()
extern void ItemDefinitions__ctor_m5FA26D8A6F36EAF891401FAE127C1E85CF053B95 (void);
// 0x0000004A System.Void QuestDefinition::.ctor()
extern void QuestDefinition__ctor_mD29C86BAF631ED37C09C2C0974AC1A8DF9B812C5 (void);
// 0x0000004B System.Void QuestDefinition/C_QuestResults::.ctor()
extern void C_QuestResults__ctor_m42A0494CC04BA0E5AF923DFF8810E07BDEE86AAD (void);
// 0x0000004C System.Void Quest_UIItem::SetQuestState(Quests/C_QuestState)
extern void Quest_UIItem_SetQuestState_m5381289366014461607291FF846AEC9972CD64A2 (void);
// 0x0000004D System.Void Quest_UIItem::.ctor()
extern void Quest_UIItem__ctor_m12BC0DAACAAC5F06E11E2BFC7B8D722DD86DDE67 (void);
// 0x0000004E System.Void Quest_UIList::Start()
extern void Quest_UIList_Start_m58AB83628D6F049E25066444E9DD2F586BC2FD6E (void);
// 0x0000004F System.Void Quest_UIList::OnQuestboockChanged()
extern void Quest_UIList_OnQuestboockChanged_mE9BEBD1499D4FE6A3F1FAB1D38CB00AF7A264260 (void);
// 0x00000050 System.Void Quest_UIList::OnDestroy()
extern void Quest_UIList_OnDestroy_m0D8ADCA53DC6B0C706C7BE3504AA88E6A78A1558 (void);
// 0x00000051 System.Void Quest_UIList::ActualizeUIList()
extern void Quest_UIList_ActualizeUIList_m466CE47BE35CA0B0A2FCDC10068EBF88F978E181 (void);
// 0x00000052 System.Void Quest_UIList::actualizeQuestNumberDisplay()
extern void Quest_UIList_actualizeQuestNumberDisplay_m2EA387E42844B0A820054A2C393FECBDB457B9AD (void);
// 0x00000053 UnityEngine.GameObject Quest_UIList::CreateQuestDisplay(Quests/C_QuestState)
extern void Quest_UIList_CreateQuestDisplay_m4279B867011EB22FB3380D2250D1E4EEAB57CBAC (void);
// 0x00000054 System.Void Quest_UIList::.ctor()
extern void Quest_UIList__ctor_mC91318BB9AD97C11A834281722210B40321F0593 (void);
// 0x00000055 System.Void Quest_UIList/mEvent::.ctor()
extern void mEvent__ctor_m201A1FB221CC521F461875448EBF68BC64FB8EE7 (void);
// 0x00000056 System.Void Quest_UIList/C_QuestNumberDisplay::.ctor()
extern void C_QuestNumberDisplay__ctor_m45EDC71C4F121D159353EC5F03F060B4DF553748 (void);
// 0x00000057 System.Void Quests::mDebug(System.String)
extern void Quests_mDebug_m0E0FBD19C188DEE8557175A71AFF25084C1DF972 (void);
// 0x00000058 System.Void Quests::Awake()
extern void Quests_Awake_m1EE1168AC0C78FFB07C9209C75A87F4BC707C523 (void);
// 0x00000059 System.Void Quests::Start()
extern void Quests_Start_m6821E778D54291BF979BFA573CEA2CADB5DBF215 (void);
// 0x0000005A System.Void Quests::OnCardDestroy()
extern void Quests_OnCardDestroy_mC95073F03854E1CAD48C600543604B67B7585CFF (void);
// 0x0000005B System.Void Quests::computeQuestChange(Quests/C_QuestChange)
extern void Quests_computeQuestChange_m3C6E6BBD4D4D6320FE9C49C102A19FE89398CC91 (void);
// 0x0000005C System.Int32 Quests::GetNrOfActiveQuests()
extern void Quests_GetNrOfActiveQuests_m07298D72D01020E69C023564576F6FCFB89E34D1 (void);
// 0x0000005D System.Int32 Quests::GetNrOfAvailableQuests()
extern void Quests_GetNrOfAvailableQuests_m7BE9091B444B80FF498829AE641BCDE9D31E7E21 (void);
// 0x0000005E System.Int32 Quests::GetNrOfFinishedQuests()
extern void Quests_GetNrOfFinishedQuests_m0EADC4BF2D6BB9ED89077290076489736E7C848D (void);
// 0x0000005F System.Void Quests::ResetQuestbook()
extern void Quests_ResetQuestbook_m774AE05027D979312B3742AB5EBDDD7182ACCC0B (void);
// 0x00000060 System.Boolean Quests::IsQuestInQuestbook(QuestDefinition)
extern void Quests_IsQuestInQuestbook_m641F390B3811C4F4243CB33B356CE7742BFE3E8A (void);
// 0x00000061 System.Void Quests::fullfill_quest_if_active(QuestDefinition)
extern void Quests_fullfill_quest_if_active_mE2504300E562BCD182123C085240109CCDF7138C (void);
// 0x00000062 System.Void Quests::createQuestFinishedPopup(Quests/C_QuestState)
extern void Quests_createQuestFinishedPopup_m36903E6F9325683A6878C2D48BE2D4CED56F7EEF (void);
// 0x00000063 System.Void Quests::checkIfActiveQuestIsFullfilledByCondition()
extern void Quests_checkIfActiveQuestIsFullfilledByCondition_m924F6F19DA3C9A579FC8A698D26EC4514E0E9F87 (void);
// 0x00000064 System.Void Quests::FillActiveQuests()
extern void Quests_FillActiveQuests_m6A8DC10220149234046E58669F5F13894DFA4324 (void);
// 0x00000065 System.Void Quests::ReselectActiveQuests()
extern void Quests_ReselectActiveQuests_m83DC48861C188643DE0F8D414A4E75C26034F586 (void);
// 0x00000066 System.Void Quests::AbortActiveQuests()
extern void Quests_AbortActiveQuests_m0FBC8F3208EB5387CC30FC702D41B6956D706A93 (void);
// 0x00000067 System.Void Quests::FullfillActiveQuests()
extern void Quests_FullfillActiveQuests_mCC9BAFB07180E39A64D66224F49B92B3851962A9 (void);
// 0x00000068 System.Void Quests::SetRandomQuestActive()
extern void Quests_SetRandomQuestActive_mC5E1B06751FDE9894ABEB46A25C6370291153480 (void);
// 0x00000069 System.Int32 Quests::SetRandomQuestActiveGetCandidateCount()
extern void Quests_SetRandomQuestActiveGetCandidateCount_m934BC993B28E4E79E7D5BA6C700A376916FDD7B0 (void);
// 0x0000006A System.Void Quests::actualizeQuestBookInfo()
extern void Quests_actualizeQuestBookInfo_m77C9DDA920F3C39E1270E1F7602E02A2B58A4658 (void);
// 0x0000006B System.Collections.IEnumerator Quests::delayedActualize()
extern void Quests_delayedActualize_m205899FC8FF90F4409663E809FCD691833237E74 (void);
// 0x0000006C Quests/C_QuestState Quests::GetQuestStateByKey(System.String)
extern void Quests_GetQuestStateByKey_mF53F5717D7D1A2A5C8E450C65FF7D93557797611 (void);
// 0x0000006D QuestDefinition Quests::GetQuestFromCatalogByKey(System.String)
extern void Quests_GetQuestFromCatalogByKey_m1AA58285B41108DBEF5FCB9E63DA283674570739 (void);
// 0x0000006E System.Void Quests::DeleteQuestbook()
extern void Quests_DeleteQuestbook_mE9E30D5AC48B6F7C2CF38CF7778B40221AB47155 (void);
// 0x0000006F System.Void Quests::GenerateQuestEvent(QuestDefinition,Quests/E_QuestEventType)
extern void Quests_GenerateQuestEvent_mB0508A6992675208BCF26D4C3D7C6F9B8AB51793 (void);
// 0x00000070 System.Void Quests::questsChanged(System.Boolean)
extern void Quests_questsChanged_m737691FBBB75B5748B762CEAD6D27D6D2645898D (void);
// 0x00000071 System.Void Quests::TryAutomaticRefill()
extern void Quests_TryAutomaticRefill_mBF9F9D01C21281E1AA6B5D538C5D42017BC5AEB0 (void);
// 0x00000072 System.Void Quests::load()
extern void Quests_load_m0DA612653D64ADBD3524D2D199B084CD1766F750 (void);
// 0x00000073 System.Void Quests::loadCatalog()
extern void Quests_loadCatalog_mC9B9593953FE57FF34A3D84B6F657751C4BACCFA (void);
// 0x00000074 System.Void Quests::save()
extern void Quests_save_mAA8D547A2A41A9E84CCCD0592B0BE0B0173439A8 (void);
// 0x00000075 System.Collections.Generic.List`1<System.String> Quests::getTranslatableTerms()
extern void Quests_getTranslatableTerms_mF85B6305C3FC21EA9F6EE92E0644E9021F517349 (void);
// 0x00000076 System.Void Quests::.ctor()
extern void Quests__ctor_m5549AEA4E1E6CC72F1E650768DF8C561F5547CBB (void);
// 0x00000077 System.Void Quests/mEvent::.ctor()
extern void mEvent__ctor_m01760A3AEEE7F7F47EE6A946894918C427B39B9D (void);
// 0x00000078 System.Void Quests/C_QuestFullfillPopup::.ctor()
extern void C_QuestFullfillPopup__ctor_mF214D4C5187AD42D70740DBEA03B46660139ABE2 (void);
// 0x00000079 System.Void Quests/C_QuestChange::.ctor()
extern void C_QuestChange__ctor_mC606C66549C0B1D8C528788A108BE8749480EED0 (void);
// 0x0000007A System.Void Quests/C_QuestEvent::.ctor()
extern void C_QuestEvent__ctor_mE23D93293D735732F83656006BE7C754D3CFE860 (void);
// 0x0000007B System.Void Quests/C_QuestState::Reset()
extern void C_QuestState_Reset_m45EC6B50AD265D77931BD364FA8F6CFAE5CBCB7E (void);
// 0x0000007C System.Void Quests/C_QuestState::.ctor()
extern void C_QuestState__ctor_mA6AA798770F5913C533F8810213F904C3F0A2809 (void);
// 0x0000007D System.Void Quests/C_Quests::.ctor()
extern void C_Quests__ctor_m04A137DE4D70A3261066178F21102A59D90CC04D (void);
// 0x0000007E System.Void Quests/<delayedActualize>d__32::.ctor(System.Int32)
extern void U3CdelayedActualizeU3Ed__32__ctor_mAEFBCD23F8BEF17E5BD56CC457FE495405C75A4F (void);
// 0x0000007F System.Void Quests/<delayedActualize>d__32::System.IDisposable.Dispose()
extern void U3CdelayedActualizeU3Ed__32_System_IDisposable_Dispose_mD2D3C71CF0DA26B65705FD1234759FDCC6CEA2A8 (void);
// 0x00000080 System.Boolean Quests/<delayedActualize>d__32::MoveNext()
extern void U3CdelayedActualizeU3Ed__32_MoveNext_mA0DB6B6C3BA509BFE2E6A0E4ED4F9418B8383FF5 (void);
// 0x00000081 System.Object Quests/<delayedActualize>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayedActualizeU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2D37E0C9702B8712E096D957CB813D1339A5FCE (void);
// 0x00000082 System.Void Quests/<delayedActualize>d__32::System.Collections.IEnumerator.Reset()
extern void U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_Reset_m57DF855CC498C59B7FB9D904D8CE6B56226F810A (void);
// 0x00000083 System.Object Quests/<delayedActualize>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_get_Current_m0FC81D0D966BCA6016E759F0CD7827D2316AABF7 (void);
// 0x00000084 System.Void HistoryEvent::.ctor()
extern void HistoryEvent__ctor_m360A306BF5E1F5E2204584FD186B8DC340C89AD5 (void);
// 0x00000085 System.Void Timeline::mDebug(System.String)
extern void Timeline_mDebug_mE7E17D100E0CB09E9185FD12A1EFFE94A9DE6B22 (void);
// 0x00000086 System.Void Timeline::Awake()
extern void Timeline_Awake_m92D1116FA6DC0C03C5CED6E79870A352C6FDB4A5 (void);
// 0x00000087 System.Void Timeline::Start()
extern void Timeline_Start_mEAF5A39D1DBBC9428E5A351E7EC21B6B73DC96AA (void);
// 0x00000088 System.Collections.IEnumerator Timeline::delayedActualize()
extern void Timeline_delayedActualize_mBDB9533D5CF13304BE38412E707BC60F6677060C (void);
// 0x00000089 System.Void Timeline::Update()
extern void Timeline_Update_m4BD4F9098A6B737B0D5D4E0CA2A9E99845BB2E11 (void);
// 0x0000008A System.Int32 Timeline::getMaxYear()
extern void Timeline_getMaxYear_m1B613C7019EF5F919730ED7C46682458C676AE06 (void);
// 0x0000008B HistoryEvent Timeline::GetEventFromCatalogByKey(System.String)
extern void Timeline_GetEventFromCatalogByKey_m287779FC0870FA360A1E0566DF0571A8FA1CE141 (void);
// 0x0000008C System.Void Timeline::ResetHistoryData()
extern void Timeline_ResetHistoryData_mEE5496AD06C05EC3A16FA8597AB69CB6759644A6 (void);
// 0x0000008D System.Void Timeline::CreateNewFollowingHistory()
extern void Timeline_CreateNewFollowingHistory_m7A60504A742E3D32D70D99317D9376ADEA2ACF7D (void);
// 0x0000008E System.Void Timeline::AddHistoryEvent(HistoryEvent,System.String)
extern void Timeline_AddHistoryEvent_m21E6D971D351E1ACA9DDB75D6E76B54C8CD7F31A (void);
// 0x0000008F System.Void Timeline::AddHistoryEvent(Timeline/C_TimelLineEventChange)
extern void Timeline_AddHistoryEvent_mDD02AF1384EC1F88F1879FC1518C3654E827306D (void);
// 0x00000090 System.Int32 Timeline::GetHistoryEventsCount(HistoryEvent)
extern void Timeline_GetHistoryEventsCount_mB56235C010BD541E6C1B0C8E3439671AB3428C85 (void);
// 0x00000091 System.Int32 Timeline::GetHistoryEventsCount()
extern void Timeline_GetHistoryEventsCount_m1739DDE01FF0CCEA6672688CA14E72A188ABDB02 (void);
// 0x00000092 System.Void Timeline::timelineChanged()
extern void Timeline_timelineChanged_mC1F3A7845B3CA28BE6EE841BB680AB09A8D221A6 (void);
// 0x00000093 Timeline/C_TimelineData Timeline::GetEventOfYear(System.Int32)
extern void Timeline_GetEventOfYear_m0EA8F5C59A714DCF0FCD04F68FB39801E93DD6E9 (void);
// 0x00000094 System.Void Timeline::load()
extern void Timeline_load_m6B33C7E0E8687CCE74190F53390F7E65EC982455 (void);
// 0x00000095 System.Void Timeline::createDisplayedHistory()
extern void Timeline_createDisplayedHistory_mA00D5649F5DA36FF10450AB90CDEBF5ADBD9B5AF (void);
// 0x00000096 System.Void Timeline::expandDisplayedHistory(Timeline/C_TimelineData)
extern void Timeline_expandDisplayedHistory_mC63710805FD3ABAFD31D2957B5B3BFD0E2F264BD (void);
// 0x00000097 System.Void Timeline::loadCatalog()
extern void Timeline_loadCatalog_m5F3EB4949964A86CC43AAB508012DB706537E595 (void);
// 0x00000098 System.Void Timeline::save()
extern void Timeline_save_mDEECAE98921B7A87F17167879AF23412EE8E0EA5 (void);
// 0x00000099 System.Collections.Generic.List`1<System.String> Timeline::getTranslatableTerms()
extern void Timeline_getTranslatableTerms_m14CD1F1EE9E34116AEEE1DD59666A25270E3D62A (void);
// 0x0000009A System.Void Timeline::.ctor()
extern void Timeline__ctor_m7EEA084D66266DA016EB4594AEEE0FF65C5F8EC2 (void);
// 0x0000009B System.Void Timeline/mEvent::.ctor()
extern void mEvent__ctor_m5FDC1C42153FD9537BD7797C7CC713F9B6440360 (void);
// 0x0000009C System.Void Timeline/C_TimelineData::.ctor()
extern void C_TimelineData__ctor_mA7C65FECA924BC5AB250DC92C360FB1F8B038051 (void);
// 0x0000009D System.Void Timeline/C_History::.ctor()
extern void C_History__ctor_m9798D9B13F648C80C92774925306325BECC846F8 (void);
// 0x0000009E System.Collections.Generic.List`1<System.String> Timeline/C_TimelLineEventChange::GetTranslatableContent()
extern void C_TimelLineEventChange_GetTranslatableContent_m243CAAFC2A8379E626BEEE29EF66E8D7CF4B28AA (void);
// 0x0000009F System.Void Timeline/C_TimelLineEventChange::.ctor()
extern void C_TimelLineEventChange__ctor_mBEE169C01DC2BC2992F216A69922F08FBBC023AF (void);
// 0x000000A0 System.Void Timeline/<delayedActualize>d__14::.ctor(System.Int32)
extern void U3CdelayedActualizeU3Ed__14__ctor_m163DE02FB61436D359EAEFF208D0BA13531A1BA2 (void);
// 0x000000A1 System.Void Timeline/<delayedActualize>d__14::System.IDisposable.Dispose()
extern void U3CdelayedActualizeU3Ed__14_System_IDisposable_Dispose_mB3F3A0C344D293C1A69F98D95489647B841E4DDB (void);
// 0x000000A2 System.Boolean Timeline/<delayedActualize>d__14::MoveNext()
extern void U3CdelayedActualizeU3Ed__14_MoveNext_m5325B85F9B52AB10E4EE8560572451A336FFF31D (void);
// 0x000000A3 System.Object Timeline/<delayedActualize>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayedActualizeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8130E9BD1C537FF6EF30645E6EB76449A451D9C (void);
// 0x000000A4 System.Void Timeline/<delayedActualize>d__14::System.Collections.IEnumerator.Reset()
extern void U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_Reset_m7B7A2424DFC67895DA2367595C79B7C13DD21EA4 (void);
// 0x000000A5 System.Object Timeline/<delayedActualize>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_get_Current_mF87B6C1ACB21AD889437D3D4A15742C1D9E15DCB (void);
// 0x000000A6 System.Void Timeline_UIEventList::Start()
extern void Timeline_UIEventList_Start_m6923F482AC7FA3D8B3C6BB484920BAD74E55C8D0 (void);
// 0x000000A7 System.Void Timeline_UIEventList::OnTimelinechanged()
extern void Timeline_UIEventList_OnTimelinechanged_m12BC50D91308A6CC65E4F0AE74148B9DDAE01955 (void);
// 0x000000A8 System.Void Timeline_UIEventList::OnTimelineCleared()
extern void Timeline_UIEventList_OnTimelineCleared_mF9B01185723C4CF952910C224EDEEC84D18B9AF7 (void);
// 0x000000A9 System.Void Timeline_UIEventList::startMoveScrollbar()
extern void Timeline_UIEventList_startMoveScrollbar_mED9F1C78C25731F6B95A01373E55762C45C57A49 (void);
// 0x000000AA System.Void Timeline_UIEventList::actualizeYearsTextAndFakeScrollbar()
extern void Timeline_UIEventList_actualizeYearsTextAndFakeScrollbar_mB58889E0C989C82C153D02A5F02824BE9DAAAC50 (void);
// 0x000000AB System.Collections.IEnumerator Timeline_UIEventList::freezableUpdate()
extern void Timeline_UIEventList_freezableUpdate_m598AE33C3613E8016693606FDCCE8D6081B567AE (void);
// 0x000000AC System.Collections.IEnumerator Timeline_UIEventList::moveScrollbar()
extern void Timeline_UIEventList_moveScrollbar_m666D995BB93273C5789809F24C0C00527C33AA68 (void);
// 0x000000AD System.Void Timeline_UIEventList::AnimateScrollbar()
extern void Timeline_UIEventList_AnimateScrollbar_m708EAB440EBFA0C7CC54609EA34104E664768ED5 (void);
// 0x000000AE System.Collections.IEnumerator Timeline_UIEventList::animateScrollbar()
extern void Timeline_UIEventList_animateScrollbar_mE9711431AE5910959C2C37E9D643486F77139250 (void);
// 0x000000AF System.Void Timeline_UIEventList::ActualizeUIList()
extern void Timeline_UIEventList_ActualizeUIList_m7D715A617E4EAA446F6BD592CB801315F89D7242 (void);
// 0x000000B0 UnityEngine.GameObject Timeline_UIEventList::CreateEvent(Timeline/C_TimelineData)
extern void Timeline_UIEventList_CreateEvent_m9C6A9B3566B260DBABC395A0A30CE8CA887DB682 (void);
// 0x000000B1 System.Void Timeline_UIEventList::.ctor()
extern void Timeline_UIEventList__ctor_mEAE369E8F02C51717278568E8373AEA2B2F4BC79 (void);
// 0x000000B2 System.Void Timeline_UIEventList/mEvent::.ctor()
extern void mEvent__ctor_m51D1B819A1FE5479E15CF549E526DABFEA90D83E (void);
// 0x000000B3 System.Void Timeline_UIEventList/<freezableUpdate>d__23::.ctor(System.Int32)
extern void U3CfreezableUpdateU3Ed__23__ctor_m4783DD803B0781C6D55CF050D10870FA162CD368 (void);
// 0x000000B4 System.Void Timeline_UIEventList/<freezableUpdate>d__23::System.IDisposable.Dispose()
extern void U3CfreezableUpdateU3Ed__23_System_IDisposable_Dispose_m4BE71898145F5E53A99ADD950576BE01407E1F0A (void);
// 0x000000B5 System.Boolean Timeline_UIEventList/<freezableUpdate>d__23::MoveNext()
extern void U3CfreezableUpdateU3Ed__23_MoveNext_mCEE3074E559B0C0333B7A7A9E96B97C8C830D369 (void);
// 0x000000B6 System.Object Timeline_UIEventList/<freezableUpdate>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CfreezableUpdateU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44F416F81FFCE8065052F150502D1C7192D336DD (void);
// 0x000000B7 System.Void Timeline_UIEventList/<freezableUpdate>d__23::System.Collections.IEnumerator.Reset()
extern void U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_Reset_mBF4FC0D2B0597223CAD3B9358590491BD5C64C40 (void);
// 0x000000B8 System.Object Timeline_UIEventList/<freezableUpdate>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_get_Current_mFB3C753DC3C7268E2671B6BDB671B18618043502 (void);
// 0x000000B9 System.Void Timeline_UIEventList/<moveScrollbar>d__24::.ctor(System.Int32)
extern void U3CmoveScrollbarU3Ed__24__ctor_m2FE1146F98C50EA6320D3850669B72148B6B5EEA (void);
// 0x000000BA System.Void Timeline_UIEventList/<moveScrollbar>d__24::System.IDisposable.Dispose()
extern void U3CmoveScrollbarU3Ed__24_System_IDisposable_Dispose_m192BB59F93A9FBE43398A6FE37C8019D0A998C3A (void);
// 0x000000BB System.Boolean Timeline_UIEventList/<moveScrollbar>d__24::MoveNext()
extern void U3CmoveScrollbarU3Ed__24_MoveNext_m0C81E7E8D0C0991A7CD9E09F7E326BAF0FCA4A67 (void);
// 0x000000BC System.Object Timeline_UIEventList/<moveScrollbar>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CmoveScrollbarU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m954B89FE0C20DA41406BF08F8C8D5BBA6AEF0334 (void);
// 0x000000BD System.Void Timeline_UIEventList/<moveScrollbar>d__24::System.Collections.IEnumerator.Reset()
extern void U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_Reset_mF290FBF737DD65386088D34E1AD99CFE4E74D4B7 (void);
// 0x000000BE System.Object Timeline_UIEventList/<moveScrollbar>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_get_Current_m72CD44092B59FF2850BB3B5A683609887BEC9A41 (void);
// 0x000000BF System.Void Timeline_UIEventList/<animateScrollbar>d__26::.ctor(System.Int32)
extern void U3CanimateScrollbarU3Ed__26__ctor_m1B4B0175B724E1193EDCDA6F797C3064FF59E6BB (void);
// 0x000000C0 System.Void Timeline_UIEventList/<animateScrollbar>d__26::System.IDisposable.Dispose()
extern void U3CanimateScrollbarU3Ed__26_System_IDisposable_Dispose_m9B030CEA5F95001C0406F30685AC322AE6B40170 (void);
// 0x000000C1 System.Boolean Timeline_UIEventList/<animateScrollbar>d__26::MoveNext()
extern void U3CanimateScrollbarU3Ed__26_MoveNext_m0FDB4582FB489EA87C2268E3222DE1E987D189E9 (void);
// 0x000000C2 System.Object Timeline_UIEventList/<animateScrollbar>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CanimateScrollbarU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21653C698A208F8970AD906340AFA09AC8EF1F11 (void);
// 0x000000C3 System.Void Timeline_UIEventList/<animateScrollbar>d__26::System.Collections.IEnumerator.Reset()
extern void U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_Reset_mE02D079FC130D8B51F2AD3D7FF1EF878B7E35B2D (void);
// 0x000000C4 System.Object Timeline_UIEventList/<animateScrollbar>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_get_Current_m5F6D100097FC1BF15D464E6AEA55105A019DBBD5 (void);
// 0x000000C5 System.Void Timeline_UIItem::SetHistoryEvent(Timeline/C_TimelineData)
extern void Timeline_UIItem_SetHistoryEvent_m330A902DE10122B55BECE0FBB7441F6ED6225C0C (void);
// 0x000000C6 System.Void Timeline_UIItem::.ctor()
extern void Timeline_UIItem__ctor_mA317C8A44F3230735638627BB8B5686BDD8EC5CE (void);
// 0x000000C7 System.Void AchievementsScript::Awake()
extern void AchievementsScript_Awake_mC6A73B18FFAF82BAF9D8225C803C682D4F893F83 (void);
// 0x000000C8 System.Void AchievementsScript::load()
extern void AchievementsScript_load_mA4C4B36ADF9CDE1AB91F17514638159D1551F69B (void);
// 0x000000C9 System.Void AchievementsScript::countAndShowAchieventProgesss()
extern void AchievementsScript_countAndShowAchieventProgesss_m28782983CC365594876B40393D5B347A1CA667C1 (void);
// 0x000000CA System.Void AchievementsScript::activateGameObjects()
extern void AchievementsScript_activateGameObjects_m1B445EC36E7E051B2DF80ABCF41EE5611B6BBE3B (void);
// 0x000000CB System.Void AchievementsScript::Start()
extern void AchievementsScript_Start_m47D05453ABFE684DBEA17690994E304EF900B715 (void);
// 0x000000CC System.Void AchievementsScript::addAchievement(AchievementsScript/achievementTyp)
extern void AchievementsScript_addAchievement_m8C2201E4571FE8D491D7880B6725A7888B2522C2 (void);
// 0x000000CD System.Void AchievementsScript::setAchievementCounter(AchievementsScript/achievementTyp,System.Int32)
extern void AchievementsScript_setAchievementCounter_mBCD635B39D79DFEE6F1CE5F1983D881C35428E47 (void);
// 0x000000CE System.Int32 AchievementsScript::getAchievementStageReached(AchievementsScript/achievementConfig)
extern void AchievementsScript_getAchievementStageReached_m8574367302C1CC015C78782204C3FAABC488A890 (void);
// 0x000000CF System.Void AchievementsScript::showAchievementAnimation(AchievementsScript/achievementConfig)
extern void AchievementsScript_showAchievementAnimation_mEA81E3B2710A5AD7ACAFFBEFD8B1C4B602D0D9EA (void);
// 0x000000D0 System.Collections.Generic.List`1<System.String> AchievementsScript::getTranslatableTerms()
extern void AchievementsScript_getTranslatableTerms_m7DE5143D6C80B2BD598534A8AC38618E327E2602 (void);
// 0x000000D1 System.Void AchievementsScript::.ctor()
extern void AchievementsScript__ctor_m77A0863949389350AACFB50F17C0C0B8171462DD (void);
// 0x000000D2 System.Void AchievementsScript/achievement::.ctor()
extern void achievement__ctor_m680865D78DA8753E7E6318190756A32CA1AFF6A4 (void);
// 0x000000D3 System.Void AchievementsScript/achievementConfig::save()
extern void achievementConfig_save_m1D63A4FBA83FA0061354952383FC182628FFA57E (void);
// 0x000000D4 System.Void AchievementsScript/achievementConfig::load()
extern void achievementConfig_load_m2888209A72606F3B708654C91D8BF458A90A786E (void);
// 0x000000D5 System.Void AchievementsScript/achievementConfig::.ctor()
extern void achievementConfig__ctor_m2068145C7CE132C73C181201A6AE698EC1670716 (void);
// 0x000000D6 System.Void AchievementsScript/achievementStage::.ctor()
extern void achievementStage__ctor_m5BF4737516BA01B2BF566F23AAF2551190E87677 (void);
// 0x000000D7 System.Void AddGameLog::addGameLogText(System.String)
extern void AddGameLog_addGameLogText_m015DF41ECAF6A776345AC3EA4CD6BFBFED37C134 (void);
// 0x000000D8 System.Void AddGameLog::.ctor()
extern void AddGameLog__ctor_m726E5AFC3D0B762D625398D5F37023AFE2E5CE0D (void);
// 0x000000D9 System.Void CardStack::Awake()
extern void CardStack_Awake_m3B0EA2DAE2411CF087F05D7AB28ABBD65FB5C8F4 (void);
// 0x000000DA System.Void CardStack::Start()
extern void CardStack_Start_m3530127B162998BD6B788C1EFA270338BEB45197 (void);
// 0x000000DB System.Void CardStack::createAllCardCntList(CardStack/cardCount[]&)
extern void CardStack_createAllCardCntList_mDBC288F39B3812BA9A899D45C780949A48726CF1 (void);
// 0x000000DC System.Void CardStack::copyAllCardCntList(CardStack/cardCount[],CardStack/cardCount[]&)
extern void CardStack_copyAllCardCntList_mD784404D51F33691CE217B3B293EE9B0AA463756 (void);
// 0x000000DD System.Void CardStack::resizeAllCardDrawCnt()
extern void CardStack_resizeAllCardDrawCnt_mD4640794DA2C432894D16D2B41850411186F7A4A (void);
// 0x000000DE System.Void CardStack::resizeAllCardBlockCnt()
extern void CardStack_resizeAllCardBlockCnt_mC7B397F98141B520FEB04B3AB0B574F75800BB39 (void);
// 0x000000DF System.Void CardStack::resetCardStack()
extern void CardStack_resetCardStack_m780BA92E9752D934DFF6F0565B2966A21AC6FC61 (void);
// 0x000000E0 System.Void CardStack::addDrawCnt(UnityEngine.GameObject)
extern void CardStack_addDrawCnt_mB24D12F0BFFB0F1220AB704E646443FE03642580 (void);
// 0x000000E1 System.Void CardStack::addBlockCnt(UnityEngine.GameObject)
extern void CardStack_addBlockCnt_mCA08711982AA48C7C3FFBF9C55FA26CD2D43AB7B (void);
// 0x000000E2 System.Void CardStack::decrementBlockCounts()
extern void CardStack_decrementBlockCounts_m0FCD81E52246FCA9549B391FA38D168BDD0A89E4 (void);
// 0x000000E3 CardStack/cardIndex CardStack::getCardIndex(UnityEngine.GameObject)
extern void CardStack_getCardIndex_mC9151956943D542080BEC1A1C2B567F925DEDDF7 (void);
// 0x000000E4 System.Boolean CardStack::loadDrawCnt()
extern void CardStack_loadDrawCnt_mAD016A0D2FCBF3CD68FA5AA3C907694BF8155B93 (void);
// 0x000000E5 System.Boolean CardStack::loadBlockCnt()
extern void CardStack_loadBlockCnt_m6D9DB6C1606106E44FEDA979E31666E58DDFFB8D (void);
// 0x000000E6 System.Void CardStack::saveDrawCnt()
extern void CardStack_saveDrawCnt_m48F6B69908C2AA1C76906003094445FA9DEBE626 (void);
// 0x000000E7 System.Void CardStack::saveBlockCnt()
extern void CardStack_saveBlockCnt_m3EAEEF01F4C7F4AB0D2A6E1C25D2E2EC14927629 (void);
// 0x000000E8 System.Void CardStack::saveCardIndex()
extern void CardStack_saveCardIndex_m18AFC2E2E96A4FD6959BBCB54DE7139FA66C23A4 (void);
// 0x000000E9 CardStack/cardIndex CardStack::getCardIndex()
extern void CardStack_getCardIndex_mF2E390CF2E9E36C113CFF5FD9A5FEA19885716A5 (void);
// 0x000000EA System.Void CardStack::loadGameCard()
extern void CardStack_loadGameCard_m12AEF00F0FC79F29D8E3EEFF888E92E8B1D03CC4 (void);
// 0x000000EB System.Boolean CardStack::cardIndexValid(CardStack/cardIndex)
extern void CardStack_cardIndexValid_m236DB98DC2C7E0F7DBC5C97B788C28945D4157A8 (void);
// 0x000000EC System.Void CardStack::setCardMoveEnable(System.Boolean)
extern void CardStack_setCardMoveEnable_m667E6819EB0FCA544BAB21E48325CC348AAE3020 (void);
// 0x000000ED System.Boolean CardStack::getCardMoveEnabled()
extern void CardStack_getCardMoveEnabled_m412E90C778FB6AB79D6152662343AD229B462C55 (void);
// 0x000000EE System.Collections.IEnumerator CardStack::CardMovement()
extern void CardStack_CardMovement_m0DE547D4A6BCEF60B388F3E65A0D013E374B8953 (void);
// 0x000000EF System.Void CardStack::getCardAnimator()
extern void CardStack_getCardAnimator_m30D536BC4351915C79B6A0875073F38891087EA1 (void);
// 0x000000F0 System.Collections.IEnumerator CardStack::moveCardOut(CardStack/E_moveOutDirection)
extern void CardStack_moveCardOut_m9E9DA3238C638AB7927FBF00FBBA89E5E40A2FF1 (void);
// 0x000000F1 System.Void CardStack::nextCard()
extern void CardStack_nextCard_m7B2BE19D63A486E4D9D0536A47B06C29DF06519A (void);
// 0x000000F2 System.Void CardStack::nextCard(CardStack/E_moveOutDirection)
extern void CardStack_nextCard_m87BF1EF19657289AC02837BBC646E5D91C6133B0 (void);
// 0x000000F3 UnityEngine.GameObject CardStack::randomStandardCard()
extern void CardStack_randomStandardCard_m667E6680A90D348F0F386A998208C303AA1511EB (void);
// 0x000000F4 UnityEngine.GameObject CardStack::spawnCard(UnityEngine.GameObject)
extern void CardStack_spawnCard_mE54CF29103BD270E074EF491B71690544A63BA2E (void);
// 0x000000F5 System.Void CardStack::DestroySpawnedCard()
extern void CardStack_DestroySpawnedCard_m66107FD9D2B2BA02EA7C3A34CA46A31D2B820172 (void);
// 0x000000F6 System.Void CardStack::newCard()
extern void CardStack_newCard_m194F8597A461EA3300E0A0DC77FD382B2AF8E429 (void);
// 0x000000F7 System.Void CardStack::sortForPossibleCards()
extern void CardStack_sortForPossibleCards_m7AECF25953490F3BBBF3DE62A94F55D422070853 (void);
// 0x000000F8 System.Void CardStack::addToFollowUpStack(UnityEngine.GameObject,System.Int32)
extern void CardStack_addToFollowUpStack_mB02117F3F05BE086CFC62DE5A3BDFFEFDCFC744A (void);
// 0x000000F9 System.Void CardStack::saveFollowUpStack()
extern void CardStack_saveFollowUpStack_m848EBCA8764C54EF58DDAE4F6B13F44CCA3FD96E (void);
// 0x000000FA System.Boolean CardStack::loadFollowUpStack()
extern void CardStack_loadFollowUpStack_m796F366A84B5AFD19016BAD00434E8DC8DC3AF41 (void);
// 0x000000FB System.Void CardStack::clearFollowUpStack()
extern void CardStack_clearFollowUpStack_m8AC8DE7FDF89892EDE1E1064180757AFCD281058 (void);
// 0x000000FC System.Void CardStack::decreaseFollowUpStackTime()
extern void CardStack_decreaseFollowUpStackTime_mCB460AE90217F917D79B004784B3A036F30E21C7 (void);
// 0x000000FD UnityEngine.GameObject CardStack::getNextFollowUpCard()
extern void CardStack_getNextFollowUpCard_m6D81E6FA0389807C20CED5A252564FBD17077B5E (void);
// 0x000000FE System.Void CardStack::leftSwipe()
extern void CardStack_leftSwipe_m42356A35E1160D5B0809E8494E4ACB7F0FD0D506 (void);
// 0x000000FF System.Void CardStack::leftSwipePreview()
extern void CardStack_leftSwipePreview_mB670E511EBB598D8135CFE0EC835C1752F7E70A3 (void);
// 0x00000100 System.Void CardStack::rightSwipe()
extern void CardStack_rightSwipe_mEDFFE432D7E8AA2B0019A83D454B2F6E694DC356 (void);
// 0x00000101 System.Void CardStack::rightSwipePreview()
extern void CardStack_rightSwipePreview_m0A4B27C8E395BF38BB172CF217236E44D7213A47 (void);
// 0x00000102 System.Void CardStack::upSwipe()
extern void CardStack_upSwipe_mE1E62DABA42CB39465F091F7CE498C88413BB8EE (void);
// 0x00000103 System.Void CardStack::upSwipePreview()
extern void CardStack_upSwipePreview_mA2A041DC28F6D774AB679D69DB9798D256CC26AF (void);
// 0x00000104 System.Void CardStack::downSwipe()
extern void CardStack_downSwipe_m7E8F7A9DCF53779105A0B3A27852DF2166DB525F (void);
// 0x00000105 System.Void CardStack::downSwipePreview()
extern void CardStack_downSwipePreview_m47EA0FEC8DD6A20A97CCCDF997BC3880DCB09FC8 (void);
// 0x00000106 System.Void CardStack::additionalChoices(System.Int32)
extern void CardStack_additionalChoices_mA037E6420FC618FFB7E956A154DDC685B78D2A3D (void);
// 0x00000107 System.Void CardStack::additionalChoicesPreview(System.Int32)
extern void CardStack_additionalChoicesPreview_mD8035D0D5591033954BCF92D0412BB0CE823CF43 (void);
// 0x00000108 System.Void CardStack::resetSwipePreview()
extern void CardStack_resetSwipePreview_m4A460A384B8A9107F1F46AF5573E41D331113E76 (void);
// 0x00000109 System.Void CardStack::testForDuplicateCards()
extern void CardStack_testForDuplicateCards_m6BEE156CFFE8136EBA2871BA02649F9BE6B8CD00 (void);
// 0x0000010A System.Collections.Generic.List`1<System.String> CardStack::getTranslatableTerms()
extern void CardStack_getTranslatableTerms_m76FDAC5D9E3C5C4F333B4CE50BE3E445B7166BC1 (void);
// 0x0000010B System.Void CardStack::.ctor()
extern void CardStack__ctor_m4BAF6CE28BF8E94BFD437BA6802F78E76D26C39F (void);
// 0x0000010C System.Void CardStack/cardCategory::.ctor()
extern void cardCategory__ctor_mFCEBB7FC744EC9261859E01AEFD1A08D3587CF66 (void);
// 0x0000010D System.Void CardStack/cardIndex::.ctor()
extern void cardIndex__ctor_m6E921EA6A3F5E20001DBC533DFB4DC169D752F25 (void);
// 0x0000010E System.Void CardStack/cardCount::.ctor()
extern void cardCount__ctor_m26B399C7BCE5C3E353FBF7F556968C64E3A50FD0 (void);
// 0x0000010F System.Void CardStack/drawCnts::.ctor()
extern void drawCnts__ctor_mFA1804B7D7B0981861B29FB7F698ECC21F944BDD (void);
// 0x00000110 System.Void CardStack/blockCount::.ctor()
extern void blockCount__ctor_mA677706926C145196E763F67BB1AD62D49B30D60 (void);
// 0x00000111 System.Void CardStack/mEvent::.ctor()
extern void mEvent__ctor_m97679BC5D2451E7C461F2F9CC48F10664AE9C4A8 (void);
// 0x00000112 System.Void CardStack/C_folluwUpStackElement::.ctor()
extern void C_folluwUpStackElement__ctor_m3BB2D7FB29A1C05AA888E82DDE966072E4D67659 (void);
// 0x00000113 System.Void CardStack/C_WrapFollowUpStack::.ctor()
extern void C_WrapFollowUpStack__ctor_m26C2D55CD7B4708E71D199F15027431C90E744AC (void);
// 0x00000114 System.Void CardStack/<CardMovement>d__49::.ctor(System.Int32)
extern void U3CCardMovementU3Ed__49__ctor_m20A81C719966A8182126B9975F568E07B82DAA4F (void);
// 0x00000115 System.Void CardStack/<CardMovement>d__49::System.IDisposable.Dispose()
extern void U3CCardMovementU3Ed__49_System_IDisposable_Dispose_m597AE27807E96B9EABB15A7B52F3A907165FCA4F (void);
// 0x00000116 System.Boolean CardStack/<CardMovement>d__49::MoveNext()
extern void U3CCardMovementU3Ed__49_MoveNext_m260905E032D12F5C5AF32A0897D7860F1738635B (void);
// 0x00000117 System.Object CardStack/<CardMovement>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCardMovementU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F948D8EEE3C7847E0339CF067F773A196FADE4B (void);
// 0x00000118 System.Void CardStack/<CardMovement>d__49::System.Collections.IEnumerator.Reset()
extern void U3CCardMovementU3Ed__49_System_Collections_IEnumerator_Reset_m2022354FDA8C877E0121F503D61E32CF4787D0A1 (void);
// 0x00000119 System.Object CardStack/<CardMovement>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CCardMovementU3Ed__49_System_Collections_IEnumerator_get_Current_m239552DA1314887EF0A85094BE7792EF445E3092 (void);
// 0x0000011A System.Void CardStack/<moveCardOut>d__53::.ctor(System.Int32)
extern void U3CmoveCardOutU3Ed__53__ctor_m1A4633BA82BE4A53DE6C58D7632CA74DBF7980C4 (void);
// 0x0000011B System.Void CardStack/<moveCardOut>d__53::System.IDisposable.Dispose()
extern void U3CmoveCardOutU3Ed__53_System_IDisposable_Dispose_m0F6008412BF065A95BF7C9837E002D54FB6AC18B (void);
// 0x0000011C System.Boolean CardStack/<moveCardOut>d__53::MoveNext()
extern void U3CmoveCardOutU3Ed__53_MoveNext_m9E0C510971686B3DCDA978C7A02A269274E9C08B (void);
// 0x0000011D System.Object CardStack/<moveCardOut>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CmoveCardOutU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB107756F0DFD44B9A92F9A90BAA1416A5FF52058 (void);
// 0x0000011E System.Void CardStack/<moveCardOut>d__53::System.Collections.IEnumerator.Reset()
extern void U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_Reset_m7ECACECB5214C8351376CC906FC0E5D13664C31A (void);
// 0x0000011F System.Object CardStack/<moveCardOut>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_get_Current_m60342D8BA597D00666EC5A995994065A6C8AEE7C (void);
// 0x00000120 System.Void CardStyle::SetStyle(KingsCardStyle)
extern void CardStyle_SetStyle_m52A237E0248DE63EF94BFA176B5D2D0352BB24BE (void);
// 0x00000121 System.String CardStyle::GetStyleName()
extern void CardStyle_GetStyleName_m0E795762F3ADCDB139A2E5F6A380A233C8C8E9A1 (void);
// 0x00000122 System.Void CardStyle::Refresh()
extern void CardStyle_Refresh_m5D5C0BB020AFA1189397F3CED1450C0CD575FEF5 (void);
// 0x00000123 System.Void CardStyle::Awake()
extern void CardStyle_Awake_mEDEF8861427E413C3FC3F96C3CCB966A01791FF5 (void);
// 0x00000124 System.Void CardStyle::Update()
extern void CardStyle_Update_m4463E02F0D5A7B48467D9D0F06285DA763B99DB5 (void);
// 0x00000125 System.Void CardStyle::actualizeColoring()
extern void CardStyle_actualizeColoring_mF2244F459A81C0E8AD587CAB3E79EAB60D34F30A (void);
// 0x00000126 System.Void CardStyle::actualizeImageColor(UnityEngine.UI.Image)
extern void CardStyle_actualizeImageColor_m0E426F4CE78AB403709CE9C013550F2F98805342 (void);
// 0x00000127 System.Void CardStyle::actualizeImages()
extern void CardStyle_actualizeImages_m792D4C68BF04FBBDE130DFDDB68EEABB97CC3E7B (void);
// 0x00000128 System.Void CardStyle::.ctor()
extern void CardStyle__ctor_m7DC380749CB217AD06CA1C4EAAD7EC29249C930D (void);
// 0x00000129 System.Void CardStyle/C_CardImages::.ctor()
extern void C_CardImages__ctor_mAC478907002F1A778AE913B1D303B216CB249A8D (void);
// 0x0000012A System.Void ConditionsAndEffects::ComputeEffects(ConditionsAndEffects/C_Changes)
extern void ConditionsAndEffects_ComputeEffects_mC75C914A6784CDEE79AB601C9E555E934C1AD5F8 (void);
// 0x0000012B System.Boolean ConditionsAndEffects::AreConditionsMet(ConditionsAndEffects/C_Conditions)
extern void ConditionsAndEffects_AreConditionsMet_m4FE763F5083B9AFA4CBDBA84C369C6AB08F9D099 (void);
// 0x0000012C System.Void ConditionsAndEffects::.ctor()
extern void ConditionsAndEffects__ctor_m11FC7E4729550ACE01724AED770A6C1A6033FDC0 (void);
// 0x0000012D System.Boolean ConditionsAndEffects/C_Conditions::AreConditionsMet()
extern void C_Conditions_AreConditionsMet_m48E4A46D2768EC9DFE687BA54D17179FA1786B24 (void);
// 0x0000012E System.Void ConditionsAndEffects/C_Conditions::.ctor()
extern void C_Conditions__ctor_m4E72B2568637EECAF276DD93E2E4BDF3779273FA (void);
// 0x0000012F System.Void ConditionsAndEffects/C_Changes::ExecuteEffect()
extern void C_Changes_ExecuteEffect_mC77113F7AE4D125AB209EFB99FD83456DC7EC237 (void);
// 0x00000130 System.Collections.Generic.List`1<System.String> ConditionsAndEffects/C_Changes::GetTranslatableContent()
extern void C_Changes_GetTranslatableContent_m03E68108533625A619AF2DD189C7F157928ECC47 (void);
// 0x00000131 System.Void ConditionsAndEffects/C_Changes::.ctor()
extern void C_Changes__ctor_mA267EFB73409984347B57B29D156D67E63BA9F60 (void);
// 0x00000132 System.Void CountryNameDisplay::Start()
extern void CountryNameDisplay_Start_m4B458B3C13D92DC9CEFFA2AF83612AF4668F6537 (void);
// 0x00000133 System.Collections.IEnumerator CountryNameDisplay::twoFrames()
extern void CountryNameDisplay_twoFrames_mA53A0B0554D766F89C664C57F2B51243139FC082 (void);
// 0x00000134 System.Void CountryNameDisplay::actualizeTexts()
extern void CountryNameDisplay_actualizeTexts_mB838DBD92AC3AAEC47F511726C73116EF36883A9 (void);
// 0x00000135 System.Void CountryNameDisplay::clearTexts()
extern void CountryNameDisplay_clearTexts_m0A8E9A445CA1B5D9592CAC59EB66AB6577246932 (void);
// 0x00000136 System.Void CountryNameDisplay::.ctor()
extern void CountryNameDisplay__ctor_mE3F0A4E828E5604AAD388C2F416E8581E98B370A (void);
// 0x00000137 System.Void CountryNameDisplay/<twoFrames>d__4::.ctor(System.Int32)
extern void U3CtwoFramesU3Ed__4__ctor_m7DDE93D188A97F1F2A8F7B8B76AB6E2BDC4E624D (void);
// 0x00000138 System.Void CountryNameDisplay/<twoFrames>d__4::System.IDisposable.Dispose()
extern void U3CtwoFramesU3Ed__4_System_IDisposable_Dispose_m831487A496655900EDA4322D9ECAAC5E548DB62C (void);
// 0x00000139 System.Boolean CountryNameDisplay/<twoFrames>d__4::MoveNext()
extern void U3CtwoFramesU3Ed__4_MoveNext_mF439C59D8CDA208A9E43B5E2B24D97A9B6DA1A24 (void);
// 0x0000013A System.Object CountryNameDisplay/<twoFrames>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtwoFramesU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F6E189D7326C4B888B4CC8D73E4E6C62C6C94B1 (void);
// 0x0000013B System.Void CountryNameDisplay/<twoFrames>d__4::System.Collections.IEnumerator.Reset()
extern void U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_Reset_mFF5E60CCA7FADEE5BCBFFF2829E501D6E6D8985A (void);
// 0x0000013C System.Object CountryNameDisplay/<twoFrames>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_get_Current_m585E0BFA9C48635E44FD2A8AF7F111842DEF9308 (void);
// 0x0000013D System.Void CountryNameGenerator::Start()
extern void CountryNameGenerator_Start_m6ED8501EC55F0E87FCC76E25B900E985B6C0BD47 (void);
// 0x0000013E System.Void CountryNameGenerator::clearUI()
extern void CountryNameGenerator_clearUI_m8010DCF2FFB7EC9C611AA4E5AEF392220235AE1E (void);
// 0x0000013F System.Collections.IEnumerator CountryNameGenerator::oneFrame()
extern void CountryNameGenerator_oneFrame_m332A4A4145FB9E105CDE00AB65B211D77BCF23FD (void);
// 0x00000140 System.Void CountryNameGenerator::createValueScriptLinks()
extern void CountryNameGenerator_createValueScriptLinks_mF77ADB48867ECE90B4A1D0477469526746B5AAA3 (void);
// 0x00000141 System.Void CountryNameGenerator::Awake()
extern void CountryNameGenerator_Awake_mB080EFFC99E9A05AFACFE2EA3C29C01944C9CC8A (void);
// 0x00000142 System.Void CountryNameGenerator::actualizeTexts(System.Boolean)
extern void CountryNameGenerator_actualizeTexts_m6C8078834FBD1A5553B064EC8D676A1E5F153B6B (void);
// 0x00000143 System.String CountryNameGenerator::GetNameString()
extern void CountryNameGenerator_GetNameString_m4D0A739485B0F33AD066860F041AFE43C5D6F7FC (void);
// 0x00000144 System.String CountryNameGenerator::GetCountryString()
extern void CountryNameGenerator_GetCountryString_m8E30D52FFCFB25CB9B237C71C774933ACE8E4AE4 (void);
// 0x00000145 System.String CountryNameGenerator::GetCountryNameString()
extern void CountryNameGenerator_GetCountryNameString_mAF5B8522946CB1C41D028F6B3CE40A9637AE1F22 (void);
// 0x00000146 CountryNameGenerator/nameGenderLink CountryNameGenerator::getNameAndGenderFromValue()
extern void CountryNameGenerator_getNameAndGenderFromValue_mC16F1FA68E235DC7ABD2B8B75D9A936CBB28F8E5 (void);
// 0x00000147 System.String CountryNameGenerator::getSurnameFromValue()
extern void CountryNameGenerator_getSurnameFromValue_mD0B89C46156249DBF05013AEA8627AB44877556F (void);
// 0x00000148 System.Int32 CountryNameGenerator::getCountryIndexFromValue()
extern void CountryNameGenerator_getCountryIndexFromValue_m46AB5802FD4F79E3714D2E7CB445CDBF20423F35 (void);
// 0x00000149 System.String CountryNameGenerator::getCountryNameText()
extern void CountryNameGenerator_getCountryNameText_mE0EFB80EA5A79720BCAEF2A51502B20B7C7AE75A (void);
// 0x0000014A System.String CountryNameGenerator::getCountryTranslatedStringFromValue()
extern void CountryNameGenerator_getCountryTranslatedStringFromValue_m32DD33930212D760A44083DA7471AD5DC5766A02 (void);
// 0x0000014B System.Collections.Generic.List`1<System.String> CountryNameGenerator::getTranslatableTerms()
extern void CountryNameGenerator_getTranslatableTerms_m7F88440605D47F3D7DA9A9E9DAB979F66D277A89 (void);
// 0x0000014C System.Void CountryNameGenerator::.ctor()
extern void CountryNameGenerator__ctor_mB1677272BF542D1A120A26DDC4E1B65D4F22575F (void);
// 0x0000014D System.Void CountryNameGenerator/nameGenderLink::.ctor()
extern void nameGenderLink__ctor_mA79D0AA488F2FDE581B4878DFE6A9AD20429CCED (void);
// 0x0000014E System.Void CountryNameGenerator/subStringList::.ctor()
extern void subStringList__ctor_mAE35EBC10DF25D0D850D544952D7B8B4B57B0EA3 (void);
// 0x0000014F System.Void CountryNameGenerator/<oneFrame>d__5::.ctor(System.Int32)
extern void U3ConeFrameU3Ed__5__ctor_m4B579F1DA5E880BA17A19AAC62C801F10B2282FE (void);
// 0x00000150 System.Void CountryNameGenerator/<oneFrame>d__5::System.IDisposable.Dispose()
extern void U3ConeFrameU3Ed__5_System_IDisposable_Dispose_m697F7F0C529FE820491FBBFB4628E70D9A547D72 (void);
// 0x00000151 System.Boolean CountryNameGenerator/<oneFrame>d__5::MoveNext()
extern void U3ConeFrameU3Ed__5_MoveNext_mB52587FF65543F3E63CFA6D536B0F7BD37CA5025 (void);
// 0x00000152 System.Object CountryNameGenerator/<oneFrame>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ConeFrameU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0CBC897829B3BE3032005EA66E369973541A768 (void);
// 0x00000153 System.Void CountryNameGenerator/<oneFrame>d__5::System.Collections.IEnumerator.Reset()
extern void U3ConeFrameU3Ed__5_System_Collections_IEnumerator_Reset_m5F621E1AA2318DC5D06E66CBD08B99C45D57C67E (void);
// 0x00000154 System.Object CountryNameGenerator/<oneFrame>d__5::System.Collections.IEnumerator.get_Current()
extern void U3ConeFrameU3Ed__5_System_Collections_IEnumerator_get_Current_m8E2260A88F81ACEE95703F8F693E5EB4C7DE3895 (void);
// 0x00000155 System.Void DeletePlayerPrefs::DeleteAll()
extern void DeletePlayerPrefs_DeleteAll_mCD4F449E918CDC512AAA042DA82B56C1281B4C5F (void);
// 0x00000156 System.Void DeletePlayerPrefs::.ctor()
extern void DeletePlayerPrefs__ctor_mAF726B932497A48665BB1E8C3FEC5E6C1ED0764A (void);
// 0x00000157 System.Void Destroy::DestroyGameobject()
extern void Destroy_DestroyGameobject_m65893A0996828BF154B263F5B3E1E43C3D302BE3 (void);
// 0x00000158 System.Void Destroy::.ctor()
extern void Destroy__ctor_m487225D87A53D11346B6FD7BFAC8B4B3C33BC02E (void);
// 0x00000159 System.Void AnimatorBool::setBool(System.Boolean)
extern void AnimatorBool_setBool_m2E7B072DC1C37C30EB33B29D2E5B2A3A321F4B90 (void);
// 0x0000015A System.Void AnimatorBool::.ctor()
extern void AnimatorBool__ctor_m1BB65151E5349E47E69D9497A894133282918863 (void);
// 0x0000015B System.Void AnimatorInt::setInt(System.Int32)
extern void AnimatorInt_setInt_m9AF5A75DA35D38AABED2D384E4106EC9D4E05B57 (void);
// 0x0000015C System.Void AnimatorInt::.ctor()
extern void AnimatorInt__ctor_mB23AA206EA2067D8E81C159519C328602076C56F (void);
// 0x0000015D System.Void ButtonPressed::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ButtonPressed_OnPointerDown_mCA15B7CF9A665231B3A8AE60D1CDC35C3BE8826C (void);
// 0x0000015E System.Void ButtonPressed::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ButtonPressed_OnPointerUp_m9231D78DEBB4872D9FB423D98516103DAC5C9342 (void);
// 0x0000015F System.Void ButtonPressed::.ctor()
extern void ButtonPressed__ctor_mB43CFB7F386E3A57035A0F29F8800A0946CD5121 (void);
// 0x00000160 System.Void DebugEvent::debug(System.String)
extern void DebugEvent_debug_m6F7369EA70C9109B33171F98E02FAD350D10B12F (void);
// 0x00000161 System.Void DebugEvent::debugBreak()
extern void DebugEvent_debugBreak_m8B3AB26CEC6445AAB476A34F1829133C5F9503D9 (void);
// 0x00000162 System.Void DebugEvent::.ctor()
extern void DebugEvent__ctor_mD2F67C0835492EF7749D7973ED9CFA6FDA0B312F (void);
// 0x00000163 System.Void DelayedEvent::startEventdelay()
extern void DelayedEvent_startEventdelay_mABF86A0893DEB981003F52298EAF4CA3CC8290B4 (void);
// 0x00000164 System.Void DelayedEvent::Start()
extern void DelayedEvent_Start_mCD56DC67EA047253CE87057D367BE2090EE3D46A (void);
// 0x00000165 System.Collections.IEnumerator DelayedEvent::_delay()
extern void DelayedEvent__delay_m4D67B05369F776238509EF536A42015264C8FE87 (void);
// 0x00000166 System.Void DelayedEvent::actualizeCountdowns(System.Single,System.Single)
extern void DelayedEvent_actualizeCountdowns_m11CF5F952A355D17B1A4E321B4F9B581D80AAA37 (void);
// 0x00000167 System.Void DelayedEvent::.ctor()
extern void DelayedEvent__ctor_m0C6637FD7CE4606E0EF7F5373BB25F19E68721FA (void);
// 0x00000168 System.Void DelayedEvent/mEvent::.ctor()
extern void mEvent__ctor_m5D8686373288E4E7A07182AF7D327D056CC326FC (void);
// 0x00000169 System.Void DelayedEvent/C_UIDisplays::.ctor()
extern void C_UIDisplays__ctor_mBE25B546586C6BAA3108B3392E72FE3981A32840 (void);
// 0x0000016A System.Void DelayedEvent/<_delay>d__11::.ctor(System.Int32)
extern void U3C_delayU3Ed__11__ctor_m5170E051F5D20856F12F90FDCA6571E68EB8F830 (void);
// 0x0000016B System.Void DelayedEvent/<_delay>d__11::System.IDisposable.Dispose()
extern void U3C_delayU3Ed__11_System_IDisposable_Dispose_m815F55D1DA38454BA27303C83782CCF5B2839C7E (void);
// 0x0000016C System.Boolean DelayedEvent/<_delay>d__11::MoveNext()
extern void U3C_delayU3Ed__11_MoveNext_mB6CDF005DEF2114CD7245AFCBA59BE9355AC4855 (void);
// 0x0000016D System.Object DelayedEvent/<_delay>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_delayU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B7C7859D2F37ABA7675B0FF7B2CEA08D5523395 (void);
// 0x0000016E System.Void DelayedEvent/<_delay>d__11::System.Collections.IEnumerator.Reset()
extern void U3C_delayU3Ed__11_System_Collections_IEnumerator_Reset_m8B109E7B8BD114E6A539836C1CCC24D3B949F6DA (void);
// 0x0000016F System.Object DelayedEvent/<_delay>d__11::System.Collections.IEnumerator.get_Current()
extern void U3C_delayU3Ed__11_System_Collections_IEnumerator_get_Current_mE1E9828FA48FE14B08726A0E3A98EFED407A4DBF (void);
// 0x00000170 System.Void GlobalMessageEventManager::Awake()
extern void GlobalMessageEventManager_Awake_mB2770724A22B7EC0A4A363805EC9D3BDA57E4DE9 (void);
// 0x00000171 System.Void GlobalMessageEventManager::buildAwake()
extern void GlobalMessageEventManager_buildAwake_mF9A30B9B9EBBFE4B550444AE3B0AB9794924B90F (void);
// 0x00000172 System.Void GlobalMessageEventManager::registerMessageReceiver(GlobalMessageEventReceiver)
extern void GlobalMessageEventManager_registerMessageReceiver_mA6ABD7FD7645482776F3E4AFAD27631F622BCCB1 (void);
// 0x00000173 System.Void GlobalMessageEventManager::unregisterMessageReceiver(GlobalMessageEventReceiver)
extern void GlobalMessageEventManager_unregisterMessageReceiver_m869F6BA38950BAD2188E7964B58AEFC200DEF926 (void);
// 0x00000174 System.Void GlobalMessageEventManager::sendToReceivers(System.String)
extern void GlobalMessageEventManager_sendToReceivers_m1AF577A411F10DEE4B0A795AA199F8936A2C574B (void);
// 0x00000175 System.Void GlobalMessageEventManager::sendToReceivers(System.String,UnityEngine.Sprite)
extern void GlobalMessageEventManager_sendToReceivers_m304446BD968CDC5ADA8431795248CE36251710CA (void);
// 0x00000176 System.Void GlobalMessageEventManager::.ctor()
extern void GlobalMessageEventManager__ctor_m29202DA2DD0D9BADE52A35A2D1F3A5D6886B957F (void);
// 0x00000177 System.Void GlobalMessageEventReceiver::Start()
extern void GlobalMessageEventReceiver_Start_mF17B34225128890DA1473CF5037F71FC84EFA5F5 (void);
// 0x00000178 System.Void GlobalMessageEventReceiver::createManagerIfNonExisting()
extern void GlobalMessageEventReceiver_createManagerIfNonExisting_mF5154BDC0AF528FA8EC5126F471E6F8D91F72698 (void);
// 0x00000179 System.Void GlobalMessageEventReceiver::OnDestroy()
extern void GlobalMessageEventReceiver_OnDestroy_mA623E63B1BE7A7E21A1BF1D09230D28965B03456 (void);
// 0x0000017A System.Void GlobalMessageEventReceiver::globalMessage(System.String)
extern void GlobalMessageEventReceiver_globalMessage_mE05590EF805B77D4E446F98439968731D95EF8BE (void);
// 0x0000017B System.Void GlobalMessageEventReceiver::globalImage(System.String,UnityEngine.Sprite)
extern void GlobalMessageEventReceiver_globalImage_m2D3850D7C7AEF31F6E58D7250B9AFA7BBA3D39A1 (void);
// 0x0000017C System.Void GlobalMessageEventReceiver::.ctor()
extern void GlobalMessageEventReceiver__ctor_m71E9E906B527A4C2E9CBB1CD1CA383FC5F51BC2B (void);
// 0x0000017D System.Void GlobalMessageEventReceiver/mEvent::.ctor()
extern void mEvent__ctor_mD7BA50F8AC6D22C83E6ABECFDAC8D08316E34241 (void);
// 0x0000017E System.Void GlobalMessageEventReceiver/MessageEvent::.ctor()
extern void MessageEvent__ctor_m4DC50C92357D3818295372C93E67BB4F893509B0 (void);
// 0x0000017F System.Void GlobalMessageEventReceiver/MessageImage::.ctor()
extern void MessageImage__ctor_m2F9FE36DE0A282020EB1E0304A72CDA66500F56E (void);
// 0x00000180 System.Void GlobalMessageEventSender::GlobalMessage(System.String)
extern void GlobalMessageEventSender_GlobalMessage_mAC3EE13DF47059D15A759A1573B946135FEAF095 (void);
// 0x00000181 System.Void GlobalMessageEventSender::.ctor()
extern void GlobalMessageEventSender__ctor_m4C702BBBEC4CBE8A5F27C694D8B0B6FD10934078 (void);
// 0x00000182 System.Void GlobalMessageImageSender::GlobalMessage(System.String)
extern void GlobalMessageImageSender_GlobalMessage_m674219A906B8BD2642C03DA840581E8C035C7A5A (void);
// 0x00000183 System.Void GlobalMessageImageSender::.ctor()
extern void GlobalMessageImageSender__ctor_mD6501450B9AF30A1D152375703DC9D788D314192 (void);
// 0x00000184 System.Void OnCollisionTag::OnCollisionEnter(UnityEngine.Collision)
extern void OnCollisionTag_OnCollisionEnter_mFAE92208B0F0EC8C7140040D7EB1EBDA47CB8E3B (void);
// 0x00000185 System.Void OnCollisionTag::.ctor()
extern void OnCollisionTag__ctor_mDDCECE2E2151B7807579F987C896F7F0C37C2BEF (void);
// 0x00000186 System.Void OnCollisionTag/mEvent::.ctor()
extern void mEvent__ctor_mB5C0776516A1FA2EBC2295E15E7E6C007AF9E041 (void);
// 0x00000187 System.Void OnEnterTag::OnTriggerEnter(UnityEngine.Collider)
extern void OnEnterTag_OnTriggerEnter_m3F9D8E58D4A734DD02861C631E8EC9F98EB78F06 (void);
// 0x00000188 System.Void OnEnterTag::.ctor()
extern void OnEnterTag__ctor_m8B2AE88D587921CD45ED3475905AFF8CC9C8830F (void);
// 0x00000189 System.Void OnEnterTag/mEvent::.ctor()
extern void mEvent__ctor_m77BEA20565F3E811DF191F0B42689D92AD649D16 (void);
// 0x0000018A System.Void RandomEvents::executeRandomEvent()
extern void RandomEvents_executeRandomEvent_mDB8335DA609EC56F2D50908B8387335E588BCF36 (void);
// 0x0000018B System.Void RandomEvents::.ctor()
extern void RandomEvents__ctor_mAD1A155C35635AC6522CBC8B43F31EC4D00B7930 (void);
// 0x0000018C System.Void RandomEvents/mEvent::.ctor()
extern void mEvent__ctor_mFADA705CF7356AC03AE100D53E207586CBD9A80C (void);
// 0x0000018D System.Void EventScript::SetImportData(System.String,System.String)
extern void EventScript_SetImportData_mEFF9A71936403D754E20312DB1C58ED112639354 (void);
// 0x0000018E System.Void EventScript::writeTextFields()
extern void EventScript_writeTextFields_mA6019FC64B2BFF6DEF6F026AE3DB9B9B017F5FD1 (void);
// 0x0000018F System.Void EventScript::writeEventTextField(EventScript/eventText,System.String)
extern void EventScript_writeEventTextField_m6D02EF385DD60DF39CB4875362EFE470F7762688 (void);
// 0x00000190 System.Void EventScript::onLeftSwipe()
extern void EventScript_onLeftSwipe_m146AB6FF617F54AD8A16BFE2C5553AA6413F8226 (void);
// 0x00000191 System.Void EventScript::onRightSwipe()
extern void EventScript_onRightSwipe_mB375350E3D4F5AEA7AA5DDC88DE619BA1B6DDA77 (void);
// 0x00000192 System.Void EventScript::onUpSwipe()
extern void EventScript_onUpSwipe_mCA730665CE42F616CF51DACA1966A9C3F646B4B0 (void);
// 0x00000193 System.Void EventScript::onDownSwipe()
extern void EventScript_onDownSwipe_m3DE6A411A295A47F23D6117585999E5514D51EAE (void);
// 0x00000194 System.Boolean EventScript::ExecuteAddtionalChoices(System.Int32)
extern void EventScript_ExecuteAddtionalChoices_m42256FEAC87FFF2A62800F299AB1211622A76592 (void);
// 0x00000195 System.Void EventScript::AdditionalChoice_0_Selection()
extern void EventScript_AdditionalChoice_0_Selection_m16293866F22C1E4DA76DA2B91EA34CE15C965C4F (void);
// 0x00000196 System.Void EventScript::AdditionalChoice_0_Preview()
extern void EventScript_AdditionalChoice_0_Preview_m38ED20072A30C589AB2AEFE66AB7345BA785B6FC (void);
// 0x00000197 System.Void EventScript::AdditionalChoice_1_Selection()
extern void EventScript_AdditionalChoice_1_Selection_m9D8C665FA05901AF5E3E72B6966BF5DBAF289529 (void);
// 0x00000198 System.Void EventScript::AdditionalChoice_1_Preview()
extern void EventScript_AdditionalChoice_1_Preview_m952FF3104D06E0061EDBFFA92B7D612A014C59F5 (void);
// 0x00000199 System.Void EventScript::onLeftSpwipePreview()
extern void EventScript_onLeftSpwipePreview_m4FAA43926C07ABAAA0CD740A9BAA2E9AAAFF765D (void);
// 0x0000019A System.Void EventScript::onRightSpwipePreview()
extern void EventScript_onRightSpwipePreview_m427707F7C9E23AA17E0597E47CAD35AA17FEB34B (void);
// 0x0000019B System.Void EventScript::onUpSpwipePreview()
extern void EventScript_onUpSpwipePreview_mDDEC20E7F768E8E8E39A7AC87E83F76936D12C79 (void);
// 0x0000019C System.Void EventScript::onDownSpwipePreview()
extern void EventScript_onDownSpwipePreview_mEA9E976878435736D39F737C710A5DD07A485D5D (void);
// 0x0000019D System.Void EventScript::computeEventResultsPreview(EventScript/mEvent)
extern void EventScript_computeEventResultsPreview_mA52B17E16E227281B5FB69C018E787E4EF0082F0 (void);
// 0x0000019E System.Void EventScript::onSwipePreviewReset()
extern void EventScript_onSwipePreviewReset_m625874C1D719B39CA4F63C03E463001427591893 (void);
// 0x0000019F System.Void EventScript::PreviewAddtionalChoices(System.Int32)
extern void EventScript_PreviewAddtionalChoices_mCCAD0C17D1EB4A05096E20251DC185FBCB2F1A3D (void);
// 0x000001A0 System.Void EventScript::computeResult(EventScript/result)
extern void EventScript_computeResult_m79678BA9B858507BC7E2B59CC14828508D6525B7 (void);
// 0x000001A1 System.Void EventScript::ComputeResultTypeDependant(EventScript/result,System.Boolean)
extern void EventScript_ComputeResultTypeDependant_mA0F22A94881AB3D98431BE3069CCD73A42368E9E (void);
// 0x000001A2 System.Void EventScript::computeTextPreview(System.String,valueDefinitions/values,System.Single&,System.Boolean&)
extern void EventScript_computeTextPreview_mA94DCBA6146E1F378511D5339972CA44C6B805E0 (void);
// 0x000001A3 EventScript/result EventScript::GetResultByDirectionString(System.String)
extern void EventScript_GetResultByDirectionString_mB6107C1BD868E652A3E76F873563F78B11076E8E (void);
// 0x000001A4 System.Void EventScript::addTextValueChanges(EventScript/modifierGroup,valueDefinitions/values,System.Single&,System.Boolean&)
extern void EventScript_addTextValueChanges_mDFF9A2F33C8320B919A1F7EA71B6AB7A553A7F82 (void);
// 0x000001A5 System.Void EventScript::computeResultPreview(EventScript/result)
extern void EventScript_computeResultPreview_m6FA8094468856E79862B5C0456BBB642EC653A31 (void);
// 0x000001A6 System.Void EventScript::resetResultPreviews()
extern void EventScript_resetResultPreviews_m4D59546F0479EE70DCB5C413B80085BB722C98CB (void);
// 0x000001A7 System.Void EventScript::executeValueChanges(EventScript/modifierGroup,System.Boolean)
extern void EventScript_executeValueChanges_mA3562B3A4947F718F47BC0019BCFF37DE32ED80A (void);
// 0x000001A8 System.Void EventScript::executeExtraChanges(EventScript/C_AdditionalModifiers[])
extern void EventScript_executeExtraChanges_mA0AB9D88B7B454E818A3BFC4CCA840629005B33A (void);
// 0x000001A9 System.Void EventScript::executeValueChange(EventScript/resultModifier)
extern void EventScript_executeValueChange_mE37110A642E3BF7D84A2818D6A223EFA40F2A71B (void);
// 0x000001AA System.Void EventScript::addPreviewValueChanges(EventScript/modifierGroup,System.Boolean)
extern void EventScript_addPreviewValueChanges_m8088EB701A823580719638106BE4D03C396FDD33 (void);
// 0x000001AB System.Boolean EventScript::AreConditinsForResultMet(EventScript/condition[])
extern void EventScript_AreConditinsForResultMet_m3D9132AE25B48B5CB629A4DC4B919E50483BEEE3 (void);
// 0x000001AC System.Void EventScript::Awake()
extern void EventScript_Awake_m44F58AD4B1684E59A0C37EA8439A18CA9E3A6E7E (void);
// 0x000001AD System.Void EventScript::Start()
extern void EventScript_Start_mD880C9B9538EF93C046B0F46573B559EF3B4E8DB (void);
// 0x000001AE System.Void EventScript::.ctor()
extern void EventScript__ctor_m37050F9D52F8FBA7FAD50FDACBCE68A6BA1AC006 (void);
// 0x000001AF System.Void EventScript::.cctor()
extern void EventScript__cctor_mDE6AC00424E9B05179B6910AB0062A3D18DE895E (void);
// 0x000001B0 System.Void EventScript/mEvent::.ctor()
extern void mEvent__ctor_mB59CF7AFF9BA652BEC62317D426BFA65FEB9C6AF (void);
// 0x000001B1 System.Void EventScript/eventText::.ctor()
extern void eventText__ctor_mDD6E3AD4E9D59F330914F61F658F2221BD7B9683 (void);
// 0x000001B2 System.Void EventScript/C_Texts::set_text(System.String)
extern void C_Texts_set_text_m0CD53D310B7C799D21FE30E1F254E48ACD06905A (void);
// 0x000001B3 System.Void EventScript/C_Texts::.ctor()
extern void C_Texts__ctor_mB87A2AA0DC8B6C9632B81CF478978A69867506EC (void);
// 0x000001B4 System.String[] EventScript/eventTexts::getCsvHeader()
extern void eventTexts_getCsvHeader_m06C94EB9DD4A4B7B14C2577CFC9460BF63A3DB64 (void);
// 0x000001B5 System.String[] EventScript/eventTexts::getCsvData()
extern void eventTexts_getCsvData_m3EB5F7402FDE1BDA2066B5C9E06CBF426EC91D0B (void);
// 0x000001B6 System.Boolean EventScript/eventTexts::setData(System.String,System.String)
extern void eventTexts_setData_m3685C0D8BBD2610397CDD3AFF0F360157494933B (void);
// 0x000001B7 System.Void EventScript/eventTexts::.ctor()
extern void eventTexts__ctor_m4AD40C807343CB49531A0430A1000A2EBEB460F2 (void);
// 0x000001B8 System.Void EventScript/condition::.ctor()
extern void condition__ctor_m0342C48BA0221B0C17719D8A508C08BDE144DDE1 (void);
// 0x000001B9 System.Void EventScript/C_RndRange::.ctor()
extern void C_RndRange__ctor_m54B2CFA13147E34233E6546EB070E57E88C77263 (void);
// 0x000001BA System.Void EventScript/resultModifier::.ctor()
extern void resultModifier__ctor_m617A0580C2042646F1862FE85C5404B315FCF8FA (void);
// 0x000001BB System.Void EventScript/C_ReducedResultModifier::.ctor()
extern void C_ReducedResultModifier__ctor_m7222DA6CF8A5504BFDD3B840D29EB249CB7C657D (void);
// 0x000001BC System.Void EventScript/resultModifierPreview::.ctor()
extern void resultModifierPreview__ctor_mF464668BBC56BA479CD82578DB2504FBBCD86D70 (void);
// 0x000001BD System.Collections.Generic.List`1<System.String> EventScript/C_AdditionalModifiers::GetTranslatableContent()
extern void C_AdditionalModifiers_GetTranslatableContent_m443C47DF2E782F3407BE9507CFDCFBB11CDCA153 (void);
// 0x000001BE System.Void EventScript/C_AdditionalModifiers::.ctor()
extern void C_AdditionalModifiers__ctor_m7C527696D2FA3DD65A53611BD529F24EC3F21DB2 (void);
// 0x000001BF System.Void EventScript/C_intRange::.ctor()
extern void C_intRange__ctor_m9EFDC904746CF6222AD9B28CBB607BC7900789FA (void);
// 0x000001C0 System.Collections.Generic.List`1<System.String> EventScript/modifierGroup::GetTranslatableContent()
extern void modifierGroup_GetTranslatableContent_m74FDA8DBA66FF96858155011FD9FC9427CDE55D3 (void);
// 0x000001C1 System.Void EventScript/modifierGroup::.ctor()
extern void modifierGroup__ctor_m52471C2A8830B98B21DB890BDC32813110138ECD (void);
// 0x000001C2 System.Collections.Generic.List`1<System.String> EventScript/result::GetTranslatableContent()
extern void result_GetTranslatableContent_mE911429149B32ABBDAE1869AC5EB4D0ADCCE88EE (void);
// 0x000001C3 System.Void EventScript/result::.ctor()
extern void result__ctor_m2ECC8E86B9C4EEA9C1A0FE4325ACD150BE0C46C5 (void);
// 0x000001C4 System.Collections.Generic.List`1<System.String> EventScript/resultGroup::GetTranslatableContent()
extern void resultGroup_GetTranslatableContent_m573EFA55AF510436CBFFDB8599914A68E4EE8B8F (void);
// 0x000001C5 System.Void EventScript/resultGroup::.ctor()
extern void resultGroup__ctor_mE3E648DC91C639B909740F0129314B693CFD154C (void);
// 0x000001C6 System.Void GameDictionary::mDebug(System.String)
extern void GameDictionary_mDebug_m7B58ED48426B42EA43ED29CD99845214E14BD14D (void);
// 0x000001C7 System.Void GameDictionary::executeDictionaryResult(GameDictionary/C_DictionaryChange)
extern void GameDictionary_executeDictionaryResult_m95D4196611B0724615F0D99E0EE45EF195D5C2D3 (void);
// 0x000001C8 System.Boolean GameDictionary::ConditionMet(GameDictionary/C_GameDictionaryCondition)
extern void GameDictionary_ConditionMet_m367813954D6632159DEBD5617CC1269E7C67012A (void);
// 0x000001C9 System.Boolean GameDictionary::ContainsKey(System.String)
extern void GameDictionary_ContainsKey_mF80910F815B03B8F366AA5E1F64FCB8E1D3A857B (void);
// 0x000001CA System.String GameDictionary::GetValue(System.String)
extern void GameDictionary_GetValue_mDFE966DFC349F8C54B9AE442EE6A58308919921B (void);
// 0x000001CB System.Boolean GameDictionary::EqualsValue(System.String,System.String)
extern void GameDictionary_EqualsValue_mFCD9C415137DACA50D80268EA5525E8BC79AE036 (void);
// 0x000001CC System.Void GameDictionary::SetEntry(System.String,System.String)
extern void GameDictionary_SetEntry_m2B5ABAE6F19026662F940E65757CC2AFB637A959 (void);
// 0x000001CD System.Void GameDictionary::StaticClear()
extern void GameDictionary_StaticClear_mFB457F945DBE70D6F40FDCCE43EB7CDFC4C4BBB4 (void);
// 0x000001CE System.Void GameDictionary::Clear()
extern void GameDictionary_Clear_mA7A61E2021608D886F5B611A819C2DE6A9FBEEBF (void);
// 0x000001CF System.Void GameDictionary::load()
extern void GameDictionary_load_m2A7F473FB0B8FED099AEFCA193C63EDE53A10D92 (void);
// 0x000001D0 System.Void GameDictionary::loadIfnecessary()
extern void GameDictionary_loadIfnecessary_m06BAFE765784CBE5CB990C5B99C940A129F8C70B (void);
// 0x000001D1 System.Void GameDictionary::save()
extern void GameDictionary_save_m36E32195215DB39F69D3A48A8B021A99658B2D3D (void);
// 0x000001D2 System.Void GameDictionary::.ctor()
extern void GameDictionary__ctor_m03EB6D629983C06FC9BE3D4B3C7E4FD1B5626566 (void);
// 0x000001D3 System.Void GameDictionary::.cctor()
extern void GameDictionary__cctor_m27C7F69582CA3EBD0B54D2B44C63990D475FF46D (void);
// 0x000001D4 System.Void GameDictionary/C_DictionaryChange::.ctor()
extern void C_DictionaryChange__ctor_m20A44DD3E263F8E92F5600A3F39BB82A9907C733 (void);
// 0x000001D5 System.Void GameDictionary/C_DictWrapper::.ctor(GameDictionary/C_DictWrapper)
extern void C_DictWrapper__ctor_m90C683099CD19037E4B9DB310611B30778EDFDB8 (void);
// 0x000001D6 System.Void GameDictionary/C_DictWrapper::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void C_DictWrapper__ctor_mAC7477DEA79F04ECC361AB555535544CD744EB3E (void);
// 0x000001D7 System.Void GameDictionary/C_DictWrapper::.ctor()
extern void C_DictWrapper__ctor_m48B58656F40F76B71CB69D170E367B1D83D9404C (void);
// 0x000001D8 System.String GameDictionary/C_DictWrapper::ToString()
extern void C_DictWrapper_ToString_mFCFC850A794B30E4A2311A5800F78D5465813087 (void);
// 0x000001D9 System.Void GameDictionary/C_keyValuePair::.ctor(System.String,System.String)
extern void C_keyValuePair__ctor_m3F9257C96CD8BC0BB9F39997EC8B917570AFB950 (void);
// 0x000001DA System.Collections.Generic.Dictionary`2<System.String,System.String> GameDictionary/C_KeyValueArray::ToDictionary()
extern void C_KeyValueArray_ToDictionary_m872863D676FA93C7D9E8AFDD0154728E6343A252 (void);
// 0x000001DB GameDictionary/C_KeyValueArray GameDictionary/C_KeyValueArray::ToKeyValuePairs(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void C_KeyValueArray_ToKeyValuePairs_m790487B860E050599FA927B4F35CFE01A6C809AC (void);
// 0x000001DC System.String GameDictionary/C_KeyValueArray::ToString()
extern void C_KeyValueArray_ToString_m1C783A006E9479D64571918DA138471B724F02B4 (void);
// 0x000001DD System.Void GameDictionary/C_KeyValueArray::.ctor()
extern void C_KeyValueArray__ctor_m699B09A2A9D1706877DFE3E6C648169693EAD7FE (void);
// 0x000001DE System.Void GameDictionary/C_GameDictionaryCondition::.ctor()
extern void C_GameDictionaryCondition__ctor_m7EABAE475AAA4F45AA3547795684D316E690B052 (void);
// 0x000001DF System.Void GameDictionary_InputField::Start()
extern void GameDictionary_InputField_Start_m9797F0EF5EF6478E6F10E56E3390F1D0B9096EB9 (void);
// 0x000001E0 System.Boolean GameDictionary_InputField::SaveEntry()
extern void GameDictionary_InputField_SaveEntry_m4EB43E1355535454A43356293CA5660634BD2EE9 (void);
// 0x000001E1 System.Void GameDictionary_InputField::Save()
extern void GameDictionary_InputField_Save_m33E7F9E5E23DF7564F7D1AFF3410330439D4FEFB (void);
// 0x000001E2 System.Void GameDictionary_InputField::OnCardDestroy()
extern void GameDictionary_InputField_OnCardDestroy_m148F1228689E714C067F13DFD0C99C9DED64CD82 (void);
// 0x000001E3 System.Void GameDictionary_InputField::.ctor()
extern void GameDictionary_InputField__ctor_m57F9F411AD1D4B93D1C1CE553FAC85A820B1967E (void);
// 0x000001E4 System.String GameLogger::getGameLog(System.Int32)
extern void GameLogger_getGameLog_m8CD1D3FDE6DA0832404F9FC06B69F201842FF3EF (void);
// 0x000001E5 System.String GameLogger::getGameLog(GameLogger/E_SubLogTarget)
extern void GameLogger_getGameLog_mE807897B6C632A91990781FB2C0B7C7167DB0B8C (void);
// 0x000001E6 System.Int32 GameLogger::subLogEnumToInt(GameLogger/E_SubLogTarget)
extern void GameLogger_subLogEnumToInt_mEC0F8631B81D34D7334A870A9D3EFC3209CB4863 (void);
// 0x000001E7 System.Void GameLogger::showGameLogUI(System.Int32)
extern void GameLogger_showGameLogUI_mC6B24146D3DB080CCDD2D2EF762628C1E5B6AC0E (void);
// 0x000001E8 System.String GameLogger::buildResultText(System.Int32)
extern void GameLogger_buildResultText_m1C67C6531503F650B9DF05B749CC30466CA6317D (void);
// 0x000001E9 System.Void GameLogger::Awake()
extern void GameLogger_Awake_m4A3F41558F55B980AB0971A786A08691B12BE5B9 (void);
// 0x000001EA System.Void GameLogger::Start()
extern void GameLogger_Start_m1BF8A4AACD45A939175D356EF28806A6D0041030 (void);
// 0x000001EB System.Boolean GameLogger::loadGameLogs()
extern void GameLogger_loadGameLogs_m0D674383B1A9F8B995F14779C7553E8E1B90D711 (void);
// 0x000001EC System.Boolean GameLogger::loadSplitGameLogs()
extern void GameLogger_loadSplitGameLogs_mE988EEA239215E1310BF0C620ED679C77CAF48B9 (void);
// 0x000001ED System.Void GameLogger::lockOutput(System.Boolean)
extern void GameLogger_lockOutput_m00E947F13EAC708A56124A7948DF829D6B2DFDE3 (void);
// 0x000001EE System.Void GameLogger::saveGameLogs()
extern void GameLogger_saveGameLogs_m8DCE667047D123E5AD818211BA4EA215FDA89CF7 (void);
// 0x000001EF System.Void GameLogger::addGameLog(System.String,GameLogger/E_SubLogTarget)
extern void GameLogger_addGameLog_m7A3108FC6FEAE8B94B09A555D7E34A0B913C5608 (void);
// 0x000001F0 System.Void GameLogger::addGameLog(System.String,System.Int32)
extern void GameLogger_addGameLog_mA3D587D4297E195201F4833A2E9CE2710DB145BA (void);
// 0x000001F1 System.Void GameLogger::clearGameLog()
extern void GameLogger_clearGameLog_mD9719464CB4677BA5C9EE1561E0AC73FD7F4385D (void);
// 0x000001F2 System.Collections.Generic.List`1<System.String> GameLogger::getTranslatableTerms()
extern void GameLogger_getTranslatableTerms_m4D5CF8BDBD4012CDFB6C6EED698ECA9A9C1DC63A (void);
// 0x000001F3 System.Void GameLogger::.ctor()
extern void GameLogger__ctor_mBC481E37C7D240349A089181935E3E571988E20A (void);
// 0x000001F4 System.Void GameLogger/C_InspectorGameLogEntry::addGameLogText(System.String)
extern void C_InspectorGameLogEntry_addGameLogText_mEBA96F6A17BDB278A9EE3A94769DDF2BC9EBDF64 (void);
// 0x000001F5 System.Collections.Generic.List`1<System.String> GameLogger/C_InspectorGameLogEntry::GetTranslatableContent()
extern void C_InspectorGameLogEntry_GetTranslatableContent_m288D6657BECC8521EA27A3E5BF9A0B577A00E8AF (void);
// 0x000001F6 System.Void GameLogger/C_InspectorGameLogEntry::.ctor()
extern void C_InspectorGameLogEntry__ctor_m9E242769CDF63ECC40C9E79CFF75CDBFD73D956D (void);
// 0x000001F7 System.Void GameLogger/strList::.ctor()
extern void strList__ctor_m2939C6DBF6BEF9CA0831EEDB04F91139248B4BD3 (void);
// 0x000001F8 System.Void GameLogger/C_encapsulatedStringList::.ctor()
extern void C_encapsulatedStringList__ctor_mD79A2C5367A1DB9559FB10D59B9EDD29709879BB (void);
// 0x000001F9 System.Void GameLogger/C_SplitLogs::.ctor()
extern void C_SplitLogs__ctor_m9634F63301AE548B06945EA0DD0A5043B1D4DAF7 (void);
// 0x000001FA System.Void GameStateManager::loadGameState()
extern void GameStateManager_loadGameState_mCE9AD4B728BD9F37B5DDA811C82536975603F497 (void);
// 0x000001FB System.Void GameStateManager::saveGameState()
extern void GameStateManager_saveGameState_mE918049CCF3154262C93FC2855BBFF613371703B (void);
// 0x000001FC System.Void GameStateManager::Awake()
extern void GameStateManager_Awake_m26F6FEFEBFCEF4B2E123D7E8F6CA0B75E852B46A (void);
// 0x000001FD System.Void GameStateManager::Start()
extern void GameStateManager_Start_m3E1D199E7F0D41A1A460F85D3D18FFA06C35BB75 (void);
// 0x000001FE System.Collections.IEnumerator GameStateManager::OneFrameDelayStartup()
extern void GameStateManager_OneFrameDelayStartup_m4224DC7C44D02F4F4D8C61CF79B3F0563D1EA193 (void);
// 0x000001FF System.Void GameStateManager::GameStartup()
extern void GameStateManager_GameStartup_mB877BD0EB531524E220573135F11A36AC69556DE (void);
// 0x00000200 System.Void GameStateManager::executeGameover()
extern void GameStateManager_executeGameover_mA3779D91F37F9670D18E2E27C7F9AB206AD7B26B (void);
// 0x00000201 System.Void GameStateManager::swipe()
extern void GameStateManager_swipe_mF7F2FE9E66B2BA266D55D2BAE64934AA2E909A86 (void);
// 0x00000202 System.Void GameStateManager::StartGame()
extern void GameStateManager_StartGame_mCBC897B251682305CF7130C1E77F14EAD7E8DDAB (void);
// 0x00000203 System.Void GameStateManager::OnDestroy()
extern void GameStateManager_OnDestroy_mE50309001DAA681328AA856222484586EC1118FF (void);
// 0x00000204 System.Void GameStateManager::.ctor()
extern void GameStateManager__ctor_mBFB14F943EA52DBFDDCDD87A5580B0E19B9A6E8C (void);
// 0x00000205 System.Void GameStateManager/mEvent::.ctor()
extern void mEvent__ctor_mAFEBD706A00B6C0A587590E4A12886BC4EC4E59F (void);
// 0x00000206 System.Void GameStateManager/<OneFrameDelayStartup>d__9::.ctor(System.Int32)
extern void U3COneFrameDelayStartupU3Ed__9__ctor_mFA4D7E5CB4F7D1FCAE440E17731FC79FE6463229 (void);
// 0x00000207 System.Void GameStateManager/<OneFrameDelayStartup>d__9::System.IDisposable.Dispose()
extern void U3COneFrameDelayStartupU3Ed__9_System_IDisposable_Dispose_mEDF62ED725AEC84B0623405D0629C09AADA5575F (void);
// 0x00000208 System.Boolean GameStateManager/<OneFrameDelayStartup>d__9::MoveNext()
extern void U3COneFrameDelayStartupU3Ed__9_MoveNext_mFDD42A5B9DBBF29CD565B1C0B13C7F8D0E79D044 (void);
// 0x00000209 System.Object GameStateManager/<OneFrameDelayStartup>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COneFrameDelayStartupU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD01966B1BEF4FBB7C85B3A95CF46DC5E3C3FEC1 (void);
// 0x0000020A System.Void GameStateManager/<OneFrameDelayStartup>d__9::System.Collections.IEnumerator.Reset()
extern void U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_Reset_mA3924A3373CF381CFB79BB3C5997F6D3D753E996 (void);
// 0x0000020B System.Object GameStateManager/<OneFrameDelayStartup>d__9::System.Collections.IEnumerator.get_Current()
extern void U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_get_Current_m72A635E851E0F7F4EC96AA006D5450B5F893E29F (void);
// 0x0000020C System.Void GenderGenerator::Awake()
extern void GenderGenerator_Awake_m27987D96ED61EC638DFC8882C16D2F16B9A724DA (void);
// 0x0000020D System.Void GenderGenerator::Start()
extern void GenderGenerator_Start_mE39BC39E19AC2D80A332F7D12AD1C4B19771D91B (void);
// 0x0000020E System.Collections.IEnumerator GenderGenerator::frameDelay()
extern void GenderGenerator_frameDelay_mD70AC765071A2E2AC81EBF1F46466AC95CCECD0E (void);
// 0x0000020F System.Void GenderGenerator::clearUI()
extern void GenderGenerator_clearUI_mF69E2AB59EF61A72391860339696370A57EE8544 (void);
// 0x00000210 System.Void GenderGenerator::actualizeUI()
extern void GenderGenerator_actualizeUI_m3C76A622F2E9998B5EAD07745389C0BFA797F446 (void);
// 0x00000211 System.String GenderGenerator::getGenderText()
extern void GenderGenerator_getGenderText_m6D6F19EB4BCE0001551EF470BB8A76B1C187AA30 (void);
// 0x00000212 UnityEngine.Sprite GenderGenerator::getGenderSprite()
extern void GenderGenerator_getGenderSprite_m2ECB3DF9B0804410AA0C17221834DAFB6922F7C3 (void);
// 0x00000213 System.Collections.Generic.List`1<System.String> GenderGenerator::getTranslatableTerms()
extern void GenderGenerator_getTranslatableTerms_m8D5C70479133F33DBBC9806DF5785C7F6362A91F (void);
// 0x00000214 System.Void GenderGenerator::.ctor()
extern void GenderGenerator__ctor_m91332D6A1076C4909679B974B710EAB33474513E (void);
// 0x00000215 System.Void GenderGenerator/gendStringList::.ctor()
extern void gendStringList__ctor_m95524946BABCF8E207FB8FB5AEDF0B419F1DECB3 (void);
// 0x00000216 System.Void GenderGenerator/<frameDelay>d__5::.ctor(System.Int32)
extern void U3CframeDelayU3Ed__5__ctor_m3D601DC493336F760EA9D07947A8848668774822 (void);
// 0x00000217 System.Void GenderGenerator/<frameDelay>d__5::System.IDisposable.Dispose()
extern void U3CframeDelayU3Ed__5_System_IDisposable_Dispose_m61ED0D2F306AD93BA1D6B09AB601CED46FFD40B6 (void);
// 0x00000218 System.Boolean GenderGenerator/<frameDelay>d__5::MoveNext()
extern void U3CframeDelayU3Ed__5_MoveNext_m3531EDDF5B6C8961D26E92D00495E82D1EBBE9BE (void);
// 0x00000219 System.Object GenderGenerator/<frameDelay>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CframeDelayU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m508D08CD2E72E437AA970E975F0FDFDA49362A35 (void);
// 0x0000021A System.Void GenderGenerator/<frameDelay>d__5::System.Collections.IEnumerator.Reset()
extern void U3CframeDelayU3Ed__5_System_Collections_IEnumerator_Reset_mF2CA8CAE3AC8A3F88666C84089B11B51F2FA7361 (void);
// 0x0000021B System.Object GenderGenerator/<frameDelay>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CframeDelayU3Ed__5_System_Collections_IEnumerator_get_Current_m289933883DE423AE7BCD2897BA8EB82EB19946AB (void);
// 0x0000021C System.Void HighScoreNameLinker::Awake()
extern void HighScoreNameLinker_Awake_m6B8C50DB1332A19A165B0E3F8BCDF3F366EC1E27 (void);
// 0x0000021D System.Void HighScoreNameLinker::Start()
extern void HighScoreNameLinker_Start_m0B3C6C26520398135B71490C4139CF51FD429F1F (void);
// 0x0000021E System.Collections.IEnumerator HighScoreNameLinker::delayed()
extern void HighScoreNameLinker_delayed_m3D64F23F54557193F3753E010799BF317CF2E2C7 (void);
// 0x0000021F System.Void HighScoreNameLinker::getVSscript()
extern void HighScoreNameLinker_getVSscript_mBDC2E521A6F695BCD2038FF91F46A2D9E9F1F065 (void);
// 0x00000220 System.Void HighScoreNameLinker::generateSaveKey()
extern void HighScoreNameLinker_generateSaveKey_m5188D46AAA97BB521243C588E194D5705BBBF8BB (void);
// 0x00000221 System.Void HighScoreNameLinker::displayHighScorePair()
extern void HighScoreNameLinker_displayHighScorePair_m5D32461C118D978D714BF2088868CF4BB7BF12C7 (void);
// 0x00000222 System.Void HighScoreNameLinker::clearUI()
extern void HighScoreNameLinker_clearUI_mE3E669BA1B04F1C9A93D41448DB4F570F2C154F3 (void);
// 0x00000223 System.Void HighScoreNameLinker::save()
extern void HighScoreNameLinker_save_m94BA1E179C8E033FB984BBA1E89D635FA2F5A389 (void);
// 0x00000224 System.Void HighScoreNameLinker::load()
extern void HighScoreNameLinker_load_m11A25767F812D70E0A7FBF6E6718AE8F9AA0FBDD (void);
// 0x00000225 System.Void HighScoreNameLinker::generateHighScoreNameLink()
extern void HighScoreNameLinker_generateHighScoreNameLink_mA167C4B972D4F9139EC36116D6E7A8B55A4CFD38 (void);
// 0x00000226 System.Int32 HighScoreNameLinker::SortByHigherScore(HighScoreNameLinker/highScoreNamePair,HighScoreNameLinker/highScoreNamePair)
extern void HighScoreNameLinker_SortByHigherScore_m048C503759EA72FE3F8AC41D6067AB7C36DD71CC (void);
// 0x00000227 System.Int32 HighScoreNameLinker::SortByLowerScore(HighScoreNameLinker/highScoreNamePair,HighScoreNameLinker/highScoreNamePair)
extern void HighScoreNameLinker_SortByLowerScore_mDB80D4A109F8A9296DC5E9DEF4FED60D87D59E77 (void);
// 0x00000228 System.Void HighScoreNameLinker::.ctor()
extern void HighScoreNameLinker__ctor_mE3B11BA7D7FC98EB6B8B9A2FEB1FA4B722C4660E (void);
// 0x00000229 System.Void HighScoreNameLinker/highScoreNamePair::.ctor()
extern void highScoreNamePair__ctor_mA792849946C60DE01560E243AD72EB3DE7992238 (void);
// 0x0000022A System.Void HighScoreNameLinker/C_cap::.ctor()
extern void C_cap__ctor_mB544A753B5E612B2BE667D82FA4B28A023ECD63F (void);
// 0x0000022B System.Void HighScoreNameLinker/C_highscoreFields::.ctor()
extern void C_highscoreFields__ctor_m1B688D8CF68DE56B8AC16D34E0F8888F23A2A8B4 (void);
// 0x0000022C System.Void HighScoreNameLinker/<delayed>d__15::.ctor(System.Int32)
extern void U3CdelayedU3Ed__15__ctor_m0918B7118476E3C20CF0E8D758735D04E1FDED1E (void);
// 0x0000022D System.Void HighScoreNameLinker/<delayed>d__15::System.IDisposable.Dispose()
extern void U3CdelayedU3Ed__15_System_IDisposable_Dispose_mE0DB7048627B25283EAB909051B30A8D4FF6224D (void);
// 0x0000022E System.Boolean HighScoreNameLinker/<delayed>d__15::MoveNext()
extern void U3CdelayedU3Ed__15_MoveNext_mB95273618B41E2C82E85788FB49A6A5E2D6458F2 (void);
// 0x0000022F System.Object HighScoreNameLinker/<delayed>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3544EE0B3D2BCE81E12BC0A9D3CF43A2DAAA781E (void);
// 0x00000230 System.Void HighScoreNameLinker/<delayed>d__15::System.Collections.IEnumerator.Reset()
extern void U3CdelayedU3Ed__15_System_Collections_IEnumerator_Reset_mA0A6E89CDEFFD236B3CF585DB50FB39703C45A7C (void);
// 0x00000231 System.Object HighScoreNameLinker/<delayed>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CdelayedU3Ed__15_System_Collections_IEnumerator_get_Current_mF80795299D99484AE4CD163215543FDCC91855CE (void);
// 0x00000232 System.Void HighScoreNameLinkerGroup::Awake()
extern void HighScoreNameLinkerGroup_Awake_m1BD4CA8E6F4115AF23CED1FCE13A4C3D3A0BDDA2 (void);
// 0x00000233 System.Void HighScoreNameLinkerGroup::Start()
extern void HighScoreNameLinkerGroup_Start_m52202319C46806DD73A1BEBF5B2B00F1420FAEC0 (void);
// 0x00000234 System.Void HighScoreNameLinkerGroup::generateLinks()
extern void HighScoreNameLinkerGroup_generateLinks_mCE54CC6DBAE99FA14FCE82A02D0E48C01F3A05BA (void);
// 0x00000235 System.Void HighScoreNameLinkerGroup::Update()
extern void HighScoreNameLinkerGroup_Update_m9D3DD7AE853316E5CD55AC47B579DE9D5C264F0F (void);
// 0x00000236 System.Void HighScoreNameLinkerGroup::.ctor()
extern void HighScoreNameLinkerGroup__ctor_mC79556126E4C9D5A2A4133B4C8E55A41A76D48D9 (void);
// 0x00000237 System.Void InfoDisplay::Start()
extern void InfoDisplay_Start_m8A2591F2B58520A40EFA4864F44FE390CCC8D57D (void);
// 0x00000238 System.Collections.IEnumerator InfoDisplay::cyclicIconCollector()
extern void InfoDisplay_cyclicIconCollector_mE8F77F9BEBD5496FEF8766E4762BE9C9636F150A (void);
// 0x00000239 System.Void InfoDisplay::startAnimationIfNotEmpty()
extern void InfoDisplay_startAnimationIfNotEmpty_m16AD68A28E7BF6208AC07AFF9FADD8D713E85449 (void);
// 0x0000023A System.Void InfoDisplay::addDisplay(UnityEngine.Sprite,System.Single)
extern void InfoDisplay_addDisplay_mB3F74FECA3C337EB5578585075E357F16DDE8656 (void);
// 0x0000023B System.Void InfoDisplay::fillDisplayElements()
extern void InfoDisplay_fillDisplayElements_m63ABCA4EEC6EAFEA788786DECCBB11A9CD5C7E4B (void);
// 0x0000023C System.Void InfoDisplay::clearDisplay()
extern void InfoDisplay_clearDisplay_m3F093403261BE3FFC986D8ED962830B55E5E9F4D (void);
// 0x0000023D System.Void InfoDisplay::.ctor()
extern void InfoDisplay__ctor_m4EC165607BF3AA4A524436EA7B28277376F38303 (void);
// 0x0000023E System.Void InfoDisplay/valueSprite::.ctor()
extern void valueSprite__ctor_m8FAB52347880428D39DB8178713D5477F2E216F5 (void);
// 0x0000023F System.Void InfoDisplay/displaySlot::.ctor()
extern void displaySlot__ctor_m8F36A911DF6FC7D4AE65FFFF5799425F2612747A (void);
// 0x00000240 System.Void InfoDisplay/<cyclicIconCollector>d__2::.ctor(System.Int32)
extern void U3CcyclicIconCollectorU3Ed__2__ctor_m9220A985B72F928F75C0CAFBE073745B9E441881 (void);
// 0x00000241 System.Void InfoDisplay/<cyclicIconCollector>d__2::System.IDisposable.Dispose()
extern void U3CcyclicIconCollectorU3Ed__2_System_IDisposable_Dispose_mFB4B957CA312D8B9041E3DE37A56A4AF017327BA (void);
// 0x00000242 System.Boolean InfoDisplay/<cyclicIconCollector>d__2::MoveNext()
extern void U3CcyclicIconCollectorU3Ed__2_MoveNext_mD098EA5DD6E28552DC5D3017E6412A18627FDED7 (void);
// 0x00000243 System.Object InfoDisplay/<cyclicIconCollector>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcyclicIconCollectorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m303A52A720B7B60184234F81392918ED44B068BE (void);
// 0x00000244 System.Void InfoDisplay/<cyclicIconCollector>d__2::System.Collections.IEnumerator.Reset()
extern void U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_Reset_m2841F544A2EFED8EE58B4425C78555D50A12453F (void);
// 0x00000245 System.Object InfoDisplay/<cyclicIconCollector>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_get_Current_mEBA04BC302FA19463B5072EFF2D48AEED7B86B2E (void);
// 0x00000246 T[] JsonHelper::getJsonArray(System.String)
// 0x00000247 System.String JsonHelper::arrayToJson(T[])
// 0x00000248 System.Void JsonHelper::.ctor()
extern void JsonHelper__ctor_mD3FCC876B35655F0018C89924306B1023F6FBAB6 (void);
// 0x00000249 System.Void JsonHelper/Wrapper`1::.ctor()
// 0x0000024A System.Void KeyboardToEvent::Update()
extern void KeyboardToEvent_Update_m75BDCADA7D872CB72169977EFCAF19D0DF3AA6EF (void);
// 0x0000024B System.Void KeyboardToEvent::.ctor()
extern void KeyboardToEvent__ctor_m6D42BA03E60087722BF6E6EDABC325242ECDFE03 (void);
// 0x0000024C System.Void KeyboardToEvent/mEvent::.ctor()
extern void mEvent__ctor_m1540FC08A4FE82844355C65BE8B79A74BA28B4DE (void);
// 0x0000024D System.Void KeyboardToEvent/C_KeyEvent::.ctor()
extern void C_KeyEvent__ctor_m05CA1B879A7E7281952876192F66157F37B9623A (void);
// 0x0000024E System.Void KingsCardStyle::.ctor()
extern void KingsCardStyle__ctor_m4EA7A34A71084473E04608823B62BE6E5A523FDB (void);
// 0x0000024F System.Boolean KingsCardStyleList::HasStyle(System.String)
extern void KingsCardStyleList_HasStyle_m4273E9971214A97C8DCA921C4D0A61A286EE16B1 (void);
// 0x00000250 System.String KingsCardStyleList::GetCardStyleDefinitionErrors()
extern void KingsCardStyleList_GetCardStyleDefinitionErrors_m92C7281CCC82BB67CA2C819F40B96471DBFECA72 (void);
// 0x00000251 KingsCardStyle KingsCardStyleList::GetStyle(System.String)
extern void KingsCardStyleList_GetStyle_mB7034AA9CE66D3103E59807EA7E191825530F8CA (void);
// 0x00000252 System.Boolean KingsCardStyleList::GetOverwriteStyle(System.String)
extern void KingsCardStyleList_GetOverwriteStyle_m1D716C53673E07051418DE9ECA9E363154E400BD (void);
// 0x00000253 System.Void KingsCardStyleList::.ctor()
extern void KingsCardStyleList__ctor_m76D7738B86D166646454ACF36BD4E8F6CAE4DE48 (void);
// 0x00000254 System.Void KingsCardStyleList/C_CardStyleNamePair::.ctor()
extern void C_CardStyleNamePair__ctor_mD7B385766BB3E9749EEE8BB391DD09040B178CAB (void);
// 0x00000255 System.Void KingsLevelUp::Awake()
extern void KingsLevelUp_Awake_m605DF0FA642C24368BB265069B555F9A803D21A1 (void);
// 0x00000256 System.Void KingsLevelUp::OnDestroy()
extern void KingsLevelUp_OnDestroy_m5E0535B90FE8DC4E1A2BF135112333C0996A7106 (void);
// 0x00000257 System.Void KingsLevelUp::TestXpCostList()
extern void KingsLevelUp_TestXpCostList_m7AD2DF6F865B3A8D57E9C69B82413B0721DA6273 (void);
// 0x00000258 System.Single KingsLevelUp::GetXpForNextLevel(System.Int32)
extern void KingsLevelUp_GetXpForNextLevel_m41287BB1AA74C4C4F7C63CC1F23B84BF8F0E0D53 (void);
// 0x00000259 System.Void KingsLevelUp::computeLevelChange()
extern void KingsLevelUp_computeLevelChange_mAF450EDCF61F79E857570E262BDC4358F67A0408 (void);
// 0x0000025A System.Single KingsLevelUp::getXpBarFilling()
extern void KingsLevelUp_getXpBarFilling_mC3600B8A576909053513AA0CBD6D83D21C4E39C2 (void);
// 0x0000025B System.Int32 KingsLevelUp::getUiIncreasingLevelNumber()
extern void KingsLevelUp_getUiIncreasingLevelNumber_m098082BFD89912373ECD37895DF257FEB3D471A7 (void);
// 0x0000025C System.Collections.IEnumerator KingsLevelUp::cyclicFillingComputations()
extern void KingsLevelUp_cyclicFillingComputations_mFC40CFAFBC0B13EF636CA12077EE61318CFC952C (void);
// 0x0000025D System.Void KingsLevelUp::actualizeUI()
extern void KingsLevelUp_actualizeUI_mB4AC88C7FF8760C2A819E7C53DCE43F7BFD2B493 (void);
// 0x0000025E System.Void KingsLevelUp::AddXp(System.Single)
extern void KingsLevelUp_AddXp_m0DC049A6CF2C47554FE9BD320C68161DCCD87F74 (void);
// 0x0000025F System.Void KingsLevelUp::save()
extern void KingsLevelUp_save_mE1F36408CC4C1BC5CA3A56C2A5C79DDD9A7C0652 (void);
// 0x00000260 System.Void KingsLevelUp::load()
extern void KingsLevelUp_load_mC7FDE6EAC383058A3D1F5ADB5E5D2F5DFA108FA5 (void);
// 0x00000261 System.Void KingsLevelUp::.ctor()
extern void KingsLevelUp__ctor_m994FF3DC7883463FBA9636A9794D2C30C1AF8F65 (void);
// 0x00000262 System.Void KingsLevelUp/mEvent::.ctor()
extern void mEvent__ctor_m271362D1DC1F31C5C1288B4CD23B699A2775DBA1 (void);
// 0x00000263 System.Void KingsLevelUp/C_LevelUpStats::.ctor()
extern void C_LevelUpStats__ctor_mF33DF9347BEA2EEDB83B7B4617DCC272812CD387 (void);
// 0x00000264 System.Void KingsLevelUp/C_XpPerLevel::.ctor()
extern void C_XpPerLevel__ctor_m04CA46A7E3BF84E1CB188543883B3304EB713C03 (void);
// 0x00000265 System.Void KingsLevelUp/C_LevelUpUiConfig::.ctor()
extern void C_LevelUpUiConfig__ctor_m074BDA0391C9295052B7FF2217618A49275F2521 (void);
// 0x00000266 System.Void KingsLevelUp/C_LevelUpEvents::.ctor()
extern void C_LevelUpEvents__ctor_m9736B63C24882D569E903AA45FD64F2A4AF2B321 (void);
// 0x00000267 System.Void KingsLevelUp/C_ValueSave::.ctor()
extern void C_ValueSave__ctor_mFD6785631A9DBF267B03AFE28E849DA75FC07AAC (void);
// 0x00000268 System.Void KingsLevelUp/<cyclicFillingComputations>d__21::.ctor(System.Int32)
extern void U3CcyclicFillingComputationsU3Ed__21__ctor_mFEB671CD9B6AB385063CB85F9EDD2FCF66CFBE5A (void);
// 0x00000269 System.Void KingsLevelUp/<cyclicFillingComputations>d__21::System.IDisposable.Dispose()
extern void U3CcyclicFillingComputationsU3Ed__21_System_IDisposable_Dispose_m44C5F2035DDB457E94FD9376930D8D740C577C88 (void);
// 0x0000026A System.Boolean KingsLevelUp/<cyclicFillingComputations>d__21::MoveNext()
extern void U3CcyclicFillingComputationsU3Ed__21_MoveNext_m04A5FD1CEF6CE0B3CDB9E42F7C64C1857D46BE65 (void);
// 0x0000026B System.Object KingsLevelUp/<cyclicFillingComputations>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcyclicFillingComputationsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9342F3240439AB5371B1AB414C9AD0438B44577D (void);
// 0x0000026C System.Void KingsLevelUp/<cyclicFillingComputations>d__21::System.Collections.IEnumerator.Reset()
extern void U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_Reset_mF95A76D31F44E278584ADBD7A02631EE22024239 (void);
// 0x0000026D System.Object KingsLevelUp/<cyclicFillingComputations>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_get_Current_m59ECC582967406007EAC87DB871D8FED4E230955 (void);
// 0x0000026E System.Void KingsTypewriter::set_text(System.String)
extern void KingsTypewriter_set_text_m003BEA7E8EC169FA8D5723673BCBE839DD8FD98F (void);
// 0x0000026F System.String KingsTypewriter::get_text()
extern void KingsTypewriter_get_text_mD92B6F10367E4D8BBCDE820CC36E6CAA217E9807 (void);
// 0x00000270 System.Void KingsTypewriter::Start()
extern void KingsTypewriter_Start_m79B3609E9E5AB71DC2CD0A8C0B37A6B5D8B09EFD (void);
// 0x00000271 System.Void KingsTypewriter::FinishTypewriting()
extern void KingsTypewriter_FinishTypewriting_mBA185FDBD961AA07F8AB166678C6677325A127C3 (void);
// 0x00000272 System.Void KingsTypewriter::RestartTypewriting()
extern void KingsTypewriter_RestartTypewriting_m6BDB5151CA320BA2853907A62DD5B7D1E84E0290 (void);
// 0x00000273 System.Collections.IEnumerator KingsTypewriter::typeText()
extern void KingsTypewriter_typeText_m368EB8071A25C0E0E88D3D13C89D7FF35B43E5D6 (void);
// 0x00000274 System.Void KingsTypewriter::actualizeTextfield(System.String)
extern void KingsTypewriter_actualizeTextfield_m32FA491B5E4A5AFD2F68968A76A5567F6634CC11 (void);
// 0x00000275 System.Void KingsTypewriter::.ctor()
extern void KingsTypewriter__ctor_mD979E8519B5C8319530AEC8E35EF55607AA93E1E (void);
// 0x00000276 System.Void KingsTypewriter/mEvent::.ctor()
extern void mEvent__ctor_m117099E5273EE7CDFD17809A324AE5A7A8B8D1C4 (void);
// 0x00000277 System.Void KingsTypewriter/<typeText>d__17::.ctor(System.Int32)
extern void U3CtypeTextU3Ed__17__ctor_m316FCE9CF3C7CB715AAA1E003D8F0EFB00A03D0E (void);
// 0x00000278 System.Void KingsTypewriter/<typeText>d__17::System.IDisposable.Dispose()
extern void U3CtypeTextU3Ed__17_System_IDisposable_Dispose_m6608A8CA82E96F5255878BAE12497DE488C43FCD (void);
// 0x00000279 System.Boolean KingsTypewriter/<typeText>d__17::MoveNext()
extern void U3CtypeTextU3Ed__17_MoveNext_m12D2F7052D840F43C97B5A84B25B562454673BD4 (void);
// 0x0000027A System.Object KingsTypewriter/<typeText>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtypeTextU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2B057A114FA28DE212251036952D43402AC45E0 (void);
// 0x0000027B System.Void KingsTypewriter/<typeText>d__17::System.Collections.IEnumerator.Reset()
extern void U3CtypeTextU3Ed__17_System_Collections_IEnumerator_Reset_mE733F3418C0E5CB7B1F92929A9734C0EA3FD9EBE (void);
// 0x0000027C System.Object KingsTypewriter/<typeText>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CtypeTextU3Ed__17_System_Collections_IEnumerator_get_Current_mC3ED16AA3344A28C0765D09F95EACD089DD6A91C (void);
// 0x0000027D System.Void LevelUpDisplay::Update()
extern void LevelUpDisplay_Update_m6ED94808844DF65D51099043AA947627AE090B3E (void);
// 0x0000027E System.Void LevelUpDisplay::.ctor()
extern void LevelUpDisplay__ctor_m18A45B875CE77C68092BD9180A9AC2FC4CD7EDB3 (void);
// 0x0000027F System.Void MakeChanges::Start()
extern void MakeChanges_Start_m3D032A5328BF8A77004F45B722D84C813C4C9CBB (void);
// 0x00000280 System.Void MakeChanges::ExecuteEffect()
extern void MakeChanges_ExecuteEffect_m36E1BBADAEABBEFB312BCBDC50A99F94DE92AD4E (void);
// 0x00000281 System.Collections.Generic.List`1<System.String> MakeChanges::GetTranslatableContent()
extern void MakeChanges_GetTranslatableContent_mA5AAF874F057B0BC2035C1F84D23A6DED60331C1 (void);
// 0x00000282 System.Collections.Generic.List`1<System.String> MakeChanges::getTranslatableTerms()
extern void MakeChanges_getTranslatableTerms_mBD3CF195F5F253A061C457B9D94D9549666A1F83 (void);
// 0x00000283 System.Void MakeChanges::.ctor()
extern void MakeChanges__ctor_m60D1291D43A17CB75518E990A6D7FA81CD9B1605 (void);
// 0x00000284 System.Single MathExtension::linearInterpolate(System.Single,System.Single,System.Single)
extern void MathExtension_linearInterpolate_m7AAFE3CF4383C746B771F6FAE750AC32FD568DCB (void);
// 0x00000285 System.Void MathExtension::.ctor()
extern void MathExtension__ctor_m149FA63BE9EF015B670BE22CD1ED2AC217D5DF11 (void);
// 0x00000286 System.Void Mobile_Leaderboard::mDebug(System.String)
extern void Mobile_Leaderboard_mDebug_m5FECF449AB006B01300AFD1E1750EDB6A3F4EC4C (void);
// 0x00000287 System.Void Mobile_Leaderboard::setAchievmentText(System.String)
extern void Mobile_Leaderboard_setAchievmentText_m6D669A60F7C6C05CD132D47554F8B364C1096871 (void);
// 0x00000288 System.Void Mobile_Leaderboard::OnEnable()
extern void Mobile_Leaderboard_OnEnable_m8D02F96473452F5B5BAED3FF90F7D37BA9D53FCE (void);
// 0x00000289 System.Collections.IEnumerator Mobile_Leaderboard::sozialActivations()
extern void Mobile_Leaderboard_sozialActivations_m670202D247ACB1CB9A2F5E886948A355E4A8B55E (void);
// 0x0000028A System.Void Mobile_Leaderboard::Awake()
extern void Mobile_Leaderboard_Awake_m34081276CE6D298F7B6819A16350DCF08FA1E5F6 (void);
// 0x0000028B System.Void Mobile_Leaderboard::UI_refreshSettings()
extern void Mobile_Leaderboard_UI_refreshSettings_m2282A50820F3A4298D3A49A2EBC3A90784CBF1A2 (void);
// 0x0000028C System.Collections.IEnumerator Mobile_Leaderboard::refreshSettingsDelayed()
extern void Mobile_Leaderboard_refreshSettingsDelayed_mBF003C2B8B4D5633265BDDD0EF26C47FE90C98C9 (void);
// 0x0000028D System.Void Mobile_Leaderboard::getAutoconnectSetting()
extern void Mobile_Leaderboard_getAutoconnectSetting_mE449EB28849092CFFD43421D4455E05C39D55FDD (void);
// 0x0000028E System.Void Mobile_Leaderboard::Start()
extern void Mobile_Leaderboard_Start_m0AE4C9D97977A32B526DFCD07DB2C7C20C0AC91E (void);
// 0x0000028F System.Void Mobile_Leaderboard::UI_call_authenticate()
extern void Mobile_Leaderboard_UI_call_authenticate_m4DD9157C30493DB2509C7C7EF8FA39F93B2BF8F2 (void);
// 0x00000290 System.Void Mobile_Leaderboard::UI_call_transmitscoreAndLeaderboard()
extern void Mobile_Leaderboard_UI_call_transmitscoreAndLeaderboard_mEC6524DD6A6D3DCEAF2351EBA5BC33760FA3C80C (void);
// 0x00000291 System.Void Mobile_Leaderboard::std_call_transmitScore()
extern void Mobile_Leaderboard_std_call_transmitScore_m7ED94FD18909F5B42418CE93F7CD92981D21A3C7 (void);
// 0x00000292 System.Void Mobile_Leaderboard::UI_call_computeAchievements()
extern void Mobile_Leaderboard_UI_call_computeAchievements_m587235F6908A979D84C7DBB2A1A9AD10485CE1EB (void);
// 0x00000293 System.Void Mobile_Leaderboard::std_call_computeAchievements()
extern void Mobile_Leaderboard_std_call_computeAchievements_m3E1188E8C77456EF9DD1DEE605431B462C03961C (void);
// 0x00000294 System.Void Mobile_Leaderboard::std_call_computeAchievements_ifConfigured()
extern void Mobile_Leaderboard_std_call_computeAchievements_ifConfigured_m97D21CC02C259795F1DA84B72874B20F910153E7 (void);
// 0x00000295 System.Boolean Mobile_Leaderboard::compute_achievements()
extern void Mobile_Leaderboard_compute_achievements_m8AAD44C2C5A5FEEB305157B9F20AB175E2035A8E (void);
// 0x00000296 System.Boolean Mobile_Leaderboard::test_for_new_achievements()
extern void Mobile_Leaderboard_test_for_new_achievements_m76615F0776C40B6C002FA4A097C9BC9224A2AD92 (void);
// 0x00000297 System.Void Mobile_Leaderboard::Update()
extern void Mobile_Leaderboard_Update_mA735EA1209F6B1657704F9520F3219F111377092 (void);
// 0x00000298 System.Void Mobile_Leaderboard::test_authenticate_only()
extern void Mobile_Leaderboard_test_authenticate_only_m07EBE25D5FCE59762C302264FF1274210519D3D8 (void);
// 0x00000299 System.Void Mobile_Leaderboard::cyclic_for_achievement()
extern void Mobile_Leaderboard_cyclic_for_achievement_m4A703A988B68CA747433F7477853BC6DABCDC4A1 (void);
// 0x0000029A System.Void Mobile_Leaderboard::cyclic_for_leaderboard()
extern void Mobile_Leaderboard_cyclic_for_leaderboard_m2D30E42CF796288E46A40AC8828BC1428ACD20E5 (void);
// 0x0000029B System.Void Mobile_Leaderboard::reportScores()
extern void Mobile_Leaderboard_reportScores_mAC61B192296E850F86901C260AE884A2B0788E7D (void);
// 0x0000029C System.Int32 Mobile_Leaderboard::readScoreFromPlayerprefs(System.String)
extern void Mobile_Leaderboard_readScoreFromPlayerprefs_m18B39BF2A2C0BD44E2043CAA97CB8D90EAB55DC9 (void);
// 0x0000029D System.Void Mobile_Leaderboard::reportScore(Mobile_Leaderboard/scoreSet)
extern void Mobile_Leaderboard_reportScore_mE09C56D554EA2F645C2D11C8BB65F605062526AB (void);
// 0x0000029E System.Void Mobile_Leaderboard::authenticate()
extern void Mobile_Leaderboard_authenticate_mA0DC9315D3CB9A0A2843B02C89364B172EC18EF7 (void);
// 0x0000029F System.Void Mobile_Leaderboard::UI_Sign_out()
extern void Mobile_Leaderboard_UI_Sign_out_mA95D540B9A93E730D98665A482E4C301C44C90E9 (void);
// 0x000002A0 System.Void Mobile_Leaderboard::.ctor()
extern void Mobile_Leaderboard__ctor_m2EE74700B0F8948640039E5010D64A30084EFCB1 (void);
// 0x000002A1 System.Boolean Mobile_Leaderboard/scoreSet::get___transmitOk()
extern void scoreSet_get___transmitOk_mD5599284B0A446E8C34BBDD3093123DD21FE1239 (void);
// 0x000002A2 System.Void Mobile_Leaderboard/scoreSet::set___transmitOk(System.Boolean)
extern void scoreSet_set___transmitOk_mBF73643240665182CB69D0C14CDC3B087234BFA8 (void);
// 0x000002A3 System.Boolean Mobile_Leaderboard/scoreSet::get___transmitFail()
extern void scoreSet_get___transmitFail_m1D46293C16444D7B24E7F299906D545B69B4219C (void);
// 0x000002A4 System.Void Mobile_Leaderboard/scoreSet::set___transmitFail(System.Boolean)
extern void scoreSet_set___transmitFail_m31C56D55D7B64105F7B67D4DE8E26B9C5CF21E87 (void);
// 0x000002A5 System.Boolean Mobile_Leaderboard/scoreSet::get___transmitted()
extern void scoreSet_get___transmitted_m24ADB7B38403047613B6D005C88BE878E031EEA8 (void);
// 0x000002A6 System.Void Mobile_Leaderboard/scoreSet::set___transmitted(System.Boolean)
extern void scoreSet_set___transmitted_mEAA4AADB1646933E1DE013DF2E0E612A0DA0F7E9 (void);
// 0x000002A7 System.Boolean Mobile_Leaderboard/scoreSet::get___transmitRequested()
extern void scoreSet_get___transmitRequested_mEE51F08862BCD8C9032E6E711AED5B14A3A09C31 (void);
// 0x000002A8 System.Void Mobile_Leaderboard/scoreSet::set___transmitRequested(System.Boolean)
extern void scoreSet_set___transmitRequested_mC218E210B8E249EA4A35AF085BE958D03C729548 (void);
// 0x000002A9 System.Int32 Mobile_Leaderboard/scoreSet::get___scoreActual()
extern void scoreSet_get___scoreActual_m1D676ADD27F828BB2CA053ADB30D939936D53B37 (void);
// 0x000002AA System.Void Mobile_Leaderboard/scoreSet::set___scoreActual(System.Int32)
extern void scoreSet_set___scoreActual_mB774F0A990B7EEE38DD922EA090B0C8579925EBF (void);
// 0x000002AB System.Int32 Mobile_Leaderboard/scoreSet::get___scoreMax()
extern void scoreSet_get___scoreMax_m38F6DB7A6C75E88B9B89D6AA1CC207982739E230 (void);
// 0x000002AC System.Void Mobile_Leaderboard/scoreSet::set___scoreMax(System.Int32)
extern void scoreSet_set___scoreMax_m092C80890879F45DDDB325959410B46A6F90E6A1 (void);
// 0x000002AD System.Int32 Mobile_Leaderboard/scoreSet::get___score_maxTransmitted()
extern void scoreSet_get___score_maxTransmitted_m4B558D48B45FD7A633C4FD7209CB6C5FE3DB4055 (void);
// 0x000002AE System.Void Mobile_Leaderboard/scoreSet::set___score_maxTransmitted(System.Int32)
extern void scoreSet_set___score_maxTransmitted_m61DBFBDABBC81A990BDBDE25ECF48B22248C593E (void);
// 0x000002AF System.Int32 Mobile_Leaderboard/scoreSet::get___scoreToTransmit()
extern void scoreSet_get___scoreToTransmit_m0695FCB38BD85E9835B80EC619936570FC808FB9 (void);
// 0x000002B0 System.Void Mobile_Leaderboard/scoreSet::set___scoreToTransmit(System.Int32)
extern void scoreSet_set___scoreToTransmit_mD05BADBFF2EF6E8FE12433BA61872C6DF71E1EDB (void);
// 0x000002B1 System.Void Mobile_Leaderboard/scoreSet::.ctor()
extern void scoreSet__ctor_mD7B3A25A8EF644D0D52C6D257BC478AB603D7CCB (void);
// 0x000002B2 System.Boolean Mobile_Leaderboard/achievement::get___reached()
extern void achievement_get___reached_m8F7AE68B934D6AA11B0E6138C2DBFDFC216A8D8F (void);
// 0x000002B3 System.Void Mobile_Leaderboard/achievement::set___reached(System.Boolean)
extern void achievement_set___reached_mFCA3399C5D82C1AF8BD992E0D8D345751B27C626 (void);
// 0x000002B4 System.Boolean Mobile_Leaderboard/achievement::get___transmitOk()
extern void achievement_get___transmitOk_mFB8E26CD83629DEACF31511CFEBC5EE71A114564 (void);
// 0x000002B5 System.Void Mobile_Leaderboard/achievement::set___transmitOk(System.Boolean)
extern void achievement_set___transmitOk_mFFF97F106EF5D1BDAA9958F254140C88727DD3C3 (void);
// 0x000002B6 System.Boolean Mobile_Leaderboard/achievement::get___transmitFail()
extern void achievement_get___transmitFail_m5F02EFBC9B8215587543228A555A0E43350CC1F8 (void);
// 0x000002B7 System.Void Mobile_Leaderboard/achievement::set___transmitFail(System.Boolean)
extern void achievement_set___transmitFail_mBF337F23A2FA59B61F444C4C0C0720B28A0D9C72 (void);
// 0x000002B8 System.Boolean Mobile_Leaderboard/achievement::get___transmitted()
extern void achievement_get___transmitted_mF97F2ABC145A00EB91739C81212F9F8C933D2C5F (void);
// 0x000002B9 System.Void Mobile_Leaderboard/achievement::set___transmitted(System.Boolean)
extern void achievement_set___transmitted_m21111F0CD8563874ED9AD3A5047C8FF9EE246A35 (void);
// 0x000002BA System.Boolean Mobile_Leaderboard/achievement::get___transmitRequested()
extern void achievement_get___transmitRequested_m34B52FBD3818A49E20F5E84D9292074CF34CE62E (void);
// 0x000002BB System.Void Mobile_Leaderboard/achievement::set___transmitRequested(System.Boolean)
extern void achievement_set___transmitRequested_m72E233F50A99B23B152EBA5DC8C809AD4B4215CF (void);
// 0x000002BC System.Void Mobile_Leaderboard/achievement::.ctor()
extern void achievement__ctor_m31F6B883FEED55C02BFDE72399BDA80DAAD40F8D (void);
// 0x000002BD System.Void Mobile_Leaderboard/<sozialActivations>d__23::.ctor(System.Int32)
extern void U3CsozialActivationsU3Ed__23__ctor_mC4E5407730B1F65B6EC62E910146B3622143C7A0 (void);
// 0x000002BE System.Void Mobile_Leaderboard/<sozialActivations>d__23::System.IDisposable.Dispose()
extern void U3CsozialActivationsU3Ed__23_System_IDisposable_Dispose_mB937EC683AE10AB216D121A11E6C668FAB08B458 (void);
// 0x000002BF System.Boolean Mobile_Leaderboard/<sozialActivations>d__23::MoveNext()
extern void U3CsozialActivationsU3Ed__23_MoveNext_m4A7600BA0AF552658D7CC68C8572E45F59A2F263 (void);
// 0x000002C0 System.Object Mobile_Leaderboard/<sozialActivations>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsozialActivationsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m020C881CD53FB93BACC71AB995500894AB90C692 (void);
// 0x000002C1 System.Void Mobile_Leaderboard/<sozialActivations>d__23::System.Collections.IEnumerator.Reset()
extern void U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_Reset_m295E47A42C79C8C71B8EEDEA1FECE0D7625EA37F (void);
// 0x000002C2 System.Object Mobile_Leaderboard/<sozialActivations>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_get_Current_mD6B3E13C1C0C3322A2959E8AEED0BC6FB2F30BDF (void);
// 0x000002C3 System.Void Mobile_Leaderboard/<refreshSettingsDelayed>d__26::.ctor(System.Int32)
extern void U3CrefreshSettingsDelayedU3Ed__26__ctor_m8279C00DD68770A1A3B9E7E112AB03B2470FD15C (void);
// 0x000002C4 System.Void Mobile_Leaderboard/<refreshSettingsDelayed>d__26::System.IDisposable.Dispose()
extern void U3CrefreshSettingsDelayedU3Ed__26_System_IDisposable_Dispose_m166F001DFF96386BCD41ACB6BC044BCCDCB82520 (void);
// 0x000002C5 System.Boolean Mobile_Leaderboard/<refreshSettingsDelayed>d__26::MoveNext()
extern void U3CrefreshSettingsDelayedU3Ed__26_MoveNext_mC3873E7B0380D199E099FA8B07A87D78C896D772 (void);
// 0x000002C6 System.Object Mobile_Leaderboard/<refreshSettingsDelayed>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CrefreshSettingsDelayedU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F538F4E1C37EE1D6ED46A6F0650E7D661A840E (void);
// 0x000002C7 System.Void Mobile_Leaderboard/<refreshSettingsDelayed>d__26::System.Collections.IEnumerator.Reset()
extern void U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_Reset_m9ED897F280F6D9C26054BA2B1C3287F5C6D3381C (void);
// 0x000002C8 System.Object Mobile_Leaderboard/<refreshSettingsDelayed>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_get_Current_mCE6AB66AD30ED3D455300DFC06217893C416382D (void);
// 0x000002C9 System.Void Mobile_Leaderboard/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m859F6993AF123809ADF26DF1B894ADC0A2E0287B (void);
// 0x000002CA System.Void Mobile_Leaderboard/<>c__DisplayClass43_0::<reportScore>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass43_0_U3CreportScoreU3Eb__0_mCA86B26C96651268299F69509C04222067863A45 (void);
// 0x000002CB System.Void OrientationHandler::Start()
extern void OrientationHandler_Start_m8F942E6E62FB92DFF5B93DE4C16BBA7DD5CAC501 (void);
// 0x000002CC System.Void OrientationHandler::Update()
extern void OrientationHandler_Update_m0627B2C85838275A414F1964C3860774B78CE95B (void);
// 0x000002CD System.Void OrientationHandler::checkOrientation()
extern void OrientationHandler_checkOrientation_m5947CB565F0CFFD12EEC242E4FA34E24564A9F4A (void);
// 0x000002CE System.Boolean OrientationHandler::hasOrientationChanged()
extern void OrientationHandler_hasOrientationChanged_m6C91C9D74F012D31818189CA27E76F51DD2B9F58 (void);
// 0x000002CF System.Void OrientationHandler::switchToLandscape()
extern void OrientationHandler_switchToLandscape_mD2448E0F92B7DE42A02AA884703CA0D9EF113BAE (void);
// 0x000002D0 System.Void OrientationHandler::switchToPortrait()
extern void OrientationHandler_switchToPortrait_m3B08B0DE9E0249C4812326A7252988FA9C946D50 (void);
// 0x000002D1 System.Void OrientationHandler::setTransform(UnityEngine.RectTransform,OrientationHandler/C_TransformModifications)
extern void OrientationHandler_setTransform_m7A433AB725DF97D07CCF6D1CB42B9AA9D9344846 (void);
// 0x000002D2 System.Void OrientationHandler::.ctor()
extern void OrientationHandler__ctor_m24887FA4A1303A9C08314823E998DE26E51BD545 (void);
// 0x000002D3 System.Void OrientationHandler/mEvent::.ctor()
extern void mEvent__ctor_m123F9F57EA6E58DAFB4B54FE7D4D3BF30B3B0EFD (void);
// 0x000002D4 System.Void OrientationHandler/C_TransformModifications::.ctor()
extern void C_TransformModifications__ctor_m4A036A82EA37292B94A6DF2BA3524D7C30DA4A31 (void);
// 0x000002D5 System.Void OrientationHandler/C_OrientationSelection::.ctor()
extern void C_OrientationSelection__ctor_m78FE7AF8DDA2ED12EC41EFC09BB4AB94D50DE16B (void);
// 0x000002D6 System.Void PersistentImages::Awake()
extern void PersistentImages_Awake_m13B4BE1E67153DD811AC914DCCF9F9F95A2A00B7 (void);
// 0x000002D7 System.Void PersistentImages::Start()
extern void PersistentImages_Start_m54281937472FBE5CCCAB2F71540A76738C618C69 (void);
// 0x000002D8 System.Void PersistentImages::Update()
extern void PersistentImages_Update_mBEAB23022F436AE187B7D9BD1CC51F1D17B899B1 (void);
// 0x000002D9 System.Void PersistentImages::SetSpriteIndex(System.Int32)
extern void PersistentImages_SetSpriteIndex_m09BBD37E537827AECA5968D7ADF0166B024DD588 (void);
// 0x000002DA System.Void PersistentImages::ActualizeImage()
extern void PersistentImages_ActualizeImage_m8E785455ABA1ABDD46D868C10627CBB6CAD7FD9F (void);
// 0x000002DB System.Void PersistentImages::limitActSpriteIndex()
extern void PersistentImages_limitActSpriteIndex_m50E0843D76BB4D3383E72F482C150216E1870FFF (void);
// 0x000002DC System.Void PersistentImages::OnDestroy()
extern void PersistentImages_OnDestroy_m45A3317921BCDA053BA316B699DEA0BDB6B288DA (void);
// 0x000002DD System.Void PersistentImages::save()
extern void PersistentImages_save_m7CD11C358F8ECB1F153B4AF241E90E5626F5A8B7 (void);
// 0x000002DE System.Void PersistentImages::load()
extern void PersistentImages_load_m237B4DB5D085682BC35DA16044C7519E89A556D6 (void);
// 0x000002DF System.Void PersistentImages::.ctor()
extern void PersistentImages__ctor_m580A58443F7877700B108F267B2E444FB8C20FD6 (void);
// 0x000002E0 System.Void PersistentImages/C_ImageConfig::.ctor()
extern void C_ImageConfig__ctor_mFDFBDFFBBDB8B180CE3CF11FF7F67160E28960F0 (void);
// 0x000002E1 System.Boolean PlayAdMob::IsActivated()
extern void PlayAdMob_IsActivated_m2CC2A00DE459CAB5C369D385D027B629234EE82A (void);
// 0x000002E2 System.Void PlayAdMob::Awake()
extern void PlayAdMob_Awake_m0AE10706C036F854ECDF3E587C3F19616992E60B (void);
// 0x000002E3 System.Void PlayAdMob::showNotEnabledWarning()
extern void PlayAdMob_showNotEnabledWarning_m6346304B037787C8C8BE0E9F732F9F081BCB8D7B (void);
// 0x000002E4 System.Void PlayAdMob::SetBirthday(System.Boolean,System.Int32,System.Int32,System.Int32)
extern void PlayAdMob_SetBirthday_mE884D91C1DF8CB159CB519547E97028B4186E7C1 (void);
// 0x000002E5 System.Void PlayAdMob::SetEnableAndSave(System.Boolean)
extern void PlayAdMob_SetEnableAndSave_m3D3A947F7458BBF762F6E1436EF330509CE49B2B (void);
// 0x000002E6 System.Boolean PlayAdMob::GetSavedEnable()
extern void PlayAdMob_GetSavedEnable_m200074F117F4AF5C7224AF9A423BB5ED45DEDA04 (void);
// 0x000002E7 System.Void PlayAdMob::mPrint(System.String)
extern void PlayAdMob_mPrint_m23B5814C469631D1A40AFE0B4ED6E97F36A3871B (void);
// 0x000002E8 System.Void PlayAdMob::Start()
extern void PlayAdMob_Start_mD5264837A49E2B9452996F43403CB8D71696336C (void);
// 0x000002E9 System.Void PlayAdMob::RequestBanner()
extern void PlayAdMob_RequestBanner_m20EE8D3F072A889EA78A5E76CEE6F091F72ACE25 (void);
// 0x000002EA System.Void PlayAdMob::OnDestroy()
extern void PlayAdMob_OnDestroy_m8D7EFBD88F886CFCF81C820CBC9FC64C54612459 (void);
// 0x000002EB System.Void PlayAdMob::DestroyBanner()
extern void PlayAdMob_DestroyBanner_mF7EB6A9A46401B9696E00B93F21FFF4319FD416C (void);
// 0x000002EC System.Void PlayAdMob::RequestInterstitial()
extern void PlayAdMob_RequestInterstitial_m1FAAF8C0ABDF3D2A194132BBB4D19EFE0E86B641 (void);
// 0x000002ED System.Void PlayAdMob::RequestRewardBasedVideo()
extern void PlayAdMob_RequestRewardBasedVideo_m66B4AF09066E3357740838114E86150056FFAEB7 (void);
// 0x000002EE System.Void PlayAdMob::ShowInterstitial()
extern void PlayAdMob_ShowInterstitial_m2FA168CC0201627A23EAD23F954FD86712E1AAD6 (void);
// 0x000002EF System.Void PlayAdMob::DestroyInterstitial()
extern void PlayAdMob_DestroyInterstitial_mB74409EA51ABD3B3AEB9708499D398F244DE2E19 (void);
// 0x000002F0 System.Void PlayAdMob::ShowRewardBasedVideo()
extern void PlayAdMob_ShowRewardBasedVideo_m21A8D32DEC469223E9D0F3C94C26A5ECA3B6690B (void);
// 0x000002F1 System.Void PlayAdMob::HandleAdLoaded(System.Object,System.EventArgs)
extern void PlayAdMob_HandleAdLoaded_mD081E99906FA41E44DC15321C4419660CF1D25B8 (void);
// 0x000002F2 System.Void PlayAdMob::HandleAdOpened(System.Object,System.EventArgs)
extern void PlayAdMob_HandleAdOpened_mCFF2367414A907C5FFE6C1B22AA1DCEC8508880C (void);
// 0x000002F3 System.Void PlayAdMob::HandleAdClosed(System.Object,System.EventArgs)
extern void PlayAdMob_HandleAdClosed_m0BDCA056ED5213E98A2D99F77280C141B2983300 (void);
// 0x000002F4 System.Void PlayAdMob::HandleAdLeftApplication(System.Object,System.EventArgs)
extern void PlayAdMob_HandleAdLeftApplication_m0E8871C403ECD0CE400B7B18829AF855CB1B636D (void);
// 0x000002F5 System.Void PlayAdMob::HandleInterstitialLoaded(System.Object,System.EventArgs)
extern void PlayAdMob_HandleInterstitialLoaded_m4994C55C474A57E6043B889FCE781527FE5248DB (void);
// 0x000002F6 System.Void PlayAdMob::HandleInterstitialOpened(System.Object,System.EventArgs)
extern void PlayAdMob_HandleInterstitialOpened_mE5E1D4CE758C6D1765DD5AE9C17B488C8427C696 (void);
// 0x000002F7 System.Void PlayAdMob::HandleInterstitialClosed(System.Object,System.EventArgs)
extern void PlayAdMob_HandleInterstitialClosed_m2B49F5993C2D7698C71314E6B23F62FBAA6A2C26 (void);
// 0x000002F8 System.Void PlayAdMob::HandleInterstitialLeftApplication(System.Object,System.EventArgs)
extern void PlayAdMob_HandleInterstitialLeftApplication_mA7C0E34DBFE0D9752E909D678E937EE58A66AB7D (void);
// 0x000002F9 System.Void PlayAdMob::HandleRewardBasedVideoLoaded(System.Object,System.EventArgs)
extern void PlayAdMob_HandleRewardBasedVideoLoaded_mD228E88BC46B0F767B1D5ABA651356DC49BB8A8B (void);
// 0x000002FA System.Void PlayAdMob::HandleRewardBasedVideoOpened(System.Object,System.EventArgs)
extern void PlayAdMob_HandleRewardBasedVideoOpened_m4272180DBC71AD14159748CFFA8DD6167B93B77D (void);
// 0x000002FB System.Void PlayAdMob::HandleRewardBasedVideoStarted(System.Object,System.EventArgs)
extern void PlayAdMob_HandleRewardBasedVideoStarted_m040C80DC038D0642F86E72385030B54987BD1A8E (void);
// 0x000002FC System.Void PlayAdMob::HandleRewardBasedVideoClosed(System.Object,System.EventArgs)
extern void PlayAdMob_HandleRewardBasedVideoClosed_mF09A0CB10E2315C42A6F5E862B65944BF70CFB2D (void);
// 0x000002FD System.Void PlayAdMob::HandleRewardBasedVideoLeftApplication(System.Object,System.EventArgs)
extern void PlayAdMob_HandleRewardBasedVideoLeftApplication_m7935544DA7D1ADCFBA93109BA31137A6D4BFC640 (void);
// 0x000002FE System.Void PlayAdMob::.ctor()
extern void PlayAdMob__ctor_m64F7DED6C446E00CCFD5DE538091CE8E7AA63B78 (void);
// 0x000002FF System.Void PlayAdMob/mDescribedEvent::Invoke()
extern void mDescribedEvent_Invoke_mBA74F4143C0423A3976A157C63130C5CE88B9C8F (void);
// 0x00000300 System.Void PlayAdMob/mDescribedEvent::.ctor()
extern void mDescribedEvent__ctor_m53C3E13229C7ED485A0D6007888106772F40622F (void);
// 0x00000301 System.Void PlayAdMob/C_Birthday::.ctor()
extern void C_Birthday__ctor_m13215A6C7026F67C8161B4843B7442AF6B011005 (void);
// 0x00000302 System.Void PlayAdMob/C_max_ad_content_rating::.ctor()
extern void C_max_ad_content_rating__ctor_m04BD41C8C1ACAB601F95178258FA23C2FFB43292 (void);
// 0x00000303 System.Void PlayAdMob/C_GenderConfig::.ctor()
extern void C_GenderConfig__ctor_mF293F8700B11EA748E74244C442C05CD8C91B07C (void);
// 0x00000304 System.Void PlayAdMob/C_EN_YES_NO::.ctor()
extern void C_EN_YES_NO__ctor_m4524537414B80870768ED69A9840A7DD82BA3BFE (void);
// 0x00000305 System.Void PlayAdMob/C_TargetAudience::.ctor()
extern void C_TargetAudience__ctor_mDD4C6F20C9E6A1E828EEAF8B2C3D00071E25CB98 (void);
// 0x00000306 System.Void PlayAdMob/C_TestConfiguration::.ctor()
extern void C_TestConfiguration__ctor_m4C9BCE17BCA83755E25CC08CD4CC1E06E58DAA42 (void);
// 0x00000307 System.Void PlayAdMob/C_IDS::.ctor()
extern void C_IDS__ctor_m0635B8B2909B2A77A91A82970107D9F06F86565A (void);
// 0x00000308 System.Void PlayAdMob/C_GeneralEvents::.ctor()
extern void C_GeneralEvents__ctor_m8ED99F81679166BFB38F888847D5F0B5D790306F (void);
// 0x00000309 System.Void PlayAdMob/C_BannerConfig::.ctor()
extern void C_BannerConfig__ctor_m05F8DF2B5CA6B7DBC8E825D695D6F935958135BE (void);
// 0x0000030A System.Void PlayAdMob/C_Error_Events::.ctor()
extern void C_Error_Events__ctor_mD1D39A280AD59676C83C6F394863626B313F6491 (void);
// 0x0000030B System.Void PlayAdMob/C_Events_Banner::.ctor()
extern void C_Events_Banner__ctor_mDDF381301AD69B8BB3A72A0317AFD80010FE6365 (void);
// 0x0000030C System.Void PlayAdMob/C_InterstitialConfig::.ctor()
extern void C_InterstitialConfig__ctor_mA9FF5E29815B7A6A383117C508E76135602A794D (void);
// 0x0000030D System.Void PlayAdMob/C_Events_Interstitial::.ctor()
extern void C_Events_Interstitial__ctor_mDC49618B7E8372414347013F552ABB9436F7737B (void);
// 0x0000030E System.Void PlayAdMob/C_RewardBasedVideoConfig::.ctor()
extern void C_RewardBasedVideoConfig__ctor_m4BB4003BB4529546545B745D4908954FFC8F5725 (void);
// 0x0000030F System.Void PlayAdMob/C_Events_RewardBasedVideo::.ctor()
extern void C_Events_RewardBasedVideo__ctor_m10D3FA15D0C970CC9F3D33CB30CB4EE452C5A1AF (void);
// 0x00000310 System.Void PlayUnityAd::Start()
extern void PlayUnityAd_Start_m20394C023E3C512EE40A86A08F40C4290C1BB48E (void);
// 0x00000311 System.Void PlayUnityAd::SetEnableAndSave(System.Boolean)
extern void PlayUnityAd_SetEnableAndSave_m1461DFC7539E3119E1C69149C5D9F966669B6AA3 (void);
// 0x00000312 System.Boolean PlayUnityAd::GetSavedEnable()
extern void PlayUnityAd_GetSavedEnable_m35D20860962D461EC9C60D4394F38CD1F1867CA4 (void);
// 0x00000313 System.Collections.IEnumerator PlayUnityAd::testForAdvertisement()
extern void PlayUnityAd_testForAdvertisement_mE3C461C09C83B9B6851C5F210F5D00D86A1DE8B2 (void);
// 0x00000314 System.Void PlayUnityAd::showAd()
extern void PlayUnityAd_showAd_m92CF616328616B318DBE887CFCB08FECF3F29891 (void);
// 0x00000315 System.Void PlayUnityAd::showRewardedAd()
extern void PlayUnityAd_showRewardedAd_m0F5CAE1A0ED77CD52E9F6050BE5575CC162BACBB (void);
// 0x00000316 System.Void PlayUnityAd::showDefaultAd()
extern void PlayUnityAd_showDefaultAd_m18E3254445CC0B95F840A7A58086D8234CBF4518 (void);
// 0x00000317 System.Void PlayUnityAd::HandleShowResult(UnityEngine.Advertisements.ShowResult)
extern void PlayUnityAd_HandleShowResult_m9A97F58238ED8502B18620FDF5B072469463492E (void);
// 0x00000318 System.Void PlayUnityAd::.ctor()
extern void PlayUnityAd__ctor_m8ADE6851C62668D112CED7477B876D3A69FE5C2F (void);
// 0x00000319 System.Void PlayUnityAd/mEvent::.ctor()
extern void mEvent__ctor_mDA1C48D04788A7D027D997347778701E9F932F80 (void);
// 0x0000031A System.Void PlayUnityAd/<testForAdvertisement>d__17::.ctor(System.Int32)
extern void U3CtestForAdvertisementU3Ed__17__ctor_m16D037C642106BB04305682B117FCE27C0C75741 (void);
// 0x0000031B System.Void PlayUnityAd/<testForAdvertisement>d__17::System.IDisposable.Dispose()
extern void U3CtestForAdvertisementU3Ed__17_System_IDisposable_Dispose_m9BC3A92ED2FB1954E5445B8A8391649DFA6957E4 (void);
// 0x0000031C System.Boolean PlayUnityAd/<testForAdvertisement>d__17::MoveNext()
extern void U3CtestForAdvertisementU3Ed__17_MoveNext_m144B2B40E93E4E44A3FC855FF77C2388FE2872E7 (void);
// 0x0000031D System.Object PlayUnityAd/<testForAdvertisement>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtestForAdvertisementU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8D44465EF33772A073C5411A44D97351670F236 (void);
// 0x0000031E System.Void PlayUnityAd/<testForAdvertisement>d__17::System.Collections.IEnumerator.Reset()
extern void U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_Reset_mE310632C52E3A44CC1892E2669BAB3E63554441F (void);
// 0x0000031F System.Object PlayUnityAd/<testForAdvertisement>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_get_Current_m7B7B5E24FB0E178AC03D8AC548434E2FD3C3C9E2 (void);
// 0x00000320 System.Void RandomElementWithPropability::Awake()
extern void RandomElementWithPropability_Awake_m1A89E9EA1F4C10BEB127EFC94A41D659504DC94D (void);
// 0x00000321 System.Void RandomElementWithPropability::resetElements()
extern void RandomElementWithPropability_resetElements_m9391B38EA8110C1AABA15D5C64E6BC8B9F7D491D (void);
// 0x00000322 System.Void RandomElementWithPropability::addElement(UnityEngine.GameObject,System.Single)
extern void RandomElementWithPropability_addElement_m467497F8FFCBE0A6B2870532F0C80F64759418C3 (void);
// 0x00000323 System.Void RandomElementWithPropability::calculateOverallPropability()
extern void RandomElementWithPropability_calculateOverallPropability_mFD179A2779BF89B565037FFBC8385B2C032ECA1B (void);
// 0x00000324 UnityEngine.GameObject RandomElementWithPropability::getRandomElement()
extern void RandomElementWithPropability_getRandomElement_m004C5D11EB0C7D53CF8BBEAD8590C36BC1ACE5B0 (void);
// 0x00000325 System.Void RandomElementWithPropability::.ctor()
extern void RandomElementWithPropability__ctor_m0337098AD84E899EF221A2FB531097D551932656 (void);
// 0x00000326 System.Void RandomElementWithPropability/propElement::.ctor()
extern void propElement__ctor_m964DE31AFED7602AD9141C45D32A898D391371E9 (void);
// 0x00000327 System.Void RatioHandler::Start()
extern void RatioHandler_Start_m7ECDDE6D751A041D47653702DEA80BED5674717E (void);
// 0x00000328 System.Void RatioHandler::Update()
extern void RatioHandler_Update_mF2F0045B4108524A7BEBB0E70215931BBEFA5D0E (void);
// 0x00000329 System.Void RatioHandler::checkRatioEvents()
extern void RatioHandler_checkRatioEvents_m45B36F634F9418FF729EC7BF20CA4DA4DB403238 (void);
// 0x0000032A System.Boolean RatioHandler::hasResolutionChanged()
extern void RatioHandler_hasResolutionChanged_mD5EB20DC957A40D4CEB217E76C56D2B6F9FC68E7 (void);
// 0x0000032B System.Int32 RatioHandler::getRatioIndex()
extern void RatioHandler_getRatioIndex_m3389AC4691EF5117A4C55CB94410EF4ADFE9B006 (void);
// 0x0000032C System.Void RatioHandler::setTransform(RatioHandler/C_TransformModification)
extern void RatioHandler_setTransform_m66F811AF0428723749C3A2D00BC72131EF85EA18 (void);
// 0x0000032D System.Void RatioHandler::.ctor()
extern void RatioHandler__ctor_m09E5A7A051A9B9860BE16EF384A39408AB9E779F (void);
// 0x0000032E System.Void RatioHandler/mEvent::.ctor()
extern void mEvent__ctor_m26D2BB5491EB6938AAE74B79BBCEA7E26E75F2C2 (void);
// 0x0000032F System.Void RatioHandler/C_TransformModification::.ctor()
extern void C_TransformModification__ctor_mB7E72926363F2F132F74E6EFD06F608E7936A6CC (void);
// 0x00000330 System.Void RatioHandler/C_RatioEvent::.ctor()
extern void C_RatioEvent__ctor_mBC2EFBBE3B3095C8E6A921DCB6E8A8B28775E19B (void);
// 0x00000331 System.Void ReadOnlyInspector::.ctor()
extern void ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469 (void);
// 0x00000332 System.Void ResultCheat::addCheats()
extern void ResultCheat_addCheats_m74FB481A9D8149547C47E65C0F8AF775DD818924 (void);
// 0x00000333 System.Void ResultCheat::.ctor()
extern void ResultCheat__ctor_mAFFCC7B149EEBA4A984754F25F9FFE43344F15BF (void);
// 0x00000334 System.Void ScrollbarIncrementer::Start()
extern void ScrollbarIncrementer_Start_m7E31E31C76D25480AA4C0BB07F47369B3FCCAFE6 (void);
// 0x00000335 System.Void ScrollbarIncrementer::Increment()
extern void ScrollbarIncrementer_Increment_m120ADE06FFF6CFE19B63A95DAA01C8017F1C198E (void);
// 0x00000336 System.Void ScrollbarIncrementer::Decrement()
extern void ScrollbarIncrementer_Decrement_mF2522250A40B7631048C377BBDD71DD78DDA0782 (void);
// 0x00000337 System.Void ScrollbarIncrementer::Update()
extern void ScrollbarIncrementer_Update_m2B6A719FF0FD105BAD917B05DD22D5A6D4B38AFA (void);
// 0x00000338 System.Void ScrollbarIncrementer::.ctor()
extern void ScrollbarIncrementer__ctor_m995F1AFFD246FC52AAD8F449EEE94078E63EB488 (void);
// 0x00000339 System.Void SecurePlayerPrefs::SetString(System.String,System.String)
extern void SecurePlayerPrefs_SetString_m2D4B33838525B7259FE13B3A0AC62AE18721C12B (void);
// 0x0000033A System.String SecurePlayerPrefs::GetString(System.String)
extern void SecurePlayerPrefs_GetString_m440032AD98A734955257A3ACE80120E00315BDAB (void);
// 0x0000033B System.Void SecurePlayerPrefs::SetFloat(System.String,System.Single)
extern void SecurePlayerPrefs_SetFloat_m65306B46C8EE84AE9260D490033332772755A3CC (void);
// 0x0000033C System.Single SecurePlayerPrefs::GetFloat(System.String)
extern void SecurePlayerPrefs_GetFloat_m25BB6422639AF9CE4AA43EBD4549DB2A377B8AC0 (void);
// 0x0000033D System.Void SecurePlayerPrefs::SetDouble(System.String,System.Double)
extern void SecurePlayerPrefs_SetDouble_mE231BF3F3E754A6C336A49816C5310CE516C7771 (void);
// 0x0000033E System.Double SecurePlayerPrefs::GetDouble(System.String)
extern void SecurePlayerPrefs_GetDouble_mA63CF2798D44B528E9F8C707DA6D66942C241A17 (void);
// 0x0000033F System.Void SecurePlayerPrefs::SetBool(System.String,System.Boolean)
extern void SecurePlayerPrefs_SetBool_mFE334658A75065CE53642D20F02221DD8D735CED (void);
// 0x00000340 System.Boolean SecurePlayerPrefs::GetBool(System.String)
extern void SecurePlayerPrefs_GetBool_m5F579F2208FAADF0511CB20CAC5E89AE43751644 (void);
// 0x00000341 System.Void SecurePlayerPrefs::SetInt(System.String,System.Int32)
extern void SecurePlayerPrefs_SetInt_m19AC906F48CC007F2493FA6B45843F550EDC89A7 (void);
// 0x00000342 System.Int32 SecurePlayerPrefs::GetInt(System.String)
extern void SecurePlayerPrefs_GetInt_m385D2D04CD57462884CFFB46D9C1A1623565706A (void);
// 0x00000343 System.String SecurePlayerPrefs::GetString(System.String,System.String)
extern void SecurePlayerPrefs_GetString_m3B547B77B8975081836923ECC4F6D694AAEE3563 (void);
// 0x00000344 System.Boolean SecurePlayerPrefs::HasKey(System.String)
extern void SecurePlayerPrefs_HasKey_m73FFDC9C1EA3EE923620DF6FBA15459570979B36 (void);
// 0x00000345 System.String SecurePlayerPrefs::xorEncryptDecrypt(System.String)
extern void SecurePlayerPrefs_xorEncryptDecrypt_m98E023D0F73343FB068D1BCFB8F40062D5C62AA4 (void);
// 0x00000346 System.String SecurePlayerPrefs::GenerateMD5(System.String)
extern void SecurePlayerPrefs_GenerateMD5_mCF87894E80778181A97424F348430F8DF4ED31F9 (void);
// 0x00000347 System.Void SecurePlayerPrefs::.cctor()
extern void SecurePlayerPrefs__cctor_mA3BB73F830F90804BDFF23302CAEF7004BB1B9F6 (void);
// 0x00000348 System.Void AudioVolume::Awake()
extern void AudioVolume_Awake_m9064D991922D874C34F26600EFABAB7D07BC16C2 (void);
// 0x00000349 System.Void AudioVolume::Start()
extern void AudioVolume_Start_m0C68AF9A4685ACD6D630994C108029C2900B05BD (void);
// 0x0000034A System.Void AudioVolume::changeVolumeSetting()
extern void AudioVolume_changeVolumeSetting_m390B40A4854CDB2C9D093583E033286A61DF326D (void);
// 0x0000034B System.Void AudioVolume::computeConfig()
extern void AudioVolume_computeConfig_m8C24532A7248B93034756CE3FB1276D9270596F5 (void);
// 0x0000034C System.Void AudioVolume::setVolume(System.Boolean)
extern void AudioVolume_setVolume_m6F33E00B0B0638B77C6BE3E3807786102E82F45B (void);
// 0x0000034D System.Void AudioVolume::.ctor()
extern void AudioVolume__ctor_mC48D4A5FB4601DCE553FA57649EFC4F481114112 (void);
// 0x0000034E System.Void BackActivate::Update()
extern void BackActivate_Update_mEDCDC43D06BC6D3C01F4DABDD6CFB75B95A3C996 (void);
// 0x0000034F System.Void BackActivate::activate()
extern void BackActivate_activate_m3A91E6D41A3BB71A4E42EA4AED5CCDCF75517456 (void);
// 0x00000350 System.Void BackActivate::.ctor()
extern void BackActivate__ctor_m0F75192133D8962F773FA81DA755087ED03CA03C (void);
// 0x00000351 System.Void BackDeActivate::Update()
extern void BackDeActivate_Update_mB0E2DB1A1F112C988282BCDB9E4743354443DD28 (void);
// 0x00000352 System.Void BackDeActivate::deactivate()
extern void BackDeActivate_deactivate_m7442D6DC0631BB1992F166DA45F5B14397E7D5EC (void);
// 0x00000353 System.Void BackDeActivate::.ctor()
extern void BackDeActivate__ctor_m41EBB285B2426EA28B83328D9ACDE9C091E56BBC (void);
// 0x00000354 System.Void ExitScript::Update()
extern void ExitScript_Update_m7EF4B6BCE51D32AFAD0223EB9D0163E27610F0E4 (void);
// 0x00000355 System.Void ExitScript::quit()
extern void ExitScript_quit_mEDE612875DC93285FF16FEFAC884CD0267891C7D (void);
// 0x00000356 System.Void ExitScript::.ctor()
extern void ExitScript__ctor_mB510824368746FADD21053DDD4766F5543E601A6 (void);
// 0x00000357 System.Void LoadScene::compute_levelload()
extern void LoadScene_compute_levelload_mBDB0B446D79454F1F404952B979564FDDC1DDD66 (void);
// 0x00000358 System.Void LoadScene::levelload()
extern void LoadScene_levelload_m0440A9C2A60005D899D7E7F013DB29996D122009 (void);
// 0x00000359 System.Collections.IEnumerator LoadScene::delayedLoad()
extern void LoadScene_delayedLoad_m6E48431E35D2FF55B1CE9B57CD496DDDCE98ED7E (void);
// 0x0000035A System.Void LoadScene::.ctor()
extern void LoadScene__ctor_m3003C0157CC4154E26B2ABFEF86DAF98ED17150F (void);
// 0x0000035B System.Void LoadScene/<delayedLoad>d__4::.ctor(System.Int32)
extern void U3CdelayedLoadU3Ed__4__ctor_m2FEDB7CE5A9390BE1C53532F8ABFBB6A6B962324 (void);
// 0x0000035C System.Void LoadScene/<delayedLoad>d__4::System.IDisposable.Dispose()
extern void U3CdelayedLoadU3Ed__4_System_IDisposable_Dispose_mF3089973DD6F7FB4A8C274A44A16B5CD2BAFE9FC (void);
// 0x0000035D System.Boolean LoadScene/<delayedLoad>d__4::MoveNext()
extern void U3CdelayedLoadU3Ed__4_MoveNext_mF5602570D6689336A4BA061B48278F82460C12D9 (void);
// 0x0000035E System.Object LoadScene/<delayedLoad>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayedLoadU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B0A66E43371D0D1D55312BED6363508A1A4AEAB (void);
// 0x0000035F System.Void LoadScene/<delayedLoad>d__4::System.Collections.IEnumerator.Reset()
extern void U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_Reset_m3463BD91919D2175B690C1A4BD4E2B9CA498A3B6 (void);
// 0x00000360 System.Object LoadScene/<delayedLoad>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_get_Current_m6B830331C28C6C1601A5DA6711C53B509B5D8C09 (void);
// 0x00000361 System.Void MusicPlayer::Start()
extern void MusicPlayer_Start_m196809CDCF89356E622F9AAA17375AC2A0F4DAB5 (void);
// 0x00000362 System.Void MusicPlayer::play()
extern void MusicPlayer_play_m55DE11211AB12E19C12EB0F332D1CEDABE26536C (void);
// 0x00000363 System.Void MusicPlayer::stop()
extern void MusicPlayer_stop_mCCDDCB3736B92E6546DC01811565F6E46E74475B (void);
// 0x00000364 System.Void MusicPlayer::PlaySong(System.Int32)
extern void MusicPlayer_PlaySong_m3159AB80E65B2EB59490C5D52D1C3F1663ED52C8 (void);
// 0x00000365 System.Collections.IEnumerator MusicPlayer::multimediaTimer()
extern void MusicPlayer_multimediaTimer_mEEEA7ED5D30B61543F2FF340A50006CC2AF3AF56 (void);
// 0x00000366 System.Void MusicPlayer::nextSong()
extern void MusicPlayer_nextSong_m4929CA47D032E4157461DE73748BF4B689906550 (void);
// 0x00000367 System.Int32 MusicPlayer::clipIndex(System.Int32)
extern void MusicPlayer_clipIndex_m56A4E5011864E61D9649BCFB0DAFC0BF4031D07A (void);
// 0x00000368 System.Void MusicPlayer::setSong(System.Int32)
extern void MusicPlayer_setSong_m5654955DDDF5871A3801F8DEC5F973B9F1BEBBCF (void);
// 0x00000369 System.Void MusicPlayer::replaySong()
extern void MusicPlayer_replaySong_mA725B6620FBFA638DD71AA5551D1C03F5458077E (void);
// 0x0000036A System.Void MusicPlayer::nextListedSong()
extern void MusicPlayer_nextListedSong_mB99FEDCC28C801FAE829D6F071CE02FED9B32C43 (void);
// 0x0000036B System.Void MusicPlayer::Update()
extern void MusicPlayer_Update_m7BD87702BA93E1CE0DA8D666975B48BF5ABC376E (void);
// 0x0000036C System.Void MusicPlayer::actualizeAudioPlay()
extern void MusicPlayer_actualizeAudioPlay_mBEC308AB679D96209206EDEC678BC4B2041B3CEB (void);
// 0x0000036D System.Void MusicPlayer::OnDisable()
extern void MusicPlayer_OnDisable_m38C48F820AB7B58614CF8DB16D3AC896C14D181D (void);
// 0x0000036E System.Void MusicPlayer::OnEnable()
extern void MusicPlayer_OnEnable_m2F00726C543053E06F55B646F38485A1F3F3EA00 (void);
// 0x0000036F System.Void MusicPlayer::save()
extern void MusicPlayer_save_mF22FDF468C84AC1353F20D07E45A5395232B64A5 (void);
// 0x00000370 System.Void MusicPlayer::load()
extern void MusicPlayer_load_mCA2285DC1F5A79CF603DD836F007678FF3303E60 (void);
// 0x00000371 System.Void MusicPlayer::.ctor()
extern void MusicPlayer__ctor_mA83600A144210E8586F4618ED75C614BFE6450FC (void);
// 0x00000372 System.Void MusicPlayer/<multimediaTimer>d__16::.ctor(System.Int32)
extern void U3CmultimediaTimerU3Ed__16__ctor_m9452FF5F9C621B2701F14069A953C1D7EDCB639E (void);
// 0x00000373 System.Void MusicPlayer/<multimediaTimer>d__16::System.IDisposable.Dispose()
extern void U3CmultimediaTimerU3Ed__16_System_IDisposable_Dispose_mB7EE0025ABA8196757B8B19E53D0AF877CCC729E (void);
// 0x00000374 System.Boolean MusicPlayer/<multimediaTimer>d__16::MoveNext()
extern void U3CmultimediaTimerU3Ed__16_MoveNext_mC9D24E8334B8C263F6AC866C27F81A215DBCBA90 (void);
// 0x00000375 System.Object MusicPlayer/<multimediaTimer>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CmultimediaTimerU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAFBBFFDC0FAD295792E66E7EB0185FBAAC74A4C (void);
// 0x00000376 System.Void MusicPlayer/<multimediaTimer>d__16::System.Collections.IEnumerator.Reset()
extern void U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_Reset_m2123A5E79B2C708B0EBAF93621053EABA2B1F5B0 (void);
// 0x00000377 System.Object MusicPlayer/<multimediaTimer>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_get_Current_m1B45D5AAF2D3A164F2C5DB0B61FFB79517282150 (void);
// 0x00000378 System.Void OpenURL::loadurl()
extern void OpenURL_loadurl_m8AAD80CD62305595E4ECBD89C0AEF7212E97AC24 (void);
// 0x00000379 System.Void OpenURL::.ctor()
extern void OpenURL__ctor_mBD893164A0E0FE8CD0999BFF1AE11E07C5244D5D (void);
// 0x0000037A System.Void UniToggle::Start()
extern void UniToggle_Start_m87ECB16D4D8F829D115134B421C62AA814F958C7 (void);
// 0x0000037B System.Void UniToggle::setScriptState(System.Boolean)
extern void UniToggle_setScriptState_m364D2EE045E5763E9C625CC0E775EF5CC3619F14 (void);
// 0x0000037C System.Void UniToggle::btnOnly_invertScriptState()
extern void UniToggle_btnOnly_invertScriptState_m91504CA85D57D8A6172E7192E5DF463434C92DA6 (void);
// 0x0000037D System.Void UniToggle::saveScriptState()
extern void UniToggle_saveScriptState_mE81754E976176B924EAFFC499C9FC4015CA57FAA (void);
// 0x0000037E System.Void UniToggle::loadScriptState()
extern void UniToggle_loadScriptState_mD4271BB150D5994105125BA6AB46B5AA9C6FACF1 (void);
// 0x0000037F System.Void UniToggle::enableDisableScript()
extern void UniToggle_enableDisableScript_m294EA8C9898F819D39E7BA7418E55F03B3E45102 (void);
// 0x00000380 System.Void UniToggle::testScriptStateOnStart()
extern void UniToggle_testScriptStateOnStart_mA73F0BE62A7D861E15B600B105B5604BB5C9E3EE (void);
// 0x00000381 System.Void UniToggle::.ctor()
extern void UniToggle__ctor_m70CC2366FE21E2BF90C532D96A1C0391592F1FBE (void);
// 0x00000382 System.Void saveSlider::mDebug(System.String)
extern void saveSlider_mDebug_mB4E44448A0F03C971FD2CE67D98399502F97C6E0 (void);
// 0x00000383 System.Void saveSlider::Start()
extern void saveSlider_Start_m417862C0623CC1118B74EA18EB9ECCB431C65487 (void);
// 0x00000384 System.Void saveSlider::loadSliderValue()
extern void saveSlider_loadSliderValue_mF1C7A305E9F61E5161EFDE6A317A0A880D5BA2A6 (void);
// 0x00000385 System.Void saveSlider::prepareForSaveSliderValue()
extern void saveSlider_prepareForSaveSliderValue_m390920FC15D1471753EA3C1926976FB0435403CF (void);
// 0x00000386 System.Collections.IEnumerator saveSlider::saveKeyDelayed()
extern void saveSlider_saveKeyDelayed_m38B8C3376FE5ECCC0C8F95B8635C123D009A3EE8 (void);
// 0x00000387 System.Void saveSlider::saveSliderValue()
extern void saveSlider_saveSliderValue_mCDAC6F440A84B7378C45EFD9260DF0287500902D (void);
// 0x00000388 System.Void saveSlider::setSliderValue(System.Single)
extern void saveSlider_setSliderValue_m4AE6182A3C4CD21C6A10F31B67A165E5592E0CD0 (void);
// 0x00000389 System.Void saveSlider::actualizeUI()
extern void saveSlider_actualizeUI_mAEBCE38B5ECC01E64EECFB87E19CD8415BF3F106 (void);
// 0x0000038A System.Void saveSlider::firstloadUI()
extern void saveSlider_firstloadUI_mB72869F64A11341BCC115EDC387BD1555C0DF895 (void);
// 0x0000038B System.Void saveSlider::.ctor()
extern void saveSlider__ctor_m48321ACABBFA0CEF96E05320C6B69F8B0F11355E (void);
// 0x0000038C System.Void saveSlider/<saveKeyDelayed>d__13::.ctor(System.Int32)
extern void U3CsaveKeyDelayedU3Ed__13__ctor_m2DC30CBDF8E9E74DCFCDF4A344C2534AA4428DAC (void);
// 0x0000038D System.Void saveSlider/<saveKeyDelayed>d__13::System.IDisposable.Dispose()
extern void U3CsaveKeyDelayedU3Ed__13_System_IDisposable_Dispose_m9C6DD24E2BD62BB987123DA584DB83021628DCCA (void);
// 0x0000038E System.Boolean saveSlider/<saveKeyDelayed>d__13::MoveNext()
extern void U3CsaveKeyDelayedU3Ed__13_MoveNext_m293BBF39D8989B4232B7BACD8A5903077B9EDF34 (void);
// 0x0000038F System.Object saveSlider/<saveKeyDelayed>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsaveKeyDelayedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C9015DDBDB5E34979C1FA1F6F29463EB224629 (void);
// 0x00000390 System.Void saveSlider/<saveKeyDelayed>d__13::System.Collections.IEnumerator.Reset()
extern void U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_Reset_m3D7E598789A659776E64F9B635AEA7FE312067AE (void);
// 0x00000391 System.Object saveSlider/<saveKeyDelayed>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_get_Current_mC4966B1DBE9334F30DC9633FB3C843A517F178AA (void);
// 0x00000392 System.Void sliderToVolume::setVolume(System.Single)
extern void sliderToVolume_setVolume_m4835D1F1733532303A17B45968B69DE162E4D27D (void);
// 0x00000393 System.Void sliderToVolume::.ctor()
extern void sliderToVolume__ctor_m48E4E332E7019DB68C903641E4784C47DEFFE503 (void);
// 0x00000394 System.Void ShowGameLog::Start()
extern void ShowGameLog_Start_m2EB10E397127A61F621B3487D88D7FB49837BD59 (void);
// 0x00000395 System.Void ShowGameLog::.ctor()
extern void ShowGameLog__ctor_m42C5022625C0295E44FED2394E6E8F0B4F181DC0 (void);
// 0x00000396 System.Void Swipe::Start()
extern void Swipe_Start_m67570B95FB0889F6D2990E8731474EE3E76929D0 (void);
// 0x00000397 UnityEngine.Vector2 Swipe::getSwipeVector()
extern void Swipe_getSwipeVector_m9E91C8E5E27D2D654CD6337F32650C1B4659CFA1 (void);
// 0x00000398 System.Void Swipe::setIdleSwipeActive(System.Boolean)
extern void Swipe_setIdleSwipeActive_m288BBA1719FA54653EEC5E886930BE3563192264 (void);
// 0x00000399 System.Boolean Swipe::getIdleSwipeActive()
extern void Swipe_getIdleSwipeActive_m91840593FFCE93729E16C78670D25C65F7EFFBDE (void);
// 0x0000039A System.Void Swipe::initScreenPositions()
extern void Swipe_initScreenPositions_m00820B37093C1FC21EAA3DFC43F33FF305B57472 (void);
// 0x0000039B System.Void Swipe::SetScreenTargetPosition(Swipe/SwipeDirections)
extern void Swipe_SetScreenTargetPosition_mC3049357A351A3996075CE0E4BBADDD3E380582A (void);
// 0x0000039C UnityEngine.Vector3 Swipe::directionToTargetPosition(Swipe/SwipeDirections)
extern void Swipe_directionToTargetPosition_m18DB48F06F70565F2B6B2F93BA0865EACC090CE4 (void);
// 0x0000039D System.Void Swipe::screenTargetPositionUpdate()
extern void Swipe_screenTargetPositionUpdate_mEA0529BD486A5757A8082B1C9C1751939034A9A3 (void);
// 0x0000039E System.Void Swipe::checkKeyActions()
extern void Swipe_checkKeyActions_m103F237032B84713AEB55F1F9D9A68AD781A937E (void);
// 0x0000039F UnityEngine.Vector2 Swipe::getScaledSwipeVector()
extern void Swipe_getScaledSwipeVector_m9CCEB9C745F69DC3EDBE00E4B2C1DC6CF9AE1744 (void);
// 0x000003A0 System.Void Swipe::Update()
extern void Swipe_Update_m5B8885067E3CB784950DB863B1BDDDA26652D0BD (void);
// 0x000003A1 System.Void Swipe::LateUpdate()
extern void Swipe_LateUpdate_m7E017407F76C5834A5B637C1EE3D8FB5B72BBEE6 (void);
// 0x000003A2 System.Void Swipe::swipeIdleMovement()
extern void Swipe_swipeIdleMovement_m02B1CD397EA2E41BAA80BB3E64391A98B4551A92 (void);
// 0x000003A3 System.Void Swipe::onKlick()
extern void Swipe_onKlick_m46A95227B19FC88ECD5528CEC36D0D34D17FE76F (void);
// 0x000003A4 System.Void Swipe::onRelease()
extern void Swipe_onRelease_m737498D2F05FFFAEAD60C8F665F3012A850E17F6 (void);
// 0x000003A5 System.Void Swipe::processSwipePreview()
extern void Swipe_processSwipePreview_m7A41B6C8B520D660F3E15D7A388549EBA9AF3EDD (void);
// 0x000003A6 System.Void Swipe::processSwipe()
extern void Swipe_processSwipe_m84B4F93491A1A89EDB3D22607A96F378F38C7B95 (void);
// 0x000003A7 System.Void Swipe::.ctor()
extern void Swipe__ctor_m97873674A6FB4BD351B24309C91C278A784F013B (void);
// 0x000003A8 System.Void Swipe/mEvent::.ctor()
extern void mEvent__ctor_m422D2976305253C6ED52A02A067CDBE6474D57F8 (void);
// 0x000003A9 System.Void Swipe/normSwipe::.ctor()
extern void normSwipe__ctor_mD68CC1F537CBC219EDC8152AB45F875217C6930E (void);
// 0x000003AA System.Void Swipe/C_IdleConfig::.ctor()
extern void C_IdleConfig__ctor_m2C70731698DECD10B260B67044FE830991E81274 (void);
// 0x000003AB System.Boolean Swipe/C_ScreenPosition::GetPositioningActive()
extern void C_ScreenPosition_GetPositioningActive_m9F6C0C389CCB7F34881E23B829EC3F76FBB6EEC6 (void);
// 0x000003AC System.Void Swipe/C_ScreenPosition::Reset()
extern void C_ScreenPosition_Reset_mFA0D2FE5B01A4512BBDD4854FF51F95C1632D633 (void);
// 0x000003AD System.Void Swipe/C_ScreenPosition::.ctor()
extern void C_ScreenPosition__ctor_m062E235BD0756EBE6BB947F400E139D8270A7882 (void);
// 0x000003AE System.Void Swipe/C_KeyConfiguration::.ctor(UnityEngine.KeyCode)
extern void C_KeyConfiguration__ctor_m862B97EE4F7D60BE68373643DAE5063BBCC68E18 (void);
// 0x000003AF System.Boolean Swipe/C_KeyConfiguration::GetKey()
extern void C_KeyConfiguration_GetKey_m18DDB7372373C749D0BDA0FFCADA2C8F24F657E3 (void);
// 0x000003B0 System.Boolean Swipe/C_KeyConfiguration::GetKeyDown()
extern void C_KeyConfiguration_GetKeyDown_m83DC83ED24128297D17FC2216400966513ED3E13 (void);
// 0x000003B1 System.Boolean Swipe/C_KeyConfiguration::GetKeyUp()
extern void C_KeyConfiguration_GetKeyUp_m79ACE79C932136C6B12143EB8CE23DF44B249D01 (void);
// 0x000003B2 System.Void Swipe/C_KeyMovements::.ctor()
extern void C_KeyMovements__ctor_m7D7C8E9BBDC5155D1D44DDF6008B4195B0C200C8 (void);
// 0x000003B3 System.String TextReplacement::TranslateAndReplace(System.String)
extern void TextReplacement_TranslateAndReplace_m9BDF2AB53EFBCC14D600FC0FB5E0F7B96ACF0ED7 (void);
// 0x000003B4 System.String TextReplacement::ReplaceStringPlaceHolders(System.String)
extern void TextReplacement_ReplaceStringPlaceHolders_m3E876EC846D9BB38E82B2A67F3FC0E1C93D44CAA (void);
// 0x000003B5 System.Void TextReplacement::.ctor()
extern void TextReplacement__ctor_mDF71E15D2F62DF9CA92174C82F55C664E2171340 (void);
// 0x000003B6 System.Void TextReplacementDisplay::Start()
extern void TextReplacementDisplay_Start_mDD5D4FB4BF6E467E9BC6689C46DFCDA9C92BF694 (void);
// 0x000003B7 System.Collections.IEnumerator TextReplacementDisplay::delay()
extern void TextReplacementDisplay_delay_mF23DE8A32576410DE6033F2477529F1961C63972 (void);
// 0x000003B8 System.Collections.IEnumerator TextReplacementDisplay::cyclicActualization()
extern void TextReplacementDisplay_cyclicActualization_m718D870A00CF1715C2A47C08E227BA09A94D963B (void);
// 0x000003B9 System.Void TextReplacementDisplay::actualizeTextOutput()
extern void TextReplacementDisplay_actualizeTextOutput_m5843A0897CFE52D048FCB6F8DAF1739C4BD5B758 (void);
// 0x000003BA System.Void TextReplacementDisplay::replace()
extern void TextReplacementDisplay_replace_m1C500DF1551ADD25FBC9A3BC3DC88BFAFB221029 (void);
// 0x000003BB System.Collections.Generic.List`1<System.String> TextReplacementDisplay::getTranslatableTerms()
extern void TextReplacementDisplay_getTranslatableTerms_mF5288822E72E50FD80076575D82EF34FB2235954 (void);
// 0x000003BC System.Void TextReplacementDisplay::.ctor()
extern void TextReplacementDisplay__ctor_m05866101F23944C2450AAC60353C19728D551737 (void);
// 0x000003BD System.Void TextReplacementDisplay/<delay>d__4::.ctor(System.Int32)
extern void U3CdelayU3Ed__4__ctor_m7CA16C5D26BDC3223FF857AC48BB7DEE4F7D5506 (void);
// 0x000003BE System.Void TextReplacementDisplay/<delay>d__4::System.IDisposable.Dispose()
extern void U3CdelayU3Ed__4_System_IDisposable_Dispose_m0B66E4484A0C881A4CD168C373129C0076E53E37 (void);
// 0x000003BF System.Boolean TextReplacementDisplay/<delay>d__4::MoveNext()
extern void U3CdelayU3Ed__4_MoveNext_mD9164FFBC739594EFA18F7BB2258E6D2FF146A9D (void);
// 0x000003C0 System.Object TextReplacementDisplay/<delay>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF00484BABD02F82575C68F2D73EC3FF32471CFD (void);
// 0x000003C1 System.Void TextReplacementDisplay/<delay>d__4::System.Collections.IEnumerator.Reset()
extern void U3CdelayU3Ed__4_System_Collections_IEnumerator_Reset_m89DC582C5637A0A9AB60E090B359DBD98470A49F (void);
// 0x000003C2 System.Object TextReplacementDisplay/<delay>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CdelayU3Ed__4_System_Collections_IEnumerator_get_Current_m1CFAA7DE6B7CC9C41DD1A9BC07A52468D9A68C2F (void);
// 0x000003C3 System.Void TextReplacementDisplay/<cyclicActualization>d__5::.ctor(System.Int32)
extern void U3CcyclicActualizationU3Ed__5__ctor_m8E38305516B55E2E423B4C542D8D7EB0CEF3BC51 (void);
// 0x000003C4 System.Void TextReplacementDisplay/<cyclicActualization>d__5::System.IDisposable.Dispose()
extern void U3CcyclicActualizationU3Ed__5_System_IDisposable_Dispose_m81C78D44F1B5A21BD65D249322F6767F9911C53E (void);
// 0x000003C5 System.Boolean TextReplacementDisplay/<cyclicActualization>d__5::MoveNext()
extern void U3CcyclicActualizationU3Ed__5_MoveNext_m99B5F15A82291E4350C2ED1D7FFE3CD30CF200D7 (void);
// 0x000003C6 System.Object TextReplacementDisplay/<cyclicActualization>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcyclicActualizationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3C2A0AF0B4BC30F2B8923FDFF34739B2892AE3B (void);
// 0x000003C7 System.Void TextReplacementDisplay/<cyclicActualization>d__5::System.Collections.IEnumerator.Reset()
extern void U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_Reset_m9CCBFCFEF27DF16814DBF2C2C6171DC25D2C4304 (void);
// 0x000003C8 System.Object TextReplacementDisplay/<cyclicActualization>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_get_Current_m7BEEAC4BD998A473557796E64438100C0E920B23 (void);
// 0x000003C9 System.Collections.Generic.List`1<System.String> TranslatableContent::getTranslatableTerms()
// 0x000003CA System.Void TranslatableContent::.ctor()
extern void TranslatableContent__ctor_mD2A22BEB762C341E99BF0787CB777DC0C7CEE11C (void);
// 0x000003CB System.String TranslationManager::translateIfAvail(System.String)
extern void TranslationManager_translateIfAvail_mA8E28FED4E2CCEDF6D36770AA210187D0F5B0E8F (void);
// 0x000003CC System.Void TranslationManager::Awake()
extern void TranslationManager_Awake_mF01B49B2064CC5724AD753CB3EFF07CCB63C95F7 (void);
// 0x000003CD System.Void TranslationManager::clearTranslatableContentScripts()
extern void TranslationManager_clearTranslatableContentScripts_m6F9188687C3E819BE613CDF62ED895F2D93FA17B (void);
// 0x000003CE System.Void TranslationManager::registerTranslateableContentScript(TranslatableContent)
extern void TranslationManager_registerTranslateableContentScript_m3AC735A23ACCDB632242A1C51DFCFD1722F08604 (void);
// 0x000003CF System.Void TranslationManager::generateTermList()
extern void TranslationManager_generateTermList_mAB7D1E1614D270095A81230E86CC4111E3BAB237 (void);
// 0x000003D0 System.Void TranslationManager::saveListToFile(System.Collections.Generic.List`1<System.String>)
extern void TranslationManager_saveListToFile_m61BE5693AC078ABC16DFC393D12F453812309981 (void);
// 0x000003D1 System.Void TranslationManager::.ctor()
extern void TranslationManager__ctor_m2E68B494A395505CFCB9B009527A196E00D7DB1A (void);
// 0x000003D2 System.Void UIPositionSwitcher::Awake()
extern void UIPositionSwitcher_Awake_m2D140D44B4FADE3A93E0C4606C5E719A507E2D6E (void);
// 0x000003D3 System.Void UIPositionSwitcher::SetRectId(System.Int32)
extern void UIPositionSwitcher_SetRectId_m5581390A0EC5E2B363E198AE0DE4A039DB72E795 (void);
// 0x000003D4 System.Void UIPositionSwitcher::.ctor()
extern void UIPositionSwitcher__ctor_mF11B2B2A04C231065A8295533743409A347DC746 (void);
// 0x000003D5 System.Void ValueChangePreview::Awake()
extern void ValueChangePreview_Awake_mF081514B288EC1654084E827B56157BF2707C129 (void);
// 0x000003D6 System.Void ValueChangePreview::setSpriteSet(System.Int32)
extern void ValueChangePreview_setSpriteSet_m4A77ABA7EFD3709EA0FA1A8F6BD064C110DBA629 (void);
// 0x000003D7 System.Single ValueChangePreview::getPreviewSize(System.Boolean,System.Single,System.Boolean)
extern void ValueChangePreview_getPreviewSize_mB133D676F7506329E131EBB02282B1211CE38C0D (void);
// 0x000003D8 UnityEngine.Sprite ValueChangePreview::getPreviewSprite(System.Boolean,System.Single,System.Boolean)
extern void ValueChangePreview_getPreviewSprite_m389A2FF826929E6F1E7189E5D445BC0483F4D47B (void);
// 0x000003D9 System.String ValueChangePreview::getPreviewText(System.Boolean,System.Single,System.Boolean)
extern void ValueChangePreview_getPreviewText_mA99B29FE978A2DC180A5B4939099166BF822F440 (void);
// 0x000003DA System.Void ValueChangePreview::.ctor()
extern void ValueChangePreview__ctor_m6DB33B9377B77062AEAAA365928CF88FF5920D0A (void);
// 0x000003DB System.Void ValueChangePreview/valueChangeSpriteSet::.ctor()
extern void valueChangeSpriteSet__ctor_mBCE0208355F095DDC984C61206B1DA63EB60B48A (void);
// 0x000003DC System.Void ValueDependentGameLogs::Awake()
extern void ValueDependentGameLogs_Awake_m4397D20348AF10580315AFF425AC61EE61001A7D (void);
// 0x000003DD System.Void ValueDependentGameLogs::Start()
extern void ValueDependentGameLogs_Start_mA04E7290429974E9994B6FA21C08451733ACA5F6 (void);
// 0x000003DE System.Void ValueDependentGameLogs::executeValueLogging(System.Boolean)
extern void ValueDependentGameLogs_executeValueLogging_m6E805705C17A5308083B75B17D4383761AEF4C8D (void);
// 0x000003DF System.Collections.Generic.List`1<System.String> ValueDependentGameLogs::getTranslatableTerms()
extern void ValueDependentGameLogs_getTranslatableTerms_mB9567EDBE971BDE6CA0D51B23E26283D1CCDAF84 (void);
// 0x000003E0 System.Void ValueDependentGameLogs::.ctor()
extern void ValueDependentGameLogs__ctor_m960D2B6CECA909D8EB933A765DD7B3EECC2B1B62 (void);
// 0x000003E1 System.Void ValueDependentGameLogs/valueLog::.ctor()
extern void valueLog__ctor_m03B33183FCEF45F675C5B02882EE992599647FB9 (void);
// 0x000003E2 System.Void ValueDisplay::showMinMaxValue()
extern void ValueDisplay_showMinMaxValue_mD6FB07FC9451A4916349EF4AD22CA76C0E80D264 (void);
// 0x000003E3 System.Void ValueDisplay::Start()
extern void ValueDisplay_Start_m1BB3D218C0D7366703EA77948A635755D5836BF8 (void);
// 0x000003E4 System.Collections.IEnumerator ValueDisplay::cycActualize()
extern void ValueDisplay_cycActualize_mC15584D6A4DFEB246FCA9306F3FCA55A62F40EF1 (void);
// 0x000003E5 System.Void ValueDisplay::.ctor()
extern void ValueDisplay__ctor_m49CCF9067A8662FD5A21EB248D20580B30598E88 (void);
// 0x000003E6 System.Void ValueDisplay/<cycActualize>d__15::.ctor(System.Int32)
extern void U3CcycActualizeU3Ed__15__ctor_m884AB86FB8663308B5D6E197DE3EEB5AF7A09B18 (void);
// 0x000003E7 System.Void ValueDisplay/<cycActualize>d__15::System.IDisposable.Dispose()
extern void U3CcycActualizeU3Ed__15_System_IDisposable_Dispose_mDAA4568D0183CA0B11C556C49CBB15509765B1FF (void);
// 0x000003E8 System.Boolean ValueDisplay/<cycActualize>d__15::MoveNext()
extern void U3CcycActualizeU3Ed__15_MoveNext_m7CDA3C19751B1D65062508BE1F3BFBFFBAC2D5F3 (void);
// 0x000003E9 System.Object ValueDisplay/<cycActualize>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcycActualizeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92D5C095669CE7EE955BD8782F09A4E5CA451711 (void);
// 0x000003EA System.Void ValueDisplay/<cycActualize>d__15::System.Collections.IEnumerator.Reset()
extern void U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_Reset_m921D4CA172B2615F151E64DCDA76E4D1796B3A04 (void);
// 0x000003EB System.Object ValueDisplay/<cycActualize>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_get_Current_m405E4962AA5731495D4C3F97E5DC3DFCEC6773A2 (void);
// 0x000003EC System.Single ValueMath::getOperationValue(ValueMath/C_MathOperation)
extern void ValueMath_getOperationValue_mA57D49567979FC85C915F8D393F3CB880B89A380 (void);
// 0x000003ED System.Void ValueMath::Calculate()
extern void ValueMath_Calculate_m5AF67FCBDDD31A861F4072347AE9FCFBD5C95234 (void);
// 0x000003EE System.Void ValueMath::.ctor()
extern void ValueMath__ctor_m07BC88C83575C56C86B6A295C033411EEF281FA4 (void);
// 0x000003EF System.Void ValueMath/mEvent::.ctor()
extern void mEvent__ctor_mD66F1FEBF6C58DEACBCA94DFA844372F42BFA2AB (void);
// 0x000003F0 System.Void ValueMath/C_MathOperation::.ctor()
extern void C_MathOperation__ctor_m034C6734164A068E50271430BF044CF33CAA025F (void);
// 0x000003F1 System.Void ValueMath/C_Mathf_rArgs::.ctor()
extern void C_Mathf_rArgs__ctor_mD7F2116B12BE65EF84AC0B448F00F8356A37EECE (void);
// 0x000003F2 System.Void ValueScript::Awake()
extern void ValueScript_Awake_m3D3FD365BB5B57155CA15DDD68E532F89D0A78A8 (void);
// 0x000003F3 System.Void ValueScript::Start()
extern void ValueScript_Start_mD37FA8AC537308A495A1834CA8D4BC954BBE924D (void);
// 0x000003F4 System.Void ValueScript::actualizeUI(System.Single)
extern void ValueScript_actualizeUI_mF92E4E69F873F03D6D46A8F5C77801B7CE539723 (void);
// 0x000003F5 System.Void ValueScript::setPreviewValues(System.Single,System.Boolean)
extern void ValueScript_setPreviewValues_mFA71740BC45527F1D56D7241CC7D4A4211BAD826 (void);
// 0x000003F6 System.Void ValueScript::clearPreviewValues()
extern void ValueScript_clearPreviewValues_mD4F140B594B9C6A02365D9FB23FC3B2E2125D93B (void);
// 0x000003F7 System.Void ValueScript::Update()
extern void ValueScript_Update_m021D6B0E64810B5AC263BDCA7EBE3054962B7222 (void);
// 0x000003F8 System.Void ValueScript::actualizePreviewElements()
extern void ValueScript_actualizePreviewElements_m99110F0413231F388BB8F6CA953631F786D2A52A (void);
// 0x000003F9 System.Void ValueScript::actualizeIconsBaseOnValue()
extern void ValueScript_actualizeIconsBaseOnValue_m51C32F0E5DAB4A635D58A6B3971DD083EA1BE123 (void);
// 0x000003FA System.Void ValueScript::testActualizeIconsBaseOnValue()
extern void ValueScript_testActualizeIconsBaseOnValue_m6FE26B8BD320095F1B68A413437AEB7828D8A263 (void);
// 0x000003FB System.Void ValueScript::limitValue()
extern void ValueScript_limitValue_mCED7B230FBEB35847217FE568E58E27961FED3B0 (void);
// 0x000003FC System.Single ValueScript::addValue(System.Single)
extern void ValueScript_addValue_m68EFFD4700890EA81A9338E20605EEAB7AB83122 (void);
// 0x000003FD System.Single ValueScript::setValue(System.Single)
extern void ValueScript_setValue_m37FFD00AB6EDB81B63C77EACF987FAAE04B560FE (void);
// 0x000003FE System.Single ValueScript::setValueRandom()
extern void ValueScript_setValueRandom_m22913683D5C903C425565E857D0C8D265AFA9C02 (void);
// 0x000003FF System.Void ValueScript::newGameStart()
extern void ValueScript_newGameStart_mDF5BD79249742B88D51A255E24BE5413CC077B27 (void);
// 0x00000400 System.Single ValueScript::multiplyValue(System.Single)
extern void ValueScript_multiplyValue_m42A7F411F66AC67D36A4E93EC12B4CAD87845FEC (void);
// 0x00000401 System.Void ValueScript::loadValue()
extern void ValueScript_loadValue_m0F75C9D364FE0CED4A4720B5E2594EA925919ACE (void);
// 0x00000402 System.Void ValueScript::saveValue()
extern void ValueScript_saveValue_m2E5DB5C97746CE20F249F7DAB26AB1A628014981 (void);
// 0x00000403 System.Void ValueScript::saveMinMax()
extern void ValueScript_saveMinMax_m5235CE2222FC273369E527C36291E3E19F2123AD (void);
// 0x00000404 System.Single ValueScript::getMaxValue()
extern void ValueScript_getMaxValue_m96433B7FCC0A234D835E494D610D9509A4978123 (void);
// 0x00000405 System.Single ValueScript::getMinValue()
extern void ValueScript_getMinValue_mF4916A33C4E62980F8DBCFE80D01074CB131573E (void);
// 0x00000406 System.Void ValueScript::OnDestroy()
extern void ValueScript_OnDestroy_m54BC66C719039F311527CC3016047A5A13C28D12 (void);
// 0x00000407 System.Void ValueScript::.ctor()
extern void ValueScript__ctor_mF28EE781916C3F1EEA78C6C459B3F29E58EAE0DE (void);
// 0x00000408 System.Void ValueScript/valueDependantIcon::.ctor()
extern void valueDependantIcon__ctor_m15E4D75B913610F255176D4DD7A6106476EDB83E (void);
// 0x00000409 System.Void ValueScript/valueToIcon::.ctor()
extern void valueToIcon__ctor_mBCFC9CB24907E6AA2AE9DBAF739801618F05B091 (void);
// 0x0000040A System.Void ValueScript/valueToText::.ctor()
extern void valueToText__ctor_m23117B9B9A35A2622A2E6D8F4416F9394A206989 (void);
// 0x0000040B System.Void ValueScript/uiConfig::.ctor()
extern void uiConfig__ctor_m327B7BEE71302A7AC020765FD1AF8A787BE400D7 (void);
// 0x0000040C System.Void ValueScript/uiValueChange::.ctor()
extern void uiValueChange__ctor_m712AD92B9ADF482D7121BDB52A9E3AEA8A600A6B (void);
// 0x0000040D System.Void ValueScript/c_nextValueChage::.ctor()
extern void c_nextValueChage__ctor_m47DDB6C4A0D43429300D73E58A04E377D544A2DD (void);
// 0x0000040E System.Void ValueScript/valueLimits::.ctor()
extern void valueLimits__ctor_m274E13B515FCD24D219EEDDA5DD607CD6E24F214 (void);
// 0x0000040F System.Void ValueScript/mEvent::.ctor()
extern void mEvent__ctor_mDA57C37775B9F55C8D1E7C3534F8F4306531A347 (void);
// 0x00000410 System.Void ValueScript/valueEvents::.ctor()
extern void valueEvents__ctor_mFC287D8D88ACAAFD0838E80AFD54D15C4D613A47 (void);
// 0x00000411 System.Void VideoPlayerEvents::Awake()
extern void VideoPlayerEvents_Awake_mF6C2EDA82125B660764E3C1E634CF606B04D8DAE (void);
// 0x00000412 System.Void VideoPlayerEvents::ClockResyncOccurred(UnityEngine.Video.VideoPlayer,System.Double)
extern void VideoPlayerEvents_ClockResyncOccurred_mD9BC3625DA332F1AD156A7A3DF723D2B9E5883C1 (void);
// 0x00000413 System.Void VideoPlayerEvents::ErrorReceived(UnityEngine.Video.VideoPlayer,System.String)
extern void VideoPlayerEvents_ErrorReceived_m25F667F760E9E702D200FC5604FBC407D4515A09 (void);
// 0x00000414 System.Void VideoPlayerEvents::EndReached(UnityEngine.Video.VideoPlayer)
extern void VideoPlayerEvents_EndReached_m76225610F5CB009140628E7890BDE26E2AA41534 (void);
// 0x00000415 System.Void VideoPlayerEvents::PrepareCompleted(UnityEngine.Video.VideoPlayer)
extern void VideoPlayerEvents_PrepareCompleted_m528C058ECEC8D07E2A65EEABFAA44DC4501C2739 (void);
// 0x00000416 System.Void VideoPlayerEvents::SeekCompleted(UnityEngine.Video.VideoPlayer)
extern void VideoPlayerEvents_SeekCompleted_m1F7DCF2A0A2F5F9BF98945424E9F0184BDE1A1BD (void);
// 0x00000417 System.Void VideoPlayerEvents::Started(UnityEngine.Video.VideoPlayer)
extern void VideoPlayerEvents_Started_m7AF191232100E8D3D1E5850DDE9960D59FA1FF68 (void);
// 0x00000418 System.Void VideoPlayerEvents::.ctor()
extern void VideoPlayerEvents__ctor_m861EB965B5B178232F3A57ECA512A288B1BE2D18 (void);
// 0x00000419 System.Void VideoPlayerEvents/mEvent::.ctor()
extern void mEvent__ctor_mC93DF36162B652A6F627787AFF002B4F788FDA3F (void);
// 0x0000041A System.Void VideoPlayerEvents/mDescribedEvent::Invoke()
extern void mDescribedEvent_Invoke_mFB64D848A306E9D3374C6EEF8E0B79A7C05C11CE (void);
// 0x0000041B System.Void VideoPlayerEvents/mDescribedEvent::.ctor()
extern void mDescribedEvent__ctor_mE8D0BB42D36F4A896D335DF54A2F8E9172B99F90 (void);
// 0x0000041C System.Void VideoPlayerEvents/C_videoEvents::.ctor()
extern void C_videoEvents__ctor_m85053570A38FA36C3B7D1E08C7501A59779774DB (void);
// 0x0000041D System.Void addAchievement::add_Achievement()
extern void addAchievement_add_Achievement_m1ADDAF21A1B039E791D26D3D9D9974E776A5A484 (void);
// 0x0000041E System.Void addAchievement::set_AchievementCounter(System.Int32)
extern void addAchievement_set_AchievementCounter_m2B89859B6DEFEC3A969D707B947281A0163F7C11 (void);
// 0x0000041F System.Void addAchievement::.ctor()
extern void addAchievement__ctor_mAE3349095E35AA1DA60FBF5A402BFAA30C753AA4 (void);
// 0x00000420 System.Void addValue::addValues()
extern void addValue_addValues_m380AC79EC53FE31C0C81B7E88AD66C4FD2E2D53D (void);
// 0x00000421 System.Void addValue::.ctor()
extern void addValue__ctor_m53423167C41C6472146527A5B29B4BDF614379BC (void);
// 0x00000422 System.Void addValueToValue::addValues()
extern void addValueToValue_addValues_mE54FCB68A324CC48E0E4A743DDCECF1B4F35263A (void);
// 0x00000423 System.Void addValueToValue::.ctor()
extern void addValueToValue__ctor_mC9C48C3ED80AA25734AAED8262B492FC20A15210 (void);
// 0x00000424 System.Void addValueToValue/resultModifierForAddingValueToValue::.ctor()
extern void resultModifierForAddingValueToValue__ctor_m7F9040A5252F66E6B389BF57CACE86A183C29AAE (void);
// 0x00000425 System.Void changeValue::ChangeValues()
extern void changeValue_ChangeValues_m5C9A3DF00CC2A692CEDA887CDBDAAFB2258E7838 (void);
// 0x00000426 System.Void changeValue::.ctor()
extern void changeValue__ctor_mDB12171B7C1C659E2B7AA8332A71E3FE5BA14AB4 (void);
// 0x00000427 System.Void conditionalActionList::Awake()
extern void conditionalActionList_Awake_mE34C8AB87F96AB4A2792C35520F73F49AFC892B1 (void);
// 0x00000428 System.Void conditionalActionList::Start()
extern void conditionalActionList_Start_mA9B5DE6D46CDE3107538B658F1067A17BA61BD21 (void);
// 0x00000429 System.Void conditionalActionList::OnDestroy()
extern void conditionalActionList_OnDestroy_m6959D55F600FF40514F7062F24F566012519C975 (void);
// 0x0000042A System.Void conditionalActionList::OnCardDestroy()
extern void conditionalActionList_OnCardDestroy_mC28F549BCB64166E154667A891F1D742E0526BE6 (void);
// 0x0000042B System.Collections.IEnumerator conditionalActionList::eachFrame()
extern void conditionalActionList_eachFrame_m2D0D706D9FE437F1B7EE6F2B1C5EC027134612EF (void);
// 0x0000042C System.Void conditionalActionList::initialize()
extern void conditionalActionList_initialize_m94DF103F8AF47FF6902DEBBA27D826AFB5B9CD75 (void);
// 0x0000042D System.Void conditionalActionList::ExecuteCheck()
extern void conditionalActionList_ExecuteCheck_m3800CA576A2E35DFADFDF2141528EF7EEB5DE1FC (void);
// 0x0000042E System.Void conditionalActionList::executionLogic()
extern void conditionalActionList_executionLogic_mCB599A8BD720FB8483FE217187D809813E27FA0F (void);
// 0x0000042F System.Collections.Generic.List`1<System.String> conditionalActionList::getTranslatableTerms()
extern void conditionalActionList_getTranslatableTerms_mEA1CA06C1A46BCC734299F0921963D81FC0B2EB5 (void);
// 0x00000430 System.Void conditionalActionList::.ctor()
extern void conditionalActionList__ctor_m635CBB4A24D14B081EA63562A60AC169F01456DC (void);
// 0x00000431 System.Void conditionalActionList/mEvent::.ctor()
extern void mEvent__ctor_m6DC599616A023B862DFD2B782BA1F4542EDF14EC (void);
// 0x00000432 System.Void conditionalActionList/C_ValueContentToAction::.ctor()
extern void C_ValueContentToAction__ctor_mDE10F180C3B8FB9A2AC177EF9673FE83FB7D4133 (void);
// 0x00000433 System.Void conditionalActionList/<eachFrame>d__13::.ctor(System.Int32)
extern void U3CeachFrameU3Ed__13__ctor_m7555B9ACB0F67EF10AE18E2C526B2D41E8D4955D (void);
// 0x00000434 System.Void conditionalActionList/<eachFrame>d__13::System.IDisposable.Dispose()
extern void U3CeachFrameU3Ed__13_System_IDisposable_Dispose_m0F460470A415E6489949AB9394BC1F8692CE282A (void);
// 0x00000435 System.Boolean conditionalActionList/<eachFrame>d__13::MoveNext()
extern void U3CeachFrameU3Ed__13_MoveNext_mCF050519B456187F624902D96CE0F4070FAB8D87 (void);
// 0x00000436 System.Object conditionalActionList/<eachFrame>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CeachFrameU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA8B187D2D5D200C97BEDECF74EB850597D69F56 (void);
// 0x00000437 System.Void conditionalActionList/<eachFrame>d__13::System.Collections.IEnumerator.Reset()
extern void U3CeachFrameU3Ed__13_System_Collections_IEnumerator_Reset_mE1C939B7483AA60C3C43B5A25C8FBB968DF43DDA (void);
// 0x00000438 System.Object conditionalActionList/<eachFrame>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CeachFrameU3Ed__13_System_Collections_IEnumerator_get_Current_mF23BE2C11DF83A40BB3750D9B86CCA27AC067457 (void);
// 0x00000439 System.Void firstStartGameObjectSetter::Start()
extern void firstStartGameObjectSetter_Start_m1134FC1606A600B38E52FBF40C6CC81A391EA22D (void);
// 0x0000043A System.Void firstStartGameObjectSetter::.ctor()
extern void firstStartGameObjectSetter__ctor_mD3457518524DDFD2717BD90E02CC52B4495C62AD (void);
// 0x0000043B System.Void scoreCalculator::calculateScore()
extern void scoreCalculator_calculateScore_m23A506CAA621D613FAFD8FFDB537F0C17B6B331A (void);
// 0x0000043C System.Void scoreCalculator::Awake()
extern void scoreCalculator_Awake_mF3B084D87D53370337CDE8DC21E3F17136E2E171 (void);
// 0x0000043D System.Void scoreCalculator::addLevelUpXp(System.Single)
extern void scoreCalculator_addLevelUpXp_m536D9EF1ECDE2A86B3E7E92EEC864F60960F9499 (void);
// 0x0000043E System.Void scoreCalculator::Start()
extern void scoreCalculator_Start_m4D99ACA7FD8E3B2AE95DFFECA01DA953BEABAFF6 (void);
// 0x0000043F System.Void scoreCalculator::Update()
extern void scoreCalculator_Update_m57CDC8272BEE5E3DEFCA8FE4D277AFCC00A09000 (void);
// 0x00000440 System.Void scoreCalculator::.ctor()
extern void scoreCalculator__ctor_m9B6788D6960DDF55F6E4171AC2539806C038CE52 (void);
// 0x00000441 System.Void scoreCalculator/mEvent::.ctor()
extern void mEvent__ctor_m929AF7D30C91997E469BA4DD5F4B2ADA26D9F280 (void);
// 0x00000442 System.Void scoreCalculator/scoreRelevantPair::.ctor()
extern void scoreRelevantPair__ctor_m3CBFCD4BD66A73D1817A3C385E37AB674E5212F0 (void);
// 0x00000443 System.Void scoreCalculator/C_ScoreSave::.ctor()
extern void C_ScoreSave__ctor_mBC24674EB6027BDC8261A88A7A283FFDECF4704A (void);
// 0x00000444 System.Void scoreCounter::increase(System.Int32)
extern void scoreCounter_increase_mCC44040870F0C3EC82E9727896DC697D20AB49CB (void);
// 0x00000445 System.Void scoreCounter::setScore(System.Int32)
extern void scoreCounter_setScore_m32A26B0537AB72F7DDBCD19D836BC4131DB3BCEF (void);
// 0x00000446 System.Int32 scoreCounter::getScore()
extern void scoreCounter_getScore_m343DCDBBA55DCB63EC5CDA07C27252C7163CBD3F (void);
// 0x00000447 System.Void scoreCounter::setMaxScore(System.Int32)
extern void scoreCounter_setMaxScore_mAA9F164C785481AAF855C0D3CF580290C1B8D2B8 (void);
// 0x00000448 System.Void scoreCounter::save()
extern void scoreCounter_save_m7E2DF3EF70FB123C78C9A6394C4C397D3E733C41 (void);
// 0x00000449 System.Int32 scoreCounter::load()
extern void scoreCounter_load_m0ADB24C1D00E2B932664D3581D621043E721A21C (void);
// 0x0000044A System.Void scoreCounter::Start()
extern void scoreCounter_Start_m9F93E1058110C052520AA6DE303AF796938CDE11 (void);
// 0x0000044B System.Void scoreCounter::Update()
extern void scoreCounter_Update_m4CBA4A996B332D8F0C257F6BFE6429509A8E74A3 (void);
// 0x0000044C System.Void scoreCounter::.ctor()
extern void scoreCounter__ctor_m5971308913C400AA1FAC6DE46BF08D0A57E711E4 (void);
// 0x0000044D System.Void setValue::setValues()
extern void setValue_setValues_m33499F09D12C3092076C485B179E9B09117FF180 (void);
// 0x0000044E System.Void setValue::.ctor()
extern void setValue__ctor_m263767AF5C10474DC79D20903E3FA83E4C6FE90F (void);
// 0x0000044F System.Void valueDefinitions::.ctor()
extern void valueDefinitions__ctor_mE859CD4238C0D5D5A96F752EDA112C342EFEE16D (void);
// 0x00000450 System.Void valueDependentConditionalImages::Start()
extern void valueDependentConditionalImages_Start_m9C4B1054EC8B883F9BC1FA8DD4A135E6DEA7C073 (void);
// 0x00000451 System.Collections.IEnumerator valueDependentConditionalImages::oneFrame()
extern void valueDependentConditionalImages_oneFrame_mAB42BA6CFBBBCF5895EB5C08C3C426F70F6E1C27 (void);
// 0x00000452 System.Void valueDependentConditionalImages::ExecuteConditionCheck()
extern void valueDependentConditionalImages_ExecuteConditionCheck_mF986358DF054FA039E18695EB45A0B6AF20ABC31 (void);
// 0x00000453 System.Void valueDependentConditionalImages::testAndActualize(System.Boolean)
extern void valueDependentConditionalImages_testAndActualize_m8494C93476227A3A525936F3C5D72647E2218F5D (void);
// 0x00000454 System.Void valueDependentConditionalImages::actualize(ValueScript/valueToIcon[])
extern void valueDependentConditionalImages_actualize_mB3002FBEEE357626EE946DEB6BAF30F719C5DD4D (void);
// 0x00000455 System.Void valueDependentConditionalImages::actualizeIconsBaseOnValue(ValueScript/valueToIcon[],System.Single)
extern void valueDependentConditionalImages_actualizeIconsBaseOnValue_mB7AB0E98F454D48ED9BD0BA36AA311124CA98E06 (void);
// 0x00000456 System.Void valueDependentConditionalImages::.ctor()
extern void valueDependentConditionalImages__ctor_m4FAF942341A4A812AC5ABC7DBE8AC0845ECAD12C (void);
// 0x00000457 System.Void valueDependentConditionalImages/mEvent::.ctor()
extern void mEvent__ctor_mB5D959224CCCDAD4222E74A658DC12282B1B392E (void);
// 0x00000458 System.Void valueDependentConditionalImages/<oneFrame>d__9::.ctor(System.Int32)
extern void U3ConeFrameU3Ed__9__ctor_m526235E2CA6FC5AB734BC6AB29863091F2F6F150 (void);
// 0x00000459 System.Void valueDependentConditionalImages/<oneFrame>d__9::System.IDisposable.Dispose()
extern void U3ConeFrameU3Ed__9_System_IDisposable_Dispose_m3473E1959E7822609AF060B96EE35B80428D6757 (void);
// 0x0000045A System.Boolean valueDependentConditionalImages/<oneFrame>d__9::MoveNext()
extern void U3ConeFrameU3Ed__9_MoveNext_m97B3136536CC5E2C94CF4915AD3C112B72388AD1 (void);
// 0x0000045B System.Object valueDependentConditionalImages/<oneFrame>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ConeFrameU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD831E212FC446D5F0C503CE9F56D6B1AC815E565 (void);
// 0x0000045C System.Void valueDependentConditionalImages/<oneFrame>d__9::System.Collections.IEnumerator.Reset()
extern void U3ConeFrameU3Ed__9_System_Collections_IEnumerator_Reset_mED64782DBA020DC52AA1107DB00A44B2D7CD3D85 (void);
// 0x0000045D System.Object valueDependentConditionalImages/<oneFrame>d__9::System.Collections.IEnumerator.get_Current()
extern void U3ConeFrameU3Ed__9_System_Collections_IEnumerator_get_Current_m6779D76973F63196D278DE6A3E80386D47855A83 (void);
// 0x0000045E System.Void valueDependentEnable::Update()
extern void valueDependentEnable_Update_m60D849DC19FEA8EC8644CD561B85B30BE0F1FBA4 (void);
// 0x0000045F System.Void valueDependentEnable::.ctor()
extern void valueDependentEnable__ctor_mEEAC850534F7F81762409DD3FE6C67646391292F (void);
// 0x00000460 System.Void valueDependentEvent::Start()
extern void valueDependentEvent_Start_mD9E9C3A575D39551CA87A7D4456478E556C873B9 (void);
// 0x00000461 System.Collections.IEnumerator valueDependentEvent::oneFrame()
extern void valueDependentEvent_oneFrame_m41A40BB51B3A6F5A3CFA19588732D053D843FAD0 (void);
// 0x00000462 System.Void valueDependentEvent::ExecuteConditionCheck()
extern void valueDependentEvent_ExecuteConditionCheck_mF07302F3AD444E7E94F8AFEBD6769740376487E2 (void);
// 0x00000463 System.Void valueDependentEvent::testAndInvoke(System.Boolean)
extern void valueDependentEvent_testAndInvoke_m953CAD0A601A41F5A4ABD5DC004567E6AE78D6F8 (void);
// 0x00000464 System.Void valueDependentEvent::.ctor()
extern void valueDependentEvent__ctor_m746B3856B70444F76185D2E22FEA9C0851D76309 (void);
// 0x00000465 System.Void valueDependentEvent/mEvent::.ctor()
extern void mEvent__ctor_mB4082415624720B574B121E9BC2BDCD550331F63 (void);
// 0x00000466 System.Void valueDependentEvent/<oneFrame>d__7::.ctor(System.Int32)
extern void U3ConeFrameU3Ed__7__ctor_m853C858DD2E0A4C942AC48957E47B406F60F0AEE (void);
// 0x00000467 System.Void valueDependentEvent/<oneFrame>d__7::System.IDisposable.Dispose()
extern void U3ConeFrameU3Ed__7_System_IDisposable_Dispose_m69CD851696FFDC0061D1ECD990D56DA0B439B7B5 (void);
// 0x00000468 System.Boolean valueDependentEvent/<oneFrame>d__7::MoveNext()
extern void U3ConeFrameU3Ed__7_MoveNext_m226B9B41C26BE20C262164E1AC7FD1518BD8CD6F (void);
// 0x00000469 System.Object valueDependentEvent/<oneFrame>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ConeFrameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m24083F1557E44580268A6637582B9AAA7D37BE63 (void);
// 0x0000046A System.Void valueDependentEvent/<oneFrame>d__7::System.Collections.IEnumerator.Reset()
extern void U3ConeFrameU3Ed__7_System_Collections_IEnumerator_Reset_mF061980C2A1E65BDF89EAF28A2B5D9A9B4BB3A1B (void);
// 0x0000046B System.Object valueDependentEvent/<oneFrame>d__7::System.Collections.IEnumerator.get_Current()
extern void U3ConeFrameU3Ed__7_System_Collections_IEnumerator_get_Current_m52A004719D90B66FF54E4931166AAD5741764477 (void);
// 0x0000046C System.Void valueDependentImages::Start()
extern void valueDependentImages_Start_m1705E3302D85626EB3130349E874E9440A056FB4 (void);
// 0x0000046D System.Collections.IEnumerator valueDependentImages::cycActulize()
extern void valueDependentImages_cycActulize_m9C138934BF325F4A8070605D6E61E0F45E9EE300 (void);
// 0x0000046E System.Void valueDependentImages::actualize()
extern void valueDependentImages_actualize_m02EED59A68D315EFC3740D940E8755F13B3BBEC2 (void);
// 0x0000046F System.Void valueDependentImages::actualizeIconsBaseOnValue()
extern void valueDependentImages_actualizeIconsBaseOnValue_m0C6AC7B382107F3ABB93E1B87C482595731488A0 (void);
// 0x00000470 System.Void valueDependentImages::.ctor()
extern void valueDependentImages__ctor_m68BD6822CF34F655D258392401E8798F544A25A2 (void);
// 0x00000471 System.Void valueDependentImages/<cycActulize>d__5::.ctor(System.Int32)
extern void U3CcycActulizeU3Ed__5__ctor_mDBBAFFFF8A92A55D8E3B43B8536C308D7D3A3433 (void);
// 0x00000472 System.Void valueDependentImages/<cycActulize>d__5::System.IDisposable.Dispose()
extern void U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_m99F9DA8C4364C3F1AC80C2CF07B952136C1A64D0 (void);
// 0x00000473 System.Boolean valueDependentImages/<cycActulize>d__5::MoveNext()
extern void U3CcycActulizeU3Ed__5_MoveNext_m82EBFB1A73D2CBE83920B1F3B7840BFC178803A3 (void);
// 0x00000474 System.Object valueDependentImages/<cycActulize>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DAB6FA561D2922E658BF2B4B8AEF4BBF91CE4EC (void);
// 0x00000475 System.Void valueDependentImages/<cycActulize>d__5::System.Collections.IEnumerator.Reset()
extern void U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_mB32402A65A3CCC658A347C707C0FE56B68D4938A (void);
// 0x00000476 System.Object valueDependentImages/<cycActulize>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_m0DE39A326FC147072798CCA18C508086C011E33F (void);
// 0x00000477 System.Void valueDependentTexts::Start()
extern void valueDependentTexts_Start_mD2964633494AFC99E42FD059130BC8AE14C1925F (void);
// 0x00000478 System.Collections.IEnumerator valueDependentTexts::cycActulize()
extern void valueDependentTexts_cycActulize_m2BB64E52678442A5E7DBCA3B48D1147759CBAC20 (void);
// 0x00000479 System.Void valueDependentTexts::actualize()
extern void valueDependentTexts_actualize_m33BE03EB6851CDB764146BD0BC1D6D56559370FC (void);
// 0x0000047A System.Void valueDependentTexts::actualizeIconsBaseOnValue()
extern void valueDependentTexts_actualizeIconsBaseOnValue_m5ECCFBE03335C7B55183DD06FCDED8885348EB64 (void);
// 0x0000047B System.Void valueDependentTexts::.ctor()
extern void valueDependentTexts__ctor_m12D09891EE1382536A0D844929654C6E5F6FAF07 (void);
// 0x0000047C System.Void valueDependentTexts/<cycActulize>d__5::.ctor(System.Int32)
extern void U3CcycActulizeU3Ed__5__ctor_mB0FEBDE8940FB3BCA8EE32254835E36CECEAD7E1 (void);
// 0x0000047D System.Void valueDependentTexts/<cycActulize>d__5::System.IDisposable.Dispose()
extern void U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_mF7D9DB7F1B0F96318D03BAE41FD73BD0057D56BC (void);
// 0x0000047E System.Boolean valueDependentTexts/<cycActulize>d__5::MoveNext()
extern void U3CcycActulizeU3Ed__5_MoveNext_mEE54800BEE635E7C52F1EC721919C5589414D25C (void);
// 0x0000047F System.Object valueDependentTexts/<cycActulize>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA1C19D6A96F6104F79E604D37F6E74EFDD5AADD (void);
// 0x00000480 System.Void valueDependentTexts/<cycActulize>d__5::System.Collections.IEnumerator.Reset()
extern void U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_m2836EF31113785AA83327C23B25385F6C218333F (void);
// 0x00000481 System.Object valueDependentTexts/<cycActulize>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_mB9051E40AE1D230D32FCD8D7C3C5F8BE831B451A (void);
// 0x00000482 System.Void valueDependentTimer::Start()
extern void valueDependentTimer_Start_m585E041219006860ABD99BD7D5F71DA8BA05998C (void);
// 0x00000483 System.Collections.IEnumerator valueDependentTimer::cyclicTestValue()
extern void valueDependentTimer_cyclicTestValue_mF25FBA3D0CAC2801FCF2FEAE5E172B553CD1BA15 (void);
// 0x00000484 System.Void valueDependentTimer::.ctor()
extern void valueDependentTimer__ctor_mAA35316957A5A2CA50F0C3BD6A2B40DE38C8D8E3 (void);
// 0x00000485 System.Void valueDependentTimer/mEvent::.ctor()
extern void mEvent__ctor_m296D1359F1CFA689462E60200E8934E294C83878 (void);
// 0x00000486 System.Void valueDependentTimer/<cyclicTestValue>d__9::.ctor(System.Int32)
extern void U3CcyclicTestValueU3Ed__9__ctor_m242DBED8F79E038793145948FBB5B3053E5C7C08 (void);
// 0x00000487 System.Void valueDependentTimer/<cyclicTestValue>d__9::System.IDisposable.Dispose()
extern void U3CcyclicTestValueU3Ed__9_System_IDisposable_Dispose_mE0B503F8CEA59D250D9A01B27100568467D9DC4D (void);
// 0x00000488 System.Boolean valueDependentTimer/<cyclicTestValue>d__9::MoveNext()
extern void U3CcyclicTestValueU3Ed__9_MoveNext_mB135BE45AB398EAE8096DA33AB9242EF15EF505F (void);
// 0x00000489 System.Object valueDependentTimer/<cyclicTestValue>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcyclicTestValueU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0575A7EE9E712A8D39FA707CA1AE13A417AF27B (void);
// 0x0000048A System.Void valueDependentTimer/<cyclicTestValue>d__9::System.Collections.IEnumerator.Reset()
extern void U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_Reset_m78215A7BE3957DE9415B27EF8DCECA35B47D8B38 (void);
// 0x0000048B System.Object valueDependentTimer/<cyclicTestValue>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_get_Current_m68C1793EA5E692C4DAC56318B4758A4491CBBF2F (void);
// 0x0000048C System.Void valueManager::Awake()
extern void valueManager_Awake_m5066643BB1CAF4995BA76CF7DAA8FC89604524E9 (void);
// 0x0000048D System.Void valueManager::registerValueScript(ValueScript)
extern void valueManager_registerValueScript_mF85F7EFCC662C96794835B4D8E702DC38E08723E (void);
// 0x0000048E ValueScript valueManager::getFirstFittingValue(valueDefinitions/values)
extern void valueManager_getFirstFittingValue_m12DB2EFD1CB46F6EE8FFB99A3C75F3AF59489588 (void);
// 0x0000048F System.Void valueManager::saveAllMinMaxValues()
extern void valueManager_saveAllMinMaxValues_m69700B29A92FBD6BCF72FDAD47B75187B2381481 (void);
// 0x00000490 System.Boolean valueManager::getConditionMet(EventScript/condition,System.Boolean)
extern void valueManager_getConditionMet_mC58154863B22C8F7F967834FF4C5CB92ACA84126 (void);
// 0x00000491 System.Boolean valueManager::AreConditinsForResultMet(EventScript/condition[])
extern void valueManager_AreConditinsForResultMet_m86A79E143AA6786A74A4997383A37DA541C3EDA1 (void);
// 0x00000492 System.Void valueManager::addValue(valueDefinitions/values,System.Single)
extern void valueManager_addValue_m584801B586C29A4584D3B9D21997F5C46477FED9 (void);
// 0x00000493 System.Void valueManager::changeValue(valueDefinitions/values,System.Single)
extern void valueManager_changeValue_m48152EC781421E4FD890F77E73E8E6C24012E198 (void);
// 0x00000494 System.Void valueManager::setValue(valueDefinitions/values,System.Single)
extern void valueManager_setValue_m0A922450126E57860354F4A8A5FC6ED90F756114 (void);
// 0x00000495 System.Void valueManager::setRandomValues()
extern void valueManager_setRandomValues_mBE8DAEE3C146BAE85CA236B7D2023FE572BC10C1 (void);
// 0x00000496 System.Void valueManager::clearAllPreviews()
extern void valueManager_clearAllPreviews_m5F1E4FC5F2618E5FAD24D4199CD6CCA565C9501D (void);
// 0x00000497 System.Void valueManager::setPreviews(System.Collections.Generic.List`1<EventScript/resultModifierPreview>&)
extern void valueManager_setPreviews_m090BD77F6E6274055D681951FE2822C03D53721B (void);
// 0x00000498 System.Void valueManager::setPreview(valueDefinitions/values,System.Single,System.Boolean)
extern void valueManager_setPreview_m506AD9CD688657E718AB0D7F69DDDC340FE9512B (void);
// 0x00000499 System.Void valueManager::testForDuplicatesAndMissingValues()
extern void valueManager_testForDuplicatesAndMissingValues_mC4AAACA8F8A7DE84A307C30B31D6970146C4758C (void);
// 0x0000049A System.Void valueManager::.ctor()
extern void valueManager__ctor_mD14BD63AAF2D72F350929EA867277394423A64E1 (void);
// 0x0000049B System.Void Crystal.SafeArea::Awake()
extern void SafeArea_Awake_m3369BF6C60075BD99152C8F6B4120900E56EBC95 (void);
// 0x0000049C System.Void Crystal.SafeArea::Update()
extern void SafeArea_Update_mFFD9106833750FA9657EAE3BC564A688F9216D81 (void);
// 0x0000049D System.Void Crystal.SafeArea::Refresh()
extern void SafeArea_Refresh_mBFFBE8630A66D7F813DCB6095B64629A5ED542CE (void);
// 0x0000049E UnityEngine.Rect Crystal.SafeArea::GetSafeArea()
extern void SafeArea_GetSafeArea_m1D364DF35EFEC726EA65743C99DB36850CB3FC42 (void);
// 0x0000049F System.Void Crystal.SafeArea::ApplySafeArea(UnityEngine.Rect)
extern void SafeArea_ApplySafeArea_m167F7C50FCBB28FFE1EBE54407172834884A9AEA (void);
// 0x000004A0 System.Void Crystal.SafeArea::.ctor()
extern void SafeArea__ctor_m1A9E3BE6B986EC5205BFADF6AD825188C25703CE (void);
// 0x000004A1 System.Void Crystal.SafeArea::.cctor()
extern void SafeArea__cctor_m235200F1CD3FA25E844437CC4D9D3856ADCADE84 (void);
// 0x000004A2 System.Void Crystal.SafeAreaDemo::Awake()
extern void SafeAreaDemo_Awake_m6374A22B6C219B6E18C129F5DF5C1D983DA20ED8 (void);
// 0x000004A3 System.Void Crystal.SafeAreaDemo::Update()
extern void SafeAreaDemo_Update_m537DACEBA3D2CF9CDD2E89BAFC2F0875BB2B74C6 (void);
// 0x000004A4 System.Void Crystal.SafeAreaDemo::ToggleSafeArea()
extern void SafeAreaDemo_ToggleSafeArea_mA872BFB7696B1BA2703CC41BD6731B8AB975E21F (void);
// 0x000004A5 System.Void Crystal.SafeAreaDemo::.ctor()
extern void SafeAreaDemo__ctor_m86686D30D1C7F983FCCD478348A1D9EAED508494 (void);
// 0x000004A6 System.Void DT.LogicalStateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void LogicalStateMachineBehaviour_OnStateEnter_mED92D8C50732590FCBB14B919C8BF90D71CD1DAA (void);
// 0x000004A7 System.Void DT.LogicalStateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void LogicalStateMachineBehaviour_OnStateExit_m55C8681C6EBA6C6797175D4F35B10DA50CA6A241 (void);
// 0x000004A8 System.Void DT.LogicalStateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void LogicalStateMachineBehaviour_OnStateUpdate_mBE32483D677ED83072649B518115BD1B303AD6D3 (void);
// 0x000004A9 UnityEngine.Animator DT.LogicalStateMachineBehaviour::get_Animator()
extern void LogicalStateMachineBehaviour_get_Animator_mDC4507F8809D9FB66D14DD9F1ECDECA694B50F51 (void);
// 0x000004AA System.Void DT.LogicalStateMachineBehaviour::set_Animator(UnityEngine.Animator)
extern void LogicalStateMachineBehaviour_set_Animator_mE95CB9BB31E180CFF313B6587C993E61F7C8C4AA (void);
// 0x000004AB System.Void DT.LogicalStateMachineBehaviour::OnDisable()
extern void LogicalStateMachineBehaviour_OnDisable_m364CDAF763BBC678DF65D2E1BEC58F5A69011FCE (void);
// 0x000004AC System.Void DT.LogicalStateMachineBehaviour::OnStateEntered()
extern void LogicalStateMachineBehaviour_OnStateEntered_m19DF958B0475A8BCB092569931B95994D27F058E (void);
// 0x000004AD System.Void DT.LogicalStateMachineBehaviour::OnStateExited()
extern void LogicalStateMachineBehaviour_OnStateExited_m82D5FFDD45FCF6C728C77841F21120BED7BF5352 (void);
// 0x000004AE System.Void DT.LogicalStateMachineBehaviour::OnStateUpdated()
extern void LogicalStateMachineBehaviour_OnStateUpdated_mFECCD82D3748E2EDCC408F5408D024EE9666B87A (void);
// 0x000004AF System.Void DT.LogicalStateMachineBehaviour::.ctor()
extern void LogicalStateMachineBehaviour__ctor_m0108DAAEA632E273AE3697878CAD702D91E786E5 (void);
// 0x000004B0 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
static Il2CppMethodPointer s_methodPointers[1200] = 
{
	Inventory_mDebug_mB459EE7ED35047B6EE7C811851AAF53AD118BD86,
	Inventory_SelectItem_m7F857BF0258E93A959E557A3502CCF66E16E6ABB,
	Inventory_TryToCloseItemDetailPanel_m8BCBD521DF7890417A4E40B43A412E88F11F216B,
	Inventory_CreateNewDetailViewPanel_m1CFC060EE5E03C2FD8F25B4977AFBC8AAECC2695,
	Inventory_CreateNewUpdateViewPanel_mC77C9D7E3BCCE7960CCB00D3F5C464CE880A05F5,
	Inventory_ShowNewItems_m730E58CD1F058A1050B9702ADAA94EBBEE955155,
	Inventory_Awake_m2040A4C75E81B9580543F04EFA2364F3F1B9EEE0,
	Inventory_Start_mD4A36B4F39D7CDE6707542A799D6EF9D88756155,
	Inventory_delayedActualize_mD1E356C8C8C2EE201B75C5724CA238FDD552BD2D,
	Inventory_GetItemByKey_m675DFD8C6621C9F28256FDFA7C87CBB6A8D7986B,
	Inventory_GetItemFromCatalogByKey_m8DC60D78B6FECE05CE3910AC8C2A488238DC6E47,
	Inventory_RemoveItem_mB3AF8F32E3759B3D5623849E6371493608A4808D,
	Inventory_DeleteInventory_mFBC0065645CC6768664B5E928E93FC7867C315DC,
	Inventory_AddItemAmount_m86EDEFDFD3A1AA7CDC4C2A7A583C185CB1B3F53F,
	Inventory_SetItemAmount_mF77009EF8BDA39B83AC1D8A58B3561967557B68B,
	Inventory_GenerateItemEvent_mDB06AEB6B1F923E9A2E8BAF7CD0A5B68540DBBB9,
	Inventory_GenerateCategoryEvent_m687B0C581B1E76E65B3EA77ED20020FFD48AD435,
	Inventory_ConsumeItem_mC2DEAAD748709E2EFC4888117EC8AA0F8709ECD0,
	Inventory_GetItemCount_m30C9151653ED4990DD226955CAF69B3BEB525CD7,
	Inventory_GetItemCount_m9620EFA9D81413179C909868F13F6F63570A793C,
	Inventory_inventoryChanged_mAE9B9C2A62DE22D806581AB21A7131F2E23367F5,
	Inventory_load_m9DAB414CA54DA79E931AD4C34338B75351BBF43C,
	Inventory_loadCatalog_m310FA7C9DBD1DB96AA9DF94A0C7DA69C61F239B3,
	Inventory_save_m45FD5249D7EEA5B3D11276E812A4787C98B43463,
	Inventory_getTranslatableTerms_mFF4B3CF2611E3FFF4A07C2905B37BC74B1305CF2,
	Inventory__ctor_mF2ACBF005FF40F23F68AE8E9E416A4870EC4B27C,
	mEvent__ctor_m96011D90D4F03173045F7BF890276D6A1F5EB216,
	C_ItemDetail__ctor_m6C0817C48A16580E034F6D13B332D1588C39848E,
	C_ItemUpdateInformation__ctor_mC7F0FDBA5A9EBA8F3C3C063CCD16544A21DF8A0E,
	C_ItemEvent__ctor_mF06EBE26C12B781CF55D622B5089E6642624CEEF,
	C_CategoryEvent__ctor_m67F9EB2C610B7D3E3635E2F91A408BDA40CB0E7A,
	C_ItemAmount__ctor_m333DA3DC423474D3202CA00283716AE24B535BA7,
	C_Inventory__ctor_m70F386B20AAF70984FE851EA975B68D3B07497F4,
	U3CShowNewItemsU3Ed__11__ctor_m4F93C52A740A6C441BE1B043BEDD162235DC2E30,
	U3CShowNewItemsU3Ed__11_System_IDisposable_Dispose_m6A49C06D643DC646E3C55A44E5EE1AF4F627CB77,
	U3CShowNewItemsU3Ed__11_MoveNext_m8A7D2618753296EFBBB1B1266B958A29D1572F0A,
	U3CShowNewItemsU3Ed__11_U3CU3Em__Finally1_m32E5D89A0455F0FF9B70ED3FDBC826328A8AA501,
	U3CShowNewItemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6967EF8B284149E3D86878CF77A0D80B53DD0B64,
	U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_Reset_mFA50BB0229FBC728D2870928863FC9ABAC51774B,
	U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_get_Current_m76B1459E38683876F202AA8467A1C975E8068998,
	U3CdelayedActualizeU3Ed__18__ctor_mDF4DDB44936F798F1832FF0E64A6FA3A9A668481,
	U3CdelayedActualizeU3Ed__18_System_IDisposable_Dispose_mA51C00F967C3090D966C0D77B56F0B40453EE909,
	U3CdelayedActualizeU3Ed__18_MoveNext_mA911D479758C0C43D8940E619521E6E136611347,
	U3CdelayedActualizeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m271AEFDD2D8343B07E406091F9C94AFB84B0F4FC,
	U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_Reset_mDEAD3BE3EAC90C8504A165A30341EC72AAF597BC,
	U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_get_Current_m7B2A880A7C4382F85173E90E349433A1DBB661D1,
	InventoryItem__ctor_mDA2BEAE4ED6F565E6B7762D8A33C6D12BB7FD133,
	C_ItemColors__ctor_m644BBFCB015D4FB60529A98076371780A3741F91,
	Inventory_ChangeItem_executeItemChange_m079B4C7C3D4F105A80552E9434950A2E210F3B5D,
	Inventory_ChangeItem_AddItems_m8FF60DA3F6FC5FEB46D74FEA4AB0EB50F63E04A0,
	Inventory_ChangeItem__ctor_mA476F083EACA7B34E60D87B3200A519321CEE7D8,
	itemModifier__ctor_mD83C5B04A7C979683D32D1715490D9F5DC334549,
	Inventory_UIItem_SetItem_mC618F66DF12DEDA8C537BE014A2CAE49349E5B56,
	Inventory_UIItem_ActualizeAfterConsume_m5F32AC4C595891151C4559BEA2176E5CF1B1D554,
	Inventory_UIItem_ConsumeItem_m60C2AEC5BB9F9EEBC40C16018B5512285A6DCD51,
	Inventory_UIItem_SelectItem_mA2FCFC8E9AF9E5DE08E204D0759E0800918420C1,
	Inventory_UIItem__ctor_mA4A0076AE2C2E526318EC428481163590B876431,
	Inventory_UIItemList_getRequiredInstances_m744AEE8F25AB3ACF59411CB6422B2B8311496EAA,
	Inventory_UIItemList_Start_mCC4F36CEC4BC36B5EDF97E193774487957202A58,
	Inventory_UIItemList_OnInventoryChanged_m67AC049CCD1881D1EAE9ECA0FDD9B7AB7A3ED59E,
	Inventory_UIItemList_OnEnable_m033FA8C04C0ABA2C0091B958707190945A5A1AB4,
	Inventory_UIItemList_delayedInit_m3EAD041EF763A6EA538C188CA2DD32339660B976,
	Inventory_UIItemList_OnDestroy_mA5E1413BAE7DE8FD3668CCFA65D7BDFB9580B8A4,
	Inventory_UIItemList_GetShowItem_m61FC1ACD1DFA11EC8CD20D19C65B1D2B49EE9FE2,
	Inventory_UIItemList_ActualizeUIList_m027F3C56B680D93A3D442C894878257F2B730991,
	Inventory_UIItemList__ctor_m5A596042B9924EA2C5E01F7F2E99C8A7903AFC9A,
	U3CdelayedInitU3Ed__10__ctor_m5157CE64BFCC04D43B1A35144363AF0F3182CC60,
	U3CdelayedInitU3Ed__10_System_IDisposable_Dispose_mFA7F9DEEF57B724FD1133C7AA67D24BBE2D5B8D2,
	U3CdelayedInitU3Ed__10_MoveNext_mEA81EF9C62EF9C16B075D8759BDC785B338FDB94,
	U3CdelayedInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45AD32E3413F826B1340FE94180B993E9A7C9609,
	U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_Reset_mF78B2D14D8B84D2B048230AB5BA8BC2256EAB1F6,
	U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_get_Current_m723D3863BA16237C6A75150DA0E6000F8445C5C6,
	ItemDefinitions__ctor_m5FA26D8A6F36EAF891401FAE127C1E85CF053B95,
	QuestDefinition__ctor_mD29C86BAF631ED37C09C2C0974AC1A8DF9B812C5,
	C_QuestResults__ctor_m42A0494CC04BA0E5AF923DFF8810E07BDEE86AAD,
	Quest_UIItem_SetQuestState_m5381289366014461607291FF846AEC9972CD64A2,
	Quest_UIItem__ctor_m12BC0DAACAAC5F06E11E2BFC7B8D722DD86DDE67,
	Quest_UIList_Start_m58AB83628D6F049E25066444E9DD2F586BC2FD6E,
	Quest_UIList_OnQuestboockChanged_mE9BEBD1499D4FE6A3F1FAB1D38CB00AF7A264260,
	Quest_UIList_OnDestroy_m0D8ADCA53DC6B0C706C7BE3504AA88E6A78A1558,
	Quest_UIList_ActualizeUIList_m466CE47BE35CA0B0A2FCDC10068EBF88F978E181,
	Quest_UIList_actualizeQuestNumberDisplay_m2EA387E42844B0A820054A2C393FECBDB457B9AD,
	Quest_UIList_CreateQuestDisplay_m4279B867011EB22FB3380D2250D1E4EEAB57CBAC,
	Quest_UIList__ctor_mC91318BB9AD97C11A834281722210B40321F0593,
	mEvent__ctor_m201A1FB221CC521F461875448EBF68BC64FB8EE7,
	C_QuestNumberDisplay__ctor_m45EDC71C4F121D159353EC5F03F060B4DF553748,
	Quests_mDebug_m0E0FBD19C188DEE8557175A71AFF25084C1DF972,
	Quests_Awake_m1EE1168AC0C78FFB07C9209C75A87F4BC707C523,
	Quests_Start_m6821E778D54291BF979BFA573CEA2CADB5DBF215,
	Quests_OnCardDestroy_mC95073F03854E1CAD48C600543604B67B7585CFF,
	Quests_computeQuestChange_m3C6E6BBD4D4D6320FE9C49C102A19FE89398CC91,
	Quests_GetNrOfActiveQuests_m07298D72D01020E69C023564576F6FCFB89E34D1,
	Quests_GetNrOfAvailableQuests_m7BE9091B444B80FF498829AE641BCDE9D31E7E21,
	Quests_GetNrOfFinishedQuests_m0EADC4BF2D6BB9ED89077290076489736E7C848D,
	Quests_ResetQuestbook_m774AE05027D979312B3742AB5EBDDD7182ACCC0B,
	Quests_IsQuestInQuestbook_m641F390B3811C4F4243CB33B356CE7742BFE3E8A,
	Quests_fullfill_quest_if_active_mE2504300E562BCD182123C085240109CCDF7138C,
	Quests_createQuestFinishedPopup_m36903E6F9325683A6878C2D48BE2D4CED56F7EEF,
	Quests_checkIfActiveQuestIsFullfilledByCondition_m924F6F19DA3C9A579FC8A698D26EC4514E0E9F87,
	Quests_FillActiveQuests_m6A8DC10220149234046E58669F5F13894DFA4324,
	Quests_ReselectActiveQuests_m83DC48861C188643DE0F8D414A4E75C26034F586,
	Quests_AbortActiveQuests_m0FBC8F3208EB5387CC30FC702D41B6956D706A93,
	Quests_FullfillActiveQuests_mCC9BAFB07180E39A64D66224F49B92B3851962A9,
	Quests_SetRandomQuestActive_mC5E1B06751FDE9894ABEB46A25C6370291153480,
	Quests_SetRandomQuestActiveGetCandidateCount_m934BC993B28E4E79E7D5BA6C700A376916FDD7B0,
	Quests_actualizeQuestBookInfo_m77C9DDA920F3C39E1270E1F7602E02A2B58A4658,
	Quests_delayedActualize_m205899FC8FF90F4409663E809FCD691833237E74,
	Quests_GetQuestStateByKey_mF53F5717D7D1A2A5C8E450C65FF7D93557797611,
	Quests_GetQuestFromCatalogByKey_m1AA58285B41108DBEF5FCB9E63DA283674570739,
	Quests_DeleteQuestbook_mE9E30D5AC48B6F7C2CF38CF7778B40221AB47155,
	Quests_GenerateQuestEvent_mB0508A6992675208BCF26D4C3D7C6F9B8AB51793,
	Quests_questsChanged_m737691FBBB75B5748B762CEAD6D27D6D2645898D,
	Quests_TryAutomaticRefill_mBF9F9D01C21281E1AA6B5D538C5D42017BC5AEB0,
	Quests_load_m0DA612653D64ADBD3524D2D199B084CD1766F750,
	Quests_loadCatalog_mC9B9593953FE57FF34A3D84B6F657751C4BACCFA,
	Quests_save_mAA8D547A2A41A9E84CCCD0592B0BE0B0173439A8,
	Quests_getTranslatableTerms_mF85B6305C3FC21EA9F6EE92E0644E9021F517349,
	Quests__ctor_m5549AEA4E1E6CC72F1E650768DF8C561F5547CBB,
	mEvent__ctor_m01760A3AEEE7F7F47EE6A946894918C427B39B9D,
	C_QuestFullfillPopup__ctor_mF214D4C5187AD42D70740DBEA03B46660139ABE2,
	C_QuestChange__ctor_mC606C66549C0B1D8C528788A108BE8749480EED0,
	C_QuestEvent__ctor_mE23D93293D735732F83656006BE7C754D3CFE860,
	C_QuestState_Reset_m45EC6B50AD265D77931BD364FA8F6CFAE5CBCB7E,
	C_QuestState__ctor_mA6AA798770F5913C533F8810213F904C3F0A2809,
	C_Quests__ctor_m04A137DE4D70A3261066178F21102A59D90CC04D,
	U3CdelayedActualizeU3Ed__32__ctor_mAEFBCD23F8BEF17E5BD56CC457FE495405C75A4F,
	U3CdelayedActualizeU3Ed__32_System_IDisposable_Dispose_mD2D3C71CF0DA26B65705FD1234759FDCC6CEA2A8,
	U3CdelayedActualizeU3Ed__32_MoveNext_mA0DB6B6C3BA509BFE2E6A0E4ED4F9418B8383FF5,
	U3CdelayedActualizeU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2D37E0C9702B8712E096D957CB813D1339A5FCE,
	U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_Reset_m57DF855CC498C59B7FB9D904D8CE6B56226F810A,
	U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_get_Current_m0FC81D0D966BCA6016E759F0CD7827D2316AABF7,
	HistoryEvent__ctor_m360A306BF5E1F5E2204584FD186B8DC340C89AD5,
	Timeline_mDebug_mE7E17D100E0CB09E9185FD12A1EFFE94A9DE6B22,
	Timeline_Awake_m92D1116FA6DC0C03C5CED6E79870A352C6FDB4A5,
	Timeline_Start_mEAF5A39D1DBBC9428E5A351E7EC21B6B73DC96AA,
	Timeline_delayedActualize_mBDB9533D5CF13304BE38412E707BC60F6677060C,
	Timeline_Update_m4BD4F9098A6B737B0D5D4E0CA2A9E99845BB2E11,
	Timeline_getMaxYear_m1B613C7019EF5F919730ED7C46682458C676AE06,
	Timeline_GetEventFromCatalogByKey_m287779FC0870FA360A1E0566DF0571A8FA1CE141,
	Timeline_ResetHistoryData_mEE5496AD06C05EC3A16FA8597AB69CB6759644A6,
	Timeline_CreateNewFollowingHistory_m7A60504A742E3D32D70D99317D9376ADEA2ACF7D,
	Timeline_AddHistoryEvent_m21E6D971D351E1ACA9DDB75D6E76B54C8CD7F31A,
	Timeline_AddHistoryEvent_mDD02AF1384EC1F88F1879FC1518C3654E827306D,
	Timeline_GetHistoryEventsCount_mB56235C010BD541E6C1B0C8E3439671AB3428C85,
	Timeline_GetHistoryEventsCount_m1739DDE01FF0CCEA6672688CA14E72A188ABDB02,
	Timeline_timelineChanged_mC1F3A7845B3CA28BE6EE841BB680AB09A8D221A6,
	Timeline_GetEventOfYear_m0EA8F5C59A714DCF0FCD04F68FB39801E93DD6E9,
	Timeline_load_m6B33C7E0E8687CCE74190F53390F7E65EC982455,
	Timeline_createDisplayedHistory_mA00D5649F5DA36FF10450AB90CDEBF5ADBD9B5AF,
	Timeline_expandDisplayedHistory_mC63710805FD3ABAFD31D2957B5B3BFD0E2F264BD,
	Timeline_loadCatalog_m5F3EB4949964A86CC43AAB508012DB706537E595,
	Timeline_save_mDEECAE98921B7A87F17167879AF23412EE8E0EA5,
	Timeline_getTranslatableTerms_m14CD1F1EE9E34116AEEE1DD59666A25270E3D62A,
	Timeline__ctor_m7EEA084D66266DA016EB4594AEEE0FF65C5F8EC2,
	mEvent__ctor_m5FDC1C42153FD9537BD7797C7CC713F9B6440360,
	C_TimelineData__ctor_mA7C65FECA924BC5AB250DC92C360FB1F8B038051,
	C_History__ctor_m9798D9B13F648C80C92774925306325BECC846F8,
	C_TimelLineEventChange_GetTranslatableContent_m243CAAFC2A8379E626BEEE29EF66E8D7CF4B28AA,
	C_TimelLineEventChange__ctor_mBEE169C01DC2BC2992F216A69922F08FBBC023AF,
	U3CdelayedActualizeU3Ed__14__ctor_m163DE02FB61436D359EAEFF208D0BA13531A1BA2,
	U3CdelayedActualizeU3Ed__14_System_IDisposable_Dispose_mB3F3A0C344D293C1A69F98D95489647B841E4DDB,
	U3CdelayedActualizeU3Ed__14_MoveNext_m5325B85F9B52AB10E4EE8560572451A336FFF31D,
	U3CdelayedActualizeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8130E9BD1C537FF6EF30645E6EB76449A451D9C,
	U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_Reset_m7B7A2424DFC67895DA2367595C79B7C13DD21EA4,
	U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_get_Current_mF87B6C1ACB21AD889437D3D4A15742C1D9E15DCB,
	Timeline_UIEventList_Start_m6923F482AC7FA3D8B3C6BB484920BAD74E55C8D0,
	Timeline_UIEventList_OnTimelinechanged_m12BC50D91308A6CC65E4F0AE74148B9DDAE01955,
	Timeline_UIEventList_OnTimelineCleared_mF9B01185723C4CF952910C224EDEEC84D18B9AF7,
	Timeline_UIEventList_startMoveScrollbar_mED9F1C78C25731F6B95A01373E55762C45C57A49,
	Timeline_UIEventList_actualizeYearsTextAndFakeScrollbar_mB58889E0C989C82C153D02A5F02824BE9DAAAC50,
	Timeline_UIEventList_freezableUpdate_m598AE33C3613E8016693606FDCCE8D6081B567AE,
	Timeline_UIEventList_moveScrollbar_m666D995BB93273C5789809F24C0C00527C33AA68,
	Timeline_UIEventList_AnimateScrollbar_m708EAB440EBFA0C7CC54609EA34104E664768ED5,
	Timeline_UIEventList_animateScrollbar_mE9711431AE5910959C2C37E9D643486F77139250,
	Timeline_UIEventList_ActualizeUIList_m7D715A617E4EAA446F6BD592CB801315F89D7242,
	Timeline_UIEventList_CreateEvent_m9C6A9B3566B260DBABC395A0A30CE8CA887DB682,
	Timeline_UIEventList__ctor_mEAE369E8F02C51717278568E8373AEA2B2F4BC79,
	mEvent__ctor_m51D1B819A1FE5479E15CF549E526DABFEA90D83E,
	U3CfreezableUpdateU3Ed__23__ctor_m4783DD803B0781C6D55CF050D10870FA162CD368,
	U3CfreezableUpdateU3Ed__23_System_IDisposable_Dispose_m4BE71898145F5E53A99ADD950576BE01407E1F0A,
	U3CfreezableUpdateU3Ed__23_MoveNext_mCEE3074E559B0C0333B7A7A9E96B97C8C830D369,
	U3CfreezableUpdateU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44F416F81FFCE8065052F150502D1C7192D336DD,
	U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_Reset_mBF4FC0D2B0597223CAD3B9358590491BD5C64C40,
	U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_get_Current_mFB3C753DC3C7268E2671B6BDB671B18618043502,
	U3CmoveScrollbarU3Ed__24__ctor_m2FE1146F98C50EA6320D3850669B72148B6B5EEA,
	U3CmoveScrollbarU3Ed__24_System_IDisposable_Dispose_m192BB59F93A9FBE43398A6FE37C8019D0A998C3A,
	U3CmoveScrollbarU3Ed__24_MoveNext_m0C81E7E8D0C0991A7CD9E09F7E326BAF0FCA4A67,
	U3CmoveScrollbarU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m954B89FE0C20DA41406BF08F8C8D5BBA6AEF0334,
	U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_Reset_mF290FBF737DD65386088D34E1AD99CFE4E74D4B7,
	U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_get_Current_m72CD44092B59FF2850BB3B5A683609887BEC9A41,
	U3CanimateScrollbarU3Ed__26__ctor_m1B4B0175B724E1193EDCDA6F797C3064FF59E6BB,
	U3CanimateScrollbarU3Ed__26_System_IDisposable_Dispose_m9B030CEA5F95001C0406F30685AC322AE6B40170,
	U3CanimateScrollbarU3Ed__26_MoveNext_m0FDB4582FB489EA87C2268E3222DE1E987D189E9,
	U3CanimateScrollbarU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21653C698A208F8970AD906340AFA09AC8EF1F11,
	U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_Reset_mE02D079FC130D8B51F2AD3D7FF1EF878B7E35B2D,
	U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_get_Current_m5F6D100097FC1BF15D464E6AEA55105A019DBBD5,
	Timeline_UIItem_SetHistoryEvent_m330A902DE10122B55BECE0FBB7441F6ED6225C0C,
	Timeline_UIItem__ctor_mA317C8A44F3230735638627BB8B5686BDD8EC5CE,
	AchievementsScript_Awake_mC6A73B18FFAF82BAF9D8225C803C682D4F893F83,
	AchievementsScript_load_mA4C4B36ADF9CDE1AB91F17514638159D1551F69B,
	AchievementsScript_countAndShowAchieventProgesss_m28782983CC365594876B40393D5B347A1CA667C1,
	AchievementsScript_activateGameObjects_m1B445EC36E7E051B2DF80ABCF41EE5611B6BBE3B,
	AchievementsScript_Start_m47D05453ABFE684DBEA17690994E304EF900B715,
	AchievementsScript_addAchievement_m8C2201E4571FE8D491D7880B6725A7888B2522C2,
	AchievementsScript_setAchievementCounter_mBCD635B39D79DFEE6F1CE5F1983D881C35428E47,
	AchievementsScript_getAchievementStageReached_m8574367302C1CC015C78782204C3FAABC488A890,
	AchievementsScript_showAchievementAnimation_mEA81E3B2710A5AD7ACAFFBEFD8B1C4B602D0D9EA,
	AchievementsScript_getTranslatableTerms_m7DE5143D6C80B2BD598534A8AC38618E327E2602,
	AchievementsScript__ctor_m77A0863949389350AACFB50F17C0C0B8171462DD,
	achievement__ctor_m680865D78DA8753E7E6318190756A32CA1AFF6A4,
	achievementConfig_save_m1D63A4FBA83FA0061354952383FC182628FFA57E,
	achievementConfig_load_m2888209A72606F3B708654C91D8BF458A90A786E,
	achievementConfig__ctor_m2068145C7CE132C73C181201A6AE698EC1670716,
	achievementStage__ctor_m5BF4737516BA01B2BF566F23AAF2551190E87677,
	AddGameLog_addGameLogText_m015DF41ECAF6A776345AC3EA4CD6BFBFED37C134,
	AddGameLog__ctor_m726E5AFC3D0B762D625398D5F37023AFE2E5CE0D,
	CardStack_Awake_m3B0EA2DAE2411CF087F05D7AB28ABBD65FB5C8F4,
	CardStack_Start_m3530127B162998BD6B788C1EFA270338BEB45197,
	CardStack_createAllCardCntList_mDBC288F39B3812BA9A899D45C780949A48726CF1,
	CardStack_copyAllCardCntList_mD784404D51F33691CE217B3B293EE9B0AA463756,
	CardStack_resizeAllCardDrawCnt_mD4640794DA2C432894D16D2B41850411186F7A4A,
	CardStack_resizeAllCardBlockCnt_mC7B397F98141B520FEB04B3AB0B574F75800BB39,
	CardStack_resetCardStack_m780BA92E9752D934DFF6F0565B2966A21AC6FC61,
	CardStack_addDrawCnt_mB24D12F0BFFB0F1220AB704E646443FE03642580,
	CardStack_addBlockCnt_mCA08711982AA48C7C3FFBF9C55FA26CD2D43AB7B,
	CardStack_decrementBlockCounts_m0FCD81E52246FCA9549B391FA38D168BDD0A89E4,
	CardStack_getCardIndex_mC9151956943D542080BEC1A1C2B567F925DEDDF7,
	CardStack_loadDrawCnt_mAD016A0D2FCBF3CD68FA5AA3C907694BF8155B93,
	CardStack_loadBlockCnt_m6D9DB6C1606106E44FEDA979E31666E58DDFFB8D,
	CardStack_saveDrawCnt_m48F6B69908C2AA1C76906003094445FA9DEBE626,
	CardStack_saveBlockCnt_m3EAEEF01F4C7F4AB0D2A6E1C25D2E2EC14927629,
	CardStack_saveCardIndex_m18AFC2E2E96A4FD6959BBCB54DE7139FA66C23A4,
	CardStack_getCardIndex_mF2E390CF2E9E36C113CFF5FD9A5FEA19885716A5,
	CardStack_loadGameCard_m12AEF00F0FC79F29D8E3EEFF888E92E8B1D03CC4,
	CardStack_cardIndexValid_m236DB98DC2C7E0F7DBC5C97B788C28945D4157A8,
	CardStack_setCardMoveEnable_m667E6819EB0FCA544BAB21E48325CC348AAE3020,
	CardStack_getCardMoveEnabled_m412E90C778FB6AB79D6152662343AD229B462C55,
	CardStack_CardMovement_m0DE547D4A6BCEF60B388F3E65A0D013E374B8953,
	CardStack_getCardAnimator_m30D536BC4351915C79B6A0875073F38891087EA1,
	CardStack_moveCardOut_m9E9DA3238C638AB7927FBF00FBBA89E5E40A2FF1,
	CardStack_nextCard_m7B2BE19D63A486E4D9D0536A47B06C29DF06519A,
	CardStack_nextCard_m87BF1EF19657289AC02837BBC646E5D91C6133B0,
	CardStack_randomStandardCard_m667E6680A90D348F0F386A998208C303AA1511EB,
	CardStack_spawnCard_mE54CF29103BD270E074EF491B71690544A63BA2E,
	CardStack_DestroySpawnedCard_m66107FD9D2B2BA02EA7C3A34CA46A31D2B820172,
	CardStack_newCard_m194F8597A461EA3300E0A0DC77FD382B2AF8E429,
	CardStack_sortForPossibleCards_m7AECF25953490F3BBBF3DE62A94F55D422070853,
	CardStack_addToFollowUpStack_mB02117F3F05BE086CFC62DE5A3BDFFEFDCFC744A,
	CardStack_saveFollowUpStack_m848EBCA8764C54EF58DDAE4F6B13F44CCA3FD96E,
	CardStack_loadFollowUpStack_m796F366A84B5AFD19016BAD00434E8DC8DC3AF41,
	CardStack_clearFollowUpStack_m8AC8DE7FDF89892EDE1E1064180757AFCD281058,
	CardStack_decreaseFollowUpStackTime_mCB460AE90217F917D79B004784B3A036F30E21C7,
	CardStack_getNextFollowUpCard_m6D81E6FA0389807C20CED5A252564FBD17077B5E,
	CardStack_leftSwipe_m42356A35E1160D5B0809E8494E4ACB7F0FD0D506,
	CardStack_leftSwipePreview_mB670E511EBB598D8135CFE0EC835C1752F7E70A3,
	CardStack_rightSwipe_mEDFFE432D7E8AA2B0019A83D454B2F6E694DC356,
	CardStack_rightSwipePreview_m0A4B27C8E395BF38BB172CF217236E44D7213A47,
	CardStack_upSwipe_mE1E62DABA42CB39465F091F7CE498C88413BB8EE,
	CardStack_upSwipePreview_mA2A041DC28F6D774AB679D69DB9798D256CC26AF,
	CardStack_downSwipe_m7E8F7A9DCF53779105A0B3A27852DF2166DB525F,
	CardStack_downSwipePreview_m47EA0FEC8DD6A20A97CCCDF997BC3880DCB09FC8,
	CardStack_additionalChoices_mA037E6420FC618FFB7E956A154DDC685B78D2A3D,
	CardStack_additionalChoicesPreview_mD8035D0D5591033954BCF92D0412BB0CE823CF43,
	CardStack_resetSwipePreview_m4A460A384B8A9107F1F46AF5573E41D331113E76,
	CardStack_testForDuplicateCards_m6BEE156CFFE8136EBA2871BA02649F9BE6B8CD00,
	CardStack_getTranslatableTerms_m76FDAC5D9E3C5C4F333B4CE50BE3E445B7166BC1,
	CardStack__ctor_m4BAF6CE28BF8E94BFD437BA6802F78E76D26C39F,
	cardCategory__ctor_mFCEBB7FC744EC9261859E01AEFD1A08D3587CF66,
	cardIndex__ctor_m6E921EA6A3F5E20001DBC533DFB4DC169D752F25,
	cardCount__ctor_m26B399C7BCE5C3E353FBF7F556968C64E3A50FD0,
	drawCnts__ctor_mFA1804B7D7B0981861B29FB7F698ECC21F944BDD,
	blockCount__ctor_mA677706926C145196E763F67BB1AD62D49B30D60,
	mEvent__ctor_m97679BC5D2451E7C461F2F9CC48F10664AE9C4A8,
	C_folluwUpStackElement__ctor_m3BB2D7FB29A1C05AA888E82DDE966072E4D67659,
	C_WrapFollowUpStack__ctor_m26C2D55CD7B4708E71D199F15027431C90E744AC,
	U3CCardMovementU3Ed__49__ctor_m20A81C719966A8182126B9975F568E07B82DAA4F,
	U3CCardMovementU3Ed__49_System_IDisposable_Dispose_m597AE27807E96B9EABB15A7B52F3A907165FCA4F,
	U3CCardMovementU3Ed__49_MoveNext_m260905E032D12F5C5AF32A0897D7860F1738635B,
	U3CCardMovementU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F948D8EEE3C7847E0339CF067F773A196FADE4B,
	U3CCardMovementU3Ed__49_System_Collections_IEnumerator_Reset_m2022354FDA8C877E0121F503D61E32CF4787D0A1,
	U3CCardMovementU3Ed__49_System_Collections_IEnumerator_get_Current_m239552DA1314887EF0A85094BE7792EF445E3092,
	U3CmoveCardOutU3Ed__53__ctor_m1A4633BA82BE4A53DE6C58D7632CA74DBF7980C4,
	U3CmoveCardOutU3Ed__53_System_IDisposable_Dispose_m0F6008412BF065A95BF7C9837E002D54FB6AC18B,
	U3CmoveCardOutU3Ed__53_MoveNext_m9E0C510971686B3DCDA978C7A02A269274E9C08B,
	U3CmoveCardOutU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB107756F0DFD44B9A92F9A90BAA1416A5FF52058,
	U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_Reset_m7ECACECB5214C8351376CC906FC0E5D13664C31A,
	U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_get_Current_m60342D8BA597D00666EC5A995994065A6C8AEE7C,
	CardStyle_SetStyle_m52A237E0248DE63EF94BFA176B5D2D0352BB24BE,
	CardStyle_GetStyleName_m0E795762F3ADCDB139A2E5F6A380A233C8C8E9A1,
	CardStyle_Refresh_m5D5C0BB020AFA1189397F3CED1450C0CD575FEF5,
	CardStyle_Awake_mEDEF8861427E413C3FC3F96C3CCB966A01791FF5,
	CardStyle_Update_m4463E02F0D5A7B48467D9D0F06285DA763B99DB5,
	CardStyle_actualizeColoring_mF2244F459A81C0E8AD587CAB3E79EAB60D34F30A,
	CardStyle_actualizeImageColor_m0E426F4CE78AB403709CE9C013550F2F98805342,
	CardStyle_actualizeImages_m792D4C68BF04FBBDE130DFDDB68EEABB97CC3E7B,
	CardStyle__ctor_m7DC380749CB217AD06CA1C4EAAD7EC29249C930D,
	C_CardImages__ctor_mAC478907002F1A778AE913B1D303B216CB249A8D,
	ConditionsAndEffects_ComputeEffects_mC75C914A6784CDEE79AB601C9E555E934C1AD5F8,
	ConditionsAndEffects_AreConditionsMet_m4FE763F5083B9AFA4CBDBA84C369C6AB08F9D099,
	ConditionsAndEffects__ctor_m11FC7E4729550ACE01724AED770A6C1A6033FDC0,
	C_Conditions_AreConditionsMet_m48E4A46D2768EC9DFE687BA54D17179FA1786B24,
	C_Conditions__ctor_m4E72B2568637EECAF276DD93E2E4BDF3779273FA,
	C_Changes_ExecuteEffect_mC77113F7AE4D125AB209EFB99FD83456DC7EC237,
	C_Changes_GetTranslatableContent_m03E68108533625A619AF2DD189C7F157928ECC47,
	C_Changes__ctor_mA267EFB73409984347B57B29D156D67E63BA9F60,
	CountryNameDisplay_Start_m4B458B3C13D92DC9CEFFA2AF83612AF4668F6537,
	CountryNameDisplay_twoFrames_mA53A0B0554D766F89C664C57F2B51243139FC082,
	CountryNameDisplay_actualizeTexts_mB838DBD92AC3AAEC47F511726C73116EF36883A9,
	CountryNameDisplay_clearTexts_m0A8E9A445CA1B5D9592CAC59EB66AB6577246932,
	CountryNameDisplay__ctor_mE3F0A4E828E5604AAD388C2F416E8581E98B370A,
	U3CtwoFramesU3Ed__4__ctor_m7DDE93D188A97F1F2A8F7B8B76AB6E2BDC4E624D,
	U3CtwoFramesU3Ed__4_System_IDisposable_Dispose_m831487A496655900EDA4322D9ECAAC5E548DB62C,
	U3CtwoFramesU3Ed__4_MoveNext_mF439C59D8CDA208A9E43B5E2B24D97A9B6DA1A24,
	U3CtwoFramesU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F6E189D7326C4B888B4CC8D73E4E6C62C6C94B1,
	U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_Reset_mFF5E60CCA7FADEE5BCBFFF2829E501D6E6D8985A,
	U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_get_Current_m585E0BFA9C48635E44FD2A8AF7F111842DEF9308,
	CountryNameGenerator_Start_m6ED8501EC55F0E87FCC76E25B900E985B6C0BD47,
	CountryNameGenerator_clearUI_m8010DCF2FFB7EC9C611AA4E5AEF392220235AE1E,
	CountryNameGenerator_oneFrame_m332A4A4145FB9E105CDE00AB65B211D77BCF23FD,
	CountryNameGenerator_createValueScriptLinks_mF77ADB48867ECE90B4A1D0477469526746B5AAA3,
	CountryNameGenerator_Awake_mB080EFFC99E9A05AFACFE2EA3C29C01944C9CC8A,
	CountryNameGenerator_actualizeTexts_m6C8078834FBD1A5553B064EC8D676A1E5F153B6B,
	CountryNameGenerator_GetNameString_m4D0A739485B0F33AD066860F041AFE43C5D6F7FC,
	CountryNameGenerator_GetCountryString_m8E30D52FFCFB25CB9B237C71C774933ACE8E4AE4,
	CountryNameGenerator_GetCountryNameString_mAF5B8522946CB1C41D028F6B3CE40A9637AE1F22,
	CountryNameGenerator_getNameAndGenderFromValue_mC16F1FA68E235DC7ABD2B8B75D9A936CBB28F8E5,
	CountryNameGenerator_getSurnameFromValue_mD0B89C46156249DBF05013AEA8627AB44877556F,
	CountryNameGenerator_getCountryIndexFromValue_m46AB5802FD4F79E3714D2E7CB445CDBF20423F35,
	CountryNameGenerator_getCountryNameText_mE0EFB80EA5A79720BCAEF2A51502B20B7C7AE75A,
	CountryNameGenerator_getCountryTranslatedStringFromValue_m32DD33930212D760A44083DA7471AD5DC5766A02,
	CountryNameGenerator_getTranslatableTerms_m7F88440605D47F3D7DA9A9E9DAB979F66D277A89,
	CountryNameGenerator__ctor_mB1677272BF542D1A120A26DDC4E1B65D4F22575F,
	nameGenderLink__ctor_mA79D0AA488F2FDE581B4878DFE6A9AD20429CCED,
	subStringList__ctor_mAE35EBC10DF25D0D850D544952D7B8B4B57B0EA3,
	U3ConeFrameU3Ed__5__ctor_m4B579F1DA5E880BA17A19AAC62C801F10B2282FE,
	U3ConeFrameU3Ed__5_System_IDisposable_Dispose_m697F7F0C529FE820491FBBFB4628E70D9A547D72,
	U3ConeFrameU3Ed__5_MoveNext_mB52587FF65543F3E63CFA6D536B0F7BD37CA5025,
	U3ConeFrameU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0CBC897829B3BE3032005EA66E369973541A768,
	U3ConeFrameU3Ed__5_System_Collections_IEnumerator_Reset_m5F621E1AA2318DC5D06E66CBD08B99C45D57C67E,
	U3ConeFrameU3Ed__5_System_Collections_IEnumerator_get_Current_m8E2260A88F81ACEE95703F8F693E5EB4C7DE3895,
	DeletePlayerPrefs_DeleteAll_mCD4F449E918CDC512AAA042DA82B56C1281B4C5F,
	DeletePlayerPrefs__ctor_mAF726B932497A48665BB1E8C3FEC5E6C1ED0764A,
	Destroy_DestroyGameobject_m65893A0996828BF154B263F5B3E1E43C3D302BE3,
	Destroy__ctor_m487225D87A53D11346B6FD7BFAC8B4B3C33BC02E,
	AnimatorBool_setBool_m2E7B072DC1C37C30EB33B29D2E5B2A3A321F4B90,
	AnimatorBool__ctor_m1BB65151E5349E47E69D9497A894133282918863,
	AnimatorInt_setInt_m9AF5A75DA35D38AABED2D384E4106EC9D4E05B57,
	AnimatorInt__ctor_mB23AA206EA2067D8E81C159519C328602076C56F,
	ButtonPressed_OnPointerDown_mCA15B7CF9A665231B3A8AE60D1CDC35C3BE8826C,
	ButtonPressed_OnPointerUp_m9231D78DEBB4872D9FB423D98516103DAC5C9342,
	ButtonPressed__ctor_mB43CFB7F386E3A57035A0F29F8800A0946CD5121,
	DebugEvent_debug_m6F7369EA70C9109B33171F98E02FAD350D10B12F,
	DebugEvent_debugBreak_m8B3AB26CEC6445AAB476A34F1829133C5F9503D9,
	DebugEvent__ctor_mD2F67C0835492EF7749D7973ED9CFA6FDA0B312F,
	DelayedEvent_startEventdelay_mABF86A0893DEB981003F52298EAF4CA3CC8290B4,
	DelayedEvent_Start_mCD56DC67EA047253CE87057D367BE2090EE3D46A,
	DelayedEvent__delay_m4D67B05369F776238509EF536A42015264C8FE87,
	DelayedEvent_actualizeCountdowns_m11CF5F952A355D17B1A4E321B4F9B581D80AAA37,
	DelayedEvent__ctor_m0C6637FD7CE4606E0EF7F5373BB25F19E68721FA,
	mEvent__ctor_m5D8686373288E4E7A07182AF7D327D056CC326FC,
	C_UIDisplays__ctor_mBE25B546586C6BAA3108B3392E72FE3981A32840,
	U3C_delayU3Ed__11__ctor_m5170E051F5D20856F12F90FDCA6571E68EB8F830,
	U3C_delayU3Ed__11_System_IDisposable_Dispose_m815F55D1DA38454BA27303C83782CCF5B2839C7E,
	U3C_delayU3Ed__11_MoveNext_mB6CDF005DEF2114CD7245AFCBA59BE9355AC4855,
	U3C_delayU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B7C7859D2F37ABA7675B0FF7B2CEA08D5523395,
	U3C_delayU3Ed__11_System_Collections_IEnumerator_Reset_m8B109E7B8BD114E6A539836C1CCC24D3B949F6DA,
	U3C_delayU3Ed__11_System_Collections_IEnumerator_get_Current_mE1E9828FA48FE14B08726A0E3A98EFED407A4DBF,
	GlobalMessageEventManager_Awake_mB2770724A22B7EC0A4A363805EC9D3BDA57E4DE9,
	GlobalMessageEventManager_buildAwake_mF9A30B9B9EBBFE4B550444AE3B0AB9794924B90F,
	GlobalMessageEventManager_registerMessageReceiver_mA6ABD7FD7645482776F3E4AFAD27631F622BCCB1,
	GlobalMessageEventManager_unregisterMessageReceiver_m869F6BA38950BAD2188E7964B58AEFC200DEF926,
	GlobalMessageEventManager_sendToReceivers_m1AF577A411F10DEE4B0A795AA199F8936A2C574B,
	GlobalMessageEventManager_sendToReceivers_m304446BD968CDC5ADA8431795248CE36251710CA,
	GlobalMessageEventManager__ctor_m29202DA2DD0D9BADE52A35A2D1F3A5D6886B957F,
	GlobalMessageEventReceiver_Start_mF17B34225128890DA1473CF5037F71FC84EFA5F5,
	GlobalMessageEventReceiver_createManagerIfNonExisting_mF5154BDC0AF528FA8EC5126F471E6F8D91F72698,
	GlobalMessageEventReceiver_OnDestroy_mA623E63B1BE7A7E21A1BF1D09230D28965B03456,
	GlobalMessageEventReceiver_globalMessage_mE05590EF805B77D4E446F98439968731D95EF8BE,
	GlobalMessageEventReceiver_globalImage_m2D3850D7C7AEF31F6E58D7250B9AFA7BBA3D39A1,
	GlobalMessageEventReceiver__ctor_m71E9E906B527A4C2E9CBB1CD1CA383FC5F51BC2B,
	mEvent__ctor_mD7BA50F8AC6D22C83E6ABECFDAC8D08316E34241,
	MessageEvent__ctor_m4DC50C92357D3818295372C93E67BB4F893509B0,
	MessageImage__ctor_m2F9FE36DE0A282020EB1E0304A72CDA66500F56E,
	GlobalMessageEventSender_GlobalMessage_mAC3EE13DF47059D15A759A1573B946135FEAF095,
	GlobalMessageEventSender__ctor_m4C702BBBEC4CBE8A5F27C694D8B0B6FD10934078,
	GlobalMessageImageSender_GlobalMessage_m674219A906B8BD2642C03DA840581E8C035C7A5A,
	GlobalMessageImageSender__ctor_mD6501450B9AF30A1D152375703DC9D788D314192,
	OnCollisionTag_OnCollisionEnter_mFAE92208B0F0EC8C7140040D7EB1EBDA47CB8E3B,
	OnCollisionTag__ctor_mDDCECE2E2151B7807579F987C896F7F0C37C2BEF,
	mEvent__ctor_mB5C0776516A1FA2EBC2295E15E7E6C007AF9E041,
	OnEnterTag_OnTriggerEnter_m3F9D8E58D4A734DD02861C631E8EC9F98EB78F06,
	OnEnterTag__ctor_m8B2AE88D587921CD45ED3475905AFF8CC9C8830F,
	mEvent__ctor_m77BEA20565F3E811DF191F0B42689D92AD649D16,
	RandomEvents_executeRandomEvent_mDB8335DA609EC56F2D50908B8387335E588BCF36,
	RandomEvents__ctor_mAD1A155C35635AC6522CBC8B43F31EC4D00B7930,
	mEvent__ctor_mFADA705CF7356AC03AE100D53E207586CBD9A80C,
	EventScript_SetImportData_mEFF9A71936403D754E20312DB1C58ED112639354,
	EventScript_writeTextFields_mA6019FC64B2BFF6DEF6F026AE3DB9B9B017F5FD1,
	EventScript_writeEventTextField_m6D02EF385DD60DF39CB4875362EFE470F7762688,
	EventScript_onLeftSwipe_m146AB6FF617F54AD8A16BFE2C5553AA6413F8226,
	EventScript_onRightSwipe_mB375350E3D4F5AEA7AA5DDC88DE619BA1B6DDA77,
	EventScript_onUpSwipe_mCA730665CE42F616CF51DACA1966A9C3F646B4B0,
	EventScript_onDownSwipe_m3DE6A411A295A47F23D6117585999E5514D51EAE,
	EventScript_ExecuteAddtionalChoices_m42256FEAC87FFF2A62800F299AB1211622A76592,
	EventScript_AdditionalChoice_0_Selection_m16293866F22C1E4DA76DA2B91EA34CE15C965C4F,
	EventScript_AdditionalChoice_0_Preview_m38ED20072A30C589AB2AEFE66AB7345BA785B6FC,
	EventScript_AdditionalChoice_1_Selection_m9D8C665FA05901AF5E3E72B6966BF5DBAF289529,
	EventScript_AdditionalChoice_1_Preview_m952FF3104D06E0061EDBFFA92B7D612A014C59F5,
	EventScript_onLeftSpwipePreview_m4FAA43926C07ABAAA0CD740A9BAA2E9AAAFF765D,
	EventScript_onRightSpwipePreview_m427707F7C9E23AA17E0597E47CAD35AA17FEB34B,
	EventScript_onUpSpwipePreview_mDDEC20E7F768E8E8E39A7AC87E83F76936D12C79,
	EventScript_onDownSpwipePreview_mEA9E976878435736D39F737C710A5DD07A485D5D,
	EventScript_computeEventResultsPreview_mA52B17E16E227281B5FB69C018E787E4EF0082F0,
	EventScript_onSwipePreviewReset_m625874C1D719B39CA4F63C03E463001427591893,
	EventScript_PreviewAddtionalChoices_mCCAD0C17D1EB4A05096E20251DC185FBCB2F1A3D,
	EventScript_computeResult_m79678BA9B858507BC7E2B59CC14828508D6525B7,
	EventScript_ComputeResultTypeDependant_mA0F22A94881AB3D98431BE3069CCD73A42368E9E,
	EventScript_computeTextPreview_mA94DCBA6146E1F378511D5339972CA44C6B805E0,
	EventScript_GetResultByDirectionString_mB6107C1BD868E652A3E76F873563F78B11076E8E,
	EventScript_addTextValueChanges_mDFF9A2F33C8320B919A1F7EA71B6AB7A553A7F82,
	EventScript_computeResultPreview_m6FA8094468856E79862B5C0456BBB642EC653A31,
	EventScript_resetResultPreviews_m4D59546F0479EE70DCB5C413B80085BB722C98CB,
	EventScript_executeValueChanges_mA3562B3A4947F718F47BC0019BCFF37DE32ED80A,
	EventScript_executeExtraChanges_mA0AB9D88B7B454E818A3BFC4CCA840629005B33A,
	EventScript_executeValueChange_mE37110A642E3BF7D84A2818D6A223EFA40F2A71B,
	EventScript_addPreviewValueChanges_m8088EB701A823580719638106BE4D03C396FDD33,
	EventScript_AreConditinsForResultMet_m3D9132AE25B48B5CB629A4DC4B919E50483BEEE3,
	EventScript_Awake_m44F58AD4B1684E59A0C37EA8439A18CA9E3A6E7E,
	EventScript_Start_mD880C9B9538EF93C046B0F46573B559EF3B4E8DB,
	EventScript__ctor_m37050F9D52F8FBA7FAD50FDACBCE68A6BA1AC006,
	EventScript__cctor_mDE6AC00424E9B05179B6910AB0062A3D18DE895E,
	mEvent__ctor_mB59CF7AFF9BA652BEC62317D426BFA65FEB9C6AF,
	eventText__ctor_mDD6E3AD4E9D59F330914F61F658F2221BD7B9683,
	C_Texts_set_text_m0CD53D310B7C799D21FE30E1F254E48ACD06905A,
	C_Texts__ctor_mB87A2AA0DC8B6C9632B81CF478978A69867506EC,
	eventTexts_getCsvHeader_m06C94EB9DD4A4B7B14C2577CFC9460BF63A3DB64,
	eventTexts_getCsvData_m3EB5F7402FDE1BDA2066B5C9E06CBF426EC91D0B,
	eventTexts_setData_m3685C0D8BBD2610397CDD3AFF0F360157494933B,
	eventTexts__ctor_m4AD40C807343CB49531A0430A1000A2EBEB460F2,
	condition__ctor_m0342C48BA0221B0C17719D8A508C08BDE144DDE1,
	C_RndRange__ctor_m54B2CFA13147E34233E6546EB070E57E88C77263,
	resultModifier__ctor_m617A0580C2042646F1862FE85C5404B315FCF8FA,
	C_ReducedResultModifier__ctor_m7222DA6CF8A5504BFDD3B840D29EB249CB7C657D,
	resultModifierPreview__ctor_mF464668BBC56BA479CD82578DB2504FBBCD86D70,
	C_AdditionalModifiers_GetTranslatableContent_m443C47DF2E782F3407BE9507CFDCFBB11CDCA153,
	C_AdditionalModifiers__ctor_m7C527696D2FA3DD65A53611BD529F24EC3F21DB2,
	C_intRange__ctor_m9EFDC904746CF6222AD9B28CBB607BC7900789FA,
	modifierGroup_GetTranslatableContent_m74FDA8DBA66FF96858155011FD9FC9427CDE55D3,
	modifierGroup__ctor_m52471C2A8830B98B21DB890BDC32813110138ECD,
	result_GetTranslatableContent_mE911429149B32ABBDAE1869AC5EB4D0ADCCE88EE,
	result__ctor_m2ECC8E86B9C4EEA9C1A0FE4325ACD150BE0C46C5,
	resultGroup_GetTranslatableContent_m573EFA55AF510436CBFFDB8599914A68E4EE8B8F,
	resultGroup__ctor_mE3E648DC91C639B909740F0129314B693CFD154C,
	GameDictionary_mDebug_m7B58ED48426B42EA43ED29CD99845214E14BD14D,
	GameDictionary_executeDictionaryResult_m95D4196611B0724615F0D99E0EE45EF195D5C2D3,
	GameDictionary_ConditionMet_m367813954D6632159DEBD5617CC1269E7C67012A,
	GameDictionary_ContainsKey_mF80910F815B03B8F366AA5E1F64FCB8E1D3A857B,
	GameDictionary_GetValue_mDFE966DFC349F8C54B9AE442EE6A58308919921B,
	GameDictionary_EqualsValue_mFCD9C415137DACA50D80268EA5525E8BC79AE036,
	GameDictionary_SetEntry_m2B5ABAE6F19026662F940E65757CC2AFB637A959,
	GameDictionary_StaticClear_mFB457F945DBE70D6F40FDCCE43EB7CDFC4C4BBB4,
	GameDictionary_Clear_mA7A61E2021608D886F5B611A819C2DE6A9FBEEBF,
	GameDictionary_load_m2A7F473FB0B8FED099AEFCA193C63EDE53A10D92,
	GameDictionary_loadIfnecessary_m06BAFE765784CBE5CB990C5B99C940A129F8C70B,
	GameDictionary_save_m36E32195215DB39F69D3A48A8B021A99658B2D3D,
	GameDictionary__ctor_m03EB6D629983C06FC9BE3D4B3C7E4FD1B5626566,
	GameDictionary__cctor_m27C7F69582CA3EBD0B54D2B44C63990D475FF46D,
	C_DictionaryChange__ctor_m20A44DD3E263F8E92F5600A3F39BB82A9907C733,
	C_DictWrapper__ctor_m90C683099CD19037E4B9DB310611B30778EDFDB8,
	C_DictWrapper__ctor_mAC7477DEA79F04ECC361AB555535544CD744EB3E,
	C_DictWrapper__ctor_m48B58656F40F76B71CB69D170E367B1D83D9404C,
	C_DictWrapper_ToString_mFCFC850A794B30E4A2311A5800F78D5465813087,
	C_keyValuePair__ctor_m3F9257C96CD8BC0BB9F39997EC8B917570AFB950,
	C_KeyValueArray_ToDictionary_m872863D676FA93C7D9E8AFDD0154728E6343A252,
	C_KeyValueArray_ToKeyValuePairs_m790487B860E050599FA927B4F35CFE01A6C809AC,
	C_KeyValueArray_ToString_m1C783A006E9479D64571918DA138471B724F02B4,
	C_KeyValueArray__ctor_m699B09A2A9D1706877DFE3E6C648169693EAD7FE,
	C_GameDictionaryCondition__ctor_m7EABAE475AAA4F45AA3547795684D316E690B052,
	GameDictionary_InputField_Start_m9797F0EF5EF6478E6F10E56E3390F1D0B9096EB9,
	GameDictionary_InputField_SaveEntry_m4EB43E1355535454A43356293CA5660634BD2EE9,
	GameDictionary_InputField_Save_m33E7F9E5E23DF7564F7D1AFF3410330439D4FEFB,
	GameDictionary_InputField_OnCardDestroy_m148F1228689E714C067F13DFD0C99C9DED64CD82,
	GameDictionary_InputField__ctor_m57F9F411AD1D4B93D1C1CE553FAC85A820B1967E,
	GameLogger_getGameLog_m8CD1D3FDE6DA0832404F9FC06B69F201842FF3EF,
	GameLogger_getGameLog_mE807897B6C632A91990781FB2C0B7C7167DB0B8C,
	GameLogger_subLogEnumToInt_mEC0F8631B81D34D7334A870A9D3EFC3209CB4863,
	GameLogger_showGameLogUI_mC6B24146D3DB080CCDD2D2EF762628C1E5B6AC0E,
	GameLogger_buildResultText_m1C67C6531503F650B9DF05B749CC30466CA6317D,
	GameLogger_Awake_m4A3F41558F55B980AB0971A786A08691B12BE5B9,
	GameLogger_Start_m1BF8A4AACD45A939175D356EF28806A6D0041030,
	GameLogger_loadGameLogs_m0D674383B1A9F8B995F14779C7553E8E1B90D711,
	GameLogger_loadSplitGameLogs_mE988EEA239215E1310BF0C620ED679C77CAF48B9,
	GameLogger_lockOutput_m00E947F13EAC708A56124A7948DF829D6B2DFDE3,
	GameLogger_saveGameLogs_m8DCE667047D123E5AD818211BA4EA215FDA89CF7,
	GameLogger_addGameLog_m7A3108FC6FEAE8B94B09A555D7E34A0B913C5608,
	GameLogger_addGameLog_mA3D587D4297E195201F4833A2E9CE2710DB145BA,
	GameLogger_clearGameLog_mD9719464CB4677BA5C9EE1561E0AC73FD7F4385D,
	GameLogger_getTranslatableTerms_m4D5CF8BDBD4012CDFB6C6EED698ECA9A9C1DC63A,
	GameLogger__ctor_mBC481E37C7D240349A089181935E3E571988E20A,
	C_InspectorGameLogEntry_addGameLogText_mEBA96F6A17BDB278A9EE3A94769DDF2BC9EBDF64,
	C_InspectorGameLogEntry_GetTranslatableContent_m288D6657BECC8521EA27A3E5BF9A0B577A00E8AF,
	C_InspectorGameLogEntry__ctor_m9E242769CDF63ECC40C9E79CFF75CDBFD73D956D,
	strList__ctor_m2939C6DBF6BEF9CA0831EEDB04F91139248B4BD3,
	C_encapsulatedStringList__ctor_mD79A2C5367A1DB9559FB10D59B9EDD29709879BB,
	C_SplitLogs__ctor_m9634F63301AE548B06945EA0DD0A5043B1D4DAF7,
	GameStateManager_loadGameState_mCE9AD4B728BD9F37B5DDA811C82536975603F497,
	GameStateManager_saveGameState_mE918049CCF3154262C93FC2855BBFF613371703B,
	GameStateManager_Awake_m26F6FEFEBFCEF4B2E123D7E8F6CA0B75E852B46A,
	GameStateManager_Start_m3E1D199E7F0D41A1A460F85D3D18FFA06C35BB75,
	GameStateManager_OneFrameDelayStartup_m4224DC7C44D02F4F4D8C61CF79B3F0563D1EA193,
	GameStateManager_GameStartup_mB877BD0EB531524E220573135F11A36AC69556DE,
	GameStateManager_executeGameover_mA3779D91F37F9670D18E2E27C7F9AB206AD7B26B,
	GameStateManager_swipe_mF7F2FE9E66B2BA266D55D2BAE64934AA2E909A86,
	GameStateManager_StartGame_mCBC897B251682305CF7130C1E77F14EAD7E8DDAB,
	GameStateManager_OnDestroy_mE50309001DAA681328AA856222484586EC1118FF,
	GameStateManager__ctor_mBFB14F943EA52DBFDDCDD87A5580B0E19B9A6E8C,
	mEvent__ctor_mAFEBD706A00B6C0A587590E4A12886BC4EC4E59F,
	U3COneFrameDelayStartupU3Ed__9__ctor_mFA4D7E5CB4F7D1FCAE440E17731FC79FE6463229,
	U3COneFrameDelayStartupU3Ed__9_System_IDisposable_Dispose_mEDF62ED725AEC84B0623405D0629C09AADA5575F,
	U3COneFrameDelayStartupU3Ed__9_MoveNext_mFDD42A5B9DBBF29CD565B1C0B13C7F8D0E79D044,
	U3COneFrameDelayStartupU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD01966B1BEF4FBB7C85B3A95CF46DC5E3C3FEC1,
	U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_Reset_mA3924A3373CF381CFB79BB3C5997F6D3D753E996,
	U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_get_Current_m72A635E851E0F7F4EC96AA006D5450B5F893E29F,
	GenderGenerator_Awake_m27987D96ED61EC638DFC8882C16D2F16B9A724DA,
	GenderGenerator_Start_mE39BC39E19AC2D80A332F7D12AD1C4B19771D91B,
	GenderGenerator_frameDelay_mD70AC765071A2E2AC81EBF1F46466AC95CCECD0E,
	GenderGenerator_clearUI_mF69E2AB59EF61A72391860339696370A57EE8544,
	GenderGenerator_actualizeUI_m3C76A622F2E9998B5EAD07745389C0BFA797F446,
	GenderGenerator_getGenderText_m6D6F19EB4BCE0001551EF470BB8A76B1C187AA30,
	GenderGenerator_getGenderSprite_m2ECB3DF9B0804410AA0C17221834DAFB6922F7C3,
	GenderGenerator_getTranslatableTerms_m8D5C70479133F33DBBC9806DF5785C7F6362A91F,
	GenderGenerator__ctor_m91332D6A1076C4909679B974B710EAB33474513E,
	gendStringList__ctor_m95524946BABCF8E207FB8FB5AEDF0B419F1DECB3,
	U3CframeDelayU3Ed__5__ctor_m3D601DC493336F760EA9D07947A8848668774822,
	U3CframeDelayU3Ed__5_System_IDisposable_Dispose_m61ED0D2F306AD93BA1D6B09AB601CED46FFD40B6,
	U3CframeDelayU3Ed__5_MoveNext_m3531EDDF5B6C8961D26E92D00495E82D1EBBE9BE,
	U3CframeDelayU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m508D08CD2E72E437AA970E975F0FDFDA49362A35,
	U3CframeDelayU3Ed__5_System_Collections_IEnumerator_Reset_mF2CA8CAE3AC8A3F88666C84089B11B51F2FA7361,
	U3CframeDelayU3Ed__5_System_Collections_IEnumerator_get_Current_m289933883DE423AE7BCD2897BA8EB82EB19946AB,
	HighScoreNameLinker_Awake_m6B8C50DB1332A19A165B0E3F8BCDF3F366EC1E27,
	HighScoreNameLinker_Start_m0B3C6C26520398135B71490C4139CF51FD429F1F,
	HighScoreNameLinker_delayed_m3D64F23F54557193F3753E010799BF317CF2E2C7,
	HighScoreNameLinker_getVSscript_mBDC2E521A6F695BCD2038FF91F46A2D9E9F1F065,
	HighScoreNameLinker_generateSaveKey_m5188D46AAA97BB521243C588E194D5705BBBF8BB,
	HighScoreNameLinker_displayHighScorePair_m5D32461C118D978D714BF2088868CF4BB7BF12C7,
	HighScoreNameLinker_clearUI_mE3E669BA1B04F1C9A93D41448DB4F570F2C154F3,
	HighScoreNameLinker_save_m94BA1E179C8E033FB984BBA1E89D635FA2F5A389,
	HighScoreNameLinker_load_m11A25767F812D70E0A7FBF6E6718AE8F9AA0FBDD,
	HighScoreNameLinker_generateHighScoreNameLink_mA167C4B972D4F9139EC36116D6E7A8B55A4CFD38,
	HighScoreNameLinker_SortByHigherScore_m048C503759EA72FE3F8AC41D6067AB7C36DD71CC,
	HighScoreNameLinker_SortByLowerScore_mDB80D4A109F8A9296DC5E9DEF4FED60D87D59E77,
	HighScoreNameLinker__ctor_mE3B11BA7D7FC98EB6B8B9A2FEB1FA4B722C4660E,
	highScoreNamePair__ctor_mA792849946C60DE01560E243AD72EB3DE7992238,
	C_cap__ctor_mB544A753B5E612B2BE667D82FA4B28A023ECD63F,
	C_highscoreFields__ctor_m1B688D8CF68DE56B8AC16D34E0F8888F23A2A8B4,
	U3CdelayedU3Ed__15__ctor_m0918B7118476E3C20CF0E8D758735D04E1FDED1E,
	U3CdelayedU3Ed__15_System_IDisposable_Dispose_mE0DB7048627B25283EAB909051B30A8D4FF6224D,
	U3CdelayedU3Ed__15_MoveNext_mB95273618B41E2C82E85788FB49A6A5E2D6458F2,
	U3CdelayedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3544EE0B3D2BCE81E12BC0A9D3CF43A2DAAA781E,
	U3CdelayedU3Ed__15_System_Collections_IEnumerator_Reset_mA0A6E89CDEFFD236B3CF585DB50FB39703C45A7C,
	U3CdelayedU3Ed__15_System_Collections_IEnumerator_get_Current_mF80795299D99484AE4CD163215543FDCC91855CE,
	HighScoreNameLinkerGroup_Awake_m1BD4CA8E6F4115AF23CED1FCE13A4C3D3A0BDDA2,
	HighScoreNameLinkerGroup_Start_m52202319C46806DD73A1BEBF5B2B00F1420FAEC0,
	HighScoreNameLinkerGroup_generateLinks_mCE54CC6DBAE99FA14FCE82A02D0E48C01F3A05BA,
	HighScoreNameLinkerGroup_Update_m9D3DD7AE853316E5CD55AC47B579DE9D5C264F0F,
	HighScoreNameLinkerGroup__ctor_mC79556126E4C9D5A2A4133B4C8E55A41A76D48D9,
	InfoDisplay_Start_m8A2591F2B58520A40EFA4864F44FE390CCC8D57D,
	InfoDisplay_cyclicIconCollector_mE8F77F9BEBD5496FEF8766E4762BE9C9636F150A,
	InfoDisplay_startAnimationIfNotEmpty_m16AD68A28E7BF6208AC07AFF9FADD8D713E85449,
	InfoDisplay_addDisplay_mB3F74FECA3C337EB5578585075E357F16DDE8656,
	InfoDisplay_fillDisplayElements_m63ABCA4EEC6EAFEA788786DECCBB11A9CD5C7E4B,
	InfoDisplay_clearDisplay_m3F093403261BE3FFC986D8ED962830B55E5E9F4D,
	InfoDisplay__ctor_m4EC165607BF3AA4A524436EA7B28277376F38303,
	valueSprite__ctor_m8FAB52347880428D39DB8178713D5477F2E216F5,
	displaySlot__ctor_m8F36A911DF6FC7D4AE65FFFF5799425F2612747A,
	U3CcyclicIconCollectorU3Ed__2__ctor_m9220A985B72F928F75C0CAFBE073745B9E441881,
	U3CcyclicIconCollectorU3Ed__2_System_IDisposable_Dispose_mFB4B957CA312D8B9041E3DE37A56A4AF017327BA,
	U3CcyclicIconCollectorU3Ed__2_MoveNext_mD098EA5DD6E28552DC5D3017E6412A18627FDED7,
	U3CcyclicIconCollectorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m303A52A720B7B60184234F81392918ED44B068BE,
	U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_Reset_m2841F544A2EFED8EE58B4425C78555D50A12453F,
	U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_get_Current_mEBA04BC302FA19463B5072EFF2D48AEED7B86B2E,
	NULL,
	NULL,
	JsonHelper__ctor_mD3FCC876B35655F0018C89924306B1023F6FBAB6,
	NULL,
	KeyboardToEvent_Update_m75BDCADA7D872CB72169977EFCAF19D0DF3AA6EF,
	KeyboardToEvent__ctor_m6D42BA03E60087722BF6E6EDABC325242ECDFE03,
	mEvent__ctor_m1540FC08A4FE82844355C65BE8B79A74BA28B4DE,
	C_KeyEvent__ctor_m05CA1B879A7E7281952876192F66157F37B9623A,
	KingsCardStyle__ctor_m4EA7A34A71084473E04608823B62BE6E5A523FDB,
	KingsCardStyleList_HasStyle_m4273E9971214A97C8DCA921C4D0A61A286EE16B1,
	KingsCardStyleList_GetCardStyleDefinitionErrors_m92C7281CCC82BB67CA2C819F40B96471DBFECA72,
	KingsCardStyleList_GetStyle_mB7034AA9CE66D3103E59807EA7E191825530F8CA,
	KingsCardStyleList_GetOverwriteStyle_m1D716C53673E07051418DE9ECA9E363154E400BD,
	KingsCardStyleList__ctor_m76D7738B86D166646454ACF36BD4E8F6CAE4DE48,
	C_CardStyleNamePair__ctor_mD7B385766BB3E9749EEE8BB391DD09040B178CAB,
	KingsLevelUp_Awake_m605DF0FA642C24368BB265069B555F9A803D21A1,
	KingsLevelUp_OnDestroy_m5E0535B90FE8DC4E1A2BF135112333C0996A7106,
	KingsLevelUp_TestXpCostList_m7AD2DF6F865B3A8D57E9C69B82413B0721DA6273,
	KingsLevelUp_GetXpForNextLevel_m41287BB1AA74C4C4F7C63CC1F23B84BF8F0E0D53,
	KingsLevelUp_computeLevelChange_mAF450EDCF61F79E857570E262BDC4358F67A0408,
	KingsLevelUp_getXpBarFilling_mC3600B8A576909053513AA0CBD6D83D21C4E39C2,
	KingsLevelUp_getUiIncreasingLevelNumber_m098082BFD89912373ECD37895DF257FEB3D471A7,
	KingsLevelUp_cyclicFillingComputations_mFC40CFAFBC0B13EF636CA12077EE61318CFC952C,
	KingsLevelUp_actualizeUI_mB4AC88C7FF8760C2A819E7C53DCE43F7BFD2B493,
	KingsLevelUp_AddXp_m0DC049A6CF2C47554FE9BD320C68161DCCD87F74,
	KingsLevelUp_save_mE1F36408CC4C1BC5CA3A56C2A5C79DDD9A7C0652,
	KingsLevelUp_load_mC7FDE6EAC383058A3D1F5ADB5E5D2F5DFA108FA5,
	KingsLevelUp__ctor_m994FF3DC7883463FBA9636A9794D2C30C1AF8F65,
	mEvent__ctor_m271362D1DC1F31C5C1288B4CD23B699A2775DBA1,
	C_LevelUpStats__ctor_mF33DF9347BEA2EEDB83B7B4617DCC272812CD387,
	C_XpPerLevel__ctor_m04CA46A7E3BF84E1CB188543883B3304EB713C03,
	C_LevelUpUiConfig__ctor_m074BDA0391C9295052B7FF2217618A49275F2521,
	C_LevelUpEvents__ctor_m9736B63C24882D569E903AA45FD64F2A4AF2B321,
	C_ValueSave__ctor_mFD6785631A9DBF267B03AFE28E849DA75FC07AAC,
	U3CcyclicFillingComputationsU3Ed__21__ctor_mFEB671CD9B6AB385063CB85F9EDD2FCF66CFBE5A,
	U3CcyclicFillingComputationsU3Ed__21_System_IDisposable_Dispose_m44C5F2035DDB457E94FD9376930D8D740C577C88,
	U3CcyclicFillingComputationsU3Ed__21_MoveNext_m04A5FD1CEF6CE0B3CDB9E42F7C64C1857D46BE65,
	U3CcyclicFillingComputationsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9342F3240439AB5371B1AB414C9AD0438B44577D,
	U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_Reset_mF95A76D31F44E278584ADBD7A02631EE22024239,
	U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_get_Current_m59ECC582967406007EAC87DB871D8FED4E230955,
	KingsTypewriter_set_text_m003BEA7E8EC169FA8D5723673BCBE839DD8FD98F,
	KingsTypewriter_get_text_mD92B6F10367E4D8BBCDE820CC36E6CAA217E9807,
	KingsTypewriter_Start_m79B3609E9E5AB71DC2CD0A8C0B37A6B5D8B09EFD,
	KingsTypewriter_FinishTypewriting_mBA185FDBD961AA07F8AB166678C6677325A127C3,
	KingsTypewriter_RestartTypewriting_m6BDB5151CA320BA2853907A62DD5B7D1E84E0290,
	KingsTypewriter_typeText_m368EB8071A25C0E0E88D3D13C89D7FF35B43E5D6,
	KingsTypewriter_actualizeTextfield_m32FA491B5E4A5AFD2F68968A76A5567F6634CC11,
	KingsTypewriter__ctor_mD979E8519B5C8319530AEC8E35EF55607AA93E1E,
	mEvent__ctor_m117099E5273EE7CDFD17809A324AE5A7A8B8D1C4,
	U3CtypeTextU3Ed__17__ctor_m316FCE9CF3C7CB715AAA1E003D8F0EFB00A03D0E,
	U3CtypeTextU3Ed__17_System_IDisposable_Dispose_m6608A8CA82E96F5255878BAE12497DE488C43FCD,
	U3CtypeTextU3Ed__17_MoveNext_m12D2F7052D840F43C97B5A84B25B562454673BD4,
	U3CtypeTextU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2B057A114FA28DE212251036952D43402AC45E0,
	U3CtypeTextU3Ed__17_System_Collections_IEnumerator_Reset_mE733F3418C0E5CB7B1F92929A9734C0EA3FD9EBE,
	U3CtypeTextU3Ed__17_System_Collections_IEnumerator_get_Current_mC3ED16AA3344A28C0765D09F95EACD089DD6A91C,
	LevelUpDisplay_Update_m6ED94808844DF65D51099043AA947627AE090B3E,
	LevelUpDisplay__ctor_m18A45B875CE77C68092BD9180A9AC2FC4CD7EDB3,
	MakeChanges_Start_m3D032A5328BF8A77004F45B722D84C813C4C9CBB,
	MakeChanges_ExecuteEffect_m36E1BBADAEABBEFB312BCBDC50A99F94DE92AD4E,
	MakeChanges_GetTranslatableContent_mA5AAF874F057B0BC2035C1F84D23A6DED60331C1,
	MakeChanges_getTranslatableTerms_mBD3CF195F5F253A061C457B9D94D9549666A1F83,
	MakeChanges__ctor_m60D1291D43A17CB75518E990A6D7FA81CD9B1605,
	MathExtension_linearInterpolate_m7AAFE3CF4383C746B771F6FAE750AC32FD568DCB,
	MathExtension__ctor_m149FA63BE9EF015B670BE22CD1ED2AC217D5DF11,
	Mobile_Leaderboard_mDebug_m5FECF449AB006B01300AFD1E1750EDB6A3F4EC4C,
	Mobile_Leaderboard_setAchievmentText_m6D669A60F7C6C05CD132D47554F8B364C1096871,
	Mobile_Leaderboard_OnEnable_m8D02F96473452F5B5BAED3FF90F7D37BA9D53FCE,
	Mobile_Leaderboard_sozialActivations_m670202D247ACB1CB9A2F5E886948A355E4A8B55E,
	Mobile_Leaderboard_Awake_m34081276CE6D298F7B6819A16350DCF08FA1E5F6,
	Mobile_Leaderboard_UI_refreshSettings_m2282A50820F3A4298D3A49A2EBC3A90784CBF1A2,
	Mobile_Leaderboard_refreshSettingsDelayed_mBF003C2B8B4D5633265BDDD0EF26C47FE90C98C9,
	Mobile_Leaderboard_getAutoconnectSetting_mE449EB28849092CFFD43421D4455E05C39D55FDD,
	Mobile_Leaderboard_Start_m0AE4C9D97977A32B526DFCD07DB2C7C20C0AC91E,
	Mobile_Leaderboard_UI_call_authenticate_m4DD9157C30493DB2509C7C7EF8FA39F93B2BF8F2,
	Mobile_Leaderboard_UI_call_transmitscoreAndLeaderboard_mEC6524DD6A6D3DCEAF2351EBA5BC33760FA3C80C,
	Mobile_Leaderboard_std_call_transmitScore_m7ED94FD18909F5B42418CE93F7CD92981D21A3C7,
	Mobile_Leaderboard_UI_call_computeAchievements_m587235F6908A979D84C7DBB2A1A9AD10485CE1EB,
	Mobile_Leaderboard_std_call_computeAchievements_m3E1188E8C77456EF9DD1DEE605431B462C03961C,
	Mobile_Leaderboard_std_call_computeAchievements_ifConfigured_m97D21CC02C259795F1DA84B72874B20F910153E7,
	Mobile_Leaderboard_compute_achievements_m8AAD44C2C5A5FEEB305157B9F20AB175E2035A8E,
	Mobile_Leaderboard_test_for_new_achievements_m76615F0776C40B6C002FA4A097C9BC9224A2AD92,
	Mobile_Leaderboard_Update_mA735EA1209F6B1657704F9520F3219F111377092,
	Mobile_Leaderboard_test_authenticate_only_m07EBE25D5FCE59762C302264FF1274210519D3D8,
	Mobile_Leaderboard_cyclic_for_achievement_m4A703A988B68CA747433F7477853BC6DABCDC4A1,
	Mobile_Leaderboard_cyclic_for_leaderboard_m2D30E42CF796288E46A40AC8828BC1428ACD20E5,
	Mobile_Leaderboard_reportScores_mAC61B192296E850F86901C260AE884A2B0788E7D,
	Mobile_Leaderboard_readScoreFromPlayerprefs_m18B39BF2A2C0BD44E2043CAA97CB8D90EAB55DC9,
	Mobile_Leaderboard_reportScore_mE09C56D554EA2F645C2D11C8BB65F605062526AB,
	Mobile_Leaderboard_authenticate_mA0DC9315D3CB9A0A2843B02C89364B172EC18EF7,
	Mobile_Leaderboard_UI_Sign_out_mA95D540B9A93E730D98665A482E4C301C44C90E9,
	Mobile_Leaderboard__ctor_m2EE74700B0F8948640039E5010D64A30084EFCB1,
	scoreSet_get___transmitOk_mD5599284B0A446E8C34BBDD3093123DD21FE1239,
	scoreSet_set___transmitOk_mBF73643240665182CB69D0C14CDC3B087234BFA8,
	scoreSet_get___transmitFail_m1D46293C16444D7B24E7F299906D545B69B4219C,
	scoreSet_set___transmitFail_m31C56D55D7B64105F7B67D4DE8E26B9C5CF21E87,
	scoreSet_get___transmitted_m24ADB7B38403047613B6D005C88BE878E031EEA8,
	scoreSet_set___transmitted_mEAA4AADB1646933E1DE013DF2E0E612A0DA0F7E9,
	scoreSet_get___transmitRequested_mEE51F08862BCD8C9032E6E711AED5B14A3A09C31,
	scoreSet_set___transmitRequested_mC218E210B8E249EA4A35AF085BE958D03C729548,
	scoreSet_get___scoreActual_m1D676ADD27F828BB2CA053ADB30D939936D53B37,
	scoreSet_set___scoreActual_mB774F0A990B7EEE38DD922EA090B0C8579925EBF,
	scoreSet_get___scoreMax_m38F6DB7A6C75E88B9B89D6AA1CC207982739E230,
	scoreSet_set___scoreMax_m092C80890879F45DDDB325959410B46A6F90E6A1,
	scoreSet_get___score_maxTransmitted_m4B558D48B45FD7A633C4FD7209CB6C5FE3DB4055,
	scoreSet_set___score_maxTransmitted_m61DBFBDABBC81A990BDBDE25ECF48B22248C593E,
	scoreSet_get___scoreToTransmit_m0695FCB38BD85E9835B80EC619936570FC808FB9,
	scoreSet_set___scoreToTransmit_mD05BADBFF2EF6E8FE12433BA61872C6DF71E1EDB,
	scoreSet__ctor_mD7B3A25A8EF644D0D52C6D257BC478AB603D7CCB,
	achievement_get___reached_m8F7AE68B934D6AA11B0E6138C2DBFDFC216A8D8F,
	achievement_set___reached_mFCA3399C5D82C1AF8BD992E0D8D345751B27C626,
	achievement_get___transmitOk_mFB8E26CD83629DEACF31511CFEBC5EE71A114564,
	achievement_set___transmitOk_mFFF97F106EF5D1BDAA9958F254140C88727DD3C3,
	achievement_get___transmitFail_m5F02EFBC9B8215587543228A555A0E43350CC1F8,
	achievement_set___transmitFail_mBF337F23A2FA59B61F444C4C0C0720B28A0D9C72,
	achievement_get___transmitted_mF97F2ABC145A00EB91739C81212F9F8C933D2C5F,
	achievement_set___transmitted_m21111F0CD8563874ED9AD3A5047C8FF9EE246A35,
	achievement_get___transmitRequested_m34B52FBD3818A49E20F5E84D9292074CF34CE62E,
	achievement_set___transmitRequested_m72E233F50A99B23B152EBA5DC8C809AD4B4215CF,
	achievement__ctor_m31F6B883FEED55C02BFDE72399BDA80DAAD40F8D,
	U3CsozialActivationsU3Ed__23__ctor_mC4E5407730B1F65B6EC62E910146B3622143C7A0,
	U3CsozialActivationsU3Ed__23_System_IDisposable_Dispose_mB937EC683AE10AB216D121A11E6C668FAB08B458,
	U3CsozialActivationsU3Ed__23_MoveNext_m4A7600BA0AF552658D7CC68C8572E45F59A2F263,
	U3CsozialActivationsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m020C881CD53FB93BACC71AB995500894AB90C692,
	U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_Reset_m295E47A42C79C8C71B8EEDEA1FECE0D7625EA37F,
	U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_get_Current_mD6B3E13C1C0C3322A2959E8AEED0BC6FB2F30BDF,
	U3CrefreshSettingsDelayedU3Ed__26__ctor_m8279C00DD68770A1A3B9E7E112AB03B2470FD15C,
	U3CrefreshSettingsDelayedU3Ed__26_System_IDisposable_Dispose_m166F001DFF96386BCD41ACB6BC044BCCDCB82520,
	U3CrefreshSettingsDelayedU3Ed__26_MoveNext_mC3873E7B0380D199E099FA8B07A87D78C896D772,
	U3CrefreshSettingsDelayedU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F538F4E1C37EE1D6ED46A6F0650E7D661A840E,
	U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_Reset_m9ED897F280F6D9C26054BA2B1C3287F5C6D3381C,
	U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_get_Current_mCE6AB66AD30ED3D455300DFC06217893C416382D,
	U3CU3Ec__DisplayClass43_0__ctor_m859F6993AF123809ADF26DF1B894ADC0A2E0287B,
	U3CU3Ec__DisplayClass43_0_U3CreportScoreU3Eb__0_mCA86B26C96651268299F69509C04222067863A45,
	OrientationHandler_Start_m8F942E6E62FB92DFF5B93DE4C16BBA7DD5CAC501,
	OrientationHandler_Update_m0627B2C85838275A414F1964C3860774B78CE95B,
	OrientationHandler_checkOrientation_m5947CB565F0CFFD12EEC242E4FA34E24564A9F4A,
	OrientationHandler_hasOrientationChanged_m6C91C9D74F012D31818189CA27E76F51DD2B9F58,
	OrientationHandler_switchToLandscape_mD2448E0F92B7DE42A02AA884703CA0D9EF113BAE,
	OrientationHandler_switchToPortrait_m3B08B0DE9E0249C4812326A7252988FA9C946D50,
	OrientationHandler_setTransform_m7A433AB725DF97D07CCF6D1CB42B9AA9D9344846,
	OrientationHandler__ctor_m24887FA4A1303A9C08314823E998DE26E51BD545,
	mEvent__ctor_m123F9F57EA6E58DAFB4B54FE7D4D3BF30B3B0EFD,
	C_TransformModifications__ctor_m4A036A82EA37292B94A6DF2BA3524D7C30DA4A31,
	C_OrientationSelection__ctor_m78FE7AF8DDA2ED12EC41EFC09BB4AB94D50DE16B,
	PersistentImages_Awake_m13B4BE1E67153DD811AC914DCCF9F9F95A2A00B7,
	PersistentImages_Start_m54281937472FBE5CCCAB2F71540A76738C618C69,
	PersistentImages_Update_mBEAB23022F436AE187B7D9BD1CC51F1D17B899B1,
	PersistentImages_SetSpriteIndex_m09BBD37E537827AECA5968D7ADF0166B024DD588,
	PersistentImages_ActualizeImage_m8E785455ABA1ABDD46D868C10627CBB6CAD7FD9F,
	PersistentImages_limitActSpriteIndex_m50E0843D76BB4D3383E72F482C150216E1870FFF,
	PersistentImages_OnDestroy_m45A3317921BCDA053BA316B699DEA0BDB6B288DA,
	PersistentImages_save_m7CD11C358F8ECB1F153B4AF241E90E5626F5A8B7,
	PersistentImages_load_m237B4DB5D085682BC35DA16044C7519E89A556D6,
	PersistentImages__ctor_m580A58443F7877700B108F267B2E444FB8C20FD6,
	C_ImageConfig__ctor_mFDFBDFFBBDB8B180CE3CF11FF7F67160E28960F0,
	PlayAdMob_IsActivated_m2CC2A00DE459CAB5C369D385D027B629234EE82A,
	PlayAdMob_Awake_m0AE10706C036F854ECDF3E587C3F19616992E60B,
	PlayAdMob_showNotEnabledWarning_m6346304B037787C8C8BE0E9F732F9F081BCB8D7B,
	PlayAdMob_SetBirthday_mE884D91C1DF8CB159CB519547E97028B4186E7C1,
	PlayAdMob_SetEnableAndSave_m3D3A947F7458BBF762F6E1436EF330509CE49B2B,
	PlayAdMob_GetSavedEnable_m200074F117F4AF5C7224AF9A423BB5ED45DEDA04,
	PlayAdMob_mPrint_m23B5814C469631D1A40AFE0B4ED6E97F36A3871B,
	PlayAdMob_Start_mD5264837A49E2B9452996F43403CB8D71696336C,
	PlayAdMob_RequestBanner_m20EE8D3F072A889EA78A5E76CEE6F091F72ACE25,
	PlayAdMob_OnDestroy_m8D7EFBD88F886CFCF81C820CBC9FC64C54612459,
	PlayAdMob_DestroyBanner_mF7EB6A9A46401B9696E00B93F21FFF4319FD416C,
	PlayAdMob_RequestInterstitial_m1FAAF8C0ABDF3D2A194132BBB4D19EFE0E86B641,
	PlayAdMob_RequestRewardBasedVideo_m66B4AF09066E3357740838114E86150056FFAEB7,
	PlayAdMob_ShowInterstitial_m2FA168CC0201627A23EAD23F954FD86712E1AAD6,
	PlayAdMob_DestroyInterstitial_mB74409EA51ABD3B3AEB9708499D398F244DE2E19,
	PlayAdMob_ShowRewardBasedVideo_m21A8D32DEC469223E9D0F3C94C26A5ECA3B6690B,
	PlayAdMob_HandleAdLoaded_mD081E99906FA41E44DC15321C4419660CF1D25B8,
	PlayAdMob_HandleAdOpened_mCFF2367414A907C5FFE6C1B22AA1DCEC8508880C,
	PlayAdMob_HandleAdClosed_m0BDCA056ED5213E98A2D99F77280C141B2983300,
	PlayAdMob_HandleAdLeftApplication_m0E8871C403ECD0CE400B7B18829AF855CB1B636D,
	PlayAdMob_HandleInterstitialLoaded_m4994C55C474A57E6043B889FCE781527FE5248DB,
	PlayAdMob_HandleInterstitialOpened_mE5E1D4CE758C6D1765DD5AE9C17B488C8427C696,
	PlayAdMob_HandleInterstitialClosed_m2B49F5993C2D7698C71314E6B23F62FBAA6A2C26,
	PlayAdMob_HandleInterstitialLeftApplication_mA7C0E34DBFE0D9752E909D678E937EE58A66AB7D,
	PlayAdMob_HandleRewardBasedVideoLoaded_mD228E88BC46B0F767B1D5ABA651356DC49BB8A8B,
	PlayAdMob_HandleRewardBasedVideoOpened_m4272180DBC71AD14159748CFFA8DD6167B93B77D,
	PlayAdMob_HandleRewardBasedVideoStarted_m040C80DC038D0642F86E72385030B54987BD1A8E,
	PlayAdMob_HandleRewardBasedVideoClosed_mF09A0CB10E2315C42A6F5E862B65944BF70CFB2D,
	PlayAdMob_HandleRewardBasedVideoLeftApplication_m7935544DA7D1ADCFBA93109BA31137A6D4BFC640,
	PlayAdMob__ctor_m64F7DED6C446E00CCFD5DE538091CE8E7AA63B78,
	mDescribedEvent_Invoke_mBA74F4143C0423A3976A157C63130C5CE88B9C8F,
	mDescribedEvent__ctor_m53C3E13229C7ED485A0D6007888106772F40622F,
	C_Birthday__ctor_m13215A6C7026F67C8161B4843B7442AF6B011005,
	C_max_ad_content_rating__ctor_m04BD41C8C1ACAB601F95178258FA23C2FFB43292,
	C_GenderConfig__ctor_mF293F8700B11EA748E74244C442C05CD8C91B07C,
	C_EN_YES_NO__ctor_m4524537414B80870768ED69A9840A7DD82BA3BFE,
	C_TargetAudience__ctor_mDD4C6F20C9E6A1E828EEAF8B2C3D00071E25CB98,
	C_TestConfiguration__ctor_m4C9BCE17BCA83755E25CC08CD4CC1E06E58DAA42,
	C_IDS__ctor_m0635B8B2909B2A77A91A82970107D9F06F86565A,
	C_GeneralEvents__ctor_m8ED99F81679166BFB38F888847D5F0B5D790306F,
	C_BannerConfig__ctor_m05F8DF2B5CA6B7DBC8E825D695D6F935958135BE,
	C_Error_Events__ctor_mD1D39A280AD59676C83C6F394863626B313F6491,
	C_Events_Banner__ctor_mDDF381301AD69B8BB3A72A0317AFD80010FE6365,
	C_InterstitialConfig__ctor_mA9FF5E29815B7A6A383117C508E76135602A794D,
	C_Events_Interstitial__ctor_mDC49618B7E8372414347013F552ABB9436F7737B,
	C_RewardBasedVideoConfig__ctor_m4BB4003BB4529546545B745D4908954FFC8F5725,
	C_Events_RewardBasedVideo__ctor_m10D3FA15D0C970CC9F3D33CB30CB4EE452C5A1AF,
	PlayUnityAd_Start_m20394C023E3C512EE40A86A08F40C4290C1BB48E,
	PlayUnityAd_SetEnableAndSave_m1461DFC7539E3119E1C69149C5D9F966669B6AA3,
	PlayUnityAd_GetSavedEnable_m35D20860962D461EC9C60D4394F38CD1F1867CA4,
	PlayUnityAd_testForAdvertisement_mE3C461C09C83B9B6851C5F210F5D00D86A1DE8B2,
	PlayUnityAd_showAd_m92CF616328616B318DBE887CFCB08FECF3F29891,
	PlayUnityAd_showRewardedAd_m0F5CAE1A0ED77CD52E9F6050BE5575CC162BACBB,
	PlayUnityAd_showDefaultAd_m18E3254445CC0B95F840A7A58086D8234CBF4518,
	PlayUnityAd_HandleShowResult_m9A97F58238ED8502B18620FDF5B072469463492E,
	PlayUnityAd__ctor_m8ADE6851C62668D112CED7477B876D3A69FE5C2F,
	mEvent__ctor_mDA1C48D04788A7D027D997347778701E9F932F80,
	U3CtestForAdvertisementU3Ed__17__ctor_m16D037C642106BB04305682B117FCE27C0C75741,
	U3CtestForAdvertisementU3Ed__17_System_IDisposable_Dispose_m9BC3A92ED2FB1954E5445B8A8391649DFA6957E4,
	U3CtestForAdvertisementU3Ed__17_MoveNext_m144B2B40E93E4E44A3FC855FF77C2388FE2872E7,
	U3CtestForAdvertisementU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8D44465EF33772A073C5411A44D97351670F236,
	U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_Reset_mE310632C52E3A44CC1892E2669BAB3E63554441F,
	U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_get_Current_m7B7B5E24FB0E178AC03D8AC548434E2FD3C3C9E2,
	RandomElementWithPropability_Awake_m1A89E9EA1F4C10BEB127EFC94A41D659504DC94D,
	RandomElementWithPropability_resetElements_m9391B38EA8110C1AABA15D5C64E6BC8B9F7D491D,
	RandomElementWithPropability_addElement_m467497F8FFCBE0A6B2870532F0C80F64759418C3,
	RandomElementWithPropability_calculateOverallPropability_mFD179A2779BF89B565037FFBC8385B2C032ECA1B,
	RandomElementWithPropability_getRandomElement_m004C5D11EB0C7D53CF8BBEAD8590C36BC1ACE5B0,
	RandomElementWithPropability__ctor_m0337098AD84E899EF221A2FB531097D551932656,
	propElement__ctor_m964DE31AFED7602AD9141C45D32A898D391371E9,
	RatioHandler_Start_m7ECDDE6D751A041D47653702DEA80BED5674717E,
	RatioHandler_Update_mF2F0045B4108524A7BEBB0E70215931BBEFA5D0E,
	RatioHandler_checkRatioEvents_m45B36F634F9418FF729EC7BF20CA4DA4DB403238,
	RatioHandler_hasResolutionChanged_mD5EB20DC957A40D4CEB217E76C56D2B6F9FC68E7,
	RatioHandler_getRatioIndex_m3389AC4691EF5117A4C55CB94410EF4ADFE9B006,
	RatioHandler_setTransform_m66F811AF0428723749C3A2D00BC72131EF85EA18,
	RatioHandler__ctor_m09E5A7A051A9B9860BE16EF384A39408AB9E779F,
	mEvent__ctor_m26D2BB5491EB6938AAE74B79BBCEA7E26E75F2C2,
	C_TransformModification__ctor_mB7E72926363F2F132F74E6EFD06F608E7936A6CC,
	C_RatioEvent__ctor_mBC2EFBBE3B3095C8E6A921DCB6E8A8B28775E19B,
	ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469,
	ResultCheat_addCheats_m74FB481A9D8149547C47E65C0F8AF775DD818924,
	ResultCheat__ctor_mAFFCC7B149EEBA4A984754F25F9FFE43344F15BF,
	ScrollbarIncrementer_Start_m7E31E31C76D25480AA4C0BB07F47369B3FCCAFE6,
	ScrollbarIncrementer_Increment_m120ADE06FFF6CFE19B63A95DAA01C8017F1C198E,
	ScrollbarIncrementer_Decrement_mF2522250A40B7631048C377BBDD71DD78DDA0782,
	ScrollbarIncrementer_Update_m2B6A719FF0FD105BAD917B05DD22D5A6D4B38AFA,
	ScrollbarIncrementer__ctor_m995F1AFFD246FC52AAD8F449EEE94078E63EB488,
	SecurePlayerPrefs_SetString_m2D4B33838525B7259FE13B3A0AC62AE18721C12B,
	SecurePlayerPrefs_GetString_m440032AD98A734955257A3ACE80120E00315BDAB,
	SecurePlayerPrefs_SetFloat_m65306B46C8EE84AE9260D490033332772755A3CC,
	SecurePlayerPrefs_GetFloat_m25BB6422639AF9CE4AA43EBD4549DB2A377B8AC0,
	SecurePlayerPrefs_SetDouble_mE231BF3F3E754A6C336A49816C5310CE516C7771,
	SecurePlayerPrefs_GetDouble_mA63CF2798D44B528E9F8C707DA6D66942C241A17,
	SecurePlayerPrefs_SetBool_mFE334658A75065CE53642D20F02221DD8D735CED,
	SecurePlayerPrefs_GetBool_m5F579F2208FAADF0511CB20CAC5E89AE43751644,
	SecurePlayerPrefs_SetInt_m19AC906F48CC007F2493FA6B45843F550EDC89A7,
	SecurePlayerPrefs_GetInt_m385D2D04CD57462884CFFB46D9C1A1623565706A,
	SecurePlayerPrefs_GetString_m3B547B77B8975081836923ECC4F6D694AAEE3563,
	SecurePlayerPrefs_HasKey_m73FFDC9C1EA3EE923620DF6FBA15459570979B36,
	SecurePlayerPrefs_xorEncryptDecrypt_m98E023D0F73343FB068D1BCFB8F40062D5C62AA4,
	SecurePlayerPrefs_GenerateMD5_mCF87894E80778181A97424F348430F8DF4ED31F9,
	SecurePlayerPrefs__cctor_mA3BB73F830F90804BDFF23302CAEF7004BB1B9F6,
	AudioVolume_Awake_m9064D991922D874C34F26600EFABAB7D07BC16C2,
	AudioVolume_Start_m0C68AF9A4685ACD6D630994C108029C2900B05BD,
	AudioVolume_changeVolumeSetting_m390B40A4854CDB2C9D093583E033286A61DF326D,
	AudioVolume_computeConfig_m8C24532A7248B93034756CE3FB1276D9270596F5,
	AudioVolume_setVolume_m6F33E00B0B0638B77C6BE3E3807786102E82F45B,
	AudioVolume__ctor_mC48D4A5FB4601DCE553FA57649EFC4F481114112,
	BackActivate_Update_mEDCDC43D06BC6D3C01F4DABDD6CFB75B95A3C996,
	BackActivate_activate_m3A91E6D41A3BB71A4E42EA4AED5CCDCF75517456,
	BackActivate__ctor_m0F75192133D8962F773FA81DA755087ED03CA03C,
	BackDeActivate_Update_mB0E2DB1A1F112C988282BCDB9E4743354443DD28,
	BackDeActivate_deactivate_m7442D6DC0631BB1992F166DA45F5B14397E7D5EC,
	BackDeActivate__ctor_m41EBB285B2426EA28B83328D9ACDE9C091E56BBC,
	ExitScript_Update_m7EF4B6BCE51D32AFAD0223EB9D0163E27610F0E4,
	ExitScript_quit_mEDE612875DC93285FF16FEFAC884CD0267891C7D,
	ExitScript__ctor_mB510824368746FADD21053DDD4766F5543E601A6,
	LoadScene_compute_levelload_mBDB0B446D79454F1F404952B979564FDDC1DDD66,
	LoadScene_levelload_m0440A9C2A60005D899D7E7F013DB29996D122009,
	LoadScene_delayedLoad_m6E48431E35D2FF55B1CE9B57CD496DDDCE98ED7E,
	LoadScene__ctor_m3003C0157CC4154E26B2ABFEF86DAF98ED17150F,
	U3CdelayedLoadU3Ed__4__ctor_m2FEDB7CE5A9390BE1C53532F8ABFBB6A6B962324,
	U3CdelayedLoadU3Ed__4_System_IDisposable_Dispose_mF3089973DD6F7FB4A8C274A44A16B5CD2BAFE9FC,
	U3CdelayedLoadU3Ed__4_MoveNext_mF5602570D6689336A4BA061B48278F82460C12D9,
	U3CdelayedLoadU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B0A66E43371D0D1D55312BED6363508A1A4AEAB,
	U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_Reset_m3463BD91919D2175B690C1A4BD4E2B9CA498A3B6,
	U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_get_Current_m6B830331C28C6C1601A5DA6711C53B509B5D8C09,
	MusicPlayer_Start_m196809CDCF89356E622F9AAA17375AC2A0F4DAB5,
	MusicPlayer_play_m55DE11211AB12E19C12EB0F332D1CEDABE26536C,
	MusicPlayer_stop_mCCDDCB3736B92E6546DC01811565F6E46E74475B,
	MusicPlayer_PlaySong_m3159AB80E65B2EB59490C5D52D1C3F1663ED52C8,
	MusicPlayer_multimediaTimer_mEEEA7ED5D30B61543F2FF340A50006CC2AF3AF56,
	MusicPlayer_nextSong_m4929CA47D032E4157461DE73748BF4B689906550,
	MusicPlayer_clipIndex_m56A4E5011864E61D9649BCFB0DAFC0BF4031D07A,
	MusicPlayer_setSong_m5654955DDDF5871A3801F8DEC5F973B9F1BEBBCF,
	MusicPlayer_replaySong_mA725B6620FBFA638DD71AA5551D1C03F5458077E,
	MusicPlayer_nextListedSong_mB99FEDCC28C801FAE829D6F071CE02FED9B32C43,
	MusicPlayer_Update_m7BD87702BA93E1CE0DA8D666975B48BF5ABC376E,
	MusicPlayer_actualizeAudioPlay_mBEC308AB679D96209206EDEC678BC4B2041B3CEB,
	MusicPlayer_OnDisable_m38C48F820AB7B58614CF8DB16D3AC896C14D181D,
	MusicPlayer_OnEnable_m2F00726C543053E06F55B646F38485A1F3F3EA00,
	MusicPlayer_save_mF22FDF468C84AC1353F20D07E45A5395232B64A5,
	MusicPlayer_load_mCA2285DC1F5A79CF603DD836F007678FF3303E60,
	MusicPlayer__ctor_mA83600A144210E8586F4618ED75C614BFE6450FC,
	U3CmultimediaTimerU3Ed__16__ctor_m9452FF5F9C621B2701F14069A953C1D7EDCB639E,
	U3CmultimediaTimerU3Ed__16_System_IDisposable_Dispose_mB7EE0025ABA8196757B8B19E53D0AF877CCC729E,
	U3CmultimediaTimerU3Ed__16_MoveNext_mC9D24E8334B8C263F6AC866C27F81A215DBCBA90,
	U3CmultimediaTimerU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAFBBFFDC0FAD295792E66E7EB0185FBAAC74A4C,
	U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_Reset_m2123A5E79B2C708B0EBAF93621053EABA2B1F5B0,
	U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_get_Current_m1B45D5AAF2D3A164F2C5DB0B61FFB79517282150,
	OpenURL_loadurl_m8AAD80CD62305595E4ECBD89C0AEF7212E97AC24,
	OpenURL__ctor_mBD893164A0E0FE8CD0999BFF1AE11E07C5244D5D,
	UniToggle_Start_m87ECB16D4D8F829D115134B421C62AA814F958C7,
	UniToggle_setScriptState_m364D2EE045E5763E9C625CC0E775EF5CC3619F14,
	UniToggle_btnOnly_invertScriptState_m91504CA85D57D8A6172E7192E5DF463434C92DA6,
	UniToggle_saveScriptState_mE81754E976176B924EAFFC499C9FC4015CA57FAA,
	UniToggle_loadScriptState_mD4271BB150D5994105125BA6AB46B5AA9C6FACF1,
	UniToggle_enableDisableScript_m294EA8C9898F819D39E7BA7418E55F03B3E45102,
	UniToggle_testScriptStateOnStart_mA73F0BE62A7D861E15B600B105B5604BB5C9E3EE,
	UniToggle__ctor_m70CC2366FE21E2BF90C532D96A1C0391592F1FBE,
	saveSlider_mDebug_mB4E44448A0F03C971FD2CE67D98399502F97C6E0,
	saveSlider_Start_m417862C0623CC1118B74EA18EB9ECCB431C65487,
	saveSlider_loadSliderValue_mF1C7A305E9F61E5161EFDE6A317A0A880D5BA2A6,
	saveSlider_prepareForSaveSliderValue_m390920FC15D1471753EA3C1926976FB0435403CF,
	saveSlider_saveKeyDelayed_m38B8C3376FE5ECCC0C8F95B8635C123D009A3EE8,
	saveSlider_saveSliderValue_mCDAC6F440A84B7378C45EFD9260DF0287500902D,
	saveSlider_setSliderValue_m4AE6182A3C4CD21C6A10F31B67A165E5592E0CD0,
	saveSlider_actualizeUI_mAEBCE38B5ECC01E64EECFB87E19CD8415BF3F106,
	saveSlider_firstloadUI_mB72869F64A11341BCC115EDC387BD1555C0DF895,
	saveSlider__ctor_m48321ACABBFA0CEF96E05320C6B69F8B0F11355E,
	U3CsaveKeyDelayedU3Ed__13__ctor_m2DC30CBDF8E9E74DCFCDF4A344C2534AA4428DAC,
	U3CsaveKeyDelayedU3Ed__13_System_IDisposable_Dispose_m9C6DD24E2BD62BB987123DA584DB83021628DCCA,
	U3CsaveKeyDelayedU3Ed__13_MoveNext_m293BBF39D8989B4232B7BACD8A5903077B9EDF34,
	U3CsaveKeyDelayedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C9015DDBDB5E34979C1FA1F6F29463EB224629,
	U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_Reset_m3D7E598789A659776E64F9B635AEA7FE312067AE,
	U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_get_Current_mC4966B1DBE9334F30DC9633FB3C843A517F178AA,
	sliderToVolume_setVolume_m4835D1F1733532303A17B45968B69DE162E4D27D,
	sliderToVolume__ctor_m48E4E332E7019DB68C903641E4784C47DEFFE503,
	ShowGameLog_Start_m2EB10E397127A61F621B3487D88D7FB49837BD59,
	ShowGameLog__ctor_m42C5022625C0295E44FED2394E6E8F0B4F181DC0,
	Swipe_Start_m67570B95FB0889F6D2990E8731474EE3E76929D0,
	Swipe_getSwipeVector_m9E91C8E5E27D2D654CD6337F32650C1B4659CFA1,
	Swipe_setIdleSwipeActive_m288BBA1719FA54653EEC5E886930BE3563192264,
	Swipe_getIdleSwipeActive_m91840593FFCE93729E16C78670D25C65F7EFFBDE,
	Swipe_initScreenPositions_m00820B37093C1FC21EAA3DFC43F33FF305B57472,
	Swipe_SetScreenTargetPosition_mC3049357A351A3996075CE0E4BBADDD3E380582A,
	Swipe_directionToTargetPosition_m18DB48F06F70565F2B6B2F93BA0865EACC090CE4,
	Swipe_screenTargetPositionUpdate_mEA0529BD486A5757A8082B1C9C1751939034A9A3,
	Swipe_checkKeyActions_m103F237032B84713AEB55F1F9D9A68AD781A937E,
	Swipe_getScaledSwipeVector_m9CCEB9C745F69DC3EDBE00E4B2C1DC6CF9AE1744,
	Swipe_Update_m5B8885067E3CB784950DB863B1BDDDA26652D0BD,
	Swipe_LateUpdate_m7E017407F76C5834A5B637C1EE3D8FB5B72BBEE6,
	Swipe_swipeIdleMovement_m02B1CD397EA2E41BAA80BB3E64391A98B4551A92,
	Swipe_onKlick_m46A95227B19FC88ECD5528CEC36D0D34D17FE76F,
	Swipe_onRelease_m737498D2F05FFFAEAD60C8F665F3012A850E17F6,
	Swipe_processSwipePreview_m7A41B6C8B520D660F3E15D7A388549EBA9AF3EDD,
	Swipe_processSwipe_m84B4F93491A1A89EDB3D22607A96F378F38C7B95,
	Swipe__ctor_m97873674A6FB4BD351B24309C91C278A784F013B,
	mEvent__ctor_m422D2976305253C6ED52A02A067CDBE6474D57F8,
	normSwipe__ctor_mD68CC1F537CBC219EDC8152AB45F875217C6930E,
	C_IdleConfig__ctor_m2C70731698DECD10B260B67044FE830991E81274,
	C_ScreenPosition_GetPositioningActive_m9F6C0C389CCB7F34881E23B829EC3F76FBB6EEC6,
	C_ScreenPosition_Reset_mFA0D2FE5B01A4512BBDD4854FF51F95C1632D633,
	C_ScreenPosition__ctor_m062E235BD0756EBE6BB947F400E139D8270A7882,
	C_KeyConfiguration__ctor_m862B97EE4F7D60BE68373643DAE5063BBCC68E18,
	C_KeyConfiguration_GetKey_m18DDB7372373C749D0BDA0FFCADA2C8F24F657E3,
	C_KeyConfiguration_GetKeyDown_m83DC83ED24128297D17FC2216400966513ED3E13,
	C_KeyConfiguration_GetKeyUp_m79ACE79C932136C6B12143EB8CE23DF44B249D01,
	C_KeyMovements__ctor_m7D7C8E9BBDC5155D1D44DDF6008B4195B0C200C8,
	TextReplacement_TranslateAndReplace_m9BDF2AB53EFBCC14D600FC0FB5E0F7B96ACF0ED7,
	TextReplacement_ReplaceStringPlaceHolders_m3E876EC846D9BB38E82B2A67F3FC0E1C93D44CAA,
	TextReplacement__ctor_mDF71E15D2F62DF9CA92174C82F55C664E2171340,
	TextReplacementDisplay_Start_mDD5D4FB4BF6E467E9BC6689C46DFCDA9C92BF694,
	TextReplacementDisplay_delay_mF23DE8A32576410DE6033F2477529F1961C63972,
	TextReplacementDisplay_cyclicActualization_m718D870A00CF1715C2A47C08E227BA09A94D963B,
	TextReplacementDisplay_actualizeTextOutput_m5843A0897CFE52D048FCB6F8DAF1739C4BD5B758,
	TextReplacementDisplay_replace_m1C500DF1551ADD25FBC9A3BC3DC88BFAFB221029,
	TextReplacementDisplay_getTranslatableTerms_mF5288822E72E50FD80076575D82EF34FB2235954,
	TextReplacementDisplay__ctor_m05866101F23944C2450AAC60353C19728D551737,
	U3CdelayU3Ed__4__ctor_m7CA16C5D26BDC3223FF857AC48BB7DEE4F7D5506,
	U3CdelayU3Ed__4_System_IDisposable_Dispose_m0B66E4484A0C881A4CD168C373129C0076E53E37,
	U3CdelayU3Ed__4_MoveNext_mD9164FFBC739594EFA18F7BB2258E6D2FF146A9D,
	U3CdelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF00484BABD02F82575C68F2D73EC3FF32471CFD,
	U3CdelayU3Ed__4_System_Collections_IEnumerator_Reset_m89DC582C5637A0A9AB60E090B359DBD98470A49F,
	U3CdelayU3Ed__4_System_Collections_IEnumerator_get_Current_m1CFAA7DE6B7CC9C41DD1A9BC07A52468D9A68C2F,
	U3CcyclicActualizationU3Ed__5__ctor_m8E38305516B55E2E423B4C542D8D7EB0CEF3BC51,
	U3CcyclicActualizationU3Ed__5_System_IDisposable_Dispose_m81C78D44F1B5A21BD65D249322F6767F9911C53E,
	U3CcyclicActualizationU3Ed__5_MoveNext_m99B5F15A82291E4350C2ED1D7FFE3CD30CF200D7,
	U3CcyclicActualizationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3C2A0AF0B4BC30F2B8923FDFF34739B2892AE3B,
	U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_Reset_m9CCBFCFEF27DF16814DBF2C2C6171DC25D2C4304,
	U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_get_Current_m7BEEAC4BD998A473557796E64438100C0E920B23,
	NULL,
	TranslatableContent__ctor_mD2A22BEB762C341E99BF0787CB777DC0C7CEE11C,
	TranslationManager_translateIfAvail_mA8E28FED4E2CCEDF6D36770AA210187D0F5B0E8F,
	TranslationManager_Awake_mF01B49B2064CC5724AD753CB3EFF07CCB63C95F7,
	TranslationManager_clearTranslatableContentScripts_m6F9188687C3E819BE613CDF62ED895F2D93FA17B,
	TranslationManager_registerTranslateableContentScript_m3AC735A23ACCDB632242A1C51DFCFD1722F08604,
	TranslationManager_generateTermList_mAB7D1E1614D270095A81230E86CC4111E3BAB237,
	TranslationManager_saveListToFile_m61BE5693AC078ABC16DFC393D12F453812309981,
	TranslationManager__ctor_m2E68B494A395505CFCB9B009527A196E00D7DB1A,
	UIPositionSwitcher_Awake_m2D140D44B4FADE3A93E0C4606C5E719A507E2D6E,
	UIPositionSwitcher_SetRectId_m5581390A0EC5E2B363E198AE0DE4A039DB72E795,
	UIPositionSwitcher__ctor_mF11B2B2A04C231065A8295533743409A347DC746,
	ValueChangePreview_Awake_mF081514B288EC1654084E827B56157BF2707C129,
	ValueChangePreview_setSpriteSet_m4A77ABA7EFD3709EA0FA1A8F6BD064C110DBA629,
	ValueChangePreview_getPreviewSize_mB133D676F7506329E131EBB02282B1211CE38C0D,
	ValueChangePreview_getPreviewSprite_m389A2FF826929E6F1E7189E5D445BC0483F4D47B,
	ValueChangePreview_getPreviewText_mA99B29FE978A2DC180A5B4939099166BF822F440,
	ValueChangePreview__ctor_m6DB33B9377B77062AEAAA365928CF88FF5920D0A,
	valueChangeSpriteSet__ctor_mBCE0208355F095DDC984C61206B1DA63EB60B48A,
	ValueDependentGameLogs_Awake_m4397D20348AF10580315AFF425AC61EE61001A7D,
	ValueDependentGameLogs_Start_mA04E7290429974E9994B6FA21C08451733ACA5F6,
	ValueDependentGameLogs_executeValueLogging_m6E805705C17A5308083B75B17D4383761AEF4C8D,
	ValueDependentGameLogs_getTranslatableTerms_mB9567EDBE971BDE6CA0D51B23E26283D1CCDAF84,
	ValueDependentGameLogs__ctor_m960D2B6CECA909D8EB933A765DD7B3EECC2B1B62,
	valueLog__ctor_m03B33183FCEF45F675C5B02882EE992599647FB9,
	ValueDisplay_showMinMaxValue_mD6FB07FC9451A4916349EF4AD22CA76C0E80D264,
	ValueDisplay_Start_m1BB3D218C0D7366703EA77948A635755D5836BF8,
	ValueDisplay_cycActualize_mC15584D6A4DFEB246FCA9306F3FCA55A62F40EF1,
	ValueDisplay__ctor_m49CCF9067A8662FD5A21EB248D20580B30598E88,
	U3CcycActualizeU3Ed__15__ctor_m884AB86FB8663308B5D6E197DE3EEB5AF7A09B18,
	U3CcycActualizeU3Ed__15_System_IDisposable_Dispose_mDAA4568D0183CA0B11C556C49CBB15509765B1FF,
	U3CcycActualizeU3Ed__15_MoveNext_m7CDA3C19751B1D65062508BE1F3BFBFFBAC2D5F3,
	U3CcycActualizeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92D5C095669CE7EE955BD8782F09A4E5CA451711,
	U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_Reset_m921D4CA172B2615F151E64DCDA76E4D1796B3A04,
	U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_get_Current_m405E4962AA5731495D4C3F97E5DC3DFCEC6773A2,
	ValueMath_getOperationValue_mA57D49567979FC85C915F8D393F3CB880B89A380,
	ValueMath_Calculate_m5AF67FCBDDD31A861F4072347AE9FCFBD5C95234,
	ValueMath__ctor_m07BC88C83575C56C86B6A295C033411EEF281FA4,
	mEvent__ctor_mD66F1FEBF6C58DEACBCA94DFA844372F42BFA2AB,
	C_MathOperation__ctor_m034C6734164A068E50271430BF044CF33CAA025F,
	C_Mathf_rArgs__ctor_mD7F2116B12BE65EF84AC0B448F00F8356A37EECE,
	ValueScript_Awake_m3D3FD365BB5B57155CA15DDD68E532F89D0A78A8,
	ValueScript_Start_mD37FA8AC537308A495A1834CA8D4BC954BBE924D,
	ValueScript_actualizeUI_mF92E4E69F873F03D6D46A8F5C77801B7CE539723,
	ValueScript_setPreviewValues_mFA71740BC45527F1D56D7241CC7D4A4211BAD826,
	ValueScript_clearPreviewValues_mD4F140B594B9C6A02365D9FB23FC3B2E2125D93B,
	ValueScript_Update_m021D6B0E64810B5AC263BDCA7EBE3054962B7222,
	ValueScript_actualizePreviewElements_m99110F0413231F388BB8F6CA953631F786D2A52A,
	ValueScript_actualizeIconsBaseOnValue_m51C32F0E5DAB4A635D58A6B3971DD083EA1BE123,
	ValueScript_testActualizeIconsBaseOnValue_m6FE26B8BD320095F1B68A413437AEB7828D8A263,
	ValueScript_limitValue_mCED7B230FBEB35847217FE568E58E27961FED3B0,
	ValueScript_addValue_m68EFFD4700890EA81A9338E20605EEAB7AB83122,
	ValueScript_setValue_m37FFD00AB6EDB81B63C77EACF987FAAE04B560FE,
	ValueScript_setValueRandom_m22913683D5C903C425565E857D0C8D265AFA9C02,
	ValueScript_newGameStart_mDF5BD79249742B88D51A255E24BE5413CC077B27,
	ValueScript_multiplyValue_m42A7F411F66AC67D36A4E93EC12B4CAD87845FEC,
	ValueScript_loadValue_m0F75C9D364FE0CED4A4720B5E2594EA925919ACE,
	ValueScript_saveValue_m2E5DB5C97746CE20F249F7DAB26AB1A628014981,
	ValueScript_saveMinMax_m5235CE2222FC273369E527C36291E3E19F2123AD,
	ValueScript_getMaxValue_m96433B7FCC0A234D835E494D610D9509A4978123,
	ValueScript_getMinValue_mF4916A33C4E62980F8DBCFE80D01074CB131573E,
	ValueScript_OnDestroy_m54BC66C719039F311527CC3016047A5A13C28D12,
	ValueScript__ctor_mF28EE781916C3F1EEA78C6C459B3F29E58EAE0DE,
	valueDependantIcon__ctor_m15E4D75B913610F255176D4DD7A6106476EDB83E,
	valueToIcon__ctor_mBCFC9CB24907E6AA2AE9DBAF739801618F05B091,
	valueToText__ctor_m23117B9B9A35A2622A2E6D8F4416F9394A206989,
	uiConfig__ctor_m327B7BEE71302A7AC020765FD1AF8A787BE400D7,
	uiValueChange__ctor_m712AD92B9ADF482D7121BDB52A9E3AEA8A600A6B,
	c_nextValueChage__ctor_m47DDB6C4A0D43429300D73E58A04E377D544A2DD,
	valueLimits__ctor_m274E13B515FCD24D219EEDDA5DD607CD6E24F214,
	mEvent__ctor_mDA57C37775B9F55C8D1E7C3534F8F4306531A347,
	valueEvents__ctor_mFC287D8D88ACAAFD0838E80AFD54D15C4D613A47,
	VideoPlayerEvents_Awake_mF6C2EDA82125B660764E3C1E634CF606B04D8DAE,
	VideoPlayerEvents_ClockResyncOccurred_mD9BC3625DA332F1AD156A7A3DF723D2B9E5883C1,
	VideoPlayerEvents_ErrorReceived_m25F667F760E9E702D200FC5604FBC407D4515A09,
	VideoPlayerEvents_EndReached_m76225610F5CB009140628E7890BDE26E2AA41534,
	VideoPlayerEvents_PrepareCompleted_m528C058ECEC8D07E2A65EEABFAA44DC4501C2739,
	VideoPlayerEvents_SeekCompleted_m1F7DCF2A0A2F5F9BF98945424E9F0184BDE1A1BD,
	VideoPlayerEvents_Started_m7AF191232100E8D3D1E5850DDE9960D59FA1FF68,
	VideoPlayerEvents__ctor_m861EB965B5B178232F3A57ECA512A288B1BE2D18,
	mEvent__ctor_mC93DF36162B652A6F627787AFF002B4F788FDA3F,
	mDescribedEvent_Invoke_mFB64D848A306E9D3374C6EEF8E0B79A7C05C11CE,
	mDescribedEvent__ctor_mE8D0BB42D36F4A896D335DF54A2F8E9172B99F90,
	C_videoEvents__ctor_m85053570A38FA36C3B7D1E08C7501A59779774DB,
	addAchievement_add_Achievement_m1ADDAF21A1B039E791D26D3D9D9974E776A5A484,
	addAchievement_set_AchievementCounter_m2B89859B6DEFEC3A969D707B947281A0163F7C11,
	addAchievement__ctor_mAE3349095E35AA1DA60FBF5A402BFAA30C753AA4,
	addValue_addValues_m380AC79EC53FE31C0C81B7E88AD66C4FD2E2D53D,
	addValue__ctor_m53423167C41C6472146527A5B29B4BDF614379BC,
	addValueToValue_addValues_mE54FCB68A324CC48E0E4A743DDCECF1B4F35263A,
	addValueToValue__ctor_mC9C48C3ED80AA25734AAED8262B492FC20A15210,
	resultModifierForAddingValueToValue__ctor_m7F9040A5252F66E6B389BF57CACE86A183C29AAE,
	changeValue_ChangeValues_m5C9A3DF00CC2A692CEDA887CDBDAAFB2258E7838,
	changeValue__ctor_mDB12171B7C1C659E2B7AA8332A71E3FE5BA14AB4,
	conditionalActionList_Awake_mE34C8AB87F96AB4A2792C35520F73F49AFC892B1,
	conditionalActionList_Start_mA9B5DE6D46CDE3107538B658F1067A17BA61BD21,
	conditionalActionList_OnDestroy_m6959D55F600FF40514F7062F24F566012519C975,
	conditionalActionList_OnCardDestroy_mC28F549BCB64166E154667A891F1D742E0526BE6,
	conditionalActionList_eachFrame_m2D0D706D9FE437F1B7EE6F2B1C5EC027134612EF,
	conditionalActionList_initialize_m94DF103F8AF47FF6902DEBBA27D826AFB5B9CD75,
	conditionalActionList_ExecuteCheck_m3800CA576A2E35DFADFDF2141528EF7EEB5DE1FC,
	conditionalActionList_executionLogic_mCB599A8BD720FB8483FE217187D809813E27FA0F,
	conditionalActionList_getTranslatableTerms_mEA1CA06C1A46BCC734299F0921963D81FC0B2EB5,
	conditionalActionList__ctor_m635CBB4A24D14B081EA63562A60AC169F01456DC,
	mEvent__ctor_m6DC599616A023B862DFD2B782BA1F4542EDF14EC,
	C_ValueContentToAction__ctor_mDE10F180C3B8FB9A2AC177EF9673FE83FB7D4133,
	U3CeachFrameU3Ed__13__ctor_m7555B9ACB0F67EF10AE18E2C526B2D41E8D4955D,
	U3CeachFrameU3Ed__13_System_IDisposable_Dispose_m0F460470A415E6489949AB9394BC1F8692CE282A,
	U3CeachFrameU3Ed__13_MoveNext_mCF050519B456187F624902D96CE0F4070FAB8D87,
	U3CeachFrameU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA8B187D2D5D200C97BEDECF74EB850597D69F56,
	U3CeachFrameU3Ed__13_System_Collections_IEnumerator_Reset_mE1C939B7483AA60C3C43B5A25C8FBB968DF43DDA,
	U3CeachFrameU3Ed__13_System_Collections_IEnumerator_get_Current_mF23BE2C11DF83A40BB3750D9B86CCA27AC067457,
	firstStartGameObjectSetter_Start_m1134FC1606A600B38E52FBF40C6CC81A391EA22D,
	firstStartGameObjectSetter__ctor_mD3457518524DDFD2717BD90E02CC52B4495C62AD,
	scoreCalculator_calculateScore_m23A506CAA621D613FAFD8FFDB537F0C17B6B331A,
	scoreCalculator_Awake_mF3B084D87D53370337CDE8DC21E3F17136E2E171,
	scoreCalculator_addLevelUpXp_m536D9EF1ECDE2A86B3E7E92EEC864F60960F9499,
	scoreCalculator_Start_m4D99ACA7FD8E3B2AE95DFFECA01DA953BEABAFF6,
	scoreCalculator_Update_m57CDC8272BEE5E3DEFCA8FE4D277AFCC00A09000,
	scoreCalculator__ctor_m9B6788D6960DDF55F6E4171AC2539806C038CE52,
	mEvent__ctor_m929AF7D30C91997E469BA4DD5F4B2ADA26D9F280,
	scoreRelevantPair__ctor_m3CBFCD4BD66A73D1817A3C385E37AB674E5212F0,
	C_ScoreSave__ctor_mBC24674EB6027BDC8261A88A7A283FFDECF4704A,
	scoreCounter_increase_mCC44040870F0C3EC82E9727896DC697D20AB49CB,
	scoreCounter_setScore_m32A26B0537AB72F7DDBCD19D836BC4131DB3BCEF,
	scoreCounter_getScore_m343DCDBBA55DCB63EC5CDA07C27252C7163CBD3F,
	scoreCounter_setMaxScore_mAA9F164C785481AAF855C0D3CF580290C1B8D2B8,
	scoreCounter_save_m7E2DF3EF70FB123C78C9A6394C4C397D3E733C41,
	scoreCounter_load_m0ADB24C1D00E2B932664D3581D621043E721A21C,
	scoreCounter_Start_m9F93E1058110C052520AA6DE303AF796938CDE11,
	scoreCounter_Update_m4CBA4A996B332D8F0C257F6BFE6429509A8E74A3,
	scoreCounter__ctor_m5971308913C400AA1FAC6DE46BF08D0A57E711E4,
	setValue_setValues_m33499F09D12C3092076C485B179E9B09117FF180,
	setValue__ctor_m263767AF5C10474DC79D20903E3FA83E4C6FE90F,
	valueDefinitions__ctor_mE859CD4238C0D5D5A96F752EDA112C342EFEE16D,
	valueDependentConditionalImages_Start_m9C4B1054EC8B883F9BC1FA8DD4A135E6DEA7C073,
	valueDependentConditionalImages_oneFrame_mAB42BA6CFBBBCF5895EB5C08C3C426F70F6E1C27,
	valueDependentConditionalImages_ExecuteConditionCheck_mF986358DF054FA039E18695EB45A0B6AF20ABC31,
	valueDependentConditionalImages_testAndActualize_m8494C93476227A3A525936F3C5D72647E2218F5D,
	valueDependentConditionalImages_actualize_mB3002FBEEE357626EE946DEB6BAF30F719C5DD4D,
	valueDependentConditionalImages_actualizeIconsBaseOnValue_mB7AB0E98F454D48ED9BD0BA36AA311124CA98E06,
	valueDependentConditionalImages__ctor_m4FAF942341A4A812AC5ABC7DBE8AC0845ECAD12C,
	mEvent__ctor_mB5D959224CCCDAD4222E74A658DC12282B1B392E,
	U3ConeFrameU3Ed__9__ctor_m526235E2CA6FC5AB734BC6AB29863091F2F6F150,
	U3ConeFrameU3Ed__9_System_IDisposable_Dispose_m3473E1959E7822609AF060B96EE35B80428D6757,
	U3ConeFrameU3Ed__9_MoveNext_m97B3136536CC5E2C94CF4915AD3C112B72388AD1,
	U3ConeFrameU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD831E212FC446D5F0C503CE9F56D6B1AC815E565,
	U3ConeFrameU3Ed__9_System_Collections_IEnumerator_Reset_mED64782DBA020DC52AA1107DB00A44B2D7CD3D85,
	U3ConeFrameU3Ed__9_System_Collections_IEnumerator_get_Current_m6779D76973F63196D278DE6A3E80386D47855A83,
	valueDependentEnable_Update_m60D849DC19FEA8EC8644CD561B85B30BE0F1FBA4,
	valueDependentEnable__ctor_mEEAC850534F7F81762409DD3FE6C67646391292F,
	valueDependentEvent_Start_mD9E9C3A575D39551CA87A7D4456478E556C873B9,
	valueDependentEvent_oneFrame_m41A40BB51B3A6F5A3CFA19588732D053D843FAD0,
	valueDependentEvent_ExecuteConditionCheck_mF07302F3AD444E7E94F8AFEBD6769740376487E2,
	valueDependentEvent_testAndInvoke_m953CAD0A601A41F5A4ABD5DC004567E6AE78D6F8,
	valueDependentEvent__ctor_m746B3856B70444F76185D2E22FEA9C0851D76309,
	mEvent__ctor_mB4082415624720B574B121E9BC2BDCD550331F63,
	U3ConeFrameU3Ed__7__ctor_m853C858DD2E0A4C942AC48957E47B406F60F0AEE,
	U3ConeFrameU3Ed__7_System_IDisposable_Dispose_m69CD851696FFDC0061D1ECD990D56DA0B439B7B5,
	U3ConeFrameU3Ed__7_MoveNext_m226B9B41C26BE20C262164E1AC7FD1518BD8CD6F,
	U3ConeFrameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m24083F1557E44580268A6637582B9AAA7D37BE63,
	U3ConeFrameU3Ed__7_System_Collections_IEnumerator_Reset_mF061980C2A1E65BDF89EAF28A2B5D9A9B4BB3A1B,
	U3ConeFrameU3Ed__7_System_Collections_IEnumerator_get_Current_m52A004719D90B66FF54E4931166AAD5741764477,
	valueDependentImages_Start_m1705E3302D85626EB3130349E874E9440A056FB4,
	valueDependentImages_cycActulize_m9C138934BF325F4A8070605D6E61E0F45E9EE300,
	valueDependentImages_actualize_m02EED59A68D315EFC3740D940E8755F13B3BBEC2,
	valueDependentImages_actualizeIconsBaseOnValue_m0C6AC7B382107F3ABB93E1B87C482595731488A0,
	valueDependentImages__ctor_m68BD6822CF34F655D258392401E8798F544A25A2,
	U3CcycActulizeU3Ed__5__ctor_mDBBAFFFF8A92A55D8E3B43B8536C308D7D3A3433,
	U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_m99F9DA8C4364C3F1AC80C2CF07B952136C1A64D0,
	U3CcycActulizeU3Ed__5_MoveNext_m82EBFB1A73D2CBE83920B1F3B7840BFC178803A3,
	U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DAB6FA561D2922E658BF2B4B8AEF4BBF91CE4EC,
	U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_mB32402A65A3CCC658A347C707C0FE56B68D4938A,
	U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_m0DE39A326FC147072798CCA18C508086C011E33F,
	valueDependentTexts_Start_mD2964633494AFC99E42FD059130BC8AE14C1925F,
	valueDependentTexts_cycActulize_m2BB64E52678442A5E7DBCA3B48D1147759CBAC20,
	valueDependentTexts_actualize_m33BE03EB6851CDB764146BD0BC1D6D56559370FC,
	valueDependentTexts_actualizeIconsBaseOnValue_m5ECCFBE03335C7B55183DD06FCDED8885348EB64,
	valueDependentTexts__ctor_m12D09891EE1382536A0D844929654C6E5F6FAF07,
	U3CcycActulizeU3Ed__5__ctor_mB0FEBDE8940FB3BCA8EE32254835E36CECEAD7E1,
	U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_mF7D9DB7F1B0F96318D03BAE41FD73BD0057D56BC,
	U3CcycActulizeU3Ed__5_MoveNext_mEE54800BEE635E7C52F1EC721919C5589414D25C,
	U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA1C19D6A96F6104F79E604D37F6E74EFDD5AADD,
	U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_m2836EF31113785AA83327C23B25385F6C218333F,
	U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_mB9051E40AE1D230D32FCD8D7C3C5F8BE831B451A,
	valueDependentTimer_Start_m585E041219006860ABD99BD7D5F71DA8BA05998C,
	valueDependentTimer_cyclicTestValue_mF25FBA3D0CAC2801FCF2FEAE5E172B553CD1BA15,
	valueDependentTimer__ctor_mAA35316957A5A2CA50F0C3BD6A2B40DE38C8D8E3,
	mEvent__ctor_m296D1359F1CFA689462E60200E8934E294C83878,
	U3CcyclicTestValueU3Ed__9__ctor_m242DBED8F79E038793145948FBB5B3053E5C7C08,
	U3CcyclicTestValueU3Ed__9_System_IDisposable_Dispose_mE0B503F8CEA59D250D9A01B27100568467D9DC4D,
	U3CcyclicTestValueU3Ed__9_MoveNext_mB135BE45AB398EAE8096DA33AB9242EF15EF505F,
	U3CcyclicTestValueU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0575A7EE9E712A8D39FA707CA1AE13A417AF27B,
	U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_Reset_m78215A7BE3957DE9415B27EF8DCECA35B47D8B38,
	U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_get_Current_m68C1793EA5E692C4DAC56318B4758A4491CBBF2F,
	valueManager_Awake_m5066643BB1CAF4995BA76CF7DAA8FC89604524E9,
	valueManager_registerValueScript_mF85F7EFCC662C96794835B4D8E702DC38E08723E,
	valueManager_getFirstFittingValue_m12DB2EFD1CB46F6EE8FFB99A3C75F3AF59489588,
	valueManager_saveAllMinMaxValues_m69700B29A92FBD6BCF72FDAD47B75187B2381481,
	valueManager_getConditionMet_mC58154863B22C8F7F967834FF4C5CB92ACA84126,
	valueManager_AreConditinsForResultMet_m86A79E143AA6786A74A4997383A37DA541C3EDA1,
	valueManager_addValue_m584801B586C29A4584D3B9D21997F5C46477FED9,
	valueManager_changeValue_m48152EC781421E4FD890F77E73E8E6C24012E198,
	valueManager_setValue_m0A922450126E57860354F4A8A5FC6ED90F756114,
	valueManager_setRandomValues_mBE8DAEE3C146BAE85CA236B7D2023FE572BC10C1,
	valueManager_clearAllPreviews_m5F1E4FC5F2618E5FAD24D4199CD6CCA565C9501D,
	valueManager_setPreviews_m090BD77F6E6274055D681951FE2822C03D53721B,
	valueManager_setPreview_m506AD9CD688657E718AB0D7F69DDDC340FE9512B,
	valueManager_testForDuplicatesAndMissingValues_mC4AAACA8F8A7DE84A307C30B31D6970146C4758C,
	valueManager__ctor_mD14BD63AAF2D72F350929EA867277394423A64E1,
	SafeArea_Awake_m3369BF6C60075BD99152C8F6B4120900E56EBC95,
	SafeArea_Update_mFFD9106833750FA9657EAE3BC564A688F9216D81,
	SafeArea_Refresh_mBFFBE8630A66D7F813DCB6095B64629A5ED542CE,
	SafeArea_GetSafeArea_m1D364DF35EFEC726EA65743C99DB36850CB3FC42,
	SafeArea_ApplySafeArea_m167F7C50FCBB28FFE1EBE54407172834884A9AEA,
	SafeArea__ctor_m1A9E3BE6B986EC5205BFADF6AD825188C25703CE,
	SafeArea__cctor_m235200F1CD3FA25E844437CC4D9D3856ADCADE84,
	SafeAreaDemo_Awake_m6374A22B6C219B6E18C129F5DF5C1D983DA20ED8,
	SafeAreaDemo_Update_m537DACEBA3D2CF9CDD2E89BAFC2F0875BB2B74C6,
	SafeAreaDemo_ToggleSafeArea_mA872BFB7696B1BA2703CC41BD6731B8AB975E21F,
	SafeAreaDemo__ctor_m86686D30D1C7F983FCCD478348A1D9EAED508494,
	LogicalStateMachineBehaviour_OnStateEnter_mED92D8C50732590FCBB14B919C8BF90D71CD1DAA,
	LogicalStateMachineBehaviour_OnStateExit_m55C8681C6EBA6C6797175D4F35B10DA50CA6A241,
	LogicalStateMachineBehaviour_OnStateUpdate_mBE32483D677ED83072649B518115BD1B303AD6D3,
	LogicalStateMachineBehaviour_get_Animator_mDC4507F8809D9FB66D14DD9F1ECDECA694B50F51,
	LogicalStateMachineBehaviour_set_Animator_mE95CB9BB31E180CFF313B6587C993E61F7C8C4AA,
	LogicalStateMachineBehaviour_OnDisable_m364CDAF763BBC678DF65D2E1BEC58F5A69011FCE,
	LogicalStateMachineBehaviour_OnStateEntered_m19DF958B0475A8BCB092569931B95994D27F058E,
	LogicalStateMachineBehaviour_OnStateExited_m82D5FFDD45FCF6C728C77841F21120BED7BF5352,
	LogicalStateMachineBehaviour_OnStateUpdated_mFECCD82D3748E2EDCC408F5408D024EE9666B87A,
	LogicalStateMachineBehaviour__ctor_m0108DAAEA632E273AE3697878CAD702D91E786E5,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
};
static const int32_t s_InvokerIndices[1200] = 
{
	1023,
	1023,
	1023,
	1023,
	1023,
	1162,
	1193,
	1193,
	1162,
	823,
	823,
	1023,
	1193,
	666,
	666,
	666,
	616,
	1023,
	775,
	775,
	1039,
	1193,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1193,
	1162,
	1193,
	1162,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	2001,
	1193,
	1193,
	1193,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	906,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	823,
	1193,
	1193,
	1193,
	1023,
	1193,
	1193,
	1193,
	1023,
	1151,
	1151,
	1151,
	1193,
	906,
	1023,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1151,
	1193,
	1162,
	823,
	823,
	1193,
	666,
	1039,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1023,
	1193,
	1193,
	1162,
	1193,
	1151,
	823,
	1193,
	1193,
	669,
	1023,
	775,
	1151,
	1193,
	820,
	1193,
	1193,
	1023,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1162,
	1193,
	1162,
	1193,
	823,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	616,
	775,
	1023,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1023,
	1193,
	1193,
	1193,
	990,
	660,
	1193,
	1193,
	1193,
	1023,
	1023,
	1193,
	823,
	1181,
	1181,
	1193,
	1193,
	1193,
	1162,
	1193,
	906,
	1039,
	1181,
	1162,
	1193,
	820,
	1193,
	1014,
	1162,
	823,
	1193,
	1193,
	1193,
	666,
	1193,
	1181,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1014,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1023,
	1162,
	1193,
	1193,
	1193,
	1193,
	1023,
	1193,
	1193,
	1193,
	2001,
	1966,
	1193,
	1181,
	1193,
	1193,
	1162,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1162,
	1193,
	1193,
	1039,
	1162,
	1162,
	1162,
	1162,
	1162,
	1151,
	1162,
	1162,
	1162,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1039,
	1193,
	1014,
	1193,
	1023,
	1023,
	1193,
	1023,
	1193,
	1193,
	1193,
	1193,
	1162,
	685,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	2001,
	2001,
	2001,
	1851,
	1193,
	1193,
	1193,
	1193,
	1023,
	669,
	1193,
	1193,
	1193,
	1193,
	1023,
	1193,
	1023,
	1193,
	1023,
	1193,
	1193,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	669,
	1193,
	669,
	1193,
	1193,
	1193,
	1193,
	895,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1023,
	1193,
	1014,
	1023,
	1854,
	266,
	823,
	266,
	1023,
	1193,
	1854,
	2001,
	2001,
	671,
	1966,
	1193,
	1193,
	1193,
	2038,
	1193,
	1193,
	1023,
	1193,
	1162,
	1162,
	546,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1193,
	1162,
	1193,
	1162,
	1193,
	1162,
	1193,
	2001,
	2001,
	1966,
	1966,
	1945,
	1784,
	1851,
	2038,
	1193,
	2038,
	2038,
	2038,
	1193,
	2038,
	1193,
	1023,
	1023,
	1193,
	1162,
	669,
	1162,
	823,
	1162,
	1193,
	1193,
	1193,
	1181,
	1193,
	1193,
	1193,
	820,
	820,
	767,
	1014,
	820,
	1193,
	1193,
	1181,
	1181,
	1039,
	1193,
	666,
	666,
	1193,
	1162,
	1193,
	1023,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1162,
	1193,
	1193,
	1162,
	1162,
	1162,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1703,
	1703,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	672,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	-1,
	-1,
	1193,
	-1,
	1193,
	1193,
	1193,
	1193,
	1193,
	906,
	1162,
	823,
	906,
	1193,
	1193,
	1193,
	1193,
	1193,
	948,
	1193,
	1183,
	1151,
	1162,
	1193,
	1041,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1023,
	1162,
	1193,
	1193,
	1193,
	1162,
	1023,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1162,
	1162,
	1193,
	1598,
	1193,
	1023,
	1023,
	1193,
	1162,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1181,
	1181,
	1193,
	1193,
	1193,
	1193,
	1193,
	775,
	1023,
	1193,
	1193,
	1193,
	1181,
	1039,
	1181,
	1039,
	1181,
	1039,
	1181,
	1039,
	1151,
	1014,
	1151,
	1014,
	1151,
	1014,
	1151,
	1014,
	1193,
	1181,
	1039,
	1181,
	1039,
	1181,
	1039,
	1181,
	1039,
	1181,
	1039,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1039,
	1193,
	1193,
	1193,
	1181,
	1193,
	1193,
	669,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1181,
	1193,
	1193,
	292,
	1039,
	1181,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	669,
	669,
	669,
	669,
	669,
	669,
	669,
	669,
	669,
	669,
	669,
	669,
	669,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1039,
	1181,
	1162,
	1193,
	1193,
	1193,
	1014,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	672,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1193,
	1181,
	1151,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1851,
	1945,
	1855,
	1979,
	1847,
	1887,
	1854,
	1966,
	1848,
	1910,
	1737,
	1966,
	1945,
	1945,
	2038,
	1193,
	1193,
	1193,
	1193,
	1039,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1014,
	1162,
	1193,
	767,
	1014,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1039,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1023,
	1193,
	1193,
	1193,
	1162,
	1193,
	1041,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1041,
	1193,
	1193,
	1193,
	1193,
	1189,
	1039,
	1181,
	1193,
	1014,
	968,
	1193,
	1193,
	1189,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1181,
	1193,
	1193,
	1014,
	1181,
	1181,
	1181,
	1193,
	1945,
	1945,
	1193,
	1193,
	1162,
	1162,
	1193,
	1193,
	1162,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1162,
	1193,
	1945,
	1193,
	1193,
	1023,
	1193,
	1023,
	1193,
	1193,
	1014,
	1193,
	1193,
	1014,
	390,
	350,
	350,
	1193,
	1193,
	1193,
	1193,
	1039,
	1162,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	949,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1041,
	684,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	950,
	950,
	1183,
	1193,
	950,
	1193,
	1193,
	1193,
	1183,
	1183,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	664,
	669,
	1023,
	1023,
	1023,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1162,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1193,
	1041,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1014,
	1014,
	1151,
	1014,
	1193,
	1151,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1193,
	1162,
	1193,
	1039,
	1023,
	672,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1162,
	1193,
	1039,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1162,
	1193,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1162,
	1193,
	1193,
	1014,
	1193,
	1181,
	1162,
	1193,
	1162,
	1193,
	1023,
	820,
	1193,
	547,
	906,
	636,
	636,
	636,
	1193,
	1193,
	990,
	411,
	1193,
	1193,
	1193,
	1193,
	1193,
	1174,
	1036,
	1193,
	2038,
	1193,
	1193,
	1193,
	1193,
	419,
	419,
	419,
	1162,
	1023,
	1193,
	1193,
	1193,
	1193,
	1193,
	1910,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000246, { 0, 1 } },
	{ 0x06000247, { 1, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)3, 6578 },
	{ (Il2CppRGCTXDataType)2, 1480 },
	{ (Il2CppRGCTXDataType)3, 5397 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1200,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
