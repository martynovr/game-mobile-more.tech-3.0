﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// ReadOnlyInspector
struct ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// ReadOnlyInspector
struct ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void ReadOnlyInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469 (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_verboseDebug(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x2F\x44\x69\x73\x61\x62\x6C\x65\x20\x65\x78\x74\x65\x6E\x64\x65\x64\x20\x64\x65\x62\x75\x67\x67\x69\x6E\x67\x20\x6F\x75\x74\x70\x75\x74\x2E"), NULL);
	}
}
static void Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_catalog(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_inventory(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_Inventory_ShowNewItems_m730E58CD1F058A1050B9702ADAA94EBBEE955155(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_0_0_0_var), NULL);
	}
}
static void Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_Inventory_delayedActualize_mD1E356C8C8C2EE201B75C5724CA238FDD552BD2D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_0_0_0_var), NULL);
	}
}
static void C_ItemDetail_t93A956A946A2E44CFC84E88F42DFD6B4FA53B63D_CustomAttributesCacheGenerator_selectedElement(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ItemDetail_t93A956A946A2E44CFC84E88F42DFD6B4FA53B63D_CustomAttributesCacheGenerator_uiItem(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ItemDetail_t93A956A946A2E44CFC84E88F42DFD6B4FA53B63D_CustomAttributesCacheGenerator_spawnedDetailViewPanel(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_uiItem(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_newItems(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_spawnedNewItemPanel(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_lockedForShowing(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_itemKey(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_amount(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_loadedFromCatalog(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11__ctor_m4F93C52A740A6C441BE1B043BEDD162235DC2E30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_IDisposable_Dispose_m6A49C06D643DC646E3C55A44E5EE1AF4F627CB77(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6967EF8B284149E3D86878CF77A0D80B53DD0B64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_Reset_mFA50BB0229FBC728D2870928863FC9ABAC51774B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_get_Current_m76B1459E38683876F202AA8467A1C975E8068998(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18__ctor_mDF4DDB44936F798F1832FF0E64A6FA3A9A668481(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_IDisposable_Dispose_mA51C00F967C3090D966C0D77B56F0B40453EE909(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m271AEFDD2D8343B07E406091F9C94AFB84B0F4FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_Reset_mDEAD3BE3EAC90C8504A165A30341EC72AAF597BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_get_Current_m7B2A880A7C4382F85173E90E349433A1DBB661D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x49\x74\x65\x6D"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x49\x74\x65\x6D"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 51LL, NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_itemName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x74\x68\x61\x74\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E\x20\x50\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x64\x69\x73\x74\x69\x6E\x67\x75\x69\x73\x68\x61\x62\x6C\x65\x20\x6E\x61\x6D\x65\x73\x20\x66\x6F\x72\x20\x69\x74\x65\x6D\x73"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_description(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x2E\x20\x49\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_consumeText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x61\x6E\x20\x73\x74\x72\x69\x6E\x67\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x75\x6D\x65\x20\x6F\x66\x20\x61\x6E\x20\x69\x74\x65\x6D\x2E\x20\x45\x2E\x67\x2E\x20\x27\x43\x6F\x6E\x73\x75\x6D\x65\x27\x2C\x20\x27\x55\x73\x65\x27\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x70\x72\x6F\x76\x69\x64\x65\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x77\x68\x69\x63\x68\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_maxItems(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x61\x6C\x20\x63\x6F\x75\x6E\x74\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_visible(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x61\x62\x6C\x65\x20\x28\x73\x65\x74\x20\x74\x6F\x20\x66\x61\x6C\x73\x65\x29\x20\x76\x69\x73\x69\x62\x6C\x65\x20\x69\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x73\x68\x61\x6C\x6C\x20\x62\x65\x20\x68\x69\x64\x64\x65\x6E\x20\x69\x6E\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_consumeType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x69\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x63\x61\x6E\x20\x62\x65\x20\x63\x6F\x6E\x73\x75\x6D\x65\x64\x20\x6F\x72\x20\x69\x73\x20\x6E\x6F\x6E\x2D\x63\x6F\x6E\x73\x75\x6D\x61\x62\x6C\x65\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_category(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x74\x65\x6D\x20\x63\x61\x6E\x20\x62\x65\x20\x61\x73\x73\x69\x67\x6E\x65\x64\x20\x74\x6F\x20\x63\x61\x74\x68\x65\x67\x6F\x72\x69\x65\x73\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x73\x65\x20\x63\x61\x74\x65\x67\x6F\x72\x69\x65\x73\x20\x79\x6F\x75\x72\x73\x65\x6C\x66\x20\x69\x6E\x20\x74\x68\x65\x20\x27\x49\x74\x65\x6D\x44\x65\x66\x69\x6E\x69\x74\x69\x6F\x6E\x73\x2E\x63\x73\x27\x20\x73\x63\x72\x69\x70\x74\x2E\x20\x42\x79\x20\x63\x61\x74\x68\x65\x67\x6F\x72\x69\x6E\x67\x2C\x20\x65\x2E\x67\x2E\x20\x6F\x6E\x6C\x79\x20\x63\x65\x72\x74\x61\x69\x6E\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x69\x6E\x20\x63\x65\x72\x74\x61\x69\x6E\x20\x75\x73\x65\x72\x20\x69\x6E\x74\x65\x72\x66\x61\x63\x65\x73\x20\x61\x72\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_newItemOverridePanel(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x61\x6E\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x70\x61\x6E\x65\x6C\x20\x66\x6F\x72\x20\x6E\x65\x77\x20\x69\x74\x65\x6D\x73\x2E\x20\x49\x66\x20\x6E\x6F\x6E\x65\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2C\x20\x74\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x70\x61\x6E\x65\x6C\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x73\x63\x72\x69\x70\x74\x20\x69\x73\x20\x75\x73\x65\x64\x2E"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_detailItemOverridePanel(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x61\x6E\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x70\x61\x6E\x65\x6C\x20\x66\x6F\x72\x20\x64\x65\x74\x61\x69\x6C\x20\x76\x69\x65\x77\x20\x6F\x66\x20\x69\x74\x65\x6D\x73\x2E\x20\x49\x66\x20\x6E\x6F\x6E\x65\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2C\x20\x74\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x70\x61\x6E\x65\x6C\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x73\x63\x72\x69\x70\x74\x20\x69\x73\x20\x75\x73\x65\x64\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_itemAmount(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_amountText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_itemImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x73\x70\x72\x69\x74\x65\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_itemText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x6E\x61\x6D\x65\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_descriptionText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_consumeButton(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x55\x49\x20\x62\x75\x74\x74\x6F\x6E\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x65\x6E\x61\x62\x6C\x65\x20\x63\x6F\x6E\x73\x75\x6D\x70\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x2E\x20\x54\x68\x65\x20\x62\x75\x74\x74\x6F\x6E\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x27\x69\x6E\x74\x65\x72\x61\x63\x74\x61\x62\x6C\x65\x27\x20\x69\x73\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x64\x2E\x20\x54\x68\x65\x20\x63\x6F\x6E\x73\x75\x6D\x65\x2D\x65\x76\x65\x6E\x74\x20\x69\x73\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x61\x64\x64\x65\x64\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x62\x75\x74\x74\x6F\x6E\x20\x62\x79\x20\x74\x68\x69\x73\x20\x73\x63\x72\x69\x70\x74\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_consumeText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x64\x65\x66\x69\x6E\x65\x20\x61\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x66\x6F\x72\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x63\x6F\x6E\x73\x75\x6D\x65\x20\x74\x65\x78\x74\x73\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_iconGraphics(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x67\x72\x61\x70\x68\x69\x63\x61\x6C\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x6C\x69\x6B\x65\x20\x74\x65\x78\x74\x2C\x20\x69\x6D\x61\x67\x65\x73\x20\x65\x74\x63\x2E\x20\x66\x6F\x72\x20\x63\x6F\x6C\x6C\x6F\x72\x20\x61\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x69\x74\x6D\x65\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_primaryGraphics(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x67\x72\x61\x70\x68\x69\x63\x61\x6C\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x6C\x69\x6B\x65\x20\x74\x65\x78\x74\x2C\x20\x69\x6D\x61\x67\x65\x73\x20\x65\x74\x63\x2E\x20\x66\x6F\x72\x20\x63\x6F\x6C\x6C\x6F\x72\x20\x61\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x69\x74\x6D\x65\x2E"), NULL);
	}
}
static void Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_secondaryGraphics(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x67\x72\x61\x70\x68\x69\x63\x61\x6C\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x6C\x69\x6B\x65\x20\x74\x65\x78\x74\x2C\x20\x69\x6D\x61\x67\x65\x73\x20\x65\x74\x63\x2E\x20\x66\x6F\x72\x20\x63\x6F\x6C\x6C\x6F\x72\x20\x61\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x69\x74\x6D\x65\x2E"), NULL);
	}
}
static void Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_UITemplate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x6F\x66\x20\x61\x6E\x20\x69\x74\x65\x6D\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_filterType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x68\x6F\x77\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x73\x68\x61\x6C\x6C\x20\x62\x65\x20\x66\x69\x6C\x74\x65\x72\x65\x64\x2E"), NULL);
	}
}
static void Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_categoryFilter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x66\x6F\x72\x20\x77\x68\x69\x63\x68\x20\x63\x61\x74\x65\x67\x6F\x72\x69\x65\x73\x20\x74\x68\x65\x20\x66\x69\x6C\x74\x65\x72\x20\x73\x68\x61\x6C\x6C\x20\x61\x70\x70\x6C\x79\x2E\x20\x54\x68\x69\x73\x20\x6C\x69\x73\x74\x20\x68\x61\x73\x20\x6E\x6F\x20\x65\x66\x66\x65\x63\x74\x20\x69\x66\x20\x27\x53\x68\x6F\x77\x20\x41\x6C\x6C\x27\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
}
static void Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_listElements(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_Inventory_UIItemList_delayedInit_m3EAD041EF763A6EA538C188CA2DD32339660B976(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_0_0_0_var), NULL);
	}
}
static void U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10__ctor_m5157CE64BFCC04D43B1A35144363AF0F3182CC60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_IDisposable_Dispose_mFA7F9DEEF57B724FD1133C7AA67D24BBE2D5B8D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45AD32E3413F826B1340FE94180B993E9A7C9609(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_Reset_mF78B2D14D8B84D2B048230AB5BA8BC2256EAB1F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_get_Current_m723D3863BA16237C6A75150DA0E6000F8445C5C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x51\x75\x65\x73\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x51\x75\x65\x73\x74\x20\x44\x65\x66\x69\x6E\x69\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 51LL, NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_questRepeatType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x72\x65\x70\x65\x74\x69\x74\x69\x6F\x6E\x20\x74\x79\x70\x65\x20\x6F\x66\x20\x71\x75\x65\x73\x74\x73\x2E\x20\x57\x69\x74\x68\x20\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x69\x6F\x6E\x20\x27\x72\x65\x70\x65\x61\x74\x61\x62\x6C\x65\x27\x20\x74\x68\x65\x20\x71\x75\x65\x73\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x6D\x6F\x72\x65\x20\x74\x68\x61\x6E\x20\x6F\x6E\x63\x65\x2E"), NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_activatabilityConditions(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x66\x6F\x72\x20\x65\x6E\x61\x62\x6C\x69\x6E\x67\x20\x61\x20\x71\x75\x65\x73\x74\x2E\x20\x54\x68\x65\x73\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x64\x20\x77\x68\x65\x6E\x20\x6E\x65\x77\x20\x71\x75\x65\x73\x74\x73\x20\x61\x72\x65\x20\x61\x64\x64\x65\x64\x2E"), NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_questTitle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x71\x75\x65\x73\x74\x20\x74\x68\x61\x74\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E\x20\x50\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x64\x69\x73\x74\x69\x6E\x67\x75\x69\x73\x68\x61\x62\x6C\x65\x20\x6E\x61\x6D\x65\x73\x20\x66\x6F\x72\x20\x71\x75\x65\x73\x74\x73"), NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_description(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x71\x75\x65\x73\x74\x2E\x20\x49\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_rewardText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x61\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x74\x65\x78\x74\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x72\x65\x77\x61\x72\x64\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x69\x6E\x20\x77\x68\x69\x63\x68\x20\x77\x61\x79\x20\x68\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x6E\x65\x66\x69\x74\x20\x66\x72\x6F\x6D\x20\x66\x75\x6C\x6C\x66\x69\x6C\x6C\x69\x6E\x67\x20\x74\x68\x69\x73\x20\x71\x75\x65\x73\x74\x2E"), NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_rewardImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x72\x65\x77\x61\x72\x64\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x69\x6E\x20\x77\x68\x69\x63\x68\x20\x77\x61\x79\x20\x68\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x6E\x65\x66\x69\x74\x20\x66\x72\x6F\x6D\x20\x66\x75\x6C\x6C\x66\x69\x6C\x6C\x69\x6E\x67\x20\x74\x68\x69\x73\x20\x71\x75\x65\x73\x74\x2E"), NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x70\x72\x6F\x76\x69\x64\x65\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x71\x75\x65\x73\x74\x20\x77\x68\x69\x63\x68\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_fullfilmentConditions(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x66\x6F\x72\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x66\x75\x6C\x6C\x66\x69\x6C\x6C\x20\x74\x68\x65\x20\x71\x75\x65\x73\x74\x2E\x20\x54\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x69\x73\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x64\x20\x69\x66\x20\x61\x6E\x20\x63\x61\x72\x64\x20\x69\x73\x20\x64\x65\x73\x70\x61\x77\x6E\x65\x64\x2E\x20\x49\x74\x20\x69\x73\x20\x6F\x6E\x6C\x79\x20\x63\x68\x65\x63\x6B\x65\x64\x2C\x20\x69\x66\x20\x61\x74\x20\x6C\x65\x61\x73\x74\x20\x6F\x6E\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x69\x73\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x65\x64\x2E\x20\x49\x74\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x62\x65\x20\x66\x75\x6C\x6C\x66\x69\x6C\x6C\x65\x64\x20\x62\x79\x20\x61\x20\x74\x72\x69\x67\x67\x65\x72\x2E"), NULL);
	}
}
static void Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator__questState(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_questTitle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x74\x69\x74\x6C\x65\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_questImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x73\x70\x72\x69\x74\x65\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_descriptionText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_fullfillmentToggle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x6F\x67\x67\x6C\x65\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x77\x68\x69\x63\x68\x20\x73\x68\x6F\x77\x73\x20\x74\x68\x65\x20\x66\x75\x6C\x6C\x66\x69\x6C\x6C\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_rewardText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x72\x65\x77\x61\x72\x64\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_rewardImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6D\x61\x67\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x72\x65\x77\x61\x72\x64\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_UI_OpenTemplate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x6F\x66\x20\x61\x20\x71\x75\x65\x73\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_UI_ActiveTemplate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x6F\x66\x20\x61\x20\x71\x75\x65\x73\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_UI_CompletedTemplate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x6F\x66\x20\x61\x20\x71\x75\x65\x73\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_questDisplayFilters(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x66\x69\x6C\x74\x65\x72\x73\x20\x66\x6F\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x69\x6E\x67\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x2E\x20\x46\x69\x6C\x74\x65\x72\x73\x20\x61\x72\x65\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x6F\x6E\x65\x20\x62\x79\x20\x6F\x6E\x65\x20\x61\x6E\x64\x20\x61\x6C\x6C\x20\x72\x65\x73\x75\x6C\x74\x73\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x66\x69\x6C\x74\x65\x72\x20\x61\x72\x65\x20\x61\x64\x64\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x6C\x69\x73\x74\x2E"), NULL);
	}
}
static void Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_listElements(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_maxNrOfActiveQuests(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x61\x6C\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x61\x63\x74\x69\x76\x65\x20\x71\x75\x65\x73\x74\x73\x2E"), NULL);
	}
}
static void Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_autoRefillActiveQuests(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B\x20\x69\x66\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x61\x63\x74\x69\x76\x65\x20\x71\x75\x65\x73\x74\x73\x20\x73\x68\x61\x6C\x6C\x20\x62\x65\x20\x61\x75\x74\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x72\x65\x66\x69\x6C\x6C\x65\x64\x20\x74\x6F\x20\x6D\x61\x78\x69\x6D\x75\x6D\x2E\x20\x54\x68\x65\x20\x69\x6E\x74\x65\x72\x6E\x61\x6C\x20\x63\x68\x65\x63\x6B\x20\x6F\x63\x63\x75\x72\x65\x73\x20\x69\x66\x20\x73\x6F\x6D\x65\x74\x68\x69\x6E\x67\x20\x63\x68\x61\x6E\x67\x65\x73\x2E"), NULL);
	}
}
static void Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_catalog(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_questBook(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_Quests_delayedActualize_m205899FC8FF90F4409663E809FCD691833237E74(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_0_0_0_var), NULL);
	}
}
static void C_QuestFullfillPopup_t48F7A228CA366616CE659042114B52272982EF1E_CustomAttributesCacheGenerator_selectedElement(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_QuestFullfillPopup_t48F7A228CA366616CE659042114B52272982EF1E_CustomAttributesCacheGenerator_uiItem(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_QuestFullfillPopup_t48F7A228CA366616CE659042114B52272982EF1E_CustomAttributesCacheGenerator_spawnedQuestPopupPanel(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_questKey(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_quest(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_activeState(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_loadedFromCatalog(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_nrOfQuests(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_nrOfActiveQuests(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_nrOfFinishedQuests(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_quests(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_activeQuests(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32__ctor_mAEFBCD23F8BEF17E5BD56CC457FE495405C75A4F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_IDisposable_Dispose_mD2D3C71CF0DA26B65705FD1234759FDCC6CEA2A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2D37E0C9702B8712E096D957CB813D1339A5FCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_Reset_m57DF855CC498C59B7FB9D904D8CE6B56226F810A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_get_Current_m0FC81D0D966BCA6016E759F0CD7827D2316AABF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x48\x69\x73\x74\x6F\x72\x79\x45\x76\x65\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x48\x69\x73\x74\x6F\x72\x79\x20\x45\x76\x65\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 51LL, NULL);
	}
}
static void HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator_title(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x69\x74\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x68\x69\x73\x74\x6F\x72\x79\x20\x65\x76\x65\x6E\x74\x20\x74\x68\x61\x74\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E\x20\x50\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x64\x69\x73\x74\x69\x6E\x67\x75\x69\x73\x68\x61\x62\x6C\x65\x20\x6E\x61\x6D\x65\x73\x2E"), NULL);
	}
}
static void HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator_description(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x68\x69\x73\x74\x6F\x72\x79\x20\x65\x76\x65\x6E\x74\x2E\x20\x49\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x70\x72\x6F\x76\x69\x64\x65\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x68\x69\x73\x74\x6F\x72\x79\x20\x65\x76\x65\x6E\x74\x20\x77\x68\x69\x63\x68\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_verboseDebug(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x2F\x64\x69\x73\x61\x62\x6C\x65\x20\x65\x78\x74\x65\x6E\x64\x65\x64\x20\x64\x65\x62\x75\x67\x67\x69\x6E\x67\x20\x6F\x75\x74\x70\x75\x74\x2E"), NULL);
	}
}
static void Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_maxYear(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_catalog(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_history(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_displayedHistory(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_Timeline_delayedActualize_mBDB9533D5CF13304BE38412E707BC60F6677060C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_0_0_0_var), NULL);
	}
}
static void C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_year(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_addText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x20\x63\x61\x6E\x27\x74\x20\x62\x65\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x68\x65\x72\x65\x2C\x20\x62\x65\x63\x61\x75\x73\x65\x20\x74\x65\x78\x74\x2D\x72\x65\x70\x6C\x61\x63\x65\x6D\x65\x6E\x74\x20\x69\x73\x20\x61\x6C\x72\x65\x61\x64\x79\x20\x66\x69\x6E\x69\x73\x68\x65\x64\x2E\x20"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_timelineKey(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_historyEvent(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_loadedFromCatalog(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_History_t2E8D63E06C17AA8AAA0169F9DCE6FECE8AB84144_CustomAttributesCacheGenerator_startYearOffset(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14__ctor_m163DE02FB61436D359EAEFF208D0BA13531A1BA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_IDisposable_Dispose_mB3F3A0C344D293C1A69F98D95489647B841E4DDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8130E9BD1C537FF6EF30645E6EB76449A451D9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_Reset_m7B7A2424DFC67895DA2367595C79B7C13DD21EA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_get_Current_mF87B6C1ACB21AD889437D3D4A15742C1D9E15DCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_UITemplate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x6F\x66\x20\x61\x6E\x20\x68\x69\x73\x74\x6F\x72\x79\x20\x65\x76\x65\x6E\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_historyScrollbar(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x6C\x6F\x6E\x67\x20\x74\x69\x6D\x65\x6C\x69\x6E\x65\x73\x20\x61\x20\x73\x63\x72\x6F\x6C\x6C\x62\x61\x72\x20\x69\x73\x20\x6E\x65\x65\x64\x65\x64\x2E\x20\x54\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x61\x72\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x6C\x74\x65\x72\x65\x64\x20\x62\x79\x20\x74\x68\x69\x73\x20\x73\x63\x72\x69\x70\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x61\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x65\x66\x66\x65\x6B\x74\x20\x69\x66\x20\x73\x6F\x6D\x65\x74\x68\x69\x6E\x67\x20\x69\x73\x20\x61\x64\x64\x65\x64\x20\x6F\x72\x20\x69\x74\x20\x67\x65\x74\x73\x20\x61\x6E\x69\x6D\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_yearsText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x70\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x74\x65\x78\x74\x66\x69\x65\x6C\x64\x20\x77\x68\x65\x72\x65\x20\x74\x68\x65\x20\x61\x63\x75\x74\x61\x6C\x20\x79\x65\x61\x72\x20\x69\x73\x20\x73\x68\x6F\x77\x6E\x2E\x20\x4F\x6E\x6C\x79\x20\x77\x6F\x72\x6B\x73\x20\x77\x69\x74\x68\x20\x66\x69\x6C\x6C\x65\x72\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x6C\x69\x6B\x65\x20\x69\x6E\x20\x74\x68\x65\x20\x65\x78\x61\x6D\x70\x6C\x65\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_autoScrollbarMove(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x2F\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x20\x73\x63\x72\x6F\x6C\x6C\x62\x61\x72\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x77\x68\x65\x6E\x20\x6E\x65\x77\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x61\x64\x64\x65\x64\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_scrollbarSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x70\x65\x65\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x6F\x6C\x6C\x62\x61\x72\x20\x77\x68\x65\x6E\x20\x6E\x65\x77\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x61\x64\x64\x65\x64\x20\x28\x61\x75\x74\x6F\x53\x63\x72\x6F\x6C\x6C\x62\x61\x72\x4D\x6F\x76\x65\x20\x68\x61\x73\x20\x74\x6F\x20\x62\x65\x20\x65\x6E\x61\x62\x6C\x65\x64\x29\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_scrollbarAnimationDelay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x74\x68\x65\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x20\x65\x66\x66\x65\x63\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x6C\x69\x6E\x65\x3A\x20\x73\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x20\x69\x74\x20\x73\x74\x61\x79\x73\x20\x6F\x6E\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x62\x65\x76\x6F\x72\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x74\x68\x72\x6F\x75\x67\x68\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x6C\x69\x6E\x65\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_scrollbarAnimationSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x74\x68\x65\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x20\x65\x66\x66\x65\x63\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x6C\x69\x6E\x65\x3A\x20\x73\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x73\x70\x65\x65\x64\x20\x66\x6F\x72\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x74\x68\x72\x6F\x75\x67\x68\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x6C\x69\x6E\x65\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_displayFillerEvents(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x69\x66\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x66\x69\x6C\x6C\x65\x72\x73\x20\x66\x6F\x72\x20\x79\x65\x61\x72\x73\x20\x77\x69\x74\x68\x6F\x75\x74\x20\x61\x20\x68\x69\x73\x74\x6F\x72\x69\x63\x61\x6C\x20\x65\x76\x65\x6E\x74\x20\x61\x72\x65\x20\x75\x73\x65\x64\x2E"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_nrOfPostFillers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x64\x65\x66\x69\x6E\x65\x20\x68\x6F\x77\x20\x6D\x61\x6E\x79\x20\x70\x6F\x73\x74\x20\x66\x69\x6C\x6C\x65\x72\x73\x20\x66\x6F\x72\x20\x66\x6F\x72\x6D\x61\x74\x74\x69\x6E\x67\x20\x61\x72\x65\x20\x69\x6E\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x6C\x69\x6E\x65\x2E\x20\x54\x68\x65\x20\x69\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x6E\x65\x65\x64\x65\x64\x20\x74\x6F\x20\x63\x6F\x72\x72\x65\x63\x74\x6C\x79\x20\x73\x70\x61\x77\x6E\x20\x6E\x65\x77\x20\x65\x76\x65\x6E\x74\x73\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x66\x69\x6C\x6C\x65\x72\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x2E\x73"), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_listElements(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_Timeline_UIEventList_freezableUpdate_m598AE33C3613E8016693606FDCCE8D6081B567AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_0_0_0_var), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_Timeline_UIEventList_moveScrollbar_m666D995BB93273C5789809F24C0C00527C33AA68(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_0_0_0_var), NULL);
	}
}
static void Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_Timeline_UIEventList_animateScrollbar_mE9711431AE5910959C2C37E9D643486F77139250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_0_0_0_var), NULL);
	}
}
static void U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23__ctor_m4783DD803B0781C6D55CF050D10870FA162CD368(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_IDisposable_Dispose_m4BE71898145F5E53A99ADD950576BE01407E1F0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44F416F81FFCE8065052F150502D1C7192D336DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_Reset_mBF4FC0D2B0597223CAD3B9358590491BD5C64C40(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_get_Current_mFB3C753DC3C7268E2671B6BDB671B18618043502(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24__ctor_m2FE1146F98C50EA6320D3850669B72148B6B5EEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_IDisposable_Dispose_m192BB59F93A9FBE43398A6FE37C8019D0A998C3A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m954B89FE0C20DA41406BF08F8C8D5BBA6AEF0334(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_Reset_mF290FBF737DD65386088D34E1AD99CFE4E74D4B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_get_Current_m72CD44092B59FF2850BB3B5A683609887BEC9A41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26__ctor_m1B4B0175B724E1193EDCDA6F797C3064FF59E6BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_IDisposable_Dispose_m9B030CEA5F95001C0406F30685AC322AE6B40170(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21653C698A208F8970AD906340AFA09AC8EF1F11(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_Reset_mE02D079FC130D8B51F2AD3D7FF1EF878B7E35B2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_get_Current_m5F6D100097FC1BF15D464E6AEA55105A019DBBD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator__timelineData(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_eventYear(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x79\x65\x61\x72\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_eventImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x73\x70\x72\x69\x74\x65\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_eventTitle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x68\x69\x73\x74\x6F\x72\x79\x20\x65\x76\x65\x6E\x74\x20\x74\x69\x74\x6C\x65\x2E"), NULL);
	}
}
static void Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_descriptionText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x68\x69\x73\x74\x6F\x72\x79\x20\x65\x76\x65\x6E\x74\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_addText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x74\x65\x78\x74\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x61\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x74\x65\x78\x74\x73\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x2E\x20\x54\x68\x65\x79\x20\x61\x72\x65\x20\x74\x72\x61\x6E\x73\x6C\x61\x74\x65\x64\x20\x6F\x6E\x6C\x79\x20\x6F\x6E\x20\x67\x65\x6E\x65\x72\x61\x74\x69\x6F\x6E\x20\x61\x6E\x64\x20\x63\x61\x6E\x20\x6E\x6F\x74\x20\x62\x65\x20\x61\x6C\x74\x65\x72\x65\x64\x20\x6C\x61\x74\x65\x72\x21"), NULL);
	}
}
static void Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_combinedDescriptionText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x62\x69\x6E\x65\x64\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x66\x6F\x72\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x74\x65\x78\x74\x20\x61\x6E\x64\x20\x61\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x74\x65\x78\x74\x2E\x20\x54\x65\x78\x74\x73\x20\x61\x72\x65\x20\x73\x65\x70\x61\x72\x61\x74\x65\x64\x20\x62\x79\x20\x61\x6E\x20\x73\x70\x61\x63\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x2E"), NULL);
	}
}
static void AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_achievementAnimator(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x61\x6E\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x20\x77\x61\x73\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x69\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x68\x6F\x77\x6E\x20\x77\x69\x74\x68\x20\x61\x6E\x20\x61\x6E\x69\x6D\x61\x74\x6F\x72\x2E\x20\x4C\x69\x6E\x6B\x20\x74\x68\x69\x73\x20\x61\x6E\x69\x6D\x61\x74\x6F\x72\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_triggerOnAchievement(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x61\x6E\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x20\x77\x61\x73\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x69\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x68\x6F\x77\x6E\x20\x77\x69\x74\x68\x20\x61\x6E\x20\x61\x6E\x69\x6D\x61\x74\x6F\x72\x2E\x20\x53\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x74\x72\x69\x67\x67\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x6E\x69\x6D\x61\x74\x6F\x72\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_anim_titleText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x61\x6E\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x20\x77\x61\x73\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x69\x74\x20\x63\x61\x6E\x20\x75\x70\x64\x61\x74\x65\x20\x69\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x20\x61\x62\x6F\x75\x74\x20\x74\x68\x65\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x2E\x20\x53\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x70\x6C\x61\x63\x65\x68\x6F\x6C\x64\x65\x72\x20\x74\x69\x74\x6C\x65\x20\x74\x65\x78\x74\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_anim_descriptionText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x61\x6E\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x20\x77\x61\x73\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x69\x74\x20\x63\x61\x6E\x20\x75\x70\x64\x61\x74\x65\x20\x69\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x20\x61\x62\x6F\x75\x74\x20\x74\x68\x65\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x2E\x20\x53\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x70\x6C\x61\x63\x65\x68\x6F\x6C\x64\x65\x72\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x74\x65\x78\x74\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_anim_achievementImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x61\x6E\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x20\x77\x61\x73\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x69\x74\x20\x63\x61\x6E\x20\x75\x70\x64\x61\x74\x65\x20\x69\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x20\x61\x62\x6F\x75\x74\x20\x74\x68\x65\x20\x61\x63\x68\x69\x76\x65\x6D\x65\x6E\x74\x2E\x20\x53\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x70\x6C\x61\x63\x65\x68\x6F\x6C\x64\x65\x72\x20\x69\x6D\x61\x67\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_achievementProgressText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x70\x72\x6F\x67\x72\x65\x73\x73\x20\x28\x27\x33\x2F\x32\x30\x27\x29\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x2E\x20\x53\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x70\x6C\x61\x63\x65\x68\x6F\x6C\x64\x65\x72\x20\x74\x65\x78\x74\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void achievementConfig_tB0059C19EA046A74E2D2D1267DC02422921AB5FB_CustomAttributesCacheGenerator_typ(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x63\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x20\x74\x68\x65\x20\x74\x79\x70\x65\x20\x6F\x66\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x68\x65\x72\x65\x2E\x20\x46\x6F\x72\x20\x65\x78\x70\x61\x6E\x64\x69\x6E\x67\x20\x74\x68\x65\x20\x6C\x69\x73\x74\x20\x6D\x6F\x64\x69\x66\x79\x20\x74\x68\x65\x20\x73\x74\x72\x75\x63\x74\x75\x72\x65\x20\x27\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x54\x79\x70\x27\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x69\x73\x20\x73\x63\x72\x69\x70\x74\x2E"), NULL);
	}
}
static void achievementConfig_tB0059C19EA046A74E2D2D1267DC02422921AB5FB_CustomAttributesCacheGenerator_achievementCnt(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x2C\x20\x68\x6F\x77\x20\x6F\x66\x74\x65\x6E\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x77\x61\x73\x20\x6D\x65\x74\x2E\x20\x54\x68\x65\x20\x61\x6E\x69\x6D\x61\x74\x6F\x72\x20\x66\x6F\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x69\x73\x20\x6F\x6E\x6C\x79\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_achievementTarget(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x53\x74\x61\x67\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x69\x73\x74\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x69\x66\x20\x74\x68\x69\x73\x20\x63\x6F\x75\x6E\x74\x65\x72\x20\x69\x73\x20\x72\x65\x61\x63\x68\x65\x64"), NULL);
	}
}
static void achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_achievementGameobject(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x77\x61\x73\x20\x6D\x65\x74\x2C\x20\x61\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x61\x63\x74\x69\x76\x61\x74\x65\x64\x2E\x20\x45\x2E\x67\x2E\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x69\x6E\x20\x61\x20\x67\x61\x6C\x6C\x65\x72\x79\x2E"), NULL);
	}
}
static void achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_title(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x69\x74\x6C\x65\x20\x74\x65\x78\x74\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x73\x2C\x20\x74\x68\x69\x73\x20\x74\x65\x78\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x75\x73\x65\x64\x20\x61\x73\x20\x74\x65\x72\x6D\x2E"), NULL);
	}
}
static void achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_description(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x74\x65\x78\x74\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x73\x2C\x20\x74\x68\x69\x73\x20\x74\x65\x78\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x75\x73\x65\x64\x20\x61\x73\x20\x74\x65\x72\x6D\x2E"), NULL);
	}
}
static void achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_sprite(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x70\x72\x69\x74\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x73\x2C\x20\x74\x68\x69\x73\x20\x74\x65\x78\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x75\x73\x65\x64\x20\x61\x73\x20\x74\x65\x72\x6D\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_allCards(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x65\x72\x74\x20\x61\x6C\x6C\x20\x63\x61\x72\x64\x73\x20\x68\x65\x72\x65\x2E\x20\x54\x68\x65\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x63\x61\x72\x64\x73\x20\x61\x72\x65\x20\x6E\x65\x65\x64\x65\x64\x20\x74\x6F\x20\x6C\x6F\x61\x64\x2F\x73\x61\x76\x65\x20\x74\x68\x65\x20\x63\x61\x72\x64\x73\x20\x61\x6E\x64\x20\x74\x6F\x20\x64\x65\x63\x69\x64\x65\x2C\x20\x77\x68\x61\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x72\x61\x77\x6E\x20\x6E\x65\x78\x74\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_cardDrawCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x63\x6B\x69\x6E\x67\x20\x6F\x66\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x64\x72\x61\x77\x73\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x63\x61\x72\x64\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E\x20\x43\x61\x74\x65\x67\x6F\x72\x69\x7A\x65\x64\x20\x6C\x69\x6B\x65\x20\x74\x68\x65\x20\x27\x61\x6C\x6C\x43\x61\x72\x64\x73\x27\x20\x67\x72\x6F\x75\x70\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_cardBlockCount(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x63\x6B\x69\x6E\x67\x20\x74\x68\x65\x20\x72\x65\x64\x72\x61\x77\x73\x20\x62\x6C\x6F\x63\x6B\x73\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x63\x61\x72\x64\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E\x20\x43\x61\x74\x65\x67\x6F\x72\x69\x7A\x65\x64\x20\x6C\x69\x6B\x65\x20\x74\x68\x65\x20\x27\x61\x6C\x6C\x43\x61\x72\x64\x73\x27\x20\x67\x72\x6F\x75\x70\x2E"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_availableCards(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x6C\x20\x63\x61\x72\x64\x73\x2C\x20\x77\x68\x69\x63\x68\x20\x6D\x65\x65\x74\x20\x74\x68\x65\x20\x72\x65\x71\x75\x69\x72\x65\x6D\x65\x6E\x74\x73\x2E"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_followUpCard(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x72\x64\x2C\x20\x77\x68\x69\x63\x68\x20\x69\x73\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x62\x79\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x20\x63\x61\x72\x64\x20\x61\x6E\x64\x20\x66\x6F\x6C\x6C\x6F\x77\x73\x20\x6E\x65\x78\x74\x2E"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_highPriorityCards(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x72\x64\x73\x2C\x20\x77\x68\x69\x63\x68\x20\x61\x72\x65\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x61\x73\x20\x68\x69\x67\x68\x20\x70\x72\x69\x6F\x72\x69\x74\x79\x2E\x20\x54\x68\x65\x79\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x72\x61\x77\x6E\x20\x62\x65\x66\x6F\x72\x65\x20\x74\x68\x65\x20\x75\x73\x75\x61\x6C\x20\x53\x74\x61\x63\x6B\x20\x66\x72\x6F\x6D\x20\x27\x61\x76\x61\x69\x6C\x61\x62\x6C\x65\x20\x43\x61\x72\x64\x73\x27\x2C\x20\x62\x75\x74\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x27\x66\x6F\x6C\x6C\x6F\x77\x20\x55\x70\x27\x20\x63\x61\x72\x64\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_spawnedCard(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x73\x70\x61\x77\x6E\x65\x64\x20\x63\x61\x72\x64\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_fallBackCard(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x61\x72\x64\x20\x70\x72\x65\x66\x61\x62\x20\x77\x69\x6C\x6C\x20\x73\x70\x61\x77\x6E\x2C\x20\x69\x66\x20\x6E\x6F\x74\x68\x69\x6E\x67\x20\x65\x6C\x73\x65\x20\x69\x73\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_swipe(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x6E\x6B\x20\x74\x68\x65\x20\x75\x73\x65\x64\x20\x73\x77\x69\x70\x65\x20\x73\x63\x72\x69\x70\x74\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_moveBackSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x6D\x6F\x76\x65\x73\x20\x62\x61\x63\x6B\x20\x74\x6F\x20\x74\x68\x65\x20\x6D\x69\x64\x64\x6C\x65\x3A\x20\x68\x6F\x77\x20\x66\x61\x73\x74\x20\x73\x68\x61\x6C\x6C\x20\x69\x74\x20\x6D\x6F\x76\x65\x3F"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_moveOutSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x6D\x6F\x76\x65\x73\x20\x6F\x75\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x65\x65\x6E\x3A\x20\x68\x6F\x77\x20\x66\x61\x73\x74\x20\x73\x68\x61\x6C\x6C\x20\x69\x74\x20\x6D\x6F\x76\x65\x3F"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_actMoveDistance(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_CardParent(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x73\x20\x74\x68\x65\x20\x70\x61\x72\x65\x6E\x74\x20\x6F\x66\x20\x61\x20\x6E\x65\x77\x20\x73\x70\x61\x77\x6E\x65\x64\x20\x63\x61\x72\x64\x2E"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_moveOutMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x74\x69\x6C\x20\x77\x68\x69\x63\x68\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x62\x65\x20\x6D\x6F\x76\x65\x64\x20\x6F\x75\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x65\x65\x6E\x2C\x20\x75\x6E\x74\x69\x6C\x20\x61\x20\x6E\x65\x77\x20\x63\x61\x72\x64\x20\x69\x73\x20\x73\x70\x61\x77\x6E\x65\x64\x3F"), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_CardStack_CardMovement_m0DE547D4A6BCEF60B388F3E65A0D013E374B8953(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_0_0_0_var), NULL);
	}
}
static void CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_CardStack_moveCardOut_m9E9DA3238C638AB7927FBF00FBBA89E5E40A2FF1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_0_0_0_var), NULL);
	}
}
static void cardCategory_t8CFDCE2F3C6B3A783ABFD5FE0E30FAB994C5F6AE_CustomAttributesCacheGenerator_groupName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x72\x6F\x75\x70\x20\x6E\x61\x6D\x65\x20\x69\x73\x20\x6A\x75\x73\x74\x20\x66\x6F\x72\x20\x64\x65\x63\x6F\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x67\x72\x6F\x75\x70\x73\x2E"), NULL);
	}
}
static void cardCategory_t8CFDCE2F3C6B3A783ABFD5FE0E30FAB994C5F6AE_CustomAttributesCacheGenerator_subStackCondition(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x67\x72\x6F\x75\x70\x2E\x20\x54\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x72\x64\x73\x20\x74\x68\x65\x6D\x73\x65\x6C\x66\x20\x61\x72\x65\x20\x63\x6F\x6D\x70\x75\x74\x65\x64\x20\x77\x69\x74\x68\x20\x6C\x6F\x77\x65\x72\x20\x70\x72\x69\x6F\x72\x69\x74\x79\x2E"), NULL);
	}
}
static void U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49__ctor_m20A81C719966A8182126B9975F568E07B82DAA4F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_IDisposable_Dispose_m597AE27807E96B9EABB15A7B52F3A907165FCA4F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F948D8EEE3C7847E0339CF067F773A196FADE4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_Collections_IEnumerator_Reset_m2022354FDA8C877E0121F503D61E32CF4787D0A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_Collections_IEnumerator_get_Current_m239552DA1314887EF0A85094BE7792EF445E3092(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53__ctor_m1A4633BA82BE4A53DE6C58D7632CA74DBF7980C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_IDisposable_Dispose_m0F6008412BF065A95BF7C9837E002D54FB6AC18B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB107756F0DFD44B9A92F9A90BAA1416A5FF52058(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_Reset_m7ECACECB5214C8351376CC906FC0E5D13664C31A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_get_Current_m60342D8BA597D00666EC5A995994065A6C8AEE7C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CardStyle_tFDD0A60B627DE74092636DE9CA41EE2180925AC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void C_CardImages_t52505B44C1DAEAA640CAB8B05A2490558A332990_CustomAttributesCacheGenerator_color(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryNameDisplay_t19A927AAAB537780B033EA73FC13AFFA541E54B6_CustomAttributesCacheGenerator_CountryNameDisplay_twoFrames_mA53A0B0554D766F89C664C57F2B51243139FC082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_0_0_0_var), NULL);
	}
}
static void U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4__ctor_m7DDE93D188A97F1F2A8F7B8B76AB6E2BDC4E624D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_IDisposable_Dispose_m831487A496655900EDA4322D9ECAAC5E548DB62C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F6E189D7326C4B888B4CC8D73E4E6C62C6C94B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_Reset_mFF5E60CCA7FADEE5BCBFFF2829E501D6E6D8985A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_get_Current_m585E0BFA9C48635E44FD2A8AF7F111842DEF9308(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_gender(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x67\x65\x6E\x64\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x69\x64\x65\x6E\x74\x69\x74\x79\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_Countries(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x73\x74\x20\x6F\x66\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x63\x6F\x75\x6E\x74\x72\x69\x65\x73\x20\x61\x6E\x64\x20\x63\x6F\x75\x6E\x74\x72\x79\x20\x64\x65\x70\x65\x6E\x64\x61\x6E\x74\x20\x6E\x61\x6D\x65\x73\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_countryText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x63\x6F\x75\x6E\x74\x72\x79\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_nameText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x6E\x61\x6D\x65\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_countryAndNameText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x62\x69\x6E\x65\x64\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x63\x6F\x75\x6E\x74\x72\x79\x20\x61\x6E\x64\x20\x6E\x61\x6D\x65\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_countryAndNameTextFormat(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x6D\x61\x74\x74\x69\x6E\x67\x20\x6F\x66\x20\x63\x6F\x75\x6E\x74\x72\x79\x20\x61\x6E\x64\x20\x6E\x61\x6D\x65\x2E\x20\x53\x74\x61\x6E\x64\x61\x72\x64\x20\x69\x73\x20\x27\x7B\x30\x7D\x20\x7B\x31\x7D\x27"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_country(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x74\x6F\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x63\x6F\x75\x6E\x74\x72\x79\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_givenName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x74\x6F\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x67\x69\x76\x65\x6E\x20\x6E\x61\x6D\x65\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_surname(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x74\x6F\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x75\x72\x6E\x61\x6D\x65\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_gender(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x61\x6C\x75\x65\x73\x20\x74\x79\x70\x65\x2C\x20\x77\x68\x69\x63\x68\x20\x68\x6F\x6C\x64\x73\x20\x74\x68\x65\x20\x67\x65\x6E\x64\x65\x72\x2E\x20\x54\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x69\x73\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x62\x79\x20\x27\x43\x6F\x75\x6E\x74\x72\x79\x20\x4E\x61\x6D\x65\x20\x47\x65\x6E\x65\x72\x61\x74\x6F\x72\x27\x2C\x20\x6E\x6F\x74\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x2E"), NULL);
	}
}
static void CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_CountryNameGenerator_oneFrame_m332A4A4145FB9E105CDE00AB65B211D77BCF23FD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_0_0_0_var), NULL);
	}
}
static void subStringList_tD4A5695D6353194B0700B5A9AA9B99BC5E2AD172_CustomAttributesCacheGenerator_listEntry(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x75\x6E\x74\x72\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x72\x61\x6E\x64\x6F\x6D\x20\x70\x6C\x61\x79\x65\x72\x20\x69\x64\x65\x6E\x74\x69\x74\x79\x2E"), NULL);
	}
}
static void subStringList_tD4A5695D6353194B0700B5A9AA9B99BC5E2AD172_CustomAttributesCacheGenerator_nameComb(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x73\x74\x20\x6F\x66\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x63\x6F\x6D\x62\x69\x6E\x61\x74\x69\x6F\x6E\x73\x20\x6F\x66\x20\x67\x69\x76\x65\x6E\x20\x6E\x61\x6D\x65\x20\x61\x6E\x64\x20\x67\x65\x6E\x64\x65\x72\x2E\x20\x54\x68\x65\x20\x6C\x65\x6E\x67\x68\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x69\x73\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x74\x68\x65\x20\x73\x61\x6D\x65\x20\x66\x6F\x72\x20\x65\x76\x65\x72\x79\x20\x63\x6F\x75\x6E\x74\x72\x79\x2E"), NULL);
	}
}
static void subStringList_tD4A5695D6353194B0700B5A9AA9B99BC5E2AD172_CustomAttributesCacheGenerator_surname(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x73\x74\x20\x6F\x66\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x63\x6F\x6D\x62\x69\x6E\x61\x74\x69\x6F\x6E\x73\x20\x6F\x66\x20\x73\x75\x72\x6E\x61\x6D\x65\x73\x2E\x20\x54\x68\x65\x20\x6C\x65\x6E\x67\x68\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x69\x73\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x74\x68\x65\x20\x73\x61\x6D\x65\x20\x66\x6F\x72\x20\x65\x76\x65\x72\x79\x20\x63\x6F\x75\x6E\x74\x72\x79\x2E"), NULL);
	}
}
static void U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5__ctor_m4B579F1DA5E880BA17A19AAC62C801F10B2282FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_IDisposable_Dispose_m697F7F0C529FE820491FBBFB4628E70D9A547D72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0CBC897829B3BE3032005EA66E369973541A768(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_Collections_IEnumerator_Reset_m5F621E1AA2318DC5D06E66CBD08B99C45D57C67E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_Collections_IEnumerator_get_Current_m8E2260A88F81ACEE95703F8F693E5EB4C7DE3895(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_retriggerable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x20\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x20\x62\x65\x20\x72\x65\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x77\x68\x69\x6C\x65\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x72\x20\x61\x6C\x72\x65\x61\x64\x79\x20\x63\x6F\x75\x6E\x74\x73\x20\x64\x6F\x77\x6E\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x20\x67\x65\x74\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x65\x78\x74\x65\x6E\x64\x73\x20\x28\x72\x65\x74\x72\x69\x67\x67\x65\x72\x61\x62\x6C\x65\x20\x3D\x20\x74\x72\x75\x65\x29\x2C\x20\x6F\x72\x20\x77\x69\x6C\x6C\x20\x69\x74\x20\x62\x65\x20\x64\x69\x73\x63\x61\x72\x64\x65\x64\x20\x28\x72\x65\x74\x72\x69\x67\x67\x65\x72\x61\x62\x6C\x65\x20\x3D\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_unscaledTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x20\x73\x63\x61\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x20\x28\x75\x6E\x73\x63\x61\x6C\x65\x64\x54\x69\x6D\x65\x20\x3D\x20\x66\x61\x6C\x73\x65\x29\x20\x6F\x72\x20\x69\x67\x6E\x6F\x72\x65\x20\x74\x69\x6D\x65\x73\x20\x73\x63\x61\x6C\x65\x20\x28\x75\x6E\x73\x63\x61\x6C\x65\x64\x54\x69\x6D\x65\x20\x3D\x20\x74\x72\x75\x65\x29"), NULL);
	}
}
static void DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_startOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x20\x74\x68\x65\x20\x64\x65\x6C\x61\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x20\x61\x74\x20\x61\x63\x74\x69\x76\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x28\x73\x74\x61\x72\x74\x4F\x6E\x53\x74\x61\x72\x74\x20\x3D\x20\x74\x72\x75\x65\x29\x20\x6F\x72\x20\x6F\x6E\x6C\x79\x20\x62\x79\x20\x74\x72\x69\x67\x67\x65\x72\x69\x6E\x67\x20\x69\x74\x2E"), NULL);
	}
}
static void DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_AfterDelay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x20\x61\x66\x74\x65\x72\x20\x74\x69\x6D\x65\x20\x63\x6F\x75\x6E\x74\x20\x64\x6F\x77\x6E\x2E"), NULL);
	}
}
static void DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_timerActive(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_DelayedEvent__delay_m4D67B05369F776238509EF536A42015264C8FE87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_0_0_0_var), NULL);
	}
}
static void U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11__ctor_m5170E051F5D20856F12F90FDCA6571E68EB8F830(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_IDisposable_Dispose_m815F55D1DA38454BA27303C83782CCF5B2839C7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B7C7859D2F37ABA7675B0FF7B2CEA08D5523395(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_Collections_IEnumerator_Reset_m8B109E7B8BD114E6A539836C1CCC24D3B949F6DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_Collections_IEnumerator_get_Current_mE1E9828FA48FE14B08726A0E3A98EFED407A4DBF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GlobalMessageEventReceiver_tDA1B0B374131903574EBFD88C16A03E75E78B316_CustomAttributesCacheGenerator_MessageEvents(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x73\x74\x20\x6F\x66\x20\x65\x76\x65\x6E\x74\x20\x2D\x20\x6D\x65\x73\x73\x61\x67\x65\x20\x63\x6F\x6D\x62\x69\x6E\x61\x74\x69\x6F\x6E\x73\x2E\x20\x4F\x6E\x6C\x79\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x68\x61\x76\x65\x20\x74\x6F\x20\x62\x65\x20\x61\x64\x64\x65\x64\x2E"), NULL);
	}
}
static void GlobalMessageEventReceiver_tDA1B0B374131903574EBFD88C16A03E75E78B316_CustomAttributesCacheGenerator_MessageImages(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x73\x74\x20\x6F\x66\x20\x69\x6D\x61\x67\x65\x20\x2D\x20\x6D\x65\x73\x73\x61\x67\x65\x20\x63\x6F\x6D\x62\x69\x6E\x61\x74\x69\x6F\x6E\x73\x2E\x20\x4F\x6E\x6C\x79\x20\x72\x65\x6C\x65\x76\x61\x6E\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x68\x61\x76\x65\x20\x74\x6F\x20\x62\x65\x20\x61\x64\x64\x65\x64\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_textFields(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x79\x6F\x75\x72\x20\x63\x61\x72\x64\x20\x74\x65\x78\x74\x73\x20\x61\x6E\x64\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x73\x20\x68\x65\x72\x65\x2E\x20\x54\x68\x65\x20\x73\x74\x72\x69\x6E\x67\x73\x20\x63\x61\x6E\x20\x62\x65\x20\x74\x65\x72\x6D\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x27\x54\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x4D\x61\x6E\x61\x67\x65\x72\x27\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_isHighPriorityCard(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x61\x20\x63\x61\x72\x64\x20\x69\x73\x20\x68\x69\x67\x68\x20\x70\x72\x69\x6F\x72\x69\x74\x79\x2C\x20\x69\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x72\x61\x77\x20\x62\x65\x66\x6F\x72\x65\x20\x61\x6C\x6C\x20\x6F\x74\x68\x65\x72\x20\x6E\x6F\x72\x6D\x61\x6C\x20\x63\x61\x72\x64\x73\x2C\x20\x62\x75\x74\x20\x61\x66\x74\x65\x72\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x73\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_isDrawable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x6C\x79\x20\x64\x72\x61\x77\x61\x62\x6C\x65\x20\x63\x61\x72\x64\x73\x20\x63\x61\x6E\x20\x62\x65\x20\x72\x61\x6E\x64\x6F\x6D\x6C\x79\x20\x64\x72\x61\x77\x6E\x20\x62\x65\x63\x61\x75\x73\x65\x20\x6F\x66\x20\x74\x68\x65\x69\x72\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x2E\x20\x4E\x6F\x6E\x20\x64\x72\x61\x77\x61\x62\x6C\x65\x20\x63\x61\x72\x64\x73\x20\x61\x72\x65\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x73\x20\x77\x68\x69\x63\x68\x20\x61\x72\x65\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x62\x79\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x20\x63\x61\x72\x64\x73\x20\x6F\x72\x20\x63\x61\x72\x64\x73\x20\x6C\x69\x6B\x65\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x76\x65\x72\x20\x73\x74\x61\x74\x69\x73\x74\x69\x63\x73\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_cardPropability(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x72\x6F\x70\x61\x62\x69\x6C\x69\x74\x79\x20\x61\x70\x70\x6C\x69\x65\x73\x20\x74\x6F\x20\x61\x6C\x6C\x20\x63\x61\x72\x64\x73\x2C\x20\x77\x68\x69\x63\x68\x20\x6D\x65\x74\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x2E\x20\x43\x61\x72\x64\x73\x20\x77\x69\x74\x68\x20\x61\x20\x68\x69\x67\x68\x65\x72\x20\x70\x72\x6F\x70\x61\x62\x69\x6C\x69\x74\x79\x20\x61\x72\x65\x20\x6D\x6F\x72\x65\x20\x6C\x69\x6B\x65\x6C\x79\x20\x74\x6F\x20\x62\x65\x20\x64\x72\x61\x77\x6E\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_maxDraws(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x20\x6C\x69\x6D\x69\x74\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x64\x72\x61\x77\x73\x20\x6F\x66\x20\x61\x20\x63\x61\x72\x64\x20\x70\x65\x72\x20\x67\x61\x6D\x65\x2C\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x27\x6D\x61\x78\x44\x72\x61\x77\x73\x27\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_redrawBlockCnt(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x66\x74\x65\x72\x20\x61\x20\x63\x61\x72\x64\x20\x69\x73\x20\x64\x72\x61\x77\x6E\x2C\x20\x64\x65\x66\x69\x6E\x65\x20\x66\x6F\x72\x20\x68\x6F\x77\x20\x6D\x61\x6E\x79\x20\x63\x79\x63\x6C\x65\x73\x20\x69\x74\x20\x69\x73\x20\x62\x6C\x6F\x63\x6B\x65\x64\x20\x74\x6F\x20\x62\x65\x20\x72\x65\x64\x72\x61\x77\x6E\x2E\x20\x54\x68\x69\x73\x20\x64\x6F\x65\x73\x6E\x27\x74\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x73\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_conditions(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x75\x6E\x64\x65\x72\x20\x77\x69\x63\x68\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x74\x68\x69\x73\x20\x63\x61\x72\x64\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x72\x61\x77\x6E\x2E\x20\x45\x2E\x67\x2E\x20\x61\x20\x6D\x61\x72\x72\x69\x61\x67\x65\x20\x63\x61\x72\x64\x20\x73\x68\x6F\x75\x6C\x64\x20\x6F\x6E\x6C\x79\x20\x62\x65\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x69\x66\x20\x61\x20\x76\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x27\x61\x67\x65\x27\x20\x69\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x72\x61\x6E\x67\x65\x20\x6F\x66\x20\x31\x38\x20\x74\x6F\x20\x31\x30\x30\x20\x6F\x72\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x27\x6D\x61\x72\x72\x69\x61\x67\x65\x27\x20\x69\x73\x20\x7A\x65\x72\x6F\x20\x28\x6E\x6F\x74\x20\x6D\x61\x72\x72\x69\x65\x64\x20\x79\x65\x74\x29"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_swipeType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x74\x79\x70\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x77\x69\x70\x65\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x63\x61\x72\x64\x2E\x20\xA\x27\x4C\x65\x66\x74\x52\x69\x67\x68\x74\x27\x20\x65\x6E\x61\x62\x6C\x65\x73\x20\x32\x20\x63\x68\x6F\x69\x63\x65\x73\x2E\x20\xA\x27\x46\x6F\x75\x72\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x27\x20\x61\x6C\x6C\x6F\x77\x73\x20\x34\x20\x63\x68\x6F\x69\x63\x65\x73\x2E\xA\x46\x6F\x72\x20\x6D\x6F\x72\x65\x20\x63\x68\x6F\x69\x63\x65\x73\x20\x70\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x74\x68\x65\x20\x6D\x75\x6C\x74\x69\x2D\x63\x68\x6F\x69\x63\x65\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_additionalChoices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x74\x77\x6F\x20\x61\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x63\x68\x6F\x69\x63\x65\x73\x20\x66\x6F\x72\x20\x65\x2E\x20\x67\x2E\x20\x6D\x75\x6C\x74\x69\x63\x68\x6F\x69\x63\x65\x20\x63\x61\x72\x64\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_changeExtrasOnCardDespawn(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x6E\x67\x65\x73\x20\x6F\x66\x20\x69\x74\x65\x6D\x73\x2F\x64\x69\x63\x74\x69\x6F\x6E\x61\x72\x79\x2F\x71\x75\x65\x73\x74\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x63\x6F\x6D\x70\x75\x74\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x72\x65\x73\x75\x6C\x74\x73\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x69\x66\x20\x61\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x63\x68\x61\x6E\x67\x65\x64\x20\x69\x6E\x64\x65\x70\x65\x6E\x64\x65\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x2E"), NULL);
	}
}
static void EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_changeValueOnCardDespawn(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x6E\x67\x65\x73\x20\x6F\x66\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x63\x6F\x6D\x70\x75\x74\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x72\x65\x73\x75\x6C\x74\x73\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x69\x66\x20\x61\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x63\x68\x61\x6E\x67\x65\x64\x20\x69\x6E\x64\x65\x70\x65\x6E\x64\x65\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x2C\x20\x6C\x69\x6B\x65\x20\x27\x41\x67\x65\x20\x2B\x31\x27\x2E"), NULL);
	}
}
static void modifierGroup_t6BDC7DBBD28F4B74B28A2FA84B12ED34A612B9E7_CustomAttributesCacheGenerator_followUpCard(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x70\x61\x74\x68\x20\x77\x61\x73\x20\x74\x61\x6B\x65\x6E\x2C\x20\x77\x69\x6C\x6C\x20\x74\x68\x65\x72\x65\x20\x62\x65\x20\x61\x20\x27\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x27\x20\x63\x61\x72\x64\x20\x77\x68\x69\x63\x68\x20\x74\x61\x6B\x65\x73\x20\x74\x68\x65\x20\x73\x74\x6F\x72\x79\x20\x66\x75\x72\x74\x68\x65\x72\x3F\x20\x43\x61\x6E\x20\x62\x65\x20\x6C\x65\x66\x74\x20\x65\x6D\x70\x74\x79\x2E"), NULL);
	}
}
static void result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_modifiers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x69\x63\x68\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x72\x65\x20\x6D\x6F\x64\x69\x66\x69\x65\x64\x2C\x20\x69\x66\x20\x74\x68\x69\x73\x20\x72\x65\x73\x75\x6C\x74\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x3F"), NULL);
	}
}
static void result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_conditions(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x66\x75\x72\x74\x68\x65\x72\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x20\x63\x61\x6E\x20\x73\x70\x6C\x69\x74\x20\x69\x6E\x74\x6F\x20\x74\x77\x6F\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x6F\x75\x74\x63\x6F\x6D\x65\x73\x2E\x20\x49\x66\x20\x61\x6C\x6C\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x27\x4D\x6F\x64\x69\x66\x69\x65\x72\x73\x20\x54\x72\x75\x65\x27\x20\x61\x72\x65\x20\x65\x78\x65\x63\x75\x74\x65\x64\x2E\x20\x49\x66\x20\x6F\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x66\x61\x69\x6C\x73\x2C\x20\x74\x68\x65\x20\x27\x4D\x6F\x64\x69\x66\x69\x65\x72\x73\x20\x46\x61\x6C\x73\x65\x27\x2E\x20\x45\x2E\x67\x2E\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x68\x65\x20\x77\x61\x6E\x74\x73\x20\x74\x6F\x20\x74\x61\x6B\x65\x20\x61\x20\x72\x61\x63\x65\x20\x62\x75\x74\x20\x68\x69\x73\x20\x27\x61\x67\x69\x6C\x69\x74\x79\x27\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x74\x6F\x20\x6C\x6F\x77\x2C\x20\x61\x73\x20\x6F\x75\x74\x63\x6F\x6D\x65\x20\x68\x65\x20\x77\x69\x6C\x6C\x20\x6C\x6F\x73\x65\x2E"), NULL);
	}
}
static void result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_modifiersTrue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x72\x6F\x75\x70\x20\x6F\x66\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x2C\x20\x69\x66\x20\x61\x6C\x6C\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x6D\x65\x74\x2E"), NULL);
	}
}
static void result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_modifiersFalse(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x72\x6F\x75\x70\x20\x6F\x66\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x2C\x20\x69\x66\x20\x61\x74\x20\x6C\x65\x61\x73\x74\x20\x6F\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x66\x61\x69\x6C\x73\x2E"), NULL);
	}
}
static void result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_randomModifiers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x20\x72\x65\x73\x75\x6C\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x70\x6C\x69\x74\x20\x69\x6E\x20\x6D\x75\x6C\x74\x69\x62\x6C\x65\x20\x6F\x75\x74\x63\x6F\x6D\x65\x73\x2E\x20\x54\x68\x65\x20\x27\x52\x61\x6E\x64\x6F\x6D\x20\x4D\x6F\x66\x69\x66\x69\x65\x72\x73\x27\x20\x63\x61\x6E\x20\x62\x65\x20\x70\x72\x65\x64\x65\x66\x69\x6E\x65\x64\x2C\x20\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x6F\x75\x74\x63\x6F\x6D\x65\x20\x69\x73\x20\x72\x61\x6E\x64\x6F\x6D\x6C\x79\x20\x6F\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x73\x65\x2E"), NULL);
	}
}
static void resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultLeft(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x20\x28\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x69\x6E\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x6E\x64\x20\x70\x65\x72\x68\x61\x70\x73\x20\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x29\x20\x69\x66\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x77\x69\x70\x65\x73\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x6C\x65\x66\x74\x2E"), NULL);
	}
}
static void resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultRight(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x20\x28\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x69\x6E\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x6E\x64\x20\x70\x65\x72\x68\x61\x70\x73\x20\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x29\x20\x69\x66\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x77\x69\x70\x65\x73\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x72\x69\x67\x68\x74\x2E"), NULL);
	}
}
static void resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultUp(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x20\x28\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x69\x6E\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x6E\x64\x20\x70\x65\x72\x68\x61\x70\x73\x20\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x29\x20\x69\x66\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x77\x69\x70\x65\x73\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x75\x70\x2E"), NULL);
	}
}
static void resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultDown(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x20\x28\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x69\x6E\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x6E\x64\x20\x70\x65\x72\x68\x61\x70\x73\x20\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x29\x20\x69\x66\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x77\x69\x70\x65\x73\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x64\x6F\x77\x6E\x2E"), NULL);
	}
}
static void resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_additional_choice_0(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x20\x28\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x69\x6E\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x6E\x64\x20\x70\x65\x72\x68\x61\x70\x73\x20\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x29\x20\x69\x66\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x65\x6C\x65\x63\x74\x73\x20\x61\x64\x64\x74\x69\x6F\x6E\x61\x6C\x20\x63\x68\x6F\x69\x63\x65\x73\x2E"), NULL);
	}
}
static void resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_additional_choice_1(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x72\x65\x73\x75\x6C\x74\x20\x28\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x69\x6E\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x6E\x64\x20\x70\x65\x72\x68\x61\x70\x73\x20\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x75\x70\x20\x63\x61\x72\x64\x29\x20\x69\x66\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x65\x6C\x65\x63\x74\x73\x20\x61\x64\x64\x74\x69\x6F\x6E\x61\x6C\x20\x63\x68\x6F\x69\x63\x65\x73\x2E"), NULL);
	}
}
static void GameDictionary_InputField_tC5B0367D22637D7CCF43FF0DC50F3A5F82DA885E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_0_0_0_var), NULL);
	}
}
static void GameDictionary_InputField_tC5B0367D22637D7CCF43FF0DC50F3A5F82DA885E_CustomAttributesCacheGenerator_DictionaryKey(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x70\x72\x6F\x76\x69\x64\x65\x20\x61\x6E\x20\x6B\x65\x79\x20\x66\x6F\x72\x20\x61\x63\x65\x73\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x64\x61\x74\x61\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x64\x69\x63\x74\x69\x6F\x6E\x61\x72\x79\x2E"), NULL);
	}
}
static void GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_gameLogText(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_logs(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x6C\x6F\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_splitLogs(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x6C\x69\x74\x20\x6C\x6F\x67\x73\x20\x61\x72\x65\x20\x63\x61\x74\x65\x67\x6F\x72\x69\x7A\x65\x64\x20\x6C\x6F\x67\x73\x2E"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_textBreakEvery(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x74\x65\x78\x74\x42\x72\x65\x61\x6B\x45\x76\x65\x72\x79\x27\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x73\x20\x61\x20\x6C\x69\x6E\x65\x62\x72\x65\x61\x6B\x20\x65\x76\x65\x72\x20\x78\x20\x6C\x6F\x67\x73\x20\x74\x6F\x20\x66\x6F\x72\x6D\x61\x74\x20\x74\x68\x65\x20\x6F\x75\x74\x70\x75\x74\x20\x73\x74\x72\x69\x6E\x67\x20\x69\x6E\x20\x61\x20\x6D\x6F\x72\x65\x20\x72\x65\x61\x64\x61\x62\x6C\x65\x20\x74\x65\x78\x74\x2E"), NULL);
	}
}
static void GameStateManager_t581A50F0CEB605A7870C3A10FC14F9CAA6879A67_CustomAttributesCacheGenerator_swipeCounter(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GameStateManager_t581A50F0CEB605A7870C3A10FC14F9CAA6879A67_CustomAttributesCacheGenerator_gamestate(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x75\x61\x6C\x20\x73\x74\x61\x74\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void GameStateManager_t581A50F0CEB605A7870C3A10FC14F9CAA6879A67_CustomAttributesCacheGenerator_GameStateManager_OneFrameDelayStartup_m4224DC7C44D02F4F4D8C61CF79B3F0563D1EA193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_0_0_0_var), NULL);
	}
}
static void U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9__ctor_mFA4D7E5CB4F7D1FCAE440E17731FC79FE6463229(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_IDisposable_Dispose_mEDF62ED725AEC84B0623405D0629C09AADA5575F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD01966B1BEF4FBB7C85B3A95CF46DC5E3C3FEC1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_Reset_mA3924A3373CF381CFB79BB3C5997F6D3D753E996(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_get_Current_m72A635E851E0F7F4EC96AA006D5450B5F893E29F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_valueType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x77\x68\x69\x63\x68\x20\x68\x6F\x6C\x64\x73\x20\x74\x68\x65\x20\x67\x65\x6E\x64\x65\x72\x20\x76\x61\x6C\x75\x65\x2E\x20\x54\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x61\x6C\x73\x6F\x20\x62\x65\x20\x6C\x69\x6E\x6B\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x27\x43\x6F\x75\x6E\x74\x72\x79\x20\x4E\x61\x6D\x65\x20\x47\x65\x6E\x65\x72\x61\x74\x6F\x72\x27\x2E"), NULL);
	}
}
static void GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_genders(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x6E\x61\x6D\x65\x73\x20\x61\x6E\x64\x20\x70\x69\x63\x74\x6F\x67\x72\x61\x6D\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x76\x61\x69\x6C\x61\x62\x6C\x65\x20\x67\x65\x6E\x64\x65\x72\x73\x2E"), NULL);
	}
}
static void GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_outText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x66\x6F\x72\x20\x73\x68\x6F\x77\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x67\x65\x6E\x64\x65\x72\x20\x6E\x61\x6D\x65\x2E"), NULL);
	}
}
static void GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_outImg(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x69\x6D\x61\x67\x65\x20\x66\x6F\x72\x20\x73\x68\x6F\x77\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x67\x65\x6E\x64\x65\x72\x20\x70\x69\x63\x74\x6F\x67\x72\x61\x6D\x2E"), NULL);
	}
}
static void GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_GenderGenerator_frameDelay_mD70AC765071A2E2AC81EBF1F46466AC95CCECD0E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_0_0_0_var), NULL);
	}
}
static void U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5__ctor_m3D601DC493336F760EA9D07947A8848668774822(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_IDisposable_Dispose_m61ED0D2F306AD93BA1D6B09AB601CED46FFD40B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m508D08CD2E72E437AA970E975F0FDFDA49362A35(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_Collections_IEnumerator_Reset_mF2CA8CAE3AC8A3F88666C84089B11B51F2FA7361(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_Collections_IEnumerator_get_Current_m289933883DE423AE7BCD2897BA8EB82EB19946AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_highScoreSource(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x74\x68\x65\x20\x73\x6F\x75\x72\x63\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x6F\x72\x65\x2E"), NULL);
	}
}
static void HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_highScoreSort(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x74\x68\x65\x20\x68\x6F\x77\x20\x74\x68\x65\x20\x73\x63\x6F\x72\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x73\x6F\x72\x74\x65\x64\x2E"), NULL);
	}
}
static void HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_key(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x76\x65\x20\x6B\x65\x79\x20\x69\x73\x20\x61\x75\x74\x6F\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x64\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x6C\x69\x6E\x6B\x65\x64\x20\x73\x63\x72\x69\x70\x74\x73\x2E"), NULL);
	}
}
static void HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_HighScoreNameLinker_delayed_m3D64F23F54557193F3753E010799BF317CF2E2C7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_0_0_0_var), NULL);
	}
}
static void C_cap_t6FCABDF845B0C9E865B3BDFA71E32843F8849948_CustomAttributesCacheGenerator_list(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x61\x64\x65\x64\x2F\x61\x63\x74\x75\x61\x6C\x20\x68\x69\x67\x68\x73\x63\x6F\x72\x65\x20\x70\x61\x69\x72\x2E"), NULL);
	}
}
static void C_highscoreFields_t1D186C2D071A8AC88E4B158C5AACAF9B99597AD3_CustomAttributesCacheGenerator_countryNameText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x69\x64\x65\x6E\x74\x69\x74\x79\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x73\x63\x6F\x72\x65\x2E"), NULL);
	}
}
static void C_highscoreFields_t1D186C2D071A8AC88E4B158C5AACAF9B99597AD3_CustomAttributesCacheGenerator_highScoreText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x73\x63\x6F\x72\x65\x2E"), NULL);
	}
}
static void U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15__ctor_m0918B7118476E3C20CF0E8D758735D04E1FDED1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_IDisposable_Dispose_mE0DB7048627B25283EAB909051B30A8D4FF6224D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3544EE0B3D2BCE81E12BC0A9D3CF43A2DAAA781E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_Collections_IEnumerator_Reset_mA0A6E89CDEFFD236B3CF585DB50FB39703C45A7C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_Collections_IEnumerator_get_Current_mF80795299D99484AE4CD163215543FDCC91855CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InfoDisplay_t6DBEFBA78D18777ED9BA880B207C209E7F15A2BC_CustomAttributesCacheGenerator_collectionTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x63\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x54\x69\x6D\x65\x27\x20\x64\x65\x66\x69\x6E\x65\x73\x20\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x63\x6F\x6C\x6C\x65\x63\x74\x69\x6E\x67\x20\x6E\x65\x77\x20\x69\x63\x6F\x6E\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x73\x68\x6F\x77\x69\x6E\x67\x20\x74\x68\x65\x6D\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 1.0f, NULL);
	}
}
static void InfoDisplay_t6DBEFBA78D18777ED9BA880B207C209E7F15A2BC_CustomAttributesCacheGenerator_displayTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x64\x69\x73\x70\x6C\x61\x79\x54\x69\x6D\x65\x27\x20\x64\x65\x66\x69\x6E\x65\x73\x20\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x73\x68\x6F\x77\x69\x6E\x67\x20\x69\x63\x6F\x6E\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x73\x74\x61\x72\x74\x20\x63\x6F\x6C\x6C\x65\x63\x74\x69\x6E\x67\x20\x61\x67\x61\x69\x6E\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 5.0f, NULL);
	}
}
static void InfoDisplay_t6DBEFBA78D18777ED9BA880B207C209E7F15A2BC_CustomAttributesCacheGenerator_InfoDisplay_cyclicIconCollector_mE8F77F9BEBD5496FEF8766E4762BE9C9636F150A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_0_0_0_var), NULL);
	}
}
static void U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2__ctor_m9220A985B72F928F75C0CAFBE073745B9E441881(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_IDisposable_Dispose_mFB4B957CA312D8B9041E3DE37A56A4AF017327BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m303A52A720B7B60184234F81392918ED44B068BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_Reset_m2841F544A2EFED8EE58B4425C78555D50A12453F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_get_Current_mEBA04BC302FA19463B5072EFF2D48AEED7B86B2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void KingsCardStyle_tFF207306B0BCCD825EF173E4770A8B7C3C30280F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
	}
}
static void KingsCardStyleList_tA48C528A7BB67AA6B3F33DE08BA00AD8FB146796_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
	}
}
static void KingsLevelUp_t0377874F7A9F38E1B68EE371E01208C9C2D23951_CustomAttributesCacheGenerator_stats(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x6C\x65\x76\x65\x6C\x20\x75\x70\x20\x73\x74\x61\x74\x73\x2E"), NULL);
	}
}
static void KingsLevelUp_t0377874F7A9F38E1B68EE371E01208C9C2D23951_CustomAttributesCacheGenerator_uiConfig(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x61\x6E\x64\x20\x61\x64\x6A\x75\x73\x74\x69\x6E\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x61\x68\x61\x76\x69\x6F\x75\x72\x2E"), NULL);
	}
}
static void KingsLevelUp_t0377874F7A9F38E1B68EE371E01208C9C2D23951_CustomAttributesCacheGenerator_KingsLevelUp_cyclicFillingComputations_mFC40CFAFBC0B13EF636CA12077EE61318CFC952C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_0_0_0_var), NULL);
	}
}
static void C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_maxLevel(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x61\x74\x20\x69\x73\x20\x74\x68\x65\x20\x6D\x61\x78\x2D\x6C\x65\x76\x65\x6C\x3F"), NULL);
	}
}
static void C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_xpCosts(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x63\x6F\x73\x74\x73\x20\x70\x65\x72\x20\x6C\x65\x76\x65\x6C\x20\x66\x6F\x72\x20\x6C\x65\x76\x65\x6C\x6C\x69\x6E\x67\x20\x75\x70\x2E\x20\x54\x68\x65\x20\x6C\x69\x73\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x73\x74\x61\x72\x74\x20\x66\x72\x6F\x6D\x20\x6C\x65\x76\x65\x6C\x20\x30\x2E\x20\x4E\x6F\x74\x20\x6D\x65\x6E\x74\x69\x6F\x6E\x65\x64\x20\x6C\x65\x76\x65\x6C\x6E\x75\x6D\x62\x65\x72\x73\x20\x61\x72\x65\x20\x66\x69\x6C\x6C\x65\x64\x3A\xA\x4C\x65\x76\x65\x6C\x20\x30\x20\x63\x6F\x73\x74\x20\x31\x30\x30\xA\x4C\x65\x76\x65\x6C\x20\x35\x20\x63\x6F\x73\x74\x20\x35\x30\x30\xA\x4C\x65\x76\x65\x6C\x20\x31\x30\x20\x63\x6F\x73\x74\x20\x31\x30\x30\x30\xA\xA\x72\x65\x73\x75\x6C\x74\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C\x20\x63\x6F\x73\x74\x3A\xA\x4C\x65\x76\x65\x6C\x20\x31\x3A\x20\x31\x30\x30\xA\x4C\x65\x76\x65\x6C\x20\x32\x3A\x20\x31\x30\x30\xA\x2E\x2E\x2E\xA\x4C\x65\x76\x65\x6C\x20\x34\x3A\x20\x31\x30\x30\xA\x4C\x65\x76\x65\x6C\x20\x35\x3A\x20\x35\x30\x30\xA\x2E\x2E\x2E\xA\x4C\x65\x76\x65\x6C\x20\x31\x30\x3A\x20\x31\x30\x30\x30\xA\x4C\x65\x76\x65\x6C\x20\x3E\x31\x30\x3A\x20\x31\x30\x30\x30"), NULL);
	}
}
static void C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_actualLevel(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x61\x74\x20\x69\x73\x20\x74\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x70\x6C\x61\x79\x65\x72\x20\x6C\x65\x76\x65\x6C\x2C\x20\x73\x74\x61\x72\x74\x69\x6E\x67\x20\x77\x69\x74\x68\x20\x31\x2E"), NULL);
	}
}
static void C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_XpNextLevel(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x6D\x75\x63\x68\x20\x78\x70\x20\x69\x73\x20\x6E\x65\x65\x64\x65\x64\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x20\x6C\x65\x76\x65\x6C\x2E"), NULL);
	}
}
static void C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_XpAll(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x6D\x75\x63\x68\x20\x78\x70\x20\x77\x61\x73\x20\x61\x77\x61\x72\x64\x65\x64\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x69\x6E\x20\x74\x6F\x74\x61\x6C\x2E"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_XpActual(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x6D\x75\x63\x68\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x68\x61\x73\x20\x61\x63\x74\x75\x61\x6C\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x6C\x65\x76\x65\x6C\x2E"), NULL);
	}
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[1];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_XpPerLevel_tA9905A82356115C7ED585124488011846E546A3A_CustomAttributesCacheGenerator_fromLevel(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x20\x77\x68\x69\x63\x68\x20\x6C\x65\x76\x65\x6C\x20\x69\x73\x20\x74\x68\x65\x20\x63\x6F\x73\x74\x20\x61\x70\x70\x6C\x69\x63\x61\x62\x6C\x65\x3F"), NULL);
	}
}
static void C_XpPerLevel_tA9905A82356115C7ED585124488011846E546A3A_CustomAttributesCacheGenerator_xpCost(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x6D\x75\x63\x68\x20\x78\x70\x20\x64\x6F\x65\x73\x20\x69\x74\x20\x63\x6F\x73\x74\x20\x74\x6F\x20\x69\x6E\x63\x72\x65\x61\x73\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x20\x6C\x65\x76\x65\x6C\x20\x73\x74\x61\x72\x74\x69\x6E\x67\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x6C\x65\x76\x65\x6C\x3F"), NULL);
	}
}
static void C_LevelUpUiConfig_tA6003940072089B62E1EEDC129C582B0FBF6F4B3_CustomAttributesCacheGenerator_xpBarFillSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00999999978f, 10.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x66\x61\x73\x74\x20\x73\x68\x61\x6C\x6C\x20\x74\x68\x65\x20\x78\x70\x20\x62\x61\x72\x20\x62\x65\x20\x66\x69\x6C\x6C\x65\x64\x2E\x20\x56\x61\x6C\x75\x65\x20\x69\x73\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x2F\x73\x65\x63\x6F\x6E\x64\x2E"), NULL);
	}
}
static void C_LevelUpUiConfig_tA6003940072089B62E1EEDC129C582B0FBF6F4B3_CustomAttributesCacheGenerator_levelIncreaseFillDelay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x75\x69\x20\x61\x63\x74\x75\x61\x6C\x69\x7A\x65\x73\x20\x61\x20\x6C\x65\x76\x65\x6C\x2C\x20\x68\x6F\x77\x20\x6C\x6F\x6E\x67\x20\x73\x68\x61\x6C\x6C\x20\x69\x74\x20\x77\x61\x69\x74\x20\x74\x6F\x20\x66\x69\x6C\x6C\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x20\x62\x61\x72\x3F"), NULL);
	}
}
static void U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21__ctor_mFEB671CD9B6AB385063CB85F9EDD2FCF66CFBE5A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_IDisposable_Dispose_m44C5F2035DDB457E94FD9376930D8D740C577C88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9342F3240439AB5371B1AB414C9AD0438B44577D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_Reset_mF95A76D31F44E278584ADBD7A02631EE22024239(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_get_Current_m59ECC582967406007EAC87DB871D8FED4E230955(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void KingsTypewriter_t8F4F04BF8164DF484099D6DA0FBC074CE171B612_CustomAttributesCacheGenerator_typesPerSecond(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 150.0f, NULL);
	}
}
static void KingsTypewriter_t8F4F04BF8164DF484099D6DA0FBC074CE171B612_CustomAttributesCacheGenerator_startDelay(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void KingsTypewriter_t8F4F04BF8164DF484099D6DA0FBC074CE171B612_CustomAttributesCacheGenerator_KingsTypewriter_typeText_m368EB8071A25C0E0E88D3D13C89D7FF35B43E5D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_0_0_0_var), NULL);
	}
}
static void U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17__ctor_m316FCE9CF3C7CB715AAA1E003D8F0EFB00A03D0E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_IDisposable_Dispose_m6608A8CA82E96F5255878BAE12497DE488C43FCD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2B057A114FA28DE212251036952D43402AC45E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_Collections_IEnumerator_Reset_mE733F3418C0E5CB7B1F92929A9734C0EA3FD9EBE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_Collections_IEnumerator_get_Current_mC3ED16AA3344A28C0765D09F95EACD089DD6A91C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_StatusText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x28\x62\x69\x67\x29\x20\x74\x65\x78\x74\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x20\x73\x74\x61\x74\x75\x73\x20\x64\x65\x62\x75\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x77\x68\x65\x6E\x20\x74\x65\x73\x74\x69\x6E\x67\x20\x6F\x6E\x20\x61\x20\x64\x65\x76\x69\x63\x65\x2E"), NULL);
	}
}
static void Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_achievementText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x76\x69\x64\x65\x20\x61\x20\x74\x65\x78\x74\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x75\x73\x65\x72\x2E"), NULL);
	}
}
static void Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_scoresToTransmit(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x63\x6F\x72\x65\x73\x20\x74\x6F\x20\x74\x72\x61\x6E\x73\x6D\x69\x74\x2E\x20\x4E\x65\x65\x64\x73\x20\x73\x6F\x6D\x65\x20\x6B\x65\x79\x73\x20\x61\x6E\x64\x20\x73\x74\x72\x69\x6E\x67\x73\x20\x66\x72\x6F\x6D\x20\x6F\x6E\x6C\x69\x6E\x65\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_achievements(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x73\x2E\x20\x4E\x65\x65\x64\x73\x20\x73\x6F\x6D\x65\x20\x6B\x65\x79\x73\x20\x61\x6E\x64\x20\x73\x74\x72\x69\x6E\x67\x73\x20\x66\x72\x6F\x6D\x20\x6F\x6E\x6C\x69\x6E\x65\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_Mobile_Leaderboard_sozialActivations_m670202D247ACB1CB9A2F5E886948A355E4A8B55E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_0_0_0_var), NULL);
	}
}
static void Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_Mobile_Leaderboard_refreshSettingsDelayed_mBF003C2B8B4D5633265BDDD0EF26C47FE90C98C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_0_0_0_var), NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_leaderBoardName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x20\x6F\x6E\x6C\x69\x6E\x65\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x2C\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x65\x61\x64\x65\x72\x62\x6F\x61\x72\x64\x2E"), NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_leaderBoardID(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x20\x6F\x6E\x6C\x69\x6E\x65\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x2C\x20\x49\x44\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x65\x61\x64\x65\x72\x62\x6F\x61\x72\x64\x2E"), NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreKey(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x6F\x72\x65\x20\x69\x73\x20\x6C\x6F\x61\x64\x65\x64\x20\x66\x72\x6F\x6D\x20\x70\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73\x2E\x20\x50\x72\x6F\x76\x69\x64\x65\x20\x74\x68\x65\x20\x61\x63\x63\x6F\x72\x64\x69\x6E\x67\x20\x6B\x65\x79\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_maxScoreKey(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x63\x6F\x72\x65\x20\x69\x73\x20\x6C\x6F\x61\x64\x65\x64\x20\x66\x72\x6F\x6D\x20\x70\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73\x2E\x20\x50\x72\x6F\x76\x69\x64\x65\x20\x74\x68\x65\x20\x61\x63\x63\x6F\x72\x64\x69\x6E\x67\x20\x6B\x65\x79\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmitOkU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmitFailU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmittedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmitRequestedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__scoreActualU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__scoreMaxU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__score_maxTransmittedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__scoreToTransmitU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitOk_mD5599284B0A446E8C34BBDD3093123DD21FE1239(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitOk_mBF73643240665182CB69D0C14CDC3B087234BFA8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitFail_m1D46293C16444D7B24E7F299906D545B69B4219C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitFail_m31C56D55D7B64105F7B67D4DE8E26B9C5CF21E87(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitted_m24ADB7B38403047613B6D005C88BE878E031EEA8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitted_mEAA4AADB1646933E1DE013DF2E0E612A0DA0F7E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitRequested_mEE51F08862BCD8C9032E6E711AED5B14A3A09C31(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitRequested_mC218E210B8E249EA4A35AF085BE958D03C729548(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___scoreActual_m1D676ADD27F828BB2CA053ADB30D939936D53B37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___scoreActual_mB774F0A990B7EEE38DD922EA090B0C8579925EBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___scoreMax_m38F6DB7A6C75E88B9B89D6AA1CC207982739E230(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___scoreMax_m092C80890879F45DDDB325959410B46A6F90E6A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___score_maxTransmitted_m4B558D48B45FD7A633C4FD7209CB6C5FE3DB4055(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___score_maxTransmitted_m61DBFBDABBC81A990BDBDE25ECF48B22248C593E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___scoreToTransmit_m0695FCB38BD85E9835B80EC619936570FC808FB9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___scoreToTransmit_mD05BADBFF2EF6E8FE12433BA61872C6DF71E1EDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_AchievementName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x20\x6F\x6E\x6C\x69\x6E\x65\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x2C\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_AchievementID(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x6F\x6D\x20\x6F\x6E\x6C\x69\x6E\x65\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x2C\x20\x49\x44\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_scoreKey(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x61\x6E\x20\x73\x63\x6F\x72\x65\x20\x65\x78\x63\x65\x65\x64\x73\x20\x61\x6E\x20\x6D\x69\x6E\x69\x6D\x61\x6C\x20\x76\x61\x6C\x75\x65\x2C\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x20\x69\x73\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x2E\x20\x53\x63\x6F\x72\x65\x20\x69\x73\x20\x6C\x6F\x61\x64\x65\x64\x20\x66\x72\x6F\x6D\x20\x70\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73\x2E\x20\x50\x72\x6F\x76\x69\x64\x65\x20\x74\x68\x65\x20\x61\x63\x63\x6F\x72\x64\x69\x6E\x67\x20\x6B\x65\x79\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_MinScoreValue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x6D\x69\x6E\x69\x6D\x75\x6D\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x74\x72\x69\x67\x67\x65\x72\x20\x74\x68\x65\x20\x61\x63\x68\x69\x65\x76\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__reachedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmitOkU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmitFailU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmittedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmitRequestedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___reached_m8F7AE68B934D6AA11B0E6138C2DBFDFC216A8D8F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___reached_mFCA3399C5D82C1AF8BD992E0D8D345751B27C626(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitOk_mFB8E26CD83629DEACF31511CFEBC5EE71A114564(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitOk_mFFF97F106EF5D1BDAA9958F254140C88727DD3C3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitFail_m5F02EFBC9B8215587543228A555A0E43350CC1F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitFail_mBF337F23A2FA59B61F444C4C0C0720B28A0D9C72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitted_mF97F2ABC145A00EB91739C81212F9F8C933D2C5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitted_m21111F0CD8563874ED9AD3A5047C8FF9EE246A35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitRequested_m34B52FBD3818A49E20F5E84D9292074CF34CE62E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitRequested_m72E233F50A99B23B152EBA5DC8C809AD4B4215CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23__ctor_mC4E5407730B1F65B6EC62E910146B3622143C7A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_IDisposable_Dispose_mB937EC683AE10AB216D121A11E6C668FAB08B458(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m020C881CD53FB93BACC71AB995500894AB90C692(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_Reset_m295E47A42C79C8C71B8EEDEA1FECE0D7625EA37F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_get_Current_mD6B3E13C1C0C3322A2959E8AEED0BC6FB2F30BDF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26__ctor_m8279C00DD68770A1A3B9E7E112AB03B2470FD15C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_IDisposable_Dispose_m166F001DFF96386BCD41ACB6BC044BCCDCB82520(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F538F4E1C37EE1D6ED46A6F0650E7D661A840E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_Reset_m9ED897F280F6D9C26054BA2B1C3287F5C6D3381C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_get_Current_mCE6AB66AD30ED3D455300DFC06217893C416382D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass43_0_t5D55D2BF6B69E32E1377262B6647C5306F42C971_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OrientationHandler_t47CA6401465B0E879913E7656B801F9C4726D63A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void OrientationHandler_t47CA6401465B0E879913E7656B801F9C4726D63A_CustomAttributesCacheGenerator_res(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void PersistentImages_t60D3F687A28AFE34A4A1C33A2D0B6539023E37AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var), NULL);
	}
}
static void PersistentImages_t60D3F687A28AFE34A4A1C33A2D0B6539023E37AD_CustomAttributesCacheGenerator_saveKey(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void PersistentImages_t60D3F687A28AFE34A4A1C33A2D0B6539023E37AD_CustomAttributesCacheGenerator_spriteIndex(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_testConfiguration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x74\x65\x73\x74\x69\x6E\x67\x2E"), NULL);
	}
}
static void PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_targetAudience(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x65\x20\x69\x66\x20\x6E\x65\x65\x64\x65\x64\x2E\xA\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x61\x72\x67\x65\x74\x69\x6E\x67"), NULL);
	}
}
static void PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_keyWords(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x65\x20\x69\x66\x20\x6E\x65\x65\x64\x65\x64\x2E\xA\x53\x6F\x72\x72\x79\x2C\x20\x55\x6E\x69\x74\x79\x20\x64\x69\x72\x65\x63\x74\x65\x64\x20\x64\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x6D\x69\x73\x73\x69\x6E\x67\x20\x69\x6E\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79"), NULL);
	}
}
static void PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_eventsBanner(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x62\x61\x6E\x6E\x65\x72"), NULL);
	}
}
static void PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_eventsInterstitial(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x69\x6E\x74\x65\x72\x73\x74\x69\x74\x69\x61\x6C"), NULL);
	}
}
static void PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_eventsRewardBasedVideo(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x72\x65\x77\x61\x72\x64\x65\x64\x2D\x76\x69\x64\x65\x6F"), NULL);
	}
}
static void C_Birthday_tE586A02F95261CF8005A6506AEA99BE40C5765A2_CustomAttributesCacheGenerator_enable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x74\x72\x61\x6E\x73\x66\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x69\x72\x74\x68\x64\x61\x79\x2E"), NULL);
	}
}
static void C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_genderConfig(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x7C\x20\x47\x65\x6E\x64\x65\x72\xA\x53\x65\x74\x20\x74\x68\x65\x20\x67\x65\x6E\x64\x65\x72\x20\x77\x69\x74\x68\x20\x27\x53\x65\x74\x47\x65\x6E\x64\x65\x72\x28\x2E\x2E\x2E\x29\x27\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x61\x75\x64\x69\x65\x6E\x63\x65\x2E\x20\xA\x54\x6F\x20\x74\x61\x6B\x65\x20\x61\x66\x66\x65\x63\x74\x20\x62\x65\x66\x6F\x72\x65\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x61\x64\x20\x69\x73\x20\x72\x65\x71\x75\x65\x73\x74\x65\x64\x2C\x20\x73\x65\x74\x20\x74\x68\x65\x20\x62\x69\x72\x74\x68\x64\x61\x79\x20\x69\x6E\x20\x27\x41\x77\x61\x6B\x65\x28\x29\x27\x2E"), NULL);
	}
}
static void C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_birthday(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x7C\x20\x64\x61\x79\x2E\x6D\x6F\x6E\x74\x68\x2E\x79\x65\x61\x72\xA\x53\x65\x74\x20\x74\x68\x65\x20\x62\x69\x72\x74\x68\x64\x61\x79\x20\x77\x69\x74\x68\x20\x27\x53\x65\x74\x42\x69\x72\x74\x68\x64\x61\x79\x28\x2E\x2E\x2E\x29\x27\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x61\x75\x64\x69\x65\x6E\x63\x65\x2E\x20\xA\x54\x6F\x20\x74\x61\x6B\x65\x20\x61\x66\x66\x65\x63\x74\x20\x62\x65\x66\x6F\x72\x65\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x61\x64\x20\x69\x73\x20\x72\x65\x71\x75\x65\x73\x74\x65\x64\x2C\x20\x73\x65\x74\x20\x74\x68\x65\x20\x62\x69\x72\x74\x68\x64\x61\x79\x20\x69\x6E\x20\x27\x41\x77\x61\x6B\x65\x28\x29\x27\x2E"), NULL);
	}
}
static void C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_TagForChildDirectedTreatment(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x7C\x20\x54\x61\x67\xA\x4E\x6F\x74\x20\x63\x68\x61\x6E\x67\x65\x61\x62\x6C\x65\x20\x69\x6E\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_tag_for_under_age_of_consent(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x7C\x20\x54\x61\x67\xA\x4E\x6F\x74\x20\x63\x68\x61\x6E\x67\x65\x61\x62\x6C\x65\x20\x69\x6E\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_max_ad_content_rating(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x7C\x20\x52\x61\x74\x69\x6E\x67\xA\x4E\x6F\x74\x20\x63\x68\x61\x6E\x67\x65\x61\x62\x6C\x65\x20\x69\x6E\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void C_TestConfiguration_t6E9C78B598D61DFC756560B71875DC4F6667D6DD_CustomAttributesCacheGenerator_debugOutput(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x77\x20\x61\x6C\x6C\x20\x64\x65\x62\x75\x67\x20\x6F\x75\x74\x70\x75\x74\x20\x6D\x65\x73\x73\x61\x67\x65\x73\x2E\x20\x50\x6C\x65\x61\x73\x65\x20\x61\x64\x64\x20\x61\x20\x74\x65\x78\x74\x20\x62\x6F\x78\x20\x77\x69\x74\x68\x20\x61\x64\x65\x71\x75\x61\x74\x65\x20\x73\x70\x61\x63\x65\x2E"), NULL);
	}
}
static void C_TestConfiguration_t6E9C78B598D61DFC756560B71875DC4F6667D6DD_CustomAttributesCacheGenerator_testDevicesEnabled(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x73\x20\x27\x41\x64\x52\x65\x71\x75\x65\x73\x74\x2E\x54\x65\x73\x74\x44\x65\x76\x69\x63\x65\x53\x69\x6D\x75\x6C\x61\x74\x6F\x72\x27\x20\x2B\x20\x27\x54\x65\x73\x74\x20\x44\x65\x76\x69\x63\x65\x73\x27\x2E\xA\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x23\x65\x6E\x61\x62\x6C\x65\x5F\x74\x65\x73\x74\x5F\x64\x65\x76\x69\x63\x65\x73"), NULL);
	}
}
static void C_TestConfiguration_t6E9C78B598D61DFC756560B71875DC4F6667D6DD_CustomAttributesCacheGenerator_testDevices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x23\x65\x6E\x61\x62\x6C\x65\x5F\x74\x65\x73\x74\x5F\x64\x65\x76\x69\x63\x65\x73"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_appID_ANDROID(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x61\x6E\x64\x72\x6F\x69\x64\x2F\x71\x75\x69\x63\x6B\x2D\x73\x74\x61\x72\x74\x20\xA\x53\x61\x6D\x70\x6C\x65\x20\x41\x64\x4D\x6F\x62\x20\x41\x70\x70\x20\x49\x44\x3A\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x7E\x33\x33\x34\x37\x35\x31\x31\x37\x31\x33"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_banner_adUnitID_ANDROID(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x2F\x36\x33\x30\x30\x39\x37\x38\x31\x31\x31"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_Interstitial_adUnitID_ANDROID(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x2F\x31\x30\x33\x33\x31\x37\x33\x37\x31\x32"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_RewardBasedVideo_adUnitID_ANDROID(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x2F\x35\x32\x32\x34\x33\x35\x34\x39\x31\x37"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_appID_IPHONE(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x69\x6F\x73\x2F\x71\x75\x69\x63\x6B\x2D\x73\x74\x61\x72\x74\x20\xA\x53\x61\x6D\x70\x6C\x65\x20\x41\x64\x4D\x6F\x62\x20\x61\x70\x70\x20\x49\x44\x3A\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x7E\x31\x34\x35\x38\x30\x30\x32\x35\x31\x31"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_banner_adUnitID_IPHONE(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x2F\x32\x39\x33\x34\x37\x33\x35\x37\x31\x36"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_Interstitial_adUnitID_IPHONE(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x2F\x34\x34\x31\x31\x34\x36\x38\x39\x31\x30"), NULL);
	}
}
static void C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_RewardBasedVideo_adUnitID_IPHONE(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x65\x76\x65\x6C\x6F\x70\x65\x72\x73\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x61\x64\x6D\x6F\x62\x2F\x75\x6E\x69\x74\x79\x2F\x74\x65\x73\x74\x2D\x61\x64\x73\x20\xA\x63\x61\x2D\x61\x70\x70\x2D\x70\x75\x62\x2D\x33\x39\x34\x30\x32\x35\x36\x30\x39\x39\x39\x34\x32\x35\x34\x34\x2F\x31\x37\x31\x32\x34\x38\x35\x33\x31\x33"), NULL);
	}
}
static void C_GeneralEvents_tFE79D1B0409C2A741E8CD12A459CFD580F874A97_CustomAttributesCacheGenerator_OnInitialization(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x69\x6E\x69\x74\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x64\x6F\x6E\x65\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x61\x20\x67\x6F\x6F\x64\x20\x70\x6C\x61\x63\x65\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x61\x64\x20\x64\x65\x70\x65\x6E\x64\x61\x6E\x74\x20\x76\x61\x6C\x75\x65\x73\x2E"), NULL);
	}
}
static void C_GeneralEvents_tFE79D1B0409C2A741E8CD12A459CFD580F874A97_CustomAttributesCacheGenerator_OnError(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x65\x72\x72\x6F\x72\x20\x6F\x63\x63\x75\x72\x65\x64\x2E\x20\x4D\x6F\x72\x65\x20\x64\x65\x74\x61\x69\x6C\x73\x20\x63\x61\x6E\x20\x62\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x65\x72\x72\x6F\x72\x20\x65\x76\x65\x6E\x74\x73\x20\x6F\x66\x20\x62\x61\x6E\x6E\x65\x72\x2C\x20\x69\x6E\x74\x65\x72\x73\x74\x69\x74\x69\x61\x6C\x20\x6F\x72\x20\x72\x65\x77\x61\x72\x64\x42\x61\x73\x65\x64\x56\x69\x64\x65\x6F\x73\x2E"), NULL);
	}
}
static void C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnAdFailesToLoad(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x66\x61\x69\x6C\x65\x64\x20\x74\x6F\x20\x6C\x6F\x61\x64\x2E"), NULL);
	}
}
static void C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnINTERNAL_ERROR(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x6D\x65\x74\x68\x69\x6E\x67\x20\x68\x61\x70\x70\x65\x6E\x65\x64\x20\x69\x6E\x74\x65\x72\x6E\x61\x6C\x6C\x79\x3B\x20\x66\x6F\x72\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x2C\x20\x61\x6E\x20\x69\x6E\x76\x61\x6C\x69\x64\x20\x72\x65\x73\x70\x6F\x6E\x73\x65\x20\x77\x61\x73\x20\x72\x65\x63\x65\x69\x76\x65\x64\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x61\x64\x20\x73\x65\x72\x76\x65\x72\x2E"), NULL);
	}
}
static void C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnINVALID_REQUEST(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x77\x61\x73\x20\x69\x6E\x76\x61\x6C\x69\x64\x3B\x20\x66\x6F\x72\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x2C\x20\x74\x68\x65\x20\x61\x64\x20\x75\x6E\x69\x74\x20\x49\x44\x20\x77\x61\x73\x20\x69\x6E\x63\x6F\x72\x72\x65\x63\x74\x2E"), NULL);
	}
}
static void C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnNETWORK_ERROR(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x77\x61\x73\x20\x75\x6E\x73\x75\x63\x63\x65\x73\x73\x66\x75\x6C\x20\x64\x75\x65\x20\x74\x6F\x20\x6E\x65\x74\x77\x6F\x72\x6B\x20\x63\x6F\x6E\x6E\x65\x63\x74\x69\x76\x69\x74\x79\x2E"), NULL);
	}
}
static void C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnNO_FILL(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x77\x61\x73\x20\x73\x75\x63\x63\x65\x73\x73\x66\x75\x6C\x2C\x20\x62\x75\x74\x20\x6E\x6F\x20\x61\x64\x20\x77\x61\x73\x20\x72\x65\x74\x75\x72\x6E\x65\x64\x20\x64\x75\x65\x20\x74\x6F\x20\x6C\x61\x63\x6B\x20\x6F\x66\x20\x61\x64\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E"), NULL);
	}
}
static void C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdLoaded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x68\x61\x73\x20\x73\x75\x63\x63\x65\x73\x73\x66\x75\x6C\x6C\x79\x20\x6C\x6F\x61\x64\x65\x64\x2E"), NULL);
	}
}
static void C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdOpened(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x69\x73\x20\x63\x6C\x69\x63\x6B\x65\x64\x2E"), NULL);
	}
}
static void C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdLeftApplication(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x6D\x65\x74\x68\x6F\x64\x20\x69\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x61\x66\x74\x65\x72\x20\x6F\x6E\x41\x64\x4F\x70\x65\x6E\x65\x64\x28\x29\x2C\x20\x77\x68\x65\x6E\x20\x61\x20\x75\x73\x65\x72\x20\x63\x6C\x69\x63\x6B\x20\x6F\x70\x65\x6E\x73\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x61\x70\x70\x20\x28\x73\x75\x63\x68\x20\x61\x73\x20\x74\x68\x65\x20\x47\x6F\x6F\x67\x6C\x65\x20\x50\x6C\x61\x79\x29\x2C\x20\x62\x61\x63\x6B\x67\x72\x6F\x75\x6E\x64\x69\x6E\x67\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x61\x70\x70\x2E"), NULL);
	}
}
static void C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdClosed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x61\x20\x75\x73\x65\x72\x20\x72\x65\x74\x75\x72\x6E\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x61\x70\x70\x20\x61\x66\x74\x65\x72\x20\x76\x69\x65\x77\x69\x6E\x67\x20\x61\x6E\x20\x61\x64\x27\x73\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x55\x52\x4C\x2C\x20\x74\x68\x69\x73\x20\x6D\x65\x74\x68\x6F\x64\x20\x69\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x2E\x20\x59\x6F\x75\x72\x20\x61\x70\x70\x20\x63\x61\x6E\x20\x75\x73\x65\x20\x69\x74\x20\x74\x6F\x20\x72\x65\x73\x75\x6D\x65\x20\x73\x75\x73\x70\x65\x6E\x64\x65\x64\x20\x61\x63\x74\x69\x76\x69\x74\x69\x65\x73\x20\x6F\x72\x20\x70\x65\x72\x66\x6F\x72\x6D\x20\x61\x6E\x79\x20\x6F\x74\x68\x65\x72\x20\x77\x6F\x72\x6B\x20\x6E\x65\x63\x65\x73\x73\x61\x72\x79\x20\x74\x6F\x20\x6D\x61\x6B\x65\x20\x69\x74\x73\x65\x6C\x66\x20\x72\x65\x61\x64\x79\x20\x66\x6F\x72\x20\x69\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_errorEvents(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x66\x61\x69\x6C\x65\x64\x20\x74\x6F\x20\x6C\x6F\x61\x64\x2E"), NULL);
	}
}
static void C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialLoaded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x68\x61\x73\x20\x73\x75\x63\x63\x65\x73\x73\x66\x75\x6C\x6C\x79\x20\x6C\x6F\x61\x64\x65\x64\x2E"), NULL);
	}
}
static void C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialOpened(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x6D\x65\x74\x68\x6F\x64\x20\x69\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x61\x64\x20\x69\x73\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x2C\x20\x63\x6F\x76\x65\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x64\x65\x76\x69\x63\x65\x27\x73\x20\x73\x63\x72\x65\x65\x6E\x2E"), NULL);
	}
}
static void C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialLeftApplication(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x6D\x65\x74\x68\x6F\x64\x20\x69\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x75\x73\x65\x72\x20\x63\x6C\x69\x63\x6B\x20\x6F\x70\x65\x6E\x73\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x61\x70\x70\x20\x28\x73\x75\x63\x68\x20\x61\x73\x20\x74\x68\x65\x20\x47\x6F\x6F\x67\x6C\x65\x20\x50\x6C\x61\x79\x29\x2C\x20\x62\x61\x63\x6B\x67\x72\x6F\x75\x6E\x64\x69\x6E\x67\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x61\x70\x70\x2E"), NULL);
	}
}
static void C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialClosed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x6D\x65\x74\x68\x6F\x64\x20\x69\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x77\x68\x65\x6E\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x72\x73\x74\x69\x74\x69\x61\x6C\x20\x61\x64\x20\x69\x73\x20\x63\x6C\x6F\x73\x65\x64\x20\x64\x75\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x74\x61\x70\x70\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x63\x6C\x6F\x73\x65\x20\x69\x63\x6F\x6E\x20\x6F\x72\x20\x75\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x62\x61\x63\x6B\x20\x62\x75\x74\x74\x6F\x6E\x2E\x20\x49\x66\x20\x79\x6F\x75\x72\x20\x61\x70\x70\x20\x70\x61\x75\x73\x65\x64\x20\x69\x74\x73\x20\x61\x75\x64\x69\x6F\x20\x6F\x75\x74\x70\x75\x74\x20\x6F\x72\x20\x67\x61\x6D\x65\x20\x6C\x6F\x6F\x70\x2C\x20\x74\x68\x69\x73\x20\x69\x73\x20\x61\x20\x67\x72\x65\x61\x74\x20\x70\x6C\x61\x63\x65\x20\x74\x6F\x20\x72\x65\x73\x75\x6D\x65\x20\x69\x74\x2E"), NULL);
	}
}
static void C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_errorEvents(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x66\x61\x69\x6C\x65\x64\x20\x74\x6F\x20\x6C\x6F\x61\x64\x2E"), NULL);
	}
}
static void C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_HandleRewardBasedVideoLoaded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x68\x61\x73\x20\x73\x75\x63\x63\x65\x73\x73\x66\x75\x6C\x6C\x79\x20\x6C\x6F\x61\x64\x65\x64\x2E"), NULL);
	}
}
static void C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoOpened(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x69\x73\x20\x73\x68\x6F\x77\x6E\x2E"), NULL);
	}
}
static void C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoStarted(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x61\x64\x20\x73\x74\x61\x72\x74\x73\x20\x74\x6F\x20\x70\x6C\x61\x79\x2E"), NULL);
	}
}
static void C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoClosed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x61\x64\x20\x69\x73\x20\x63\x6C\x6F\x73\x65\x64\x2E"), NULL);
	}
}
static void C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoRewarded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x72\x65\x77\x61\x72\x64\x65\x64\x20\x66\x6F\x72\x20\x77\x61\x74\x63\x68\x69\x6E\x67\x20\x61\x20\x76\x69\x64\x65\x6F\x2E"), NULL);
	}
}
static void C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoLeftApplication(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x61\x64\x20\x63\x6C\x69\x63\x6B\x20\x63\x61\x75\x73\x65\x64\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x74\x6F\x20\x6C\x65\x61\x76\x65\x20\x74\x68\x65\x20\x61\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_errorEvents(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x61\x64\x20\x72\x65\x71\x75\x65\x73\x74\x20\x66\x61\x69\x6C\x65\x64\x20\x74\x6F\x20\x6C\x6F\x61\x64\x2E"), NULL);
	}
}
static void PlayUnityAd_t2868479CC495D3827DD380E8359E907DEC30820A_CustomAttributesCacheGenerator_watchAdButton(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x77\x61\x74\x63\x68\x41\x64\x42\x75\x74\x74\x6F\x6E\x27\x20\x69\x73\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2E\x20\x49\x66\x20\x61\x6E\x20\x61\x64\x76\x65\x72\x74\x69\x73\x65\x6D\x65\x6E\x74\x20\x69\x73\x20\x61\x76\x61\x69\x6C\x61\x62\x6C\x65\x2C\x20\x74\x68\x65\x20\x62\x75\x74\x74\x6F\x6E\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x65\x74\x20\x74\x6F\x20\x69\x6E\x74\x65\x72\x61\x63\x74\x61\x62\x6C\x65\x2E\x20\x54\x6F\x20\x73\x68\x6F\x77\x20\x61\x64\x20\x77\x69\x74\x68\x20\x62\x75\x74\x74\x6F\x6E\x2C\x20\x63\x61\x6C\x6C\x20\x27\x73\x68\x6F\x77\x52\x65\x77\x61\x72\x64\x65\x64\x41\x64\x28\x29\x27\x2E"), NULL);
	}
}
static void PlayUnityAd_t2868479CC495D3827DD380E8359E907DEC30820A_CustomAttributesCacheGenerator_rewardedAd(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x73\x20\x74\x68\x65\x20\x61\x64\x20\x72\x65\x77\x61\x72\x64\x65\x64\x3F\x20\x49\x66\x20\x79\x65\x73\x2C\x20\x74\x68\x65\x20\x63\x61\x6C\x6C\x62\x61\x63\x6B\x73\x20\x6F\x6E\x20\x73\x75\x63\x63\x65\x73\x73\x2F\x66\x61\x69\x6C\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x63\x61\x6C\x6C\x65\x64\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x63\x61\x6E\x27\x74\x20\x73\x6B\x69\x70\x20\x74\x68\x65\x20\x41\x64\x2E\x20\x45\x6C\x73\x65\x20\x74\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x2E"), NULL);
	}
}
static void PlayUnityAd_t2868479CC495D3827DD380E8359E907DEC30820A_CustomAttributesCacheGenerator_PlayUnityAd_testForAdvertisement_mE3C461C09C83B9B6851C5F210F5D00D86A1DE8B2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_0_0_0_var), NULL);
	}
}
static void U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17__ctor_m16D037C642106BB04305682B117FCE27C0C75741(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_IDisposable_Dispose_m9BC3A92ED2FB1954E5445B8A8391649DFA6957E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8D44465EF33772A073C5411A44D97351670F236(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_Reset_mE310632C52E3A44CC1892E2669BAB3E63554441F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_get_Current_m7B7B5E24FB0E178AC03D8AC548434E2FD3C3C9E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RatioHandler_tF1A4115B838BAF70A9D91F98B9661183178139B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void RatioHandler_tF1A4115B838BAF70A9D91F98B9661183178139B4_CustomAttributesCacheGenerator_res(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void RatioHandler_tF1A4115B838BAF70A9D91F98B9661183178139B4_CustomAttributesCacheGenerator_ratio(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void ScrollbarIncrementer_tDDEDBA4EF68223F1CED63FDD8E68D178DC18B881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var), NULL);
	}
}
static void LoadScene_t633887ABD67B5712F8CCC7C5511422C0E45084D4_CustomAttributesCacheGenerator_LoadScene_delayedLoad_m6E48431E35D2FF55B1CE9B57CD496DDDCE98ED7E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_0_0_0_var), NULL);
	}
}
static void U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4__ctor_m2FEDB7CE5A9390BE1C53532F8ABFBB6A6B962324(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_IDisposable_Dispose_mF3089973DD6F7FB4A8C274A44A16B5CD2BAFE9FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B0A66E43371D0D1D55312BED6363508A1A4AEAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_Reset_m3463BD91919D2175B690C1A4BD4E2B9CA498A3B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_get_Current_m6B830331C28C6C1601A5DA6711C53B509B5D8C09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator__audioClips(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x73\x74\x20\x6F\x66\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x61\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x73\x20\x66\x6F\x72\x20\x70\x6C\x61\x79\x69\x6E\x67\x20\x69\x6E\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_mainAudio(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x73\x6F\x75\x72\x63\x65\x20\x66\x6F\x72\x20\x70\x6C\x61\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x73\x2E"), NULL);
	}
}
static void MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_playRandomAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x6F\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x75\x64\x69\x20\x63\x6C\x69\x70\x73\x20\x72\x61\x6E\x64\x6F\x6D\x6C\x79\x20\x61\x74\x20\x61\x63\x74\x69\x76\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_loopSong(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x6F\x70\x20\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x61\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x20\x28\x6F\x72\x20\x70\x6C\x61\x79\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x63\x6C\x69\x70\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x69\x73\x74\x29\x2E"), NULL);
	}
}
static void MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_fadeDuration(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_MusicPlayer_multimediaTimer_mEEEA7ED5D30B61543F2FF340A50006CC2AF3AF56(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_0_0_0_var), NULL);
	}
}
static void U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16__ctor_m9452FF5F9C621B2701F14069A953C1D7EDCB639E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_IDisposable_Dispose_mB7EE0025ABA8196757B8B19E53D0AF877CCC729E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAFBBFFDC0FAD295792E66E7EB0185FBAAC74A4C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_Reset_m2123A5E79B2C708B0EBAF93621053EABA2B1F5B0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_get_Current_m1B45D5AAF2D3A164F2C5DB0B61FFB79517282150(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void saveSlider_t5DC0D51375EAD5363DCE8DFE87D9E940BD0B12CC_CustomAttributesCacheGenerator_saveSlider_saveKeyDelayed_m38B8C3376FE5ECCC0C8F95B8635C123D009A3EE8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_0_0_0_var), NULL);
	}
}
static void U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13__ctor_m2DC30CBDF8E9E74DCFCDF4A344C2534AA4428DAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_IDisposable_Dispose_m9C6DD24E2BD62BB987123DA584DB83021628DCCA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C9015DDBDB5E34979C1FA1F6F29463EB224629(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_Reset_m3D7E598789A659776E64F9B635AEA7FE312067AE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_get_Current_mC4966B1DBE9334F30DC9633FB3C843A517F178AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_direction(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_swipeScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x20\x74\x68\x65\x20\x73\x77\x69\x70\x65\x20\x66\x6F\x72\x20\x67\x65\x74\x74\x69\x6E\x67\x20\x69\x74\x20\x77\x69\x74\x68\x20\x27\x67\x65\x74\x53\x63\x61\x6C\x65\x64\x53\x77\x69\x70\x65\x56\x65\x63\x74\x6F\x72\x28\x29\x27\x2E\xA\x54\x68\x69\x73\x20\x69\x73\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x6C\x69\x6E\x6B\x69\x6E\x67\x20\x69\x74\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64\x20\x74\x72\x65\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_pressed(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_mousePressed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_keyActive(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_actualSwipeDistance(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void normSwipe_t53252C7CD3A172D85DD4DF252E134CABFA1F3730_CustomAttributesCacheGenerator_swipeDetectionLimit_UD(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x66\x74\x65\x72\x20\x77\x68\x69\x63\x68\x20\x73\x77\x69\x70\x65\x20\x75\x70\x2F\x64\x6F\x77\x6E\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x69\x73\x20\x61\x6E\x20\x65\x76\x65\x6E\x74\x20\x65\x78\x65\x63\x75\x74\x65\x64\x3F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void normSwipe_t53252C7CD3A172D85DD4DF252E134CABFA1F3730_CustomAttributesCacheGenerator_swipeDetectionLimit_LR(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x66\x74\x65\x72\x20\x77\x68\x69\x63\x68\x20\x73\x77\x69\x70\x65\x20\x6C\x65\x66\x74\x2F\x72\x69\x67\x68\x74\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x69\x73\x20\x61\x6E\x20\x65\x76\x65\x6E\x74\x20\x65\x78\x65\x63\x75\x74\x65\x64\x3F"), NULL);
	}
}
static void C_IdleConfig_tDDDBC553C5821BD46A93B13C910C5A3B38EC762B_CustomAttributesCacheGenerator_idleCurve(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x77\x69\x70\x65\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x66\x61\x6B\x65\x20\x75\x6E\x74\x69\x6C\x20\x75\x73\x65\x72\x20\x69\x6E\x74\x65\x72\x61\x63\x74\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x73\x63\x72\x65\x65\x6E\x2E"), NULL);
	}
}
static void C_IdleConfig_tDDDBC553C5821BD46A93B13C910C5A3B38EC762B_CustomAttributesCacheGenerator_idleTime(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_IdleConfig_tDDDBC553C5821BD46A93B13C910C5A3B38EC762B_CustomAttributesCacheGenerator_evaluation(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lerpSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 10.0f, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_center(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_left(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_right(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_up(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_down(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lerpedPosition(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_targetPosition(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_movementActiveTime(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lerpStart(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lastSetDirection(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void TextReplacementDisplay_t6B34544842E8F084AF34375EE2F28B60726CAC3A_CustomAttributesCacheGenerator_TextReplacementDisplay_delay_mF23DE8A32576410DE6033F2477529F1961C63972(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_0_0_0_var), NULL);
	}
}
static void TextReplacementDisplay_t6B34544842E8F084AF34375EE2F28B60726CAC3A_CustomAttributesCacheGenerator_TextReplacementDisplay_cyclicActualization_m718D870A00CF1715C2A47C08E227BA09A94D963B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_0_0_0_var), NULL);
	}
}
static void U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4__ctor_m7CA16C5D26BDC3223FF857AC48BB7DEE4F7D5506(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_IDisposable_Dispose_m0B66E4484A0C881A4CD168C373129C0076E53E37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF00484BABD02F82575C68F2D73EC3FF32471CFD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_Collections_IEnumerator_Reset_m89DC582C5637A0A9AB60E090B359DBD98470A49F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_Collections_IEnumerator_get_Current_m1CFAA7DE6B7CC9C41DD1A9BC07A52468D9A68C2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5__ctor_m8E38305516B55E2E423B4C542D8D7EB0CEF3BC51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_IDisposable_Dispose_m81C78D44F1B5A21BD65D249322F6767F9911C53E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3C2A0AF0B4BC30F2B8923FDFF34739B2892AE3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_Reset_m9CCBFCFEF27DF16814DBF2C2C6171DC25D2C4304(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_get_Current_m7BEEAC4BD998A473557796E64438100C0E920B23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TranslationManager_tCC3959EA463B943E0D7D653FE9D63D13C7B85540_CustomAttributesCacheGenerator_saveState(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void TranslationManager_tCC3959EA463B943E0D7D653FE9D63D13C7B85540_CustomAttributesCacheGenerator_translateableContents(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void UIPositionSwitcher_t2B00DE52A2A714D4420DD1CCD89A5752FCA31407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
}
static void ValueChangePreview_tF3009FAEAD938C6987FF7FB28B2AC6B9B074DBE8_CustomAttributesCacheGenerator_noChanges(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x61\x20\x73\x70\x72\x69\x74\x65\x20\x69\x66\x20\x6E\x6F\x74\x68\x69\x6E\x67\x20\x63\x68\x61\x6E\x67\x65\x73\x2E\x20\x49\x66\x20\x6E\x6F\x74\x68\x69\x6E\x67\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x73\x75\x70\x70\x6C\x79\x20\x61\x6E\x20\x74\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x74\x20\x73\x70\x72\x69\x74\x65\x2E"), NULL);
	}
}
static void valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_valueRises(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x65\x77\x20\x73\x70\x72\x69\x74\x65\x20\x69\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x72\x69\x73\x65\x73\x2E"), NULL);
	}
}
static void valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_valueFalls(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x65\x77\x20\x73\x70\x72\x69\x74\x65\x20\x69\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x66\x61\x6C\x6C\x73\x2E"), NULL);
	}
}
static void valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_valueUnclear(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x70\x72\x69\x74\x65\x20\x69\x66\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x6E\x6F\x74\x20\x63\x6C\x65\x61\x72\x2E\x20\x54\x68\x69\x73\x20\x63\x61\x6E\x20\x68\x61\x70\x70\x65\x6E\x20\x69\x66\x20\x74\x68\x65\x20\x6F\x75\x74\x63\x6F\x6D\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x61\x6E\x64\x6F\x6D\x69\x7A\x65\x64\x20\x69\x6E\x20\x73\x6F\x6D\x65\x20\x77\x61\x79\x2E"), NULL);
	}
}
static void valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_spriteSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x63\x61\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x72\x69\x74\x65\x20\x64\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x2E\x20\x41\x64\x6A\x75\x73\x74\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x20\x61\x78\x69\x73\x20\x74\x6F\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x2E\x20\x45\x2E\x20\x67\x2E\x20\x79\x6F\x75\x72\x20\x76\x61\x6C\x75\x65\x20\x63\x61\x6E\x20\x63\x68\x61\x6E\x67\x65\x20\x66\x72\x6F\x6D\x20\x30\x20\x74\x6F\x20\x31\x30\x30\x20\x74\x68\x65\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x69\x7A\x65\x20\x66\x6F\x72\x20\x61\x20\x74\x69\x6D\x65\x20\x66\x72\x6F\x6D\x20\x30\x20\x74\x6F\x20\x31\x30\x30\x2E"), NULL);
	}
}
static void valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_showTextValue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x77\x20\x74\x68\x65\x20\x61\x73\x20\x74\x65\x78\x74\x20\x69\x74\x20\x74\x68\x69\x73\x20\x73\x65\x74\x20\x69\x73\x20\x75\x73\x65\x64\x2E"), NULL);
	}
}
static void valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_textFormatter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x65\x78\x74\x20\x69\x73\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x61\x20\x66\x6F\x72\x6D\x61\x74\x74\x65\x72\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x68\x65\x72\x65\x2E\x20\x45\x2E\x20\x67\x2E\x20\x27\x30\x27\x20\x66\x6F\x72\x20\x73\x68\x6F\x77\x69\x6E\x67\x20\x77\x68\x6F\x6C\x65\x20\x6E\x75\x6D\x62\x65\x72\x73\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_vs(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x74\x6F\x20\x61\x6E\x20\x76\x61\x6C\x75\x65\x20\x73\x63\x72\x69\x70\x74\x2C\x20\x77\x68\x69\x63\x68\x20\x63\x6F\x6E\x74\x61\x69\x6E\x73\x20\x74\x68\x65\x20\x63\x6F\x6D\x70\x61\x72\x65\x20\x69\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x65\x6E\x61\x62\x6C\x69\x6E\x67\x2F\x64\x69\x73\x61\x62\x6C\x69\x6E\x67\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_valueTyp(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x77\x68\x69\x63\x68\x20\x6B\x69\x6E\x64\x20\x6F\x66\x20\x76\x61\x6C\x75\x65\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_currentValueText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_minValueText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x69\x6E\x69\x6D\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_maxValueText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_currentValueSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x6C\x69\x64\x65\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_minValueSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x6C\x69\x64\x65\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x69\x6E\x69\x6D\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_maxValueSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x6C\x69\x64\x65\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_currentValueImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x69\x6D\x61\x67\x65\x20\x74\x6F\x20\x66\x69\x6C\x6C\x20\x66\x6F\x72\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_minValueImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x69\x6D\x61\x67\x65\x20\x74\x6F\x20\x66\x69\x6C\x6C\x20\x66\x6F\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x69\x6E\x69\x6D\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_maxValueImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x69\x6D\x61\x67\x65\x20\x74\x6F\x20\x66\x69\x6C\x6C\x20\x66\x6F\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x69\x73\x20\x67\x61\x6D\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_formatter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x66\x6F\x72\x6D\x61\x74\x20\x66\x6F\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2E\x20\xA\x45\x2E\x67\x2E\x20\x30\x20\x6F\x72\x20\x23\x20\x66\x6F\x72\x20\x6F\x6E\x6C\x79\x20\x77\x68\x6F\x6C\x65\x20\x6E\x75\x6D\x62\x65\x72\x73\x2E\xA\x45\x2E\x67\x2E\x20\x23\x2E\x30\x30\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x77\x6F\x20\x66\x6F\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x64\x69\x67\x69\x74\x73\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_displayMultiplier(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x61\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72\x20\x66\x6F\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x69\x6E\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2E\x20\x54\x68\x69\x73\x20\x64\x6F\x65\x73\x27\x74\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_ValueDisplay_cycActualize_mC15584D6A4DFEB246FCA9306F3FCA55A62F40EF1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_0_0_0_var), NULL);
	}
}
static void U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15__ctor_m884AB86FB8663308B5D6E197DE3EEB5AF7A09B18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_IDisposable_Dispose_mDAA4568D0183CA0B11C556C49CBB15509765B1FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92D5C095669CE7EE955BD8782F09A4E5CA451711(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_Reset_m921D4CA172B2615F151E64DCDA76E4D1796B3A04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_get_Current_m405E4962AA5731495D4C3F97E5DC3DFCEC6773A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ValueMath_tDF6A728FA0D5F31B3B81EB923A943353464F2A7D_CustomAttributesCacheGenerator_AfterCalculation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x63\x61\x73\x63\x61\x64\x65\x64\x20\x63\x61\x6C\x63\x75\x61\x6C\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x6E\x65\x65\x64\x65\x64\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x74\x72\x69\x67\x67\x65\x72\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x76\x61\x6C\x75\x65\x4D\x61\x74\x68\x2E\x43\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E\x28\x29\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_valueType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x79\x6F\x75\x72\x20\x74\x79\x70\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x68\x65\x72\x65\x2E\x20\x50\x65\x72\x20\x76\x61\x6C\x75\x65\x20\x73\x63\x72\x69\x70\x74\x20\x6F\x6E\x6C\x79\x20\x6F\x6E\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x69\x73\x20\x61\x6C\x6C\x6F\x77\x65\x64\x2E"), NULL);
	}
}
static void ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_UserInterface(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x65\x61\x63\x68\x20\x76\x61\x6C\x75\x65\x20\x73\x63\x72\x69\x70\x74\x20\x74\x68\x65\x72\x65\x20\x63\x61\x6E\x20\x62\x65\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x77\x61\x79\x73\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2E\x20\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x64\x65\x74\x61\x69\x6C\x73\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_keepValue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x6B\x65\x65\x70\x56\x61\x6C\x75\x65\x27\x20\x62\x6C\x6F\x63\x6B\x73\x20\x74\x68\x65\x20\x72\x61\x6E\x64\x6F\x6D\x69\x7A\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E\x20\x61\x20\x6E\x65\x77\x20\x67\x61\x6D\x65\x20\x73\x74\x61\x72\x74\x2E\x20\x4F\x6E\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x73\x74\x61\x72\x74\x75\x70\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x2C\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x72\x61\x6E\x64\x6F\x6D\x69\x7A\x65\x64\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x27\x4C\x69\x6D\x69\x74\x73\x2E\x52\x61\x6E\x64\x6F\x6D\x4D\x69\x6E\x27\x20\x61\x6E\x64\x20\x27\x4C\x69\x6D\x69\x74\x73\x2E\x52\x61\x6E\x64\x6F\x6D\x4D\x61\x78\x27\x20\x28\x41\x63\x63\x65\x73\x73\x61\x62\x6C\x65\x20\x66\x72\x6F\x6D\x20\x49\x6E\x73\x70\x65\x63\x74\x6F\x72\x29\x2E"), NULL);
	}
}
static void ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_events(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x20\x6F\x6E\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x6F\x72\x20\x69\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x61\x74\x20\x6F\x6E\x65\x20\x6F\x66\x20\x69\x74\x73\x20\x6C\x69\x6D\x69\x74\x73\x2E"), NULL);
	}
}
static void valueDependantIcon_t45DB15F740610F572E1F18293ABBC7EBDFFD5019_CustomAttributesCacheGenerator_baseIcons(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x69\x6D\x61\x67\x65\x28\x73\x29\x2C\x20\x77\x68\x69\x63\x68\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x63\x68\x61\x6E\x67\x65\x64\x20\x64\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2E\x20\x49\x66\x20\x6E\x6F\x20\x62\x61\x73\x65\x49\x63\x6F\x6E\x20\x69\x73\x20\x64\x65\x66\x69\x6E\x65\x64\x2C\x20\x6E\x6F\x74\x68\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x63\x68\x61\x6E\x67\x65\x64\x2E"), NULL);
	}
}
static void valueDependantIcon_t45DB15F740610F572E1F18293ABBC7EBDFFD5019_CustomAttributesCacheGenerator_valueIcon(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x76\x61\x6C\x75\x65\x20\x74\x68\x65\x20\x62\x61\x73\x65\x20\x69\x63\x6F\x6E\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x70\x6C\x61\x63\x65\x64\x2E"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_uiScrollbar(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x73\x68\x61\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x61\x73\x20\x61\x20\x66\x69\x6C\x6C\x69\x6E\x67\x20\x62\x61\x72\x20\x69\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x61\x20\x73\x63\x72\x6F\x6C\x6C\x62\x61\x72\x20\x6F\x72\x20\x61\x20\x73\x6C\x69\x64\x65\x72\x2E\x20\x44\x65\x66\x69\x6E\x65\x20\x79\x6F\x75\x72\x20\x70\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_uiSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x73\x68\x61\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x61\x73\x20\x61\x20\x66\x69\x6C\x6C\x69\x6E\x67\x20\x62\x61\x72\x20\x69\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x61\x20\x73\x63\x72\x6F\x6C\x6C\x62\x61\x72\x20\x6F\x72\x20\x61\x20\x73\x6C\x69\x64\x65\x72\x2E\x20\x44\x65\x66\x69\x6E\x65\x20\x79\x6F\x75\x72\x20\x70\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_lerpSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x70\x65\x65\x64\x20\x6F\x66\x20\x66\x69\x6C\x6C\x69\x6E\x67\x20\x74\x68\x65\x20\x62\x61\x72\x2C\x20\x69\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 100.0f, NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_formatter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x66\x6F\x72\x6D\x61\x74\x20\x66\x6F\x72\x20\x64\x69\x73\x70\x6C\x61\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2E\x20\xA\x45\x2E\x67\x2E\x20\x30\x20\x6F\x72\x20\x23\x20\x66\x6F\x72\x20\x6F\x6E\x6C\x79\x20\x77\x68\x6F\x6C\x65\x20\x6E\x75\x6D\x62\x65\x72\x73\x2E\xA\x45\x2E\x67\x2E\x20\x23\x2E\x30\x30\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x77\x6F\x20\x66\x6F\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x64\x69\x67\x69\x74\x73\x2E"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_lerpedValue(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x6C\x65\x72\x70\x65\x64\x2F\x66\x69\x6C\x6C\x69\x6E\x67\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_textValue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x61\x73\x20\x74\x65\x78\x74\x2C\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x68\x65\x72\x65\x2E"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_showActualization(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x6D\x61\x6E\x61\x67\x65\x72\x20\x63\x61\x6E\x20\x73\x68\x6F\x77\x20\x61\x20\x63\x68\x61\x6E\x67\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x64\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x27\x73\x68\x6F\x77\x41\x63\x74\x75\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x27"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_miniatureSprite(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x6D\x61\x6E\x61\x67\x65\x72\x20\x63\x61\x6E\x20\x73\x68\x6F\x77\x20\x61\x20\x63\x68\x61\x6E\x67\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x77\x69\x74\x68\x20\x74\x68\x69\x73\x20\x6D\x69\x6E\x69\x61\x74\x75\x72\x65\x20\x73\x70\x72\x69\x74\x65\x2E"), NULL);
	}
}
static void uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_valueDependingIcons(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x63\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x20\x64\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x65\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x79\x70\x65\x2E"), NULL);
	}
}
static void uiValueChange_t4D8C11E87C42BB3C76298533E3A6C10FB03D3AF6_CustomAttributesCacheGenerator_valueChangeImage(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x75\x70\x63\x6F\x6D\x69\x6D\x69\x6E\x67\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x20\x63\x61\x6E\x20\x62\x65\x20\x69\x6E\x64\x69\x63\x61\x74\x65\x64\x20\x62\x79\x20\x61\x6E\x20\x69\x6D\x61\x67\x65\x2E\x20\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x69\x6D\x61\x67\x65\x20\x77\x68\x65\x72\x65\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x69\x74\x2E\xA\x54\x68\x65\x20\x73\x70\x72\x69\x74\x65\x73\x20\x61\x72\x65\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x27\x56\x61\x6C\x75\x65\x43\x68\x61\x6E\x67\x65\x50\x72\x65\x76\x69\x65\x77\x27\x2E"), NULL);
	}
}
static void uiValueChange_t4D8C11E87C42BB3C76298533E3A6C10FB03D3AF6_CustomAttributesCacheGenerator_valueChangeText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x75\x70\x63\x6F\x6D\x69\x6D\x69\x6E\x67\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x68\x6F\x77\x6E\x20\x69\x6E\x20\x61\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x2E\x20\x44\x65\x66\x69\x6E\x65\x20\x77\x68\x65\x72\x65\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x69\x74\x2E"), NULL);
	}
}
static void valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_min(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x61\x74\x20\x69\x73\x20\x74\x68\x65\x20\x6D\x69\x6E\x69\x6D\x75\x6D\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x76\x61\x6C\x75\x65\x3F"), NULL);
	}
}
static void valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_max(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x61\x74\x20\x69\x73\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x76\x61\x6C\x75\x65\x3F"), NULL);
	}
}
static void valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_randomMin(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x69\x6E\x69\x74\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x20\x77\x69\x74\x68\x20\x72\x61\x6E\x64\x6F\x6D\x20\x76\x61\x6C\x75\x65\x73\x3A\x20\x57\x68\x61\x74\x20\x69\x73\x20\x74\x68\x65\x20\x6D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x61\x6E\x64\x6F\x6D\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_randomMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x69\x6E\x69\x74\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x20\x77\x69\x74\x68\x20\x72\x61\x6E\x64\x6F\x6D\x20\x76\x61\x6C\x75\x65\x73\x3A\x20\x57\x68\x61\x74\x20\x69\x73\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x72\x61\x6E\x64\x6F\x6D\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_roundToWholeNumbers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x69\x6E\x69\x74\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x20\x77\x69\x74\x68\x20\x72\x61\x6E\x64\x6F\x6D\x20\x76\x61\x6C\x75\x65\x73\x3A\x20\x52\x6F\x75\x6E\x64\x20\x74\x6F\x20\x77\x68\x6F\x6C\x65\x20\x6E\x75\x6D\x62\x65\x72\x73\x2E"), NULL);
	}
}
static void VideoPlayerEvents_t9860577076381595D80F2508CF7A87141CBF1A78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86_0_0_0_var), NULL);
	}
}
static void C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnClockResyncOccured(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x6F\x6B\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x56\x69\x64\x65\x6F\x50\x6C\x61\x79\x65\x72\x20\x63\x6C\x6F\x63\x6B\x20\x69\x73\x20\x73\x79\x6E\x63\x65\x64\x20\x62\x61\x63\x6B\x20\x74\x6F\x20\x69\x74\x73\x20\x56\x69\x64\x65\x6F\x54\x69\x6D\x65\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x2E"), NULL);
	}
}
static void C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnErrorReceived(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x72\x72\x6F\x72\x73\x20\x73\x75\x63\x68\x20\x61\x73\x20\x48\x54\x54\x50\x20\x63\x6F\x6E\x6E\x65\x63\x74\x69\x6F\x6E\x20\x70\x72\x6F\x62\x6C\x65\x6D\x73\x20\x61\x72\x65\x20\x72\x65\x70\x6F\x72\x74\x65\x64\x20\x74\x68\x72\x6F\x75\x67\x68\x20\x74\x68\x69\x73\x20\x63\x61\x6C\x6C\x62\x61\x63\x6B\x2E"), NULL);
	}
}
static void C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnEndReached(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x6F\x6B\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x6E\x65\x77\x20\x66\x72\x61\x6D\x65\x20\x69\x73\x20\x72\x65\x61\x64\x79\x2E"), NULL);
	}
}
static void C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnPrepareCompleted(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x6F\x6B\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x56\x69\x64\x65\x6F\x50\x6C\x61\x79\x65\x72\x20\x70\x72\x65\x70\x61\x72\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x2E"), NULL);
	}
}
static void C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnSeekCompleted(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x6F\x6B\x65\x20\x61\x66\x74\x65\x72\x20\x61\x20\x73\x65\x65\x6B\x20\x6F\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x73\x2E"), NULL);
	}
}
static void C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnStarted(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x6F\x6B\x65\x64\x20\x69\x6D\x6D\x65\x64\x69\x61\x74\x65\x6C\x79\x20\x61\x66\x74\x65\x72\x20\x50\x6C\x61\x79\x20\x69\x73\x20\x63\x61\x6C\x6C\x65\x64\x2E"), NULL);
	}
}
static void addValue_tEFECB703EE7CA5A646EA2725CA007E833B09FE59_CustomAttributesCacheGenerator_valuesToChange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x77\x68\x65\x6E\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x27\x61\x64\x64\x56\x61\x6C\x75\x65\x73\x28\x29\x27"), NULL);
	}
}
static void addValue_tEFECB703EE7CA5A646EA2725CA007E833B09FE59_CustomAttributesCacheGenerator_addValue_addValues_m380AC79EC53FE31C0C81B7E88AD66C4FD2E2D53D(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x27\x61\x64\x64\x56\x61\x6C\x75\x65\x73\x28\x29\x27\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x50\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x27\x43\x68\x61\x6E\x67\x65\x56\x61\x6C\x75\x65\x73\x28\x29\x27\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x27\x63\x68\x61\x6E\x67\x65\x56\x61\x6C\x75\x65\x2E\x63\x73\x27\x20\x69\x6E\x73\x74\x65\x61\x64\x2E\x20\x27\x61\x64\x64\x56\x61\x6C\x75\x65\x73\x28\x29\x27\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E"), NULL);
	}
}
static void addValueToValue_tBC24EFDFE4AE9DECEFEAA263EF2E2873F15383D8_CustomAttributesCacheGenerator_valuesToChange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x77\x68\x65\x6E\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x27\x61\x64\x64\x56\x61\x6C\x75\x65\x73\x28\x29\x27"), NULL);
	}
}
static void changeValue_t1F36255930E6663D3CC38BBBCB689F847FC47FD8_CustomAttributesCacheGenerator_valuesToChange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x77\x68\x65\x6E\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x27\x43\x68\x61\x6E\x67\x65\x56\x61\x6C\x75\x65\x73\x28\x29\x27"), NULL);
	}
}
static void conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_preConditions(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x61\x72\x65\x20\x6F\x6E\x6C\x79\x20\x65\x78\x65\x63\x75\x74\x65\x64\x2C\x20\x69\x66\x20\x61\x6C\x6C\x20\x70\x72\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x74\x72\x75\x65\x2E"), NULL);
	}
}
static void conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_testTiming(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x74\x65\x73\x74\x73\x20\x61\x6E\x64\x20\x69\x6E\x76\x6F\x6B\x65\x73\x20\x61\x72\x65\x20\x63\x6F\x6D\x70\x75\x74\x65\x64\x3A\xA\x6F\x6E\x43\x61\x72\x64\x44\x65\x73\x74\x72\x6F\x79\x3A\x20\x41\x66\x74\x65\x72\x20\x61\x20\x63\x61\x72\x64\x20\x69\x73\x20\x64\x65\x73\x74\x72\x6F\x79\x65\x64\x2E\x20\x41\x74\x20\x74\x68\x69\x73\x20\x70\x6F\x69\x6E\x74\x20\x61\x6C\x6C\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x61\x72\x65\x20\x64\x6F\x6E\x65\x2E\xA\x63\x79\x63\x6C\x69\x63\x3A\x20\x54\x68\x65\x20\x74\x65\x73\x74\x20\x69\x73\x20\x64\x6F\x6E\x65\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x2E\x20\x53\x68\x6F\x75\x6C\x64\x20\x6F\x6E\x6C\x79\x20\x62\x65\x20\x75\x73\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x65\x78\x65\x63\x75\x74\x69\x6F\x6E\x20\x62\x65\x68\x61\x76\x69\x6F\x72\x20\x27\x6F\x6E\x53\x74\x61\x74\x65\x43\x68\x61\x6E\x67\x65\x27\x2C\x20\x65\x6C\x73\x65\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x61\x6E\x64\x20\x65\x76\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x65\x61\x63\x68\x20\x66\x72\x61\x6D\x65\x20\x61\x6E\x64\x20\x63\x61\x6E\x20\x6C\x65\x61\x64\x20\x74\x6F\x20\x73\x6F\x6D\x65\x20\x75\x6E\x77\x61\x6E\x74\x65\x64\x20\x62\x65\x68\x61\x76\x69\x6F\x72\x2E\xA\x6D\x61\x6E\x75\x61\x6C\x3A\x20\x54\x68\x65\x20\x74\x65\x73\x74\x20\x69\x73\x20\x6F\x6E\x6C\x79\x20\x64\x6F\x6E\x65\x20\x66\x72\x6F\x6D\x20\x61\x6E\x20\x65\x78\x74\x65\x72\x6E\x61\x6C\x20\x73\x6F\x75\x72\x63\x65\x2E\x20\x50\x6C\x65\x61\x73\x65\x20\x63\x61\x6C\x6C\x20\x74\x68\x65\x20\x6D\x65\x74\x68\x6F\x64\x20\x27\x45\x78\x65\x63\x75\x74\x65\x43\x68\x65\x63\x6B\x28\x29\x27\x20\x66\x72\x6F\x6D\x20\x61\x6E\x20\x65\x76\x65\x6E\x74\x20\x6F\x72\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x73\x63\x72\x69\x70\x74\x20\x74\x6F\x20\x64\x6F\x20\x73\x6F\x2E"), NULL);
	}
}
static void conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_executionBehavior(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x65\x78\x65\x63\x75\x74\x69\x6F\x6E\x20\x72\x65\x70\x65\x74\x69\x74\x69\x6F\x6E\x20\x6C\x6F\x67\x69\x63\x3A\xA\x61\x6C\x77\x61\x79\x73\x3A\x20\x45\x76\x65\x72\x79\x20\x74\x69\x6D\x65\x20\x61\x20\x63\x68\x65\x63\x6B\x20\x69\x73\x20\x64\x6F\x6E\x65\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x6D\x65\x74\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x69\x6E\x76\x6F\x6B\x65\x64\x2E\xA\x6F\x6E\x53\x74\x61\x74\x65\x43\x68\x61\x6E\x67\x65\x3A\x20\x45\x78\x65\x63\x75\x74\x65\x20\x69\x66\x20\x61\x6E\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x73\x74\x61\x74\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x28\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x72\x65\x73\x75\x6C\x74\x20\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E\x20\x66\x61\x6C\x73\x65\x20\x2D\x3E\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_conditionalActionList_eachFrame_m2D0D706D9FE437F1B7EE6F2B1C5EC027134612EF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_0_0_0_var), NULL);
	}
}
static void C_ValueContentToAction_t3C288DA07C2CFA0D49CA23EF0085C49D67108BD7_CustomAttributesCacheGenerator_elementCondition(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x69\x73\x20\x6F\x6E\x6C\x79\x20\x65\x78\x65\x63\x75\x74\x65\x64\x2C\x20\x69\x66\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x66\x6F\x72\x20\x69\x74\x20\x61\x72\x65\x20\x6D\x65\x74\x2E"), NULL);
	}
}
static void C_ValueContentToAction_t3C288DA07C2CFA0D49CA23EF0085C49D67108BD7_CustomAttributesCacheGenerator_evaluationResult(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void C_ValueContentToAction_t3C288DA07C2CFA0D49CA23EF0085C49D67108BD7_CustomAttributesCacheGenerator_lastEvaluationResult(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13__ctor_m7555B9ACB0F67EF10AE18E2C526B2D41E8D4955D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_IDisposable_Dispose_m0F460470A415E6489949AB9394BC1F8692CE282A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA8B187D2D5D200C97BEDECF74EB850597D69F56(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_Collections_IEnumerator_Reset_mE1C939B7483AA60C3C43B5A25C8FBB968DF43DDA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_Collections_IEnumerator_get_Current_mF23BE2C11DF83A40BB3750D9B86CCA27AC067457(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void scoreCounter_t1FD33B5D1E00DB982DE1297AA8FD5BE09405431F_CustomAttributesCacheGenerator_score(CustomAttributesCache* cache)
{
	{
		ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 * tmp = (ReadOnlyInspector_tF909CC270708081BEE3A47BC54BF060D50C07B38 *)cache->attributes[0];
		ReadOnlyInspector__ctor_mE0AD55C9602EF1EB108278A2C8D11DDB614A7469(tmp, NULL);
	}
}
static void setValue_t91BF21E4FCF27A76356201152E26474BFBAA9EB3_CustomAttributesCacheGenerator_valuesToChange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x77\x68\x65\x6E\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x27\x61\x64\x64\x56\x61\x6C\x75\x65\x73\x28\x29\x27"), NULL);
	}
}
static void setValue_t91BF21E4FCF27A76356201152E26474BFBAA9EB3_CustomAttributesCacheGenerator_setValue_setValues_m33499F09D12C3092076C485B179E9B09117FF180(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x27\x73\x65\x74\x56\x61\x6C\x75\x65\x73\x28\x29\x27\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x50\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x27\x43\x68\x61\x6E\x67\x65\x56\x61\x6C\x75\x65\x73\x28\x29\x27\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x72\x69\x70\x74\x20\x27\x63\x68\x61\x6E\x67\x65\x56\x61\x6C\x75\x65\x2E\x63\x73\x27\x20\x69\x6E\x73\x74\x65\x61\x64\x2E\x20\x27\x73\x65\x74\x56\x61\x6C\x75\x65\x73\x28\x29\x27\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6D\x6F\x76\x65\x64\x20\x69\x6E\x20\x66\x75\x74\x75\x72\x65\x20\x72\x65\x6C\x65\x61\x73\x65\x73\x2E"), NULL);
	}
}
static void valueDependentConditionalImages_tE54B4D8396C0BE42F0698840D475CA16CABD56E3_CustomAttributesCacheGenerator_actualizationType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x76\x61\x6C\x75\x65\x44\x65\x70\x65\x6E\x64\x65\x6E\x74\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x61\x6C\x49\x6D\x61\x67\x65\x73\x27\x20\x73\x75\x70\x70\x6F\x72\x74\x73\x20\x74\x77\x6F\x20\x6D\x6F\x64\x65\x73\x2E\xA\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x3A\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x74\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x2E\x20\xA\x6D\x61\x6E\x75\x61\x6C\x3A\x20\x20\x20\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x6F\x6E\x6C\x79\x20\x65\x76\x61\x6C\x75\x74\x61\x74\x65\x64\x20\x69\x66\x20\x74\x68\x65\x20\x66\x75\x6E\x63\x74\x69\x6F\x6E\x20\x27\x45\x78\x65\x63\x75\x74\x65\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x43\x68\x65\x63\x6B\x28\x29\x27\x20\x69\x73\x20\x63\x61\x6C\x6C\x65\x64\x2E\x20\x54\x68\x65\x20\x69\x6D\x61\x67\x65\x73\x20\x61\x72\x65\x20\x63\x68\x61\x6E\x67\x65\x64\x20\x61\x63\x63\x6F\x72\x64\x69\x6E\x67\x6C\x79\x2E"), NULL);
	}
}
static void valueDependentConditionalImages_tE54B4D8396C0BE42F0698840D475CA16CABD56E3_CustomAttributesCacheGenerator_valueDependentConditionalImages_oneFrame_mAB42BA6CFBBBCF5895EB5C08C3C426F70F6E1C27(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_0_0_0_var), NULL);
	}
}
static void U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9__ctor_m526235E2CA6FC5AB734BC6AB29863091F2F6F150(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_IDisposable_Dispose_m3473E1959E7822609AF060B96EE35B80428D6757(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD831E212FC446D5F0C503CE9F56D6B1AC815E565(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_Collections_IEnumerator_Reset_mED64782DBA020DC52AA1107DB00A44B2D7CD3D85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_Collections_IEnumerator_get_Current_m6779D76973F63196D278DE6A3E80386D47855A83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void valueDependentEnable_t9B899DC6FE53A0DB83F406F683E1152E7C7EA2C3_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x74\x6F\x20\x61\x6E\x20\x76\x61\x6C\x75\x65\x20\x73\x63\x72\x69\x70\x74\x2C\x20\x77\x68\x69\x63\x68\x20\x63\x6F\x6E\x74\x61\x69\x6E\x73\x20\x74\x68\x65\x20\x63\x6F\x6D\x70\x61\x72\x65\x20\x69\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x65\x6E\x61\x62\x6C\x69\x6E\x67\x2F\x64\x69\x73\x61\x62\x6C\x69\x6E\x67\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void valueDependentEnable_t9B899DC6FE53A0DB83F406F683E1152E7C7EA2C3_CustomAttributesCacheGenerator_go(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x2C\x20\x77\x68\x69\x63\x68\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x65\x6E\x61\x62\x6C\x65\x64\x2F\x64\x69\x73\x61\x62\x6C\x65\x64\x20\x64\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x76\x61\x6C\x75\x65\x2E\x20\x54\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x69\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x73\x63\x72\x69\x70\x74\x20\x69\x73\x20\x68\x69\x67\x68\x65\x72\x20\x74\x68\x61\x6E\x20\x74\x68\x65\x20\x6C\x69\x6D\x69\x74\x2E"), NULL);
	}
}
static void valueDependentEnable_t9B899DC6FE53A0DB83F406F683E1152E7C7EA2C3_CustomAttributesCacheGenerator_limitToEnable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x6D\x69\x74\x20\x66\x6F\x72\x20\x65\x6E\x61\x62\x6C\x65\x2F\x64\x69\x73\x61\x62\x6C\x65\x2E"), NULL);
	}
}
static void valueDependentEvent_t248446CEFB1249A642BAA80D17DF333B827B64D8_CustomAttributesCacheGenerator_triggerType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x76\x61\x6C\x75\x65\x44\x65\x70\x65\x6E\x64\x65\x6E\x74\x45\x76\x65\x6E\x74\x27\x20\x73\x75\x70\x70\x6F\x72\x74\x73\x20\x74\x77\x6F\x20\x6D\x6F\x64\x65\x73\x2E\xA\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x3A\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x74\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x2E\x20\x49\x66\x20\x74\x68\x65\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x72\x65\x73\x75\x6C\x74\x73\x20\x73\x77\x69\x74\x63\x68\x20\x66\x72\x6F\x6D\x20\x74\x72\x75\x65\x20\x74\x6F\x20\x66\x61\x6C\x73\x65\x20\x6F\x72\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x61\x6E\x20\x65\x76\x65\x6E\x74\x20\x69\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x6F\x6E\x63\x65\x2E\xA\x6D\x61\x6E\x75\x61\x6C\x3A\x20\x20\x20\x20\x63\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x20\x6F\x6E\x6C\x79\x20\x65\x76\x61\x6C\x75\x74\x61\x74\x65\x64\x20\x69\x66\x20\x74\x68\x65\x20\x66\x75\x6E\x63\x74\x69\x6F\x6E\x20\x27\x45\x78\x65\x63\x75\x74\x65\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x43\x68\x65\x63\x6B\x28\x29\x27\x20\x69\x73\x20\x63\x61\x6C\x6C\x65\x64\x2E\x20\x54\x68\x65\x20\x65\x76\x65\x6E\x74\x73\x20\x61\x72\x65\x20\x69\x6E\x76\x6F\x6B\x65\x64\x20\x61\x63\x63\x6F\x72\x64\x69\x6E\x67\x6C\x79\x2E"), NULL);
	}
}
static void valueDependentEvent_t248446CEFB1249A642BAA80D17DF333B827B64D8_CustomAttributesCacheGenerator_valueDependentEvent_oneFrame_m41A40BB51B3A6F5A3CFA19588732D053D843FAD0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_0_0_0_var), NULL);
	}
}
static void U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7__ctor_m853C858DD2E0A4C942AC48957E47B406F60F0AEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_IDisposable_Dispose_m69CD851696FFDC0061D1ECD990D56DA0B439B7B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m24083F1557E44580268A6637582B9AAA7D37BE63(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_Collections_IEnumerator_Reset_mF061980C2A1E65BDF89EAF28A2B5D9A9B4BB3A1B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_Collections_IEnumerator_get_Current_m52A004719D90B66FF54E4931166AAD5741764477(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void valueDependentImages_t7FDCC682A4DDBF736E82625CB91A11DF871017D7_CustomAttributesCacheGenerator_valueDependentImages_cycActulize_m9C138934BF325F4A8070605D6E61E0F45E9EE300(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_0_0_0_var), NULL);
	}
}
static void U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5__ctor_mDBBAFFFF8A92A55D8E3B43B8536C308D7D3A3433(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_m99F9DA8C4364C3F1AC80C2CF07B952136C1A64D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DAB6FA561D2922E658BF2B4B8AEF4BBF91CE4EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_mB32402A65A3CCC658A347C707C0FE56B68D4938A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_m0DE39A326FC147072798CCA18C508086C011E33F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void valueDependentTexts_t34FBF6069EA56165DBD17E6C06DDAFA4016CA251_CustomAttributesCacheGenerator_valueDependentTexts_cycActulize_m2BB64E52678442A5E7DBCA3B48D1147759CBAC20(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_0_0_0_var), NULL);
	}
}
static void U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5__ctor_mB0FEBDE8940FB3BCA8EE32254835E36CECEAD7E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_mF7D9DB7F1B0F96318D03BAE41FD73BD0057D56BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA1C19D6A96F6104F79E604D37F6E74EFDD5AADD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_m2836EF31113785AA83327C23B25385F6C218333F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_mB9051E40AE1D230D32FCD8D7C3C5F8BE831B451A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void valueDependentTimer_tB83525519CDA3C98979C5CF4A6E39372255C4F7F_CustomAttributesCacheGenerator_deltaTimeCycle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x64\x65\x6C\x74\x61\x54\x69\x6D\x65\x43\x79\x63\x6C\x65\x27\x20\x64\x65\x66\x69\x6E\x65\x73\x20\x77\x68\x69\x63\x68\x20\x74\x69\x6D\x65\x63\x79\x63\x6C\x65\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x74\x65\x73\x74\x65\x64\x20\x61\x6E\x64\x20\x61\x6C\x73\x6F\x20\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x73\x20\x69\x6E\x76\x6F\x6B\x65\x64\x2E"), NULL);
	}
}
static void valueDependentTimer_tB83525519CDA3C98979C5CF4A6E39372255C4F7F_CustomAttributesCacheGenerator_onlyCountWhileGameActive(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x27\x6F\x6E\x6C\x79\x43\x6F\x75\x6E\x74\x57\x68\x69\x6C\x65\x47\x61\x6D\x65\x41\x63\x74\x69\x76\x65\x27\x20\x63\x61\x6E\x20\x62\x6C\x6F\x63\x6B\x20\x74\x68\x65\x20\x63\x6F\x75\x6E\x74\x69\x6E\x67\x2C\x20\x77\x68\x69\x6C\x65\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x69\x73\x20\x6E\x6F\x74\x20\x6D\x6F\x76\x61\x62\x6C\x65\x20\x28\x65\x2E\x20\x67\x2E\x20\x77\x68\x69\x6C\x65\x20\x69\x6E\x73\x69\x64\x65\x20\x74\x68\x65\x20\x6D\x65\x6E\x75\x65\x20\x6F\x72\x20\x74\x68\x65\x20\x63\x61\x72\x64\x20\x73\x70\x61\x77\x6E\x73\x29\x2E\x20\x57\x68\x65\x6E\x20\x47\x61\x6D\x65\x6F\x76\x65\x72\x63\x61\x72\x64\x73\x20\x65\x74\x63\x2E\x20\x61\x72\x65\x20\x73\x70\x61\x77\x6E\x65\x64\x2C\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x20\x69\x73\x20\x73\x74\x69\x6C\x6C\x20\x61\x63\x74\x69\x76\x65\x2E"), NULL);
	}
}
static void valueDependentTimer_tB83525519CDA3C98979C5CF4A6E39372255C4F7F_CustomAttributesCacheGenerator_valueDependentTimer_cyclicTestValue_mF25FBA3D0CAC2801FCF2FEAE5E172B553CD1BA15(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_0_0_0_var), NULL);
	}
}
static void U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9__ctor_m242DBED8F79E038793145948FBB5B3053E5C7C08(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_IDisposable_Dispose_mE0B503F8CEA59D250D9A01B27100568467D9DC4D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0575A7EE9E712A8D39FA707CA1AE13A417AF27B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_Reset_m78215A7BE3957DE9415B27EF8DCECA35B47D8B38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_get_Current_m68C1793EA5E692C4DAC56318B4758A4491CBBF2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void valueManager_t68B8E4DE543CD0BC1A6A1990A9984AE9DA0EA58D_CustomAttributesCacheGenerator_values(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x6C\x20\x76\x61\x6C\x75\x65\x20\x73\x63\x72\x69\x70\x74\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
}
static void SafeArea_t44D5A2651DC2DA47B987B9097B633F510DC18156_CustomAttributesCacheGenerator_ConformX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SafeArea_t44D5A2651DC2DA47B987B9097B633F510DC18156_CustomAttributesCacheGenerator_ConformY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SafeAreaDemo_tC7C1D7D1BC5FA083D4A382C37576B0C06FBECE35_CustomAttributesCacheGenerator_KeySafeArea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LogicalStateMachineBehaviour_t29135DFA940C8C1648223E7441587548C1F7C25C_CustomAttributesCacheGenerator_U3CAnimatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogicalStateMachineBehaviour_t29135DFA940C8C1648223E7441587548C1F7C25C_CustomAttributesCacheGenerator_LogicalStateMachineBehaviour_get_Animator_mDC4507F8809D9FB66D14DD9F1ECDECA694B50F51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogicalStateMachineBehaviour_t29135DFA940C8C1648223E7441587548C1F7C25C_CustomAttributesCacheGenerator_LogicalStateMachineBehaviour_set_Animator_mE95CB9BB31E180CFF313B6587C993E61F7C8C4AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[676] = 
{
	U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator,
	U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator,
	U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator,
	U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator,
	HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator,
	U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator,
	U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator,
	U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator,
	U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator,
	U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator,
	U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator,
	CardStyle_tFDD0A60B627DE74092636DE9CA41EE2180925AC4_CustomAttributesCacheGenerator,
	U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator,
	U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator,
	U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator,
	GameDictionary_InputField_tC5B0367D22637D7CCF43FF0DC50F3A5F82DA885E_CustomAttributesCacheGenerator,
	U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator,
	U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator,
	U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator,
	U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator,
	KingsCardStyle_tFF207306B0BCCD825EF173E4770A8B7C3C30280F_CustomAttributesCacheGenerator,
	KingsCardStyleList_tA48C528A7BB67AA6B3F33DE08BA00AD8FB146796_CustomAttributesCacheGenerator,
	U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator,
	U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator,
	U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator,
	U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass43_0_t5D55D2BF6B69E32E1377262B6647C5306F42C971_CustomAttributesCacheGenerator,
	OrientationHandler_t47CA6401465B0E879913E7656B801F9C4726D63A_CustomAttributesCacheGenerator,
	PersistentImages_t60D3F687A28AFE34A4A1C33A2D0B6539023E37AD_CustomAttributesCacheGenerator,
	U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator,
	RatioHandler_tF1A4115B838BAF70A9D91F98B9661183178139B4_CustomAttributesCacheGenerator,
	ScrollbarIncrementer_tDDEDBA4EF68223F1CED63FDD8E68D178DC18B881_CustomAttributesCacheGenerator,
	U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator,
	U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator,
	U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator,
	U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator,
	U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator,
	UIPositionSwitcher_t2B00DE52A2A714D4420DD1CCD89A5752FCA31407_CustomAttributesCacheGenerator,
	U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator,
	VideoPlayerEvents_t9860577076381595D80F2508CF7A87141CBF1A78_CustomAttributesCacheGenerator,
	U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator,
	U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator,
	U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator,
	U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator,
	U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator,
	U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_verboseDebug,
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_catalog,
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_inventory,
	C_ItemDetail_t93A956A946A2E44CFC84E88F42DFD6B4FA53B63D_CustomAttributesCacheGenerator_selectedElement,
	C_ItemDetail_t93A956A946A2E44CFC84E88F42DFD6B4FA53B63D_CustomAttributesCacheGenerator_uiItem,
	C_ItemDetail_t93A956A946A2E44CFC84E88F42DFD6B4FA53B63D_CustomAttributesCacheGenerator_spawnedDetailViewPanel,
	C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_uiItem,
	C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_newItems,
	C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_spawnedNewItemPanel,
	C_ItemUpdateInformation_tC75130A9DEC21CC9C114EB93B2718B8901A4CE4A_CustomAttributesCacheGenerator_lockedForShowing,
	C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_itemKey,
	C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_item,
	C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_amount,
	C_ItemAmount_t6120053FBBAA1C3ABE63A2BEEF6CAB720B29C991_CustomAttributesCacheGenerator_loadedFromCatalog,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_itemName,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_description,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_consumeText,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_image,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_maxItems,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_visible,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_consumeType,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_category,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_newItemOverridePanel,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator_detailItemOverridePanel,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_itemAmount,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_amountText,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_itemImage,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_itemText,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_descriptionText,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_consumeButton,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_consumeText,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_iconGraphics,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_primaryGraphics,
	Inventory_UIItem_t55F8B563D65663374B34C24E4A1EB5E2CA042C57_CustomAttributesCacheGenerator_secondaryGraphics,
	Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_UITemplate,
	Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_filterType,
	Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_categoryFilter,
	Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_listElements,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_questRepeatType,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_activatabilityConditions,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_questTitle,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_description,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_rewardText,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_rewardImage,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_image,
	QuestDefinition_t569764134E5C0C6E55A03E409D7EE032BB545791_CustomAttributesCacheGenerator_fullfilmentConditions,
	Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator__questState,
	Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_questTitle,
	Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_questImage,
	Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_descriptionText,
	Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_fullfillmentToggle,
	Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_rewardText,
	Quest_UIItem_tA6F64143154C5409BB352A81E1FAF368BE03A209_CustomAttributesCacheGenerator_rewardImage,
	Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_UI_OpenTemplate,
	Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_UI_ActiveTemplate,
	Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_UI_CompletedTemplate,
	Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_questDisplayFilters,
	Quest_UIList_tC13A7D784A314E0EA7FA619E29A4BB0CC0B08EFC_CustomAttributesCacheGenerator_listElements,
	Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_maxNrOfActiveQuests,
	Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_autoRefillActiveQuests,
	Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_catalog,
	Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_questBook,
	C_QuestFullfillPopup_t48F7A228CA366616CE659042114B52272982EF1E_CustomAttributesCacheGenerator_selectedElement,
	C_QuestFullfillPopup_t48F7A228CA366616CE659042114B52272982EF1E_CustomAttributesCacheGenerator_uiItem,
	C_QuestFullfillPopup_t48F7A228CA366616CE659042114B52272982EF1E_CustomAttributesCacheGenerator_spawnedQuestPopupPanel,
	C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_questKey,
	C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_quest,
	C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_activeState,
	C_QuestState_t48A3FB58AFDE66890019F1491223B0A1311071B3_CustomAttributesCacheGenerator_loadedFromCatalog,
	C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_nrOfQuests,
	C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_nrOfActiveQuests,
	C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_nrOfFinishedQuests,
	C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_quests,
	C_Quests_t569732D3296CCB7E44784696516DA62EDD9C06D4_CustomAttributesCacheGenerator_activeQuests,
	HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator_title,
	HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator_description,
	HistoryEvent_tAF5B5A3C581F1C14B75BBEB77C978B191498E7F5_CustomAttributesCacheGenerator_image,
	Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_verboseDebug,
	Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_maxYear,
	Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_catalog,
	Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_history,
	Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_displayedHistory,
	C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_year,
	C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_addText,
	C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_timelineKey,
	C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_historyEvent,
	C_TimelineData_t27C2B4E6598B34874195277B5FC25A3FCDA4EC4E_CustomAttributesCacheGenerator_loadedFromCatalog,
	C_History_t2E8D63E06C17AA8AAA0169F9DCE6FECE8AB84144_CustomAttributesCacheGenerator_startYearOffset,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_UITemplate,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_historyScrollbar,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_yearsText,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_autoScrollbarMove,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_scrollbarSpeed,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_scrollbarAnimationDelay,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_scrollbarAnimationSpeed,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_displayFillerEvents,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_nrOfPostFillers,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_listElements,
	Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator__timelineData,
	Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_eventYear,
	Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_eventImage,
	Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_eventTitle,
	Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_descriptionText,
	Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_addText,
	Timeline_UIItem_t2FC93DFE86D542B30AA455E4CD26BDAC4108EC63_CustomAttributesCacheGenerator_combinedDescriptionText,
	AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_achievementAnimator,
	AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_triggerOnAchievement,
	AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_anim_titleText,
	AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_anim_descriptionText,
	AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_anim_achievementImage,
	AchievementsScript_t7E5DD1EE07BCBFA7CB1502C3FB99B753273E4CF2_CustomAttributesCacheGenerator_achievementProgressText,
	achievementConfig_tB0059C19EA046A74E2D2D1267DC02422921AB5FB_CustomAttributesCacheGenerator_typ,
	achievementConfig_tB0059C19EA046A74E2D2D1267DC02422921AB5FB_CustomAttributesCacheGenerator_achievementCnt,
	achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_achievementTarget,
	achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_achievementGameobject,
	achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_title,
	achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_description,
	achievementStage_tBCC2DFA8FD40C469A94A19787A27703DA147FFEA_CustomAttributesCacheGenerator_sprite,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_allCards,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_cardDrawCount,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_cardBlockCount,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_availableCards,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_followUpCard,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_highPriorityCards,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_spawnedCard,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_fallBackCard,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_swipe,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_moveBackSpeed,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_moveOutSpeed,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_actMoveDistance,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_CardParent,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_moveOutMax,
	cardCategory_t8CFDCE2F3C6B3A783ABFD5FE0E30FAB994C5F6AE_CustomAttributesCacheGenerator_groupName,
	cardCategory_t8CFDCE2F3C6B3A783ABFD5FE0E30FAB994C5F6AE_CustomAttributesCacheGenerator_subStackCondition,
	C_CardImages_t52505B44C1DAEAA640CAB8B05A2490558A332990_CustomAttributesCacheGenerator_color,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_gender,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_Countries,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_countryText,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_nameText,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_countryAndNameText,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_countryAndNameTextFormat,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_country,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_givenName,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_surname,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_vs_type_gender,
	subStringList_tD4A5695D6353194B0700B5A9AA9B99BC5E2AD172_CustomAttributesCacheGenerator_listEntry,
	subStringList_tD4A5695D6353194B0700B5A9AA9B99BC5E2AD172_CustomAttributesCacheGenerator_nameComb,
	subStringList_tD4A5695D6353194B0700B5A9AA9B99BC5E2AD172_CustomAttributesCacheGenerator_surname,
	DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_retriggerable,
	DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_unscaledTime,
	DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_startOnStart,
	DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_AfterDelay,
	DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_timerActive,
	GlobalMessageEventReceiver_tDA1B0B374131903574EBFD88C16A03E75E78B316_CustomAttributesCacheGenerator_MessageEvents,
	GlobalMessageEventReceiver_tDA1B0B374131903574EBFD88C16A03E75E78B316_CustomAttributesCacheGenerator_MessageImages,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_textFields,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_isHighPriorityCard,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_isDrawable,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_cardPropability,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_maxDraws,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_redrawBlockCnt,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_conditions,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_swipeType,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_additionalChoices,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_changeExtrasOnCardDespawn,
	EventScript_t0E2769F4F42F767B014F21C3CD278DE44FB5AD0F_CustomAttributesCacheGenerator_changeValueOnCardDespawn,
	modifierGroup_t6BDC7DBBD28F4B74B28A2FA84B12ED34A612B9E7_CustomAttributesCacheGenerator_followUpCard,
	result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_modifiers,
	result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_conditions,
	result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_modifiersTrue,
	result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_modifiersFalse,
	result_tE040C3C54B6B3B71CD4353A2882D29DBD106E58B_CustomAttributesCacheGenerator_randomModifiers,
	resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultLeft,
	resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultRight,
	resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultUp,
	resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_resultDown,
	resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_additional_choice_0,
	resultGroup_t2B16E7ED1478F023217091EFB5EEEF8CDC95BC70_CustomAttributesCacheGenerator_additional_choice_1,
	GameDictionary_InputField_tC5B0367D22637D7CCF43FF0DC50F3A5F82DA885E_CustomAttributesCacheGenerator_DictionaryKey,
	GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_gameLogText,
	GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_logs,
	GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_splitLogs,
	GameLogger_t88B12CCBB8FD2203552DEB0C0EAADDFC46364C03_CustomAttributesCacheGenerator_textBreakEvery,
	GameStateManager_t581A50F0CEB605A7870C3A10FC14F9CAA6879A67_CustomAttributesCacheGenerator_swipeCounter,
	GameStateManager_t581A50F0CEB605A7870C3A10FC14F9CAA6879A67_CustomAttributesCacheGenerator_gamestate,
	GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_valueType,
	GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_genders,
	GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_outText,
	GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_outImg,
	HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_highScoreSource,
	HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_highScoreSort,
	HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_key,
	C_cap_t6FCABDF845B0C9E865B3BDFA71E32843F8849948_CustomAttributesCacheGenerator_list,
	C_highscoreFields_t1D186C2D071A8AC88E4B158C5AACAF9B99597AD3_CustomAttributesCacheGenerator_countryNameText,
	C_highscoreFields_t1D186C2D071A8AC88E4B158C5AACAF9B99597AD3_CustomAttributesCacheGenerator_highScoreText,
	InfoDisplay_t6DBEFBA78D18777ED9BA880B207C209E7F15A2BC_CustomAttributesCacheGenerator_collectionTime,
	InfoDisplay_t6DBEFBA78D18777ED9BA880B207C209E7F15A2BC_CustomAttributesCacheGenerator_displayTime,
	KingsLevelUp_t0377874F7A9F38E1B68EE371E01208C9C2D23951_CustomAttributesCacheGenerator_stats,
	KingsLevelUp_t0377874F7A9F38E1B68EE371E01208C9C2D23951_CustomAttributesCacheGenerator_uiConfig,
	C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_maxLevel,
	C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_xpCosts,
	C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_actualLevel,
	C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_XpNextLevel,
	C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_XpAll,
	C_LevelUpStats_t4AE9A83F2938608431300DD13E3166E2EE2D52EA_CustomAttributesCacheGenerator_XpActual,
	C_XpPerLevel_tA9905A82356115C7ED585124488011846E546A3A_CustomAttributesCacheGenerator_fromLevel,
	C_XpPerLevel_tA9905A82356115C7ED585124488011846E546A3A_CustomAttributesCacheGenerator_xpCost,
	C_LevelUpUiConfig_tA6003940072089B62E1EEDC129C582B0FBF6F4B3_CustomAttributesCacheGenerator_xpBarFillSpeed,
	C_LevelUpUiConfig_tA6003940072089B62E1EEDC129C582B0FBF6F4B3_CustomAttributesCacheGenerator_levelIncreaseFillDelay,
	KingsTypewriter_t8F4F04BF8164DF484099D6DA0FBC074CE171B612_CustomAttributesCacheGenerator_typesPerSecond,
	KingsTypewriter_t8F4F04BF8164DF484099D6DA0FBC074CE171B612_CustomAttributesCacheGenerator_startDelay,
	Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_StatusText,
	Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_achievementText,
	Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_scoresToTransmit,
	Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_achievements,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_leaderBoardName,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_leaderBoardID,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreKey,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_maxScoreKey,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmitOkU3Ek__BackingField,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmitFailU3Ek__BackingField,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmittedU3Ek__BackingField,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__transmitRequestedU3Ek__BackingField,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__scoreActualU3Ek__BackingField,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__scoreMaxU3Ek__BackingField,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__score_maxTransmittedU3Ek__BackingField,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_U3C__scoreToTransmitU3Ek__BackingField,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_AchievementName,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_AchievementID,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_scoreKey,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_MinScoreValue,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__reachedU3Ek__BackingField,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmitOkU3Ek__BackingField,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmitFailU3Ek__BackingField,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmittedU3Ek__BackingField,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_U3C__transmitRequestedU3Ek__BackingField,
	OrientationHandler_t47CA6401465B0E879913E7656B801F9C4726D63A_CustomAttributesCacheGenerator_res,
	PersistentImages_t60D3F687A28AFE34A4A1C33A2D0B6539023E37AD_CustomAttributesCacheGenerator_saveKey,
	PersistentImages_t60D3F687A28AFE34A4A1C33A2D0B6539023E37AD_CustomAttributesCacheGenerator_spriteIndex,
	PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_testConfiguration,
	PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_targetAudience,
	PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_keyWords,
	PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_eventsBanner,
	PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_eventsInterstitial,
	PlayAdMob_t83E3F96FCAE51D1BB01B787620AA421111AE2AF0_CustomAttributesCacheGenerator_eventsRewardBasedVideo,
	C_Birthday_tE586A02F95261CF8005A6506AEA99BE40C5765A2_CustomAttributesCacheGenerator_enable,
	C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_genderConfig,
	C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_birthday,
	C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_TagForChildDirectedTreatment,
	C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_tag_for_under_age_of_consent,
	C_TargetAudience_tBC867F8E4B37B81F1CD744EA31D9E4D64EEBDDE6_CustomAttributesCacheGenerator_max_ad_content_rating,
	C_TestConfiguration_t6E9C78B598D61DFC756560B71875DC4F6667D6DD_CustomAttributesCacheGenerator_debugOutput,
	C_TestConfiguration_t6E9C78B598D61DFC756560B71875DC4F6667D6DD_CustomAttributesCacheGenerator_testDevicesEnabled,
	C_TestConfiguration_t6E9C78B598D61DFC756560B71875DC4F6667D6DD_CustomAttributesCacheGenerator_testDevices,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_appID_ANDROID,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_banner_adUnitID_ANDROID,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_Interstitial_adUnitID_ANDROID,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_RewardBasedVideo_adUnitID_ANDROID,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_appID_IPHONE,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_banner_adUnitID_IPHONE,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_Interstitial_adUnitID_IPHONE,
	C_IDS_t3F7FD3E111DA36F1325E3A9968AE8E7B4B2FE8EC_CustomAttributesCacheGenerator_RewardBasedVideo_adUnitID_IPHONE,
	C_GeneralEvents_tFE79D1B0409C2A741E8CD12A459CFD580F874A97_CustomAttributesCacheGenerator_OnInitialization,
	C_GeneralEvents_tFE79D1B0409C2A741E8CD12A459CFD580F874A97_CustomAttributesCacheGenerator_OnError,
	C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnAdFailesToLoad,
	C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnINTERNAL_ERROR,
	C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnINVALID_REQUEST,
	C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnNETWORK_ERROR,
	C_Error_Events_tF2BA38CAC7511B756EE080D6D483856542678883_CustomAttributesCacheGenerator_OnNO_FILL,
	C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdLoaded,
	C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdOpened,
	C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdLeftApplication,
	C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_OnAdClosed,
	C_Events_Banner_tFDAE0A272FE33C67D8B099DE82DDF04041659C39_CustomAttributesCacheGenerator_errorEvents,
	C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialLoaded,
	C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialOpened,
	C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialLeftApplication,
	C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_OnInterstitialClosed,
	C_Events_Interstitial_t22A6280911444F92BC12F024A826304B57E23A54_CustomAttributesCacheGenerator_errorEvents,
	C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_HandleRewardBasedVideoLoaded,
	C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoOpened,
	C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoStarted,
	C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoClosed,
	C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoRewarded,
	C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_OnRewardBasedVideoLeftApplication,
	C_Events_RewardBasedVideo_t76B312356E145509EDB093750257DA69B878BA74_CustomAttributesCacheGenerator_errorEvents,
	PlayUnityAd_t2868479CC495D3827DD380E8359E907DEC30820A_CustomAttributesCacheGenerator_watchAdButton,
	PlayUnityAd_t2868479CC495D3827DD380E8359E907DEC30820A_CustomAttributesCacheGenerator_rewardedAd,
	RatioHandler_tF1A4115B838BAF70A9D91F98B9661183178139B4_CustomAttributesCacheGenerator_res,
	RatioHandler_tF1A4115B838BAF70A9D91F98B9661183178139B4_CustomAttributesCacheGenerator_ratio,
	MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator__audioClips,
	MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_mainAudio,
	MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_playRandomAtStart,
	MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_loopSong,
	MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_fadeDuration,
	Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_direction,
	Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_swipeScale,
	Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_pressed,
	Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_mousePressed,
	Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_keyActive,
	Swipe_t40353CE9531ABF291C168BA6763354DB5B75D92C_CustomAttributesCacheGenerator_actualSwipeDistance,
	normSwipe_t53252C7CD3A172D85DD4DF252E134CABFA1F3730_CustomAttributesCacheGenerator_swipeDetectionLimit_UD,
	normSwipe_t53252C7CD3A172D85DD4DF252E134CABFA1F3730_CustomAttributesCacheGenerator_swipeDetectionLimit_LR,
	C_IdleConfig_tDDDBC553C5821BD46A93B13C910C5A3B38EC762B_CustomAttributesCacheGenerator_idleCurve,
	C_IdleConfig_tDDDBC553C5821BD46A93B13C910C5A3B38EC762B_CustomAttributesCacheGenerator_idleTime,
	C_IdleConfig_tDDDBC553C5821BD46A93B13C910C5A3B38EC762B_CustomAttributesCacheGenerator_evaluation,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lerpSpeed,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_center,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_left,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_right,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_up,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_down,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lerpedPosition,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_targetPosition,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_movementActiveTime,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lerpStart,
	C_ScreenPosition_t6F62834600A362E329FC65FC7D056B8801624464_CustomAttributesCacheGenerator_lastSetDirection,
	TranslationManager_tCC3959EA463B943E0D7D653FE9D63D13C7B85540_CustomAttributesCacheGenerator_saveState,
	TranslationManager_tCC3959EA463B943E0D7D653FE9D63D13C7B85540_CustomAttributesCacheGenerator_translateableContents,
	ValueChangePreview_tF3009FAEAD938C6987FF7FB28B2AC6B9B074DBE8_CustomAttributesCacheGenerator_noChanges,
	valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_valueRises,
	valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_valueFalls,
	valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_valueUnclear,
	valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_spriteSize,
	valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_showTextValue,
	valueChangeSpriteSet_t8D747865D47C79317FACA00EBE4FCC13E5A5EC3B_CustomAttributesCacheGenerator_textFormatter,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_vs,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_valueTyp,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_currentValueText,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_minValueText,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_maxValueText,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_currentValueSlider,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_minValueSlider,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_maxValueSlider,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_currentValueImage,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_minValueImage,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_maxValueImage,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_formatter,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_displayMultiplier,
	ValueMath_tDF6A728FA0D5F31B3B81EB923A943353464F2A7D_CustomAttributesCacheGenerator_AfterCalculation,
	ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_valueType,
	ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_value,
	ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_UserInterface,
	ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_keepValue,
	ValueScript_t68C5D3CE6F4DD6FAF3868BFAEB48B8650650C333_CustomAttributesCacheGenerator_events,
	valueDependantIcon_t45DB15F740610F572E1F18293ABBC7EBDFFD5019_CustomAttributesCacheGenerator_baseIcons,
	valueDependantIcon_t45DB15F740610F572E1F18293ABBC7EBDFFD5019_CustomAttributesCacheGenerator_valueIcon,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_uiScrollbar,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_uiSlider,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_lerpSpeed,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_formatter,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_lerpedValue,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_textValue,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_showActualization,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_miniatureSprite,
	uiConfig_tEE0EA57FC8494386CAFBBD6B48BE77E5E9EA0C57_CustomAttributesCacheGenerator_valueDependingIcons,
	uiValueChange_t4D8C11E87C42BB3C76298533E3A6C10FB03D3AF6_CustomAttributesCacheGenerator_valueChangeImage,
	uiValueChange_t4D8C11E87C42BB3C76298533E3A6C10FB03D3AF6_CustomAttributesCacheGenerator_valueChangeText,
	valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_min,
	valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_max,
	valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_randomMin,
	valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_randomMax,
	valueLimits_tBFC6A7DCC15C98478B9CCB7A8864DF5244AD04E9_CustomAttributesCacheGenerator_roundToWholeNumbers,
	C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnClockResyncOccured,
	C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnErrorReceived,
	C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnEndReached,
	C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnPrepareCompleted,
	C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnSeekCompleted,
	C_videoEvents_tE39E3A1DAC9494109C453FB3A27554F2ADFFE3F5_CustomAttributesCacheGenerator_OnStarted,
	addValue_tEFECB703EE7CA5A646EA2725CA007E833B09FE59_CustomAttributesCacheGenerator_valuesToChange,
	addValueToValue_tBC24EFDFE4AE9DECEFEAA263EF2E2873F15383D8_CustomAttributesCacheGenerator_valuesToChange,
	changeValue_t1F36255930E6663D3CC38BBBCB689F847FC47FD8_CustomAttributesCacheGenerator_valuesToChange,
	conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_preConditions,
	conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_testTiming,
	conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_executionBehavior,
	C_ValueContentToAction_t3C288DA07C2CFA0D49CA23EF0085C49D67108BD7_CustomAttributesCacheGenerator_elementCondition,
	C_ValueContentToAction_t3C288DA07C2CFA0D49CA23EF0085C49D67108BD7_CustomAttributesCacheGenerator_evaluationResult,
	C_ValueContentToAction_t3C288DA07C2CFA0D49CA23EF0085C49D67108BD7_CustomAttributesCacheGenerator_lastEvaluationResult,
	scoreCounter_t1FD33B5D1E00DB982DE1297AA8FD5BE09405431F_CustomAttributesCacheGenerator_score,
	setValue_t91BF21E4FCF27A76356201152E26474BFBAA9EB3_CustomAttributesCacheGenerator_valuesToChange,
	valueDependentConditionalImages_tE54B4D8396C0BE42F0698840D475CA16CABD56E3_CustomAttributesCacheGenerator_actualizationType,
	valueDependentEnable_t9B899DC6FE53A0DB83F406F683E1152E7C7EA2C3_CustomAttributesCacheGenerator_value,
	valueDependentEnable_t9B899DC6FE53A0DB83F406F683E1152E7C7EA2C3_CustomAttributesCacheGenerator_go,
	valueDependentEnable_t9B899DC6FE53A0DB83F406F683E1152E7C7EA2C3_CustomAttributesCacheGenerator_limitToEnable,
	valueDependentEvent_t248446CEFB1249A642BAA80D17DF333B827B64D8_CustomAttributesCacheGenerator_triggerType,
	valueDependentTimer_tB83525519CDA3C98979C5CF4A6E39372255C4F7F_CustomAttributesCacheGenerator_deltaTimeCycle,
	valueDependentTimer_tB83525519CDA3C98979C5CF4A6E39372255C4F7F_CustomAttributesCacheGenerator_onlyCountWhileGameActive,
	valueManager_t68B8E4DE543CD0BC1A6A1990A9984AE9DA0EA58D_CustomAttributesCacheGenerator_values,
	SafeArea_t44D5A2651DC2DA47B987B9097B633F510DC18156_CustomAttributesCacheGenerator_ConformX,
	SafeArea_t44D5A2651DC2DA47B987B9097B633F510DC18156_CustomAttributesCacheGenerator_ConformY,
	SafeAreaDemo_tC7C1D7D1BC5FA083D4A382C37576B0C06FBECE35_CustomAttributesCacheGenerator_KeySafeArea,
	LogicalStateMachineBehaviour_t29135DFA940C8C1648223E7441587548C1F7C25C_CustomAttributesCacheGenerator_U3CAnimatorU3Ek__BackingField,
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_Inventory_ShowNewItems_m730E58CD1F058A1050B9702ADAA94EBBEE955155,
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator_Inventory_delayedActualize_mD1E356C8C8C2EE201B75C5724CA238FDD552BD2D,
	U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11__ctor_m4F93C52A740A6C441BE1B043BEDD162235DC2E30,
	U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_IDisposable_Dispose_m6A49C06D643DC646E3C55A44E5EE1AF4F627CB77,
	U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6967EF8B284149E3D86878CF77A0D80B53DD0B64,
	U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_Reset_mFA50BB0229FBC728D2870928863FC9ABAC51774B,
	U3CShowNewItemsU3Ed__11_t56DF058AE0CA65152C2D253B917DFA84B4462C4B_CustomAttributesCacheGenerator_U3CShowNewItemsU3Ed__11_System_Collections_IEnumerator_get_Current_m76B1459E38683876F202AA8467A1C975E8068998,
	U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18__ctor_mDF4DDB44936F798F1832FF0E64A6FA3A9A668481,
	U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_IDisposable_Dispose_mA51C00F967C3090D966C0D77B56F0B40453EE909,
	U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m271AEFDD2D8343B07E406091F9C94AFB84B0F4FC,
	U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_Reset_mDEAD3BE3EAC90C8504A165A30341EC72AAF597BC,
	U3CdelayedActualizeU3Ed__18_t1DF3DEDE54C53CFF658B55CB037D276C9456665E_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__18_System_Collections_IEnumerator_get_Current_m7B2A880A7C4382F85173E90E349433A1DBB661D1,
	Inventory_UIItemList_tF3D05E5878FE24057DCCEDF7F872B3B9DA2279CF_CustomAttributesCacheGenerator_Inventory_UIItemList_delayedInit_m3EAD041EF763A6EA538C188CA2DD32339660B976,
	U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10__ctor_m5157CE64BFCC04D43B1A35144363AF0F3182CC60,
	U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_IDisposable_Dispose_mFA7F9DEEF57B724FD1133C7AA67D24BBE2D5B8D2,
	U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45AD32E3413F826B1340FE94180B993E9A7C9609,
	U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_Reset_mF78B2D14D8B84D2B048230AB5BA8BC2256EAB1F6,
	U3CdelayedInitU3Ed__10_t08FC2CBC90793510BD7F38C5E13DF9186358C97B_CustomAttributesCacheGenerator_U3CdelayedInitU3Ed__10_System_Collections_IEnumerator_get_Current_m723D3863BA16237C6A75150DA0E6000F8445C5C6,
	Quests_tA65F4E3670A79F4E6872B1561B032E47A05E8A8A_CustomAttributesCacheGenerator_Quests_delayedActualize_m205899FC8FF90F4409663E809FCD691833237E74,
	U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32__ctor_mAEFBCD23F8BEF17E5BD56CC457FE495405C75A4F,
	U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_IDisposable_Dispose_mD2D3C71CF0DA26B65705FD1234759FDCC6CEA2A8,
	U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2D37E0C9702B8712E096D957CB813D1339A5FCE,
	U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_Reset_m57DF855CC498C59B7FB9D904D8CE6B56226F810A,
	U3CdelayedActualizeU3Ed__32_tF066D9C35E9AE76C99BC12E5F181DB1A1AD1F9C3_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__32_System_Collections_IEnumerator_get_Current_m0FC81D0D966BCA6016E759F0CD7827D2316AABF7,
	Timeline_tB6DEE51AE888D379DB9C2B7F7E7C0FC36FA0EC8F_CustomAttributesCacheGenerator_Timeline_delayedActualize_mBDB9533D5CF13304BE38412E707BC60F6677060C,
	U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14__ctor_m163DE02FB61436D359EAEFF208D0BA13531A1BA2,
	U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_IDisposable_Dispose_mB3F3A0C344D293C1A69F98D95489647B841E4DDB,
	U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8130E9BD1C537FF6EF30645E6EB76449A451D9C,
	U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_Reset_m7B7A2424DFC67895DA2367595C79B7C13DD21EA4,
	U3CdelayedActualizeU3Ed__14_t1A28321065A426152F3ACC89BA2ABB0C07521E77_CustomAttributesCacheGenerator_U3CdelayedActualizeU3Ed__14_System_Collections_IEnumerator_get_Current_mF87B6C1ACB21AD889437D3D4A15742C1D9E15DCB,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_Timeline_UIEventList_freezableUpdate_m598AE33C3613E8016693606FDCCE8D6081B567AE,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_Timeline_UIEventList_moveScrollbar_m666D995BB93273C5789809F24C0C00527C33AA68,
	Timeline_UIEventList_tE0D2AD76C8518BD2A41E987AB5590BD698019DDE_CustomAttributesCacheGenerator_Timeline_UIEventList_animateScrollbar_mE9711431AE5910959C2C37E9D643486F77139250,
	U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23__ctor_m4783DD803B0781C6D55CF050D10870FA162CD368,
	U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_IDisposable_Dispose_m4BE71898145F5E53A99ADD950576BE01407E1F0A,
	U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44F416F81FFCE8065052F150502D1C7192D336DD,
	U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_Reset_mBF4FC0D2B0597223CAD3B9358590491BD5C64C40,
	U3CfreezableUpdateU3Ed__23_t50DB71D8D8CF6D424A58FBE88A77E58B4438C565_CustomAttributesCacheGenerator_U3CfreezableUpdateU3Ed__23_System_Collections_IEnumerator_get_Current_mFB3C753DC3C7268E2671B6BDB671B18618043502,
	U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24__ctor_m2FE1146F98C50EA6320D3850669B72148B6B5EEA,
	U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_IDisposable_Dispose_m192BB59F93A9FBE43398A6FE37C8019D0A998C3A,
	U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m954B89FE0C20DA41406BF08F8C8D5BBA6AEF0334,
	U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_Reset_mF290FBF737DD65386088D34E1AD99CFE4E74D4B7,
	U3CmoveScrollbarU3Ed__24_tE494B8B3248807032F7B0D33606EB79939DFD88C_CustomAttributesCacheGenerator_U3CmoveScrollbarU3Ed__24_System_Collections_IEnumerator_get_Current_m72CD44092B59FF2850BB3B5A683609887BEC9A41,
	U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26__ctor_m1B4B0175B724E1193EDCDA6F797C3064FF59E6BB,
	U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_IDisposable_Dispose_m9B030CEA5F95001C0406F30685AC322AE6B40170,
	U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21653C698A208F8970AD906340AFA09AC8EF1F11,
	U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_Reset_mE02D079FC130D8B51F2AD3D7FF1EF878B7E35B2D,
	U3CanimateScrollbarU3Ed__26_t6016E5FC076928BCDAA96B352F4F1CF6B75D962F_CustomAttributesCacheGenerator_U3CanimateScrollbarU3Ed__26_System_Collections_IEnumerator_get_Current_m5F6D100097FC1BF15D464E6AEA55105A019DBBD5,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_CardStack_CardMovement_m0DE547D4A6BCEF60B388F3E65A0D013E374B8953,
	CardStack_t45C65B445D4A6D2AAAC868B6434683BA1809672C_CustomAttributesCacheGenerator_CardStack_moveCardOut_m9E9DA3238C638AB7927FBF00FBBA89E5E40A2FF1,
	U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49__ctor_m20A81C719966A8182126B9975F568E07B82DAA4F,
	U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_IDisposable_Dispose_m597AE27807E96B9EABB15A7B52F3A907165FCA4F,
	U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F948D8EEE3C7847E0339CF067F773A196FADE4B,
	U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_Collections_IEnumerator_Reset_m2022354FDA8C877E0121F503D61E32CF4787D0A1,
	U3CCardMovementU3Ed__49_t9142F98B9140AB4D636E20FEA754411456042308_CustomAttributesCacheGenerator_U3CCardMovementU3Ed__49_System_Collections_IEnumerator_get_Current_m239552DA1314887EF0A85094BE7792EF445E3092,
	U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53__ctor_m1A4633BA82BE4A53DE6C58D7632CA74DBF7980C4,
	U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_IDisposable_Dispose_m0F6008412BF065A95BF7C9837E002D54FB6AC18B,
	U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB107756F0DFD44B9A92F9A90BAA1416A5FF52058,
	U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_Reset_m7ECACECB5214C8351376CC906FC0E5D13664C31A,
	U3CmoveCardOutU3Ed__53_t5F8A558273F136B9307B5F877954F5B531588901_CustomAttributesCacheGenerator_U3CmoveCardOutU3Ed__53_System_Collections_IEnumerator_get_Current_m60342D8BA597D00666EC5A995994065A6C8AEE7C,
	CountryNameDisplay_t19A927AAAB537780B033EA73FC13AFFA541E54B6_CustomAttributesCacheGenerator_CountryNameDisplay_twoFrames_mA53A0B0554D766F89C664C57F2B51243139FC082,
	U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4__ctor_m7DDE93D188A97F1F2A8F7B8B76AB6E2BDC4E624D,
	U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_IDisposable_Dispose_m831487A496655900EDA4322D9ECAAC5E548DB62C,
	U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F6E189D7326C4B888B4CC8D73E4E6C62C6C94B1,
	U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_Reset_mFF5E60CCA7FADEE5BCBFFF2829E501D6E6D8985A,
	U3CtwoFramesU3Ed__4_tA5B4C3484F3F0621944A226F6BBC83AF83A4BFA8_CustomAttributesCacheGenerator_U3CtwoFramesU3Ed__4_System_Collections_IEnumerator_get_Current_m585E0BFA9C48635E44FD2A8AF7F111842DEF9308,
	CountryNameGenerator_t56E17E78DC6B5616070F1B11B15B0E5F7038AE3B_CustomAttributesCacheGenerator_CountryNameGenerator_oneFrame_m332A4A4145FB9E105CDE00AB65B211D77BCF23FD,
	U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5__ctor_m4B579F1DA5E880BA17A19AAC62C801F10B2282FE,
	U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_IDisposable_Dispose_m697F7F0C529FE820491FBBFB4628E70D9A547D72,
	U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0CBC897829B3BE3032005EA66E369973541A768,
	U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_Collections_IEnumerator_Reset_m5F621E1AA2318DC5D06E66CBD08B99C45D57C67E,
	U3ConeFrameU3Ed__5_t8DDC3F4D1DB9C1A0089F1D724D41238C8D941D1A_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__5_System_Collections_IEnumerator_get_Current_m8E2260A88F81ACEE95703F8F693E5EB4C7DE3895,
	DelayedEvent_t128EE68FD2457EF92E14F0493AF65C2A5A29DFDE_CustomAttributesCacheGenerator_DelayedEvent__delay_m4D67B05369F776238509EF536A42015264C8FE87,
	U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11__ctor_m5170E051F5D20856F12F90FDCA6571E68EB8F830,
	U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_IDisposable_Dispose_m815F55D1DA38454BA27303C83782CCF5B2839C7E,
	U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B7C7859D2F37ABA7675B0FF7B2CEA08D5523395,
	U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_Collections_IEnumerator_Reset_m8B109E7B8BD114E6A539836C1CCC24D3B949F6DA,
	U3C_delayU3Ed__11_t2BA08468C57D41A43041F6B95C6BD9AD087DCCB1_CustomAttributesCacheGenerator_U3C_delayU3Ed__11_System_Collections_IEnumerator_get_Current_mE1E9828FA48FE14B08726A0E3A98EFED407A4DBF,
	GameStateManager_t581A50F0CEB605A7870C3A10FC14F9CAA6879A67_CustomAttributesCacheGenerator_GameStateManager_OneFrameDelayStartup_m4224DC7C44D02F4F4D8C61CF79B3F0563D1EA193,
	U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9__ctor_mFA4D7E5CB4F7D1FCAE440E17731FC79FE6463229,
	U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_IDisposable_Dispose_mEDF62ED725AEC84B0623405D0629C09AADA5575F,
	U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD01966B1BEF4FBB7C85B3A95CF46DC5E3C3FEC1,
	U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_Reset_mA3924A3373CF381CFB79BB3C5997F6D3D753E996,
	U3COneFrameDelayStartupU3Ed__9_tED1C4817A8ABF2D560A9B2F674CAEA63DB1A651E_CustomAttributesCacheGenerator_U3COneFrameDelayStartupU3Ed__9_System_Collections_IEnumerator_get_Current_m72A635E851E0F7F4EC96AA006D5450B5F893E29F,
	GenderGenerator_tB9A218A0DF4CDFFE1BAB4233348FE71A5E857E2A_CustomAttributesCacheGenerator_GenderGenerator_frameDelay_mD70AC765071A2E2AC81EBF1F46466AC95CCECD0E,
	U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5__ctor_m3D601DC493336F760EA9D07947A8848668774822,
	U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_IDisposable_Dispose_m61ED0D2F306AD93BA1D6B09AB601CED46FFD40B6,
	U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m508D08CD2E72E437AA970E975F0FDFDA49362A35,
	U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_Collections_IEnumerator_Reset_mF2CA8CAE3AC8A3F88666C84089B11B51F2FA7361,
	U3CframeDelayU3Ed__5_tBDB1D09A288E1B5D2838BA25BA1C0A6B42D7F41C_CustomAttributesCacheGenerator_U3CframeDelayU3Ed__5_System_Collections_IEnumerator_get_Current_m289933883DE423AE7BCD2897BA8EB82EB19946AB,
	HighScoreNameLinker_t18DB85706F620D6A5D9179A856F71606F0DB52B8_CustomAttributesCacheGenerator_HighScoreNameLinker_delayed_m3D64F23F54557193F3753E010799BF317CF2E2C7,
	U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15__ctor_m0918B7118476E3C20CF0E8D758735D04E1FDED1E,
	U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_IDisposable_Dispose_mE0DB7048627B25283EAB909051B30A8D4FF6224D,
	U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3544EE0B3D2BCE81E12BC0A9D3CF43A2DAAA781E,
	U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_Collections_IEnumerator_Reset_mA0A6E89CDEFFD236B3CF585DB50FB39703C45A7C,
	U3CdelayedU3Ed__15_tD2CCA8F92935E9D6E2D4094D908526041237F666_CustomAttributesCacheGenerator_U3CdelayedU3Ed__15_System_Collections_IEnumerator_get_Current_mF80795299D99484AE4CD163215543FDCC91855CE,
	InfoDisplay_t6DBEFBA78D18777ED9BA880B207C209E7F15A2BC_CustomAttributesCacheGenerator_InfoDisplay_cyclicIconCollector_mE8F77F9BEBD5496FEF8766E4762BE9C9636F150A,
	U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2__ctor_m9220A985B72F928F75C0CAFBE073745B9E441881,
	U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_IDisposable_Dispose_mFB4B957CA312D8B9041E3DE37A56A4AF017327BA,
	U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m303A52A720B7B60184234F81392918ED44B068BE,
	U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_Reset_m2841F544A2EFED8EE58B4425C78555D50A12453F,
	U3CcyclicIconCollectorU3Ed__2_tDCD856D950848631359EDF8DA2D468F9CB6341C0_CustomAttributesCacheGenerator_U3CcyclicIconCollectorU3Ed__2_System_Collections_IEnumerator_get_Current_mEBA04BC302FA19463B5072EFF2D48AEED7B86B2E,
	KingsLevelUp_t0377874F7A9F38E1B68EE371E01208C9C2D23951_CustomAttributesCacheGenerator_KingsLevelUp_cyclicFillingComputations_mFC40CFAFBC0B13EF636CA12077EE61318CFC952C,
	U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21__ctor_mFEB671CD9B6AB385063CB85F9EDD2FCF66CFBE5A,
	U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_IDisposable_Dispose_m44C5F2035DDB457E94FD9376930D8D740C577C88,
	U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9342F3240439AB5371B1AB414C9AD0438B44577D,
	U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_Reset_mF95A76D31F44E278584ADBD7A02631EE22024239,
	U3CcyclicFillingComputationsU3Ed__21_t4524576EFAC02F14E7DAA68F020AEF6B08A98AFE_CustomAttributesCacheGenerator_U3CcyclicFillingComputationsU3Ed__21_System_Collections_IEnumerator_get_Current_m59ECC582967406007EAC87DB871D8FED4E230955,
	KingsTypewriter_t8F4F04BF8164DF484099D6DA0FBC074CE171B612_CustomAttributesCacheGenerator_KingsTypewriter_typeText_m368EB8071A25C0E0E88D3D13C89D7FF35B43E5D6,
	U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17__ctor_m316FCE9CF3C7CB715AAA1E003D8F0EFB00A03D0E,
	U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_IDisposable_Dispose_m6608A8CA82E96F5255878BAE12497DE488C43FCD,
	U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2B057A114FA28DE212251036952D43402AC45E0,
	U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_Collections_IEnumerator_Reset_mE733F3418C0E5CB7B1F92929A9734C0EA3FD9EBE,
	U3CtypeTextU3Ed__17_tD9C6CD2115EB0E5F8117BBB4673827C7A0F94EA3_CustomAttributesCacheGenerator_U3CtypeTextU3Ed__17_System_Collections_IEnumerator_get_Current_mC3ED16AA3344A28C0765D09F95EACD089DD6A91C,
	Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_Mobile_Leaderboard_sozialActivations_m670202D247ACB1CB9A2F5E886948A355E4A8B55E,
	Mobile_Leaderboard_t3A1B7DEAAA4486198E1D3A932BD8A9C18AC897A3_CustomAttributesCacheGenerator_Mobile_Leaderboard_refreshSettingsDelayed_mBF003C2B8B4D5633265BDDD0EF26C47FE90C98C9,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitOk_mD5599284B0A446E8C34BBDD3093123DD21FE1239,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitOk_mBF73643240665182CB69D0C14CDC3B087234BFA8,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitFail_m1D46293C16444D7B24E7F299906D545B69B4219C,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitFail_m31C56D55D7B64105F7B67D4DE8E26B9C5CF21E87,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitted_m24ADB7B38403047613B6D005C88BE878E031EEA8,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitted_mEAA4AADB1646933E1DE013DF2E0E612A0DA0F7E9,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___transmitRequested_mEE51F08862BCD8C9032E6E711AED5B14A3A09C31,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___transmitRequested_mC218E210B8E249EA4A35AF085BE958D03C729548,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___scoreActual_m1D676ADD27F828BB2CA053ADB30D939936D53B37,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___scoreActual_mB774F0A990B7EEE38DD922EA090B0C8579925EBF,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___scoreMax_m38F6DB7A6C75E88B9B89D6AA1CC207982739E230,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___scoreMax_m092C80890879F45DDDB325959410B46A6F90E6A1,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___score_maxTransmitted_m4B558D48B45FD7A633C4FD7209CB6C5FE3DB4055,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___score_maxTransmitted_m61DBFBDABBC81A990BDBDE25ECF48B22248C593E,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_get___scoreToTransmit_m0695FCB38BD85E9835B80EC619936570FC808FB9,
	scoreSet_tFED72D22BB31423090C4629F683C8A01497B4BFE_CustomAttributesCacheGenerator_scoreSet_set___scoreToTransmit_mD05BADBFF2EF6E8FE12433BA61872C6DF71E1EDB,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___reached_m8F7AE68B934D6AA11B0E6138C2DBFDFC216A8D8F,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___reached_mFCA3399C5D82C1AF8BD992E0D8D345751B27C626,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitOk_mFB8E26CD83629DEACF31511CFEBC5EE71A114564,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitOk_mFFF97F106EF5D1BDAA9958F254140C88727DD3C3,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitFail_m5F02EFBC9B8215587543228A555A0E43350CC1F8,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitFail_mBF337F23A2FA59B61F444C4C0C0720B28A0D9C72,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitted_mF97F2ABC145A00EB91739C81212F9F8C933D2C5F,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitted_m21111F0CD8563874ED9AD3A5047C8FF9EE246A35,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_get___transmitRequested_m34B52FBD3818A49E20F5E84D9292074CF34CE62E,
	achievement_t5E8CE7E7547200DCCF0BAC700C8815BE331E3319_CustomAttributesCacheGenerator_achievement_set___transmitRequested_m72E233F50A99B23B152EBA5DC8C809AD4B4215CF,
	U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23__ctor_mC4E5407730B1F65B6EC62E910146B3622143C7A0,
	U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_IDisposable_Dispose_mB937EC683AE10AB216D121A11E6C668FAB08B458,
	U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m020C881CD53FB93BACC71AB995500894AB90C692,
	U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_Reset_m295E47A42C79C8C71B8EEDEA1FECE0D7625EA37F,
	U3CsozialActivationsU3Ed__23_tBFF4606528B27FC9F6B625BCD8E7073E2933F95D_CustomAttributesCacheGenerator_U3CsozialActivationsU3Ed__23_System_Collections_IEnumerator_get_Current_mD6B3E13C1C0C3322A2959E8AEED0BC6FB2F30BDF,
	U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26__ctor_m8279C00DD68770A1A3B9E7E112AB03B2470FD15C,
	U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_IDisposable_Dispose_m166F001DFF96386BCD41ACB6BC044BCCDCB82520,
	U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5F538F4E1C37EE1D6ED46A6F0650E7D661A840E,
	U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_Reset_m9ED897F280F6D9C26054BA2B1C3287F5C6D3381C,
	U3CrefreshSettingsDelayedU3Ed__26_t90C651A492064B0A300E67FAA11557375B071391_CustomAttributesCacheGenerator_U3CrefreshSettingsDelayedU3Ed__26_System_Collections_IEnumerator_get_Current_mCE6AB66AD30ED3D455300DFC06217893C416382D,
	PlayUnityAd_t2868479CC495D3827DD380E8359E907DEC30820A_CustomAttributesCacheGenerator_PlayUnityAd_testForAdvertisement_mE3C461C09C83B9B6851C5F210F5D00D86A1DE8B2,
	U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17__ctor_m16D037C642106BB04305682B117FCE27C0C75741,
	U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_IDisposable_Dispose_m9BC3A92ED2FB1954E5445B8A8391649DFA6957E4,
	U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8D44465EF33772A073C5411A44D97351670F236,
	U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_Reset_mE310632C52E3A44CC1892E2669BAB3E63554441F,
	U3CtestForAdvertisementU3Ed__17_t929F92C9A1786366D761C838695E2F6A1A8C385C_CustomAttributesCacheGenerator_U3CtestForAdvertisementU3Ed__17_System_Collections_IEnumerator_get_Current_m7B7B5E24FB0E178AC03D8AC548434E2FD3C3C9E2,
	LoadScene_t633887ABD67B5712F8CCC7C5511422C0E45084D4_CustomAttributesCacheGenerator_LoadScene_delayedLoad_m6E48431E35D2FF55B1CE9B57CD496DDDCE98ED7E,
	U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4__ctor_m2FEDB7CE5A9390BE1C53532F8ABFBB6A6B962324,
	U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_IDisposable_Dispose_mF3089973DD6F7FB4A8C274A44A16B5CD2BAFE9FC,
	U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B0A66E43371D0D1D55312BED6363508A1A4AEAB,
	U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_Reset_m3463BD91919D2175B690C1A4BD4E2B9CA498A3B6,
	U3CdelayedLoadU3Ed__4_tE4B71D916C49B08CB12A94A67CFF4CA358AAC35D_CustomAttributesCacheGenerator_U3CdelayedLoadU3Ed__4_System_Collections_IEnumerator_get_Current_m6B830331C28C6C1601A5DA6711C53B509B5D8C09,
	MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_MusicPlayer_multimediaTimer_mEEEA7ED5D30B61543F2FF340A50006CC2AF3AF56,
	U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16__ctor_m9452FF5F9C621B2701F14069A953C1D7EDCB639E,
	U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_IDisposable_Dispose_mB7EE0025ABA8196757B8B19E53D0AF877CCC729E,
	U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAFBBFFDC0FAD295792E66E7EB0185FBAAC74A4C,
	U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_Reset_m2123A5E79B2C708B0EBAF93621053EABA2B1F5B0,
	U3CmultimediaTimerU3Ed__16_t629C5D9B6D8FC13BE28D84F19B9782CF46BE97EB_CustomAttributesCacheGenerator_U3CmultimediaTimerU3Ed__16_System_Collections_IEnumerator_get_Current_m1B45D5AAF2D3A164F2C5DB0B61FFB79517282150,
	saveSlider_t5DC0D51375EAD5363DCE8DFE87D9E940BD0B12CC_CustomAttributesCacheGenerator_saveSlider_saveKeyDelayed_m38B8C3376FE5ECCC0C8F95B8635C123D009A3EE8,
	U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13__ctor_m2DC30CBDF8E9E74DCFCDF4A344C2534AA4428DAC,
	U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_IDisposable_Dispose_m9C6DD24E2BD62BB987123DA584DB83021628DCCA,
	U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C9015DDBDB5E34979C1FA1F6F29463EB224629,
	U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_Reset_m3D7E598789A659776E64F9B635AEA7FE312067AE,
	U3CsaveKeyDelayedU3Ed__13_tFA5BE9757FACAA185CCA2E655D1A93F4234FD282_CustomAttributesCacheGenerator_U3CsaveKeyDelayedU3Ed__13_System_Collections_IEnumerator_get_Current_mC4966B1DBE9334F30DC9633FB3C843A517F178AA,
	TextReplacementDisplay_t6B34544842E8F084AF34375EE2F28B60726CAC3A_CustomAttributesCacheGenerator_TextReplacementDisplay_delay_mF23DE8A32576410DE6033F2477529F1961C63972,
	TextReplacementDisplay_t6B34544842E8F084AF34375EE2F28B60726CAC3A_CustomAttributesCacheGenerator_TextReplacementDisplay_cyclicActualization_m718D870A00CF1715C2A47C08E227BA09A94D963B,
	U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4__ctor_m7CA16C5D26BDC3223FF857AC48BB7DEE4F7D5506,
	U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_IDisposable_Dispose_m0B66E4484A0C881A4CD168C373129C0076E53E37,
	U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF00484BABD02F82575C68F2D73EC3FF32471CFD,
	U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_Collections_IEnumerator_Reset_m89DC582C5637A0A9AB60E090B359DBD98470A49F,
	U3CdelayU3Ed__4_tF720D829EC87A60E6ED1CACD3C1ADE7299792A63_CustomAttributesCacheGenerator_U3CdelayU3Ed__4_System_Collections_IEnumerator_get_Current_m1CFAA7DE6B7CC9C41DD1A9BC07A52468D9A68C2F,
	U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5__ctor_m8E38305516B55E2E423B4C542D8D7EB0CEF3BC51,
	U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_IDisposable_Dispose_m81C78D44F1B5A21BD65D249322F6767F9911C53E,
	U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3C2A0AF0B4BC30F2B8923FDFF34739B2892AE3B,
	U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_Reset_m9CCBFCFEF27DF16814DBF2C2C6171DC25D2C4304,
	U3CcyclicActualizationU3Ed__5_tF3AECB298C66F3A284F281C8E856923D06ABC575_CustomAttributesCacheGenerator_U3CcyclicActualizationU3Ed__5_System_Collections_IEnumerator_get_Current_m7BEEAC4BD998A473557796E64438100C0E920B23,
	ValueDisplay_tB8F21F22C78ABC9BD356AD75883D32BBAFDBFB13_CustomAttributesCacheGenerator_ValueDisplay_cycActualize_mC15584D6A4DFEB246FCA9306F3FCA55A62F40EF1,
	U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15__ctor_m884AB86FB8663308B5D6E197DE3EEB5AF7A09B18,
	U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_IDisposable_Dispose_mDAA4568D0183CA0B11C556C49CBB15509765B1FF,
	U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92D5C095669CE7EE955BD8782F09A4E5CA451711,
	U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_Reset_m921D4CA172B2615F151E64DCDA76E4D1796B3A04,
	U3CcycActualizeU3Ed__15_t578822246D4C20798E929FC3213CF6DD02652C7F_CustomAttributesCacheGenerator_U3CcycActualizeU3Ed__15_System_Collections_IEnumerator_get_Current_m405E4962AA5731495D4C3F97E5DC3DFCEC6773A2,
	addValue_tEFECB703EE7CA5A646EA2725CA007E833B09FE59_CustomAttributesCacheGenerator_addValue_addValues_m380AC79EC53FE31C0C81B7E88AD66C4FD2E2D53D,
	conditionalActionList_t6E89CA527EB44224718322089B15384F3B81E5B3_CustomAttributesCacheGenerator_conditionalActionList_eachFrame_m2D0D706D9FE437F1B7EE6F2B1C5EC027134612EF,
	U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13__ctor_m7555B9ACB0F67EF10AE18E2C526B2D41E8D4955D,
	U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_IDisposable_Dispose_m0F460470A415E6489949AB9394BC1F8692CE282A,
	U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA8B187D2D5D200C97BEDECF74EB850597D69F56,
	U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_Collections_IEnumerator_Reset_mE1C939B7483AA60C3C43B5A25C8FBB968DF43DDA,
	U3CeachFrameU3Ed__13_t4ABBA51FC9A90B8590695BDC6A64B9152FEDC653_CustomAttributesCacheGenerator_U3CeachFrameU3Ed__13_System_Collections_IEnumerator_get_Current_mF23BE2C11DF83A40BB3750D9B86CCA27AC067457,
	setValue_t91BF21E4FCF27A76356201152E26474BFBAA9EB3_CustomAttributesCacheGenerator_setValue_setValues_m33499F09D12C3092076C485B179E9B09117FF180,
	valueDependentConditionalImages_tE54B4D8396C0BE42F0698840D475CA16CABD56E3_CustomAttributesCacheGenerator_valueDependentConditionalImages_oneFrame_mAB42BA6CFBBBCF5895EB5C08C3C426F70F6E1C27,
	U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9__ctor_m526235E2CA6FC5AB734BC6AB29863091F2F6F150,
	U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_IDisposable_Dispose_m3473E1959E7822609AF060B96EE35B80428D6757,
	U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD831E212FC446D5F0C503CE9F56D6B1AC815E565,
	U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_Collections_IEnumerator_Reset_mED64782DBA020DC52AA1107DB00A44B2D7CD3D85,
	U3ConeFrameU3Ed__9_t6E39845674A7E5943A079143C20D236351CA62B5_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__9_System_Collections_IEnumerator_get_Current_m6779D76973F63196D278DE6A3E80386D47855A83,
	valueDependentEvent_t248446CEFB1249A642BAA80D17DF333B827B64D8_CustomAttributesCacheGenerator_valueDependentEvent_oneFrame_m41A40BB51B3A6F5A3CFA19588732D053D843FAD0,
	U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7__ctor_m853C858DD2E0A4C942AC48957E47B406F60F0AEE,
	U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_IDisposable_Dispose_m69CD851696FFDC0061D1ECD990D56DA0B439B7B5,
	U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m24083F1557E44580268A6637582B9AAA7D37BE63,
	U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_Collections_IEnumerator_Reset_mF061980C2A1E65BDF89EAF28A2B5D9A9B4BB3A1B,
	U3ConeFrameU3Ed__7_tCE5724AEDCF1772129178FA92825960C927FFE01_CustomAttributesCacheGenerator_U3ConeFrameU3Ed__7_System_Collections_IEnumerator_get_Current_m52A004719D90B66FF54E4931166AAD5741764477,
	valueDependentImages_t7FDCC682A4DDBF736E82625CB91A11DF871017D7_CustomAttributesCacheGenerator_valueDependentImages_cycActulize_m9C138934BF325F4A8070605D6E61E0F45E9EE300,
	U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5__ctor_mDBBAFFFF8A92A55D8E3B43B8536C308D7D3A3433,
	U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_m99F9DA8C4364C3F1AC80C2CF07B952136C1A64D0,
	U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7DAB6FA561D2922E658BF2B4B8AEF4BBF91CE4EC,
	U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_mB32402A65A3CCC658A347C707C0FE56B68D4938A,
	U3CcycActulizeU3Ed__5_t0FFFEF54523946368ECDD88DB8FBFDA0DD083537_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_m0DE39A326FC147072798CCA18C508086C011E33F,
	valueDependentTexts_t34FBF6069EA56165DBD17E6C06DDAFA4016CA251_CustomAttributesCacheGenerator_valueDependentTexts_cycActulize_m2BB64E52678442A5E7DBCA3B48D1147759CBAC20,
	U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5__ctor_mB0FEBDE8940FB3BCA8EE32254835E36CECEAD7E1,
	U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_IDisposable_Dispose_mF7D9DB7F1B0F96318D03BAE41FD73BD0057D56BC,
	U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA1C19D6A96F6104F79E604D37F6E74EFDD5AADD,
	U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_Reset_m2836EF31113785AA83327C23B25385F6C218333F,
	U3CcycActulizeU3Ed__5_tEA0BBA45465B7B6244AD6F867377DE78E3DE8496_CustomAttributesCacheGenerator_U3CcycActulizeU3Ed__5_System_Collections_IEnumerator_get_Current_mB9051E40AE1D230D32FCD8D7C3C5F8BE831B451A,
	valueDependentTimer_tB83525519CDA3C98979C5CF4A6E39372255C4F7F_CustomAttributesCacheGenerator_valueDependentTimer_cyclicTestValue_mF25FBA3D0CAC2801FCF2FEAE5E172B553CD1BA15,
	U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9__ctor_m242DBED8F79E038793145948FBB5B3053E5C7C08,
	U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_IDisposable_Dispose_mE0B503F8CEA59D250D9A01B27100568467D9DC4D,
	U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0575A7EE9E712A8D39FA707CA1AE13A417AF27B,
	U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_Reset_m78215A7BE3957DE9415B27EF8DCECA35B47D8B38,
	U3CcyclicTestValueU3Ed__9_t4131AC77C914B4A98E718B54980003ACA8E6203E_CustomAttributesCacheGenerator_U3CcyclicTestValueU3Ed__9_System_Collections_IEnumerator_get_Current_m68C1793EA5E692C4DAC56318B4758A4491CBBF2F,
	LogicalStateMachineBehaviour_t29135DFA940C8C1648223E7441587548C1F7C25C_CustomAttributesCacheGenerator_LogicalStateMachineBehaviour_get_Animator_mDC4507F8809D9FB66D14DD9F1ECDECA694B50F51,
	LogicalStateMachineBehaviour_t29135DFA940C8C1648223E7441587548C1F7C25C_CustomAttributesCacheGenerator_LogicalStateMachineBehaviour_set_Animator_mE95CB9BB31E180CFF313B6587C993E61F7C8C4AA,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
