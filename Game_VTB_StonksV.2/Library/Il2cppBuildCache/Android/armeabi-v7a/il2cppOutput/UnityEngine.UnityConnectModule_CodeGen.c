﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_enabled()
extern void UnityAdsSettings_get_enabled_m31BA7F51C1514D088C457AB72F6AE5B43BC54FDD (void);
// 0x00000002 System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_initializeOnStartup()
extern void UnityAdsSettings_get_initializeOnStartup_m0DF15F798969A5C254665271CEFB6F47183E6E70 (void);
// 0x00000003 System.Boolean UnityEngine.Advertisements.UnityAdsSettings::get_testMode()
extern void UnityAdsSettings_get_testMode_mB7505FA841B82FB37A830EB43FE95DAB6FE38824 (void);
// 0x00000004 System.String UnityEngine.Advertisements.UnityAdsSettings::GetGameId(UnityEngine.RuntimePlatform)
extern void UnityAdsSettings_GetGameId_m7ABC873E2C483787EFDF2B614918E09AC637B207 (void);
static Il2CppMethodPointer s_methodPointers[4] = 
{
	UnityAdsSettings_get_enabled_m31BA7F51C1514D088C457AB72F6AE5B43BC54FDD,
	UnityAdsSettings_get_initializeOnStartup_m0DF15F798969A5C254665271CEFB6F47183E6E70,
	UnityAdsSettings_get_testMode_mB7505FA841B82FB37A830EB43FE95DAB6FE38824,
	UnityAdsSettings_GetGameId_m7ABC873E2C483787EFDF2B614918E09AC637B207,
};
static const int32_t s_InvokerIndices[4] = 
{
	2030,
	2030,
	2030,
	1942,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_UnityConnectModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_UnityConnectModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityConnectModule_CodeGenModule = 
{
	"UnityEngine.UnityConnectModule.dll",
	4,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_UnityConnectModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
