﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
static void UnityEngine_Advertisements_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x44\x79\x6E\x61\x6D\x69\x63\x50\x72\x6F\x78\x79\x47\x65\x6E\x41\x73\x73\x65\x6D\x62\x6C\x79\x32"), NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[3];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x41\x64\x76\x65\x72\x74\x69\x73\x65\x6D\x65\x6E\x74\x73"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[4];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[5];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x20\x54\x65\x63\x68\x6E\x6F\x6C\x6F\x67\x69\x65\x73"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[7];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x76\x65\x72\x74\x69\x73\x65\x6D\x65\x6E\x74\x73"), NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[8];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[9];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x55\x6E\x69\x74\x79\x20\x54\x65\x63\x68\x6E\x6F\x6C\x6F\x67\x69\x65\x73\x20\x32\x30\x31\x36"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x41\x64\x76\x65\x72\x74\x69\x73\x65\x6D\x65\x6E\x74\x73\x2E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x4D\x6F\x6E\x65\x74\x69\x7A\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x4D\x6F\x6E\x65\x74\x69\x7A\x61\x74\x69\x6F\x6E\x2E\x69\x4F\x53"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x4D\x6F\x6E\x65\x74\x69\x7A\x61\x74\x69\x6F\x6E\x2E\x41\x6E\x64\x72\x6F\x69\x64"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x4D\x6F\x6E\x65\x74\x69\x7A\x61\x74\x69\x6F\x6E\x2E\x55\x6E\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x4D\x6F\x6E\x65\x74\x69\x7A\x61\x74\x69\x6F\x6E\x2E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x41\x64\x76\x65\x72\x74\x69\x73\x65\x6D\x65\x6E\x74\x73\x2E\x45\x64\x69\x74\x6F\x72\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[17];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[18];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x45\x6E\x67\x69\x6E\x65\x2E\x41\x64\x76\x65\x72\x74\x69\x73\x65\x6D\x65\x6E\x74\x73\x2E\x54\x65\x73\x74\x73"), NULL);
	}
}
static void DebugLevelInternal_tF4573C0F3D1BC484CB562297FF62F53E146A0C92_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tE132ABC203FC35607C9A68D2EE5C411A29E10E23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_1_t3A687EB0AECDF64242830054A608A5025E9F244C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_2_t325072B915B2C81847CD8FF7EFA8BF9DF5318FAB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t82087E7EA37AE4C23E6D3686E3C930BBA8F264E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tFB680149A5801BFDB22502EDEADA9D63C80C8903_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t34CDFE94C717609A863226E99DA4E71D2FB51D40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_t68EE74784403A27CBFE72B29E2719837CB094CD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnShow(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnHide(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnUnload(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnLoad(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnShow_mB458F372D3DD207CA96F82388E162918002CCFE7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnShow_m45410E5D53456E1D26E2739D9D96B8AE7B250079(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnHide_m730316CF87D563577C8E8575B7932FE7B1840A22(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnHide_mBF26DD5714334DA7EAE18F9E0763012CCF6F4920(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnError_mD89F56EB324871C96484456D707F6B92F24B7E9B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnError_mCEECA5E24574CAF952B46ACBF59A09D900C3466C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnUnload_m1BF26B67825DA1F46ED4A1AA74B2E250EEDE6209(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnUnload_mF9B5937FD6742DACD006BEFEDDDC08A5314DCCB3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnLoad_mA6BD3F3063092371D316D8E498F775687ED784FB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnLoad_m1C5738D2036E92F62503550ECABE253C4F7E9ADE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_U3CHideU3Eb__25_0_m4288A525B768F059418C0DA0D2BA2AEA36DBCE86(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_U3CShowU3Eb__26_0_m4B04202038DEB894954DAD7BE90E863763FB6D87(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tAD6105258B44FC2D0ED09881B1CB8B642E8F08EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_t0059D7B3FF9873CBF55054CBC6AE4372C6704292_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t05DB1FC90FC24A4EC77DFC812A7356CFAE6BDF0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_tE54390B9CBA3DF68E5A376751E5EF8419B8C3634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_tB73A74EA3B2CB4BBFB72FC84144283F8CEFC4013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t7DAEFF5082862EED35531712429E9B3FC4978585_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_U3CshowCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_U3ChideCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_get_showCallback_m793328B516C2F58F409DDDFF299D9861A176DE58(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_set_showCallback_mB0F6E62E0D920E48A8F23D97B970D08110EC3B08(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_get_hideCallback_mCEA5BD2AC305AD7363B73563683E2CD5B58BE1C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_set_hideCallback_m3B0CE3795CA06887F96D10B95E7A247E0CC0B23A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_U3CloadCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_U3CerrorCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_get_loadCallback_m301BC53E2AD64C6A00325C4CE455AE0DBF4D3961(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_set_loadCallback_m4329F2F398B593D91C8A2BD968B381CCB8F5EB60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_get_errorCallback_m8CC522E0F86CC2236C352E7A1D1FC391C909ABC4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_set_errorCallback_m390340D855BAF0725C9D01F57D369E0EA4B9F8B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbackExecutor_tB5471E84666F700A95D8A9CDD2BBCFF57234A1B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_U3CenabledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_U3CdefaultPlacementU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_U3CplacementsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_get_enabled_mADAEF056D5A0E01FA69D62FE1B82F612E7A0F091(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_set_enabled_m2CA8460B705180466AC6010D39ABEF8C8E3776E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_get_defaultPlacement_mF8DE16AF8DB46335BD5B7E7F72018C5F3149C306(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_set_defaultPlacement_mF5E167689F8D411F9E90E6ABF6ABDA8E8FDB0DE5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_get_placements_mE3540699DF4055931FCD807291B8C3A4E1E5AF60(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_set_placements_m12D2057B3503DE4FE1C68ECF38A1FDBEDCDB2992(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Creator_t53854AD486562E01B7F2BCF0AFD6F701B77A5D82_CustomAttributesCacheGenerator_Creator_LoadRuntime_mB7761D71A241BBAB13D943F62B2EB85427D3FC2F(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_U3CerrorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_U3CmessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_get_error_m1E30CAFD188430D4587CB19F0806111FBEF1876A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_set_error_m506F8363A4E8CD857210D80CC85C889C5A472726(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_get_message_m365EB32B5200AAB8E7B02AC66F4A40A26826903A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_set_message_m1DF258C1D22BEC328A18DDA537BECF4A27DE91B0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_U3CshowResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_get_placementId_mD7AA8A53A304FF18B720BBC68D2C16CC041E82C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_set_placementId_m9DA2870864B91EF0359648D81353309809D92E8B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_get_showResult_m1F131AC5478F70C0E9327A3815A4AD385207CC93(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_set_showResult_m18E14A55B95BA4D544F64A3D615F6079CE481966(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HideEventArgs_t0E87B63ABE49D6FCB03F2B1FCFCEBACF00AE2E41_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HideEventArgs_t0E87B63ABE49D6FCB03F2B1FCFCEBACF00AE2E41_CustomAttributesCacheGenerator_HideEventArgs_get_placementId_m97CFB9AE5E30F2643462801AB415E44493FAFC81(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HideEventArgs_t0E87B63ABE49D6FCB03F2B1FCFCEBACF00AE2E41_CustomAttributesCacheGenerator_HideEventArgs_set_placementId_m41695EC676F0170C50C27028F925F0A9DCB44800(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnShow_mED74B968B1B3C20928EA3C9D9FEE41B0F6F3C13A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnShow_m9F1CC7BFD5FC42A320C64A3F959B74CF9951C4F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnHide_m3BA03303E0CF9BBDE19D7C6CE4A528F005AB71D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnHide_m9C99214F751547966119072252499B17D25AF017(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnError_m57A9E434F9DC963AE3850961431DBD113DC5A9F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnError_m673F1B243EB5826A8134D830AE363D08C7EFC23B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnUnload_m5F47B6EBCA78BEAF89DD4EF636E9BB0F5696DFA6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnUnload_m9CE8A92ECFE324CE490854C0578FE46E1AD2FDC8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnLoad_mD117A7CF92F790F029CA378B4AA9953AD0981091(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnLoad_m38D82EF8C5AC077C89CB04BE5B27A7C8467819EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnReady_mCB28D6C550B8CABC8553642307093DAE9B4B84C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnReady_mD67EFECAEAA16160825BB61344B4DF04686C6AB7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnStart_mE33CCC53A698FA89B1B7FEE7D96BD2B55E0D0704(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnStart_mD8CB53B4187BB65F0AB18831EBD34DF4CF7DC8C3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnFinish_m10F4B34BF2847806512B9D534324F05F8CC580AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnFinish_m30CA02164304DA72D2E28B05B808CFFAFB19B15E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnError_m7BF5094EAD371419E2748A18C485A67BBBB8E127(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnError_mAE1234B461EE48B89955E3D7F10A2718402435ED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetaData_tBE01A382C3BD81B14C99098223DD871E18071033_CustomAttributesCacheGenerator_U3CcategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetaData_tBE01A382C3BD81B14C99098223DD871E18071033_CustomAttributesCacheGenerator_MetaData_get_category_mABA6F5C05F3B9045D2C8796F3D466905092C2C07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MetaData_tBE01A382C3BD81B14C99098223DD871E18071033_CustomAttributesCacheGenerator_MetaData_set_category_m6A186106F731708B3BCE62D245A8CE3DF20E5FC5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator_OnFinish(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator_Placeholder_add_OnFinish_m20EF50FB8F5F846EA19B6478268C373E6A65E030(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator_Placeholder_remove_OnFinish_mBEACC10936874A7C89423DEF5768EBE74489B3EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnReady(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnStart(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnFinish(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnReady_m3BD5AD3B947EECAC0FE362324F4E7834FA6A5DD3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnReady_mDA55BE1CF7B270E883EC0D4187D10A351E1219D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnStart_m4D22DCE9B3752BCAAC63E6C778A75182F0D7C92C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnStart_mEF678584617D744F0C70A83077E3B2E342829DCF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnFinish_mC6C769216CACBFA5615A736CF355F2265EA2A4CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnFinish_m8A08CA7735D4D1ADB9FD9AC6626BF8CF2119CE45(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnError_m9846EC63A177D70427E3D5F26AE42AE385A8FAD4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnError_mDE61C7A4BEE423C8D31B1C16CF7B962D2AFCC016(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tFA2447E290B30A76C70C30619E23BBDCDDD92133_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t33822494F77A460EB4FA57D77070DD638CFD271E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t3DB3185E131EA6CCCC17F7E1023B79CE66EA0AE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tBDE9EB03DA0C945DFC9A67B015478FCAFEB93F74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReadyEventArgs_t713D51DA38734FAD87991FCC9A9DA7FA4986D567_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReadyEventArgs_t713D51DA38734FAD87991FCC9A9DA7FA4986D567_CustomAttributesCacheGenerator_ReadyEventArgs_get_placementId_m9B1A1462F7348F396D3E5F3AE4F2266B510ABA20(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReadyEventArgs_t713D51DA38734FAD87991FCC9A9DA7FA4986D567_CustomAttributesCacheGenerator_ReadyEventArgs_set_placementId_mF13E66593646DDEC39EA34E95436A614832B71BD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_U3CresultCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_U3CgamerSidU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_get_resultCallback_m884750654C53E4096E4F6EB00941DBA62282629E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_set_resultCallback_mCDCA3E88983BFC496AC7F999EAAA497A01A9CF38(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_get_gamerSid_m6D82587F5284F893760EA140F52B0C540A35C2B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_set_gamerSid_m281C98C7E79C7F11CA5C613F5D5DE8F35AB9B031(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StartEventArgs_t07FE61C20910684386B95C9DE0312066D0B74DB6_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StartEventArgs_t07FE61C20910684386B95C9DE0312066D0B74DB6_CustomAttributesCacheGenerator_StartEventArgs_get_placementId_mC0CADA1095DAE9D9C54CDDB7AA056DA68F2BE5D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StartEventArgs_t07FE61C20910684386B95C9DE0312066D0B74DB6_CustomAttributesCacheGenerator_StartEventArgs_set_placementId_m8AA1E6CADC23BB6EBC352803830BAAF00FF4C534(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnsupportedPlatform_tADC407CF434C8D0D15CFBFC1FE5890EFC148D3CA_CustomAttributesCacheGenerator_OnFinish(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnsupportedPlatform_tADC407CF434C8D0D15CFBFC1FE5890EFC148D3CA_CustomAttributesCacheGenerator_UnsupportedPlatform_add_OnFinish_m5AAFA121BBF745D8AE848A225D0D6BF9ABA3B2BD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnsupportedPlatform_tADC407CF434C8D0D15CFBFC1FE5890EFC148D3CA_CustomAttributesCacheGenerator_UnsupportedPlatform_remove_OnFinish_m610E0A8FF608B4AB6F0C0B8A4F511689B92A4611(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_UnityEngine_Advertisements_AttributeGenerators[];
const CustomAttributesCacheGenerator g_UnityEngine_Advertisements_AttributeGenerators[126] = 
{
	DebugLevelInternal_tF4573C0F3D1BC484CB562297FF62F53E146A0C92_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tE132ABC203FC35607C9A68D2EE5C411A29E10E23_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_1_t3A687EB0AECDF64242830054A608A5025E9F244C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_2_t325072B915B2C81847CD8FF7EFA8BF9DF5318FAB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t82087E7EA37AE4C23E6D3686E3C930BBA8F264E2_CustomAttributesCacheGenerator,
	U3CU3Ec_tFB680149A5801BFDB22502EDEADA9D63C80C8903_CustomAttributesCacheGenerator,
	U3CU3Ec_t34CDFE94C717609A863226E99DA4E71D2FB51D40_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_t68EE74784403A27CBFE72B29E2719837CB094CD4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tAD6105258B44FC2D0ED09881B1CB8B642E8F08EE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_t0059D7B3FF9873CBF55054CBC6AE4372C6704292_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t05DB1FC90FC24A4EC77DFC812A7356CFAE6BDF0B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_tE54390B9CBA3DF68E5A376751E5EF8419B8C3634_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_tB73A74EA3B2CB4BBFB72FC84144283F8CEFC4013_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_t7DAEFF5082862EED35531712429E9B3FC4978585_CustomAttributesCacheGenerator,
	CallbackExecutor_tB5471E84666F700A95D8A9CDD2BBCFF57234A1B6_CustomAttributesCacheGenerator,
	Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tFA2447E290B30A76C70C30619E23BBDCDDD92133_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t33822494F77A460EB4FA57D77070DD638CFD271E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t3DB3185E131EA6CCCC17F7E1023B79CE66EA0AE3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tBDE9EB03DA0C945DFC9A67B015478FCAFEB93F74_CustomAttributesCacheGenerator,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnShow,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnHide,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnError,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnUnload,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_OnLoad,
	BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_U3CshowCallbackU3Ek__BackingField,
	BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_U3ChideCallbackU3Ek__BackingField,
	BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_U3CloadCallbackU3Ek__BackingField,
	BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_U3CerrorCallbackU3Ek__BackingField,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_U3CenabledU3Ek__BackingField,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_U3CdefaultPlacementU3Ek__BackingField,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_U3CplacementsU3Ek__BackingField,
	ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_U3CerrorU3Ek__BackingField,
	ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_U3CmessageU3Ek__BackingField,
	FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField,
	FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_U3CshowResultU3Ek__BackingField,
	HideEventArgs_t0E87B63ABE49D6FCB03F2B1FCFCEBACF00AE2E41_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField,
	MetaData_tBE01A382C3BD81B14C99098223DD871E18071033_CustomAttributesCacheGenerator_U3CcategoryU3Ek__BackingField,
	Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator_OnFinish,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnReady,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnStart,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnFinish,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_OnError,
	ReadyEventArgs_t713D51DA38734FAD87991FCC9A9DA7FA4986D567_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField,
	ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_U3CresultCallbackU3Ek__BackingField,
	ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_U3CgamerSidU3Ek__BackingField,
	StartEventArgs_t07FE61C20910684386B95C9DE0312066D0B74DB6_CustomAttributesCacheGenerator_U3CplacementIdU3Ek__BackingField,
	UnsupportedPlatform_tADC407CF434C8D0D15CFBFC1FE5890EFC148D3CA_CustomAttributesCacheGenerator_OnFinish,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnShow_mB458F372D3DD207CA96F82388E162918002CCFE7,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnShow_m45410E5D53456E1D26E2739D9D96B8AE7B250079,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnHide_m730316CF87D563577C8E8575B7932FE7B1840A22,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnHide_mBF26DD5714334DA7EAE18F9E0763012CCF6F4920,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnError_mD89F56EB324871C96484456D707F6B92F24B7E9B,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnError_mCEECA5E24574CAF952B46ACBF59A09D900C3466C,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnUnload_m1BF26B67825DA1F46ED4A1AA74B2E250EEDE6209,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnUnload_mF9B5937FD6742DACD006BEFEDDDC08A5314DCCB3,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_add_OnLoad_mA6BD3F3063092371D316D8E498F775687ED784FB,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_remove_OnLoad_m1C5738D2036E92F62503550ECABE253C4F7E9ADE,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_U3CHideU3Eb__25_0_m4288A525B768F059418C0DA0D2BA2AEA36DBCE86,
	Banner_t300D36C28031CBB22841A4A45B040CE950A17F34_CustomAttributesCacheGenerator_Banner_U3CShowU3Eb__26_0_m4B04202038DEB894954DAD7BE90E863763FB6D87,
	BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_get_showCallback_m793328B516C2F58F409DDDFF299D9861A176DE58,
	BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_set_showCallback_mB0F6E62E0D920E48A8F23D97B970D08110EC3B08,
	BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_get_hideCallback_mCEA5BD2AC305AD7363B73563683E2CD5B58BE1C2,
	BannerOptions_t5B3F00A99B6ADB63BBD55E75D73F211C9F7201D4_CustomAttributesCacheGenerator_BannerOptions_set_hideCallback_m3B0CE3795CA06887F96D10B95E7A247E0CC0B23A,
	BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_get_loadCallback_m301BC53E2AD64C6A00325C4CE455AE0DBF4D3961,
	BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_set_loadCallback_m4329F2F398B593D91C8A2BD968B381CCB8F5EB60,
	BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_get_errorCallback_m8CC522E0F86CC2236C352E7A1D1FC391C909ABC4,
	BannerLoadOptions_t45798C363F28119499C20BD00D3DB5FDB0B70301_CustomAttributesCacheGenerator_BannerLoadOptions_set_errorCallback_m390340D855BAF0725C9D01F57D369E0EA4B9F8B7,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_get_enabled_mADAEF056D5A0E01FA69D62FE1B82F612E7A0F091,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_set_enabled_m2CA8460B705180466AC6010D39ABEF8C8E3776E2,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_get_defaultPlacement_mF8DE16AF8DB46335BD5B7E7F72018C5F3149C306,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_set_defaultPlacement_mF5E167689F8D411F9E90E6ABF6ABDA8E8FDB0DE5,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_get_placements_mE3540699DF4055931FCD807291B8C3A4E1E5AF60,
	Configuration_t45C2329113442E1633A7DC3B4241B1B273AB2B6D_CustomAttributesCacheGenerator_Configuration_set_placements_m12D2057B3503DE4FE1C68ECF38A1FDBEDCDB2992,
	Creator_t53854AD486562E01B7F2BCF0AFD6F701B77A5D82_CustomAttributesCacheGenerator_Creator_LoadRuntime_mB7761D71A241BBAB13D943F62B2EB85427D3FC2F,
	ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_get_error_m1E30CAFD188430D4587CB19F0806111FBEF1876A,
	ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_set_error_m506F8363A4E8CD857210D80CC85C889C5A472726,
	ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_get_message_m365EB32B5200AAB8E7B02AC66F4A40A26826903A,
	ErrorEventArgs_t2DD4BE08BE60EBAC9795EBBB9D0808F9118D9A19_CustomAttributesCacheGenerator_ErrorEventArgs_set_message_m1DF258C1D22BEC328A18DDA537BECF4A27DE91B0,
	FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_get_placementId_mD7AA8A53A304FF18B720BBC68D2C16CC041E82C1,
	FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_set_placementId_m9DA2870864B91EF0359648D81353309809D92E8B,
	FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_get_showResult_m1F131AC5478F70C0E9327A3815A4AD385207CC93,
	FinishEventArgs_t75A7FCB33BC8A9E80C0FC7A73873AE6FCB21F20E_CustomAttributesCacheGenerator_FinishEventArgs_set_showResult_m18E14A55B95BA4D544F64A3D615F6079CE481966,
	HideEventArgs_t0E87B63ABE49D6FCB03F2B1FCFCEBACF00AE2E41_CustomAttributesCacheGenerator_HideEventArgs_get_placementId_m97CFB9AE5E30F2643462801AB415E44493FAFC81,
	HideEventArgs_t0E87B63ABE49D6FCB03F2B1FCFCEBACF00AE2E41_CustomAttributesCacheGenerator_HideEventArgs_set_placementId_m41695EC676F0170C50C27028F925F0A9DCB44800,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnShow_mED74B968B1B3C20928EA3C9D9FEE41B0F6F3C13A,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnShow_m9F1CC7BFD5FC42A320C64A3F959B74CF9951C4F0,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnHide_m3BA03303E0CF9BBDE19D7C6CE4A528F005AB71D9,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnHide_m9C99214F751547966119072252499B17D25AF017,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnError_m57A9E434F9DC963AE3850961431DBD113DC5A9F2,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnError_m673F1B243EB5826A8134D830AE363D08C7EFC23B,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnUnload_m5F47B6EBCA78BEAF89DD4EF636E9BB0F5696DFA6,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnUnload_m9CE8A92ECFE324CE490854C0578FE46E1AD2FDC8,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_add_OnLoad_mD117A7CF92F790F029CA378B4AA9953AD0981091,
	IBanner_tB22AF51D47AF044F071A83E427D6CD860C6420D5_CustomAttributesCacheGenerator_IBanner_remove_OnLoad_m38D82EF8C5AC077C89CB04BE5B27A7C8467819EF,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnReady_mCB28D6C550B8CABC8553642307093DAE9B4B84C5,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnReady_mD67EFECAEAA16160825BB61344B4DF04686C6AB7,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnStart_mE33CCC53A698FA89B1B7FEE7D96BD2B55E0D0704,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnStart_mD8CB53B4187BB65F0AB18831EBD34DF4CF7DC8C3,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnFinish_m10F4B34BF2847806512B9D534324F05F8CC580AE,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnFinish_m30CA02164304DA72D2E28B05B808CFFAFB19B15E,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_add_OnError_m7BF5094EAD371419E2748A18C485A67BBBB8E127,
	IPlatform_t1BD5361F9B1B2F196260E1F89D2CBA818A89976E_CustomAttributesCacheGenerator_IPlatform_remove_OnError_mAE1234B461EE48B89955E3D7F10A2718402435ED,
	MetaData_tBE01A382C3BD81B14C99098223DD871E18071033_CustomAttributesCacheGenerator_MetaData_get_category_mABA6F5C05F3B9045D2C8796F3D466905092C2C07,
	MetaData_tBE01A382C3BD81B14C99098223DD871E18071033_CustomAttributesCacheGenerator_MetaData_set_category_m6A186106F731708B3BCE62D245A8CE3DF20E5FC5,
	Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator_Placeholder_add_OnFinish_m20EF50FB8F5F846EA19B6478268C373E6A65E030,
	Placeholder_t88E881A9AE11BDF019B81C4BD529F587B41A2CF4_CustomAttributesCacheGenerator_Placeholder_remove_OnFinish_mBEACC10936874A7C89423DEF5768EBE74489B3EE,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnReady_m3BD5AD3B947EECAC0FE362324F4E7834FA6A5DD3,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnReady_mDA55BE1CF7B270E883EC0D4187D10A351E1219D1,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnStart_m4D22DCE9B3752BCAAC63E6C778A75182F0D7C92C,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnStart_mEF678584617D744F0C70A83077E3B2E342829DCF,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnFinish_mC6C769216CACBFA5615A736CF355F2265EA2A4CA,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnFinish_m8A08CA7735D4D1ADB9FD9AC6626BF8CF2119CE45,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_add_OnError_m9846EC63A177D70427E3D5F26AE42AE385A8FAD4,
	Platform_t7E251A7059B7BD1877EA55BB1D2DC281F5E9AAC2_CustomAttributesCacheGenerator_Platform_remove_OnError_mDE61C7A4BEE423C8D31B1C16CF7B962D2AFCC016,
	ReadyEventArgs_t713D51DA38734FAD87991FCC9A9DA7FA4986D567_CustomAttributesCacheGenerator_ReadyEventArgs_get_placementId_m9B1A1462F7348F396D3E5F3AE4F2266B510ABA20,
	ReadyEventArgs_t713D51DA38734FAD87991FCC9A9DA7FA4986D567_CustomAttributesCacheGenerator_ReadyEventArgs_set_placementId_mF13E66593646DDEC39EA34E95436A614832B71BD,
	ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_get_resultCallback_m884750654C53E4096E4F6EB00941DBA62282629E,
	ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_set_resultCallback_mCDCA3E88983BFC496AC7F999EAAA497A01A9CF38,
	ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_get_gamerSid_m6D82587F5284F893760EA140F52B0C540A35C2B7,
	ShowOptions_t556B971BC769DC299070A3027186BC1FDCE0E84C_CustomAttributesCacheGenerator_ShowOptions_set_gamerSid_m281C98C7E79C7F11CA5C613F5D5DE8F35AB9B031,
	StartEventArgs_t07FE61C20910684386B95C9DE0312066D0B74DB6_CustomAttributesCacheGenerator_StartEventArgs_get_placementId_mC0CADA1095DAE9D9C54CDDB7AA056DA68F2BE5D9,
	StartEventArgs_t07FE61C20910684386B95C9DE0312066D0B74DB6_CustomAttributesCacheGenerator_StartEventArgs_set_placementId_m8AA1E6CADC23BB6EBC352803830BAAF00FF4C534,
	UnsupportedPlatform_tADC407CF434C8D0D15CFBFC1FE5890EFC148D3CA_CustomAttributesCacheGenerator_UnsupportedPlatform_add_OnFinish_m5AAFA121BBF745D8AE848A225D0D6BF9ABA3B2BD,
	UnsupportedPlatform_tADC407CF434C8D0D15CFBFC1FE5890EFC148D3CA_CustomAttributesCacheGenerator_UnsupportedPlatform_remove_OnFinish_m610E0A8FF608B4AB6F0C0B8A4F511689B92A4611,
	UnityEngine_Advertisements_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
